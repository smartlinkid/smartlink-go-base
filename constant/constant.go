package constant

var JENIS_BAYAR = map[int]string{
	1:  "Tunai",
	2:  "Kredit",
	3:  "Debit",
	4:  "E-Money",
	5:  "Deposit",
	6:  "Paylink",
	7:  "OVO",
	8:  "DANA",
	9:  "LinkAja",
	10: "QRis",
}
var TIPE_BAYAR = map[int]string{
	1: "Lunas",
	2: "DP",
	3: "Pelunasan",
}

//FILTER KOLOM KONSUMEN
const FILTER_TERDAFTAR_PADA_PERIODE = "periode_terdaftar"
const FILTER_TRANSAKSI_PADA_PERIODE = "periode_transaksi"
const FILTER_TIDAK_TRANSAKSI_PADA_PERIODE = "periode_tidak_transaksi"

//FILTER KOLOM LAYANAN
const FILTER_TRANSAKSI_DENGAN_SEMUA_LAYANAN = "transaksi_semua_layanan"
const FILTER_TRANSAKSI_DENGAN_SALAH_SATU_LAYANAN = "transaksi_salah_satu_layanan"
const FILTER_TRANSAKSI_TANPA_LAYANAN = "transaksi_tanpa_layanan"
