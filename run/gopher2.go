package main

import (
	"bitbucket.org/smartlinkid/smartlink-go-base/helper"
	"bitbucket.org/smartlinkid/smartlink-go-base/model"
	graphql "github.com/graph-gophers/graphql-go"
	"github.com/graph-gophers/graphql-go/relay"
	"log"
	"net/http"
)

// This schema defines a note-taking application with two
// simple graphs: users and notes.

const schemaString = `
	scalar Float
	schema {
		query: Query
	}
	# Define users:
	type User {
		userID: ID!
		username: String!
		emoji: String!
		notes: [Note!]!
	}
	# Define notes:
	type Note {
		noteID: ID!
		data: String!
	}

	type Customer {
		idcustomer: String!
		nama: String!
		telp: String!
	}

	type Transaksi {
		idtransaksi: String!
		lunas: Int!
	}

	type Query {
		# List users:
		users: [User!]!
		# List customer:
		customers: [Customer!]!
		transaksis: [Transaksi!]!
		# Get user:
		user(userID: ID!): User!
		# List notes:
		notes(userID: ID!): [Note!]!
		# Get note:
		note(noteID: ID!): Note!
	}
`

type User struct {
	UserID   graphql.ID
	Username string
	Emoji    string
	Notes    []Note
}

type Note struct {
	NoteID graphql.ID
	Data   string
}

// Define mock data:
var users = []User{
	{
		UserID:   graphql.ID("u-001"),
		Username: "nyxerys",
		Emoji:    "🇵🇹",
		Notes: []Note{
			{NoteID: "n-001", Data: "Olá Mundo!"},
			{NoteID: "n-002", Data: "Olá novamente, mundo!"},
			{NoteID: "n-003", Data: "Olá, escuridão!"},
		},
	}, {
		UserID:   graphql.ID("u-002"),
		Username: "rdnkta",
		Emoji:    "🇺🇦",
		Notes: []Note{
			{NoteID: "n-004", Data: "Привіт Світ!"},
			{NoteID: "n-005", Data: "Привіт ще раз, світ!"},
			{NoteID: "n-006", Data: "Привіт, темрява!"},
		},
	}, {
		UserID:   graphql.ID("u-003"),
		Username: "username_ZAYDEK",
		Emoji:    "🇺🇸",
		Notes: []Note{
			{NoteID: "n-007", Data: "Hello, world!"},
			{NoteID: "n-008", Data: "Hello again, world!"},
			{NoteID: "n-009", Data: "Hello, darkness!"},
		},
	},
}

type RootResolver struct{}

func (r *RootResolver) Transaksis() ([]model.Transaksi, error) {
	var c []model.Transaksi
	db := helper.GetDb()
	db.Limit(10).Find(&c)
	return c, nil
}

func (r *RootResolver) Customers() ([]model.Customer, error) {
	var c []model.Customer
	db := helper.GetDb()
	db.Limit(10).Find(&c)
	return c, nil
}

func (r *RootResolver) Users() ([]User, error) {
	return users, nil
}

func (r *RootResolver) User(args struct{ UserID graphql.ID }) (User, error) {
	// Find user:
	for _, user := range users {
		if args.UserID == user.UserID {
			// Found user:
			return user, nil
		}
	}
	// Didn’t find user:
	return User{}, nil
}

func (r *RootResolver) Notes(args struct{ UserID graphql.ID }) ([]Note, error) {
	// Find user to find notes:
	user, err := r.User(args) // We can reuse resolvers.
	if err != nil {
		// Didn’t find user:
		return nil, err
	}
	// Found user; return notes:
	return user.Notes, nil
}

func (r *RootResolver) Note(args struct{ NoteID graphql.ID }) (Note, error) {
	// Find note:
	for _, user := range users {
		for _, note := range user.Notes {
			if args.NoteID == note.NoteID {
				// Found note:
				return note, nil
			}
		}
	}
	// Didn’t find note:
	return Note{}, nil
}

var (
	// We can pass an option to the schema so we don’t need to
	// write a method to access each type’s field:
	opts   = []graphql.SchemaOpt{graphql.UseFieldResolvers()}
	Schema = graphql.MustParseSchema(schemaString, &RootResolver{}, opts...)
)

func main() {
	schema := graphql.MustParseSchema(schemaString, &RootResolver{}, opts...)
	http.Handle("/query", &relay.Handler{Schema: schema})
	log.Fatal(http.ListenAndServe(":8080", nil))
}
