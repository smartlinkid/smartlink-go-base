package main

import (
	"bitbucket.org/smartlinkid/smartlink-go-base/helper"
	"bitbucket.org/smartlinkid/smartlink-go-base/model"
	"encoding/json"
	"fmt"
	"github.com/friendsofgo/graphiql"
	"github.com/gin-gonic/gin"
	"github.com/graphql-go/graphql"
	"github.com/joho/godotenv"
)

type reqBody struct {
	Query string `json:"query"`
}

func getSchema() graphql.Schema {
	db := helper.GetDb()
	rootFields := graphql.Fields{
		"owner": &graphql.Field{
			Type: model.OwnerType,
			Args: graphql.FieldConfigArgument{
				"idowner": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(p graphql.ResolveParams) (i interface{}, e error) {
				var list []model.Owner
				idowner := p.Args["idowner"].(string)
				db.Where(model.Owner{Idowner: idowner}).Limit(1).Preload("Outlet").Find(&list)
				if len(list) > 0 {
					return list[0], nil
				} else {
					return nil, nil
				}
			},
		},
		"ownerList": &graphql.Field{
			Type: graphql.NewList(model.OwnerType),
			Args: graphql.FieldConfigArgument{
				"offset": &graphql.ArgumentConfig{
					Type:         graphql.Int,
					DefaultValue: 0,
				},
				"limit": &graphql.ArgumentConfig{
					Type:         graphql.Int,
					DefaultValue: 20,
				},
				"q": &graphql.ArgumentConfig{
					Type:         graphql.String,
					DefaultValue: "",
					Description:  "Query for search on alamat, nama, dll",
				},
			},
			Resolve: func(p graphql.ResolveParams) (i interface{}, e error) {
				var list []model.Owner
				db.Where("nama like ? or email like ?", "%"+p.Args["q"].(string)+"%", "%"+p.Args["q"].(string)+"%").
					Limit(p.Args["limit"]).
					Offset(p.Args["offset"]).
					Preload("Outlet").Find(&list)
				return list, nil
			},
		},
		"outletList": &graphql.Field{
			Type: graphql.NewList(model.OutletType),
			Args: graphql.FieldConfigArgument{
				"offset": &graphql.ArgumentConfig{
					Type:         graphql.Int,
					DefaultValue: 0,
				},
				"limit": &graphql.ArgumentConfig{
					Type:         graphql.Int,
					DefaultValue: 20,
				},
				"q": &graphql.ArgumentConfig{
					Type:         graphql.String,
					DefaultValue: "",
					Description:  "Query for search on alamat, nama, dll",
				},
			},
			Resolve: func(p graphql.ResolveParams) (i interface{}, e error) {
				var list []model.Outlet
				db.Where("nama like ?", "%"+p.Args["q"].(string)+"%").
					Limit(p.Args["limit"]).
					Offset(p.Args["offset"]).
					Find(&list)
				return list, nil
			},
		},
	}
	root := graphql.ObjectConfig{
		Name:   "RootQuery",
		Fields: rootFields,
	}
	schema, _ := graphql.NewSchema(graphql.SchemaConfig{Query: graphql.NewObject(root)})
	return schema
}
func main() {
	godotenv.Load()
	r := gin.Default()
	graphiqlHandler, err := graphiql.NewGraphiqlHandler("/graphql")
	if err != nil {
		panic(err)
	}
	r.GET("/graphiql", gin.WrapH(graphiqlHandler))
	r.POST("/graphql", func(context *gin.Context) {
		query := reqBody{}
		json.NewDecoder(context.Request.Body).Decode(&query)
		params := graphql.Params{Schema: getSchema(), RequestString: query.Query}
		r := graphql.Do(params)
		if len(r.Errors) > 0 {
			fmt.Printf("failed to execute graphql operation, errors: %+v", r.Errors)
		}
		ss, _ := json.Marshal(r)
		fmt.Fprintf(context.Writer, "%s", ss)
	})
	r.Run(":3001")
}
