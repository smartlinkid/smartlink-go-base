package main

import (
	"bitbucket.org/smartlinkid/smartlink-go-base/helper"
	"bitbucket.org/smartlinkid/smartlink-go-base/modelGenerated/modelv2"
	"encoding/json"
	"fmt"
	"github.com/joho/godotenv"
)

func main() {
	godotenv.Load()
	db := helper.GetDb()
	var x modelv2.Administrator
	x.Nama.Scan("alkaaf")

	db.Limit(10).Find(&x)
	b, _ := json.Marshal(x)
	fmt.Println(string(b))
}
