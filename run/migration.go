package main

import (
	"bitbucket.org/smartlinkid/smartlink-go-base/helper"
	"bitbucket.org/smartlinkid/smartlink-go-base/model"
	"fmt"
	"github.com/joho/godotenv"
)

func main() {
	godotenv.Load()
	up()
}

func up() {
	db := helper.GetDb()
	fmt.Print("#")
	db.AutoMigrate(&model.SnapBayar{})
	fmt.Print("#")
	db.AutoMigrate(&model.SnapCredential{})
	fmt.Print("#")
	db.AutoMigrate(&model.SnapHome{})
	fmt.Print("#")
	db.AutoMigrate(&model.SnapLayanan{})
	fmt.Print("#")
	db.AutoMigrate(&model.SnapLayananTag{})
	fmt.Print("#")
	db.AutoMigrate(&model.SnapMesin{})
	fmt.Print("#")
	db.AutoMigrate(&model.SnapPid{})
	fmt.Print("#")
	db.AutoMigrate(&model.SnapSerial{})
	fmt.Print("#")
	db.AutoMigrate(&model.SnapStatusHistory{})
	fmt.Print("#")
	db.AutoMigrate(&model.SnapTag{})
	fmt.Print("#")
	db.AutoMigrate(&model.SnapTarif{})
	fmt.Print("#")
	db.AutoMigrate(&model.SnapTransaksi{})
	fmt.Print("#")
	db.AutoMigrate(&model.SnapTransaksiDetail{})
	fmt.Print("#")
	db.AutoMigrate(&model.SnapTransaksiItem{})

	fmt.Print("#")
	db.AutoMigrate(&model.PaylinkBalanceHistories{})
	fmt.Print("#")
	db.AutoMigrate(&model.PaylinkBalanceTopups{})
	fmt.Print("#")
	db.AutoMigrate(&model.PaylinkBalanceTransfers{})
	fmt.Print("#")
	db.AutoMigrate(&model.PaylinkBalances{})
	fmt.Print("#")
	db.AutoMigrate(&model.PaylinkPaket{})
	fmt.Print("#")
	db.AutoMigrate(&model.PaylinkTopupCustomers{})
	db.Close()
}

func down() {
}
