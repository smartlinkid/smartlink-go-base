package main

import (
	"bitbucket.org/smartlinkid/smartlink-go-base/helper"
	"bitbucket.org/smartlinkid/smartlink-go-base/model"
	"fmt"
)

func main() {
	db := helper.GetDb()
	parser := helper.NewTemplateParser(db)
	// get kode and build template
	var master []model.JsonpathMaster
	db.Where("jenis_data = ?", "snap_transaksi").Find(&master)
	template :=
		`
Hai [sapaan] [nama_customer], 

Kami dari
[nama_outlet]
[alamat_outlet]
[kota_outlet]
[layanan]
Kami informasikan untuk nomor nota [jenis_transaksi] *[kode_transaksi]* telah selesai kami kerjakan.

Silakan datang untuk melakukan pengambilan atau jika [sapaan] [nama_customer] ingin layanan pengantaran silaakan sampaikan kepada kami.

Berikut detail penagihannya:
Total bayar: [total_bayar]
Sisa bayar: [sisa_bayar]
Status: *[status_lunas]*

Jika ingin lebih terperinci serta melihat barang yang dimaksud, [sapaan] [nama_customer] bisa mengunjungi website kami di:

Kami ucapkan terima kasih, dan selamat beraktifitas. Jika terdapat kesalahan informasi dalam pesan ini, segera sampaikan kepada kami

Salam hormat, menejemen
*[nama_outlet]*

`
	//for _, d := range master {
	//	template += fmt.Sprintf("#%s#\n[%s]\n====================\n", d.Kode, d.Kode)
	//}
	// get sample transaksi
	var snapTransaksi model.SnapTransaksi
	id := "PTB200907201311869"
	db.Where("id = ?", id).
		Preload("SnapTransaksiDetail.SnapLayanan.Satuan").
		Preload("Outlet").
		Preload("Owner").
		Preload("Karyawan").
		Preload("SnapBayar").
		First(&snapTransaksi)
	result, _ := parser.JsonPathTemplateParser("snap_transaksi", template, snapTransaksi.GetJsonForTemplate())
	fmt.Println(result)
	//fmt.Println(template)
}
