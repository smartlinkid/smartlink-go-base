module bitbucket.org/smartlinkid/smartlink-go-base

go 1.12

require (
	cloud.google.com/go v0.37.4
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/friendsofgo/graphiql v0.2.2
	github.com/gin-gonic/gin v1.5.0
	github.com/go-redis/redis v6.15.5+incompatible
	github.com/go-sql-driver/mysql v1.6.0
	github.com/graph-gophers/graphql-go v0.0.0-20191115155744-f33e81362277
	github.com/graphql-go/graphql v0.7.8
	github.com/jinzhu/gorm v1.9.11
	github.com/joho/godotenv v1.3.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/nats-io/nats-server/v2 v2.1.2 // indirect
	github.com/nats-io/nats.go v1.9.1
	gopkg.in/errgo.v2 v2.1.0
	gorm.io/driver/mysql v1.4.4 // indirect
	gorm.io/gorm v1.24.2 // indirect
)
