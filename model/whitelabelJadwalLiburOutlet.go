package model

import "time"

type WhitelabelJadwalLiburOutlet struct {
	ID                      string     `json:"id"`
	Idowner                 string     `json:"idowner"`
	Idoutlet                string     `json:"idoutlet"`
	IdwhitelabelJadwalLibur string     `json:"idwhitelabel_jadwal_libur"`
	CreatedAt               *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	Outlet                  *Outlet    `json:"outlet" gorm:"foreignKey:idoutlet;association_foreignkey:idoutlet;references:idoutlet"`
}
