package model

type WaBotPromoHarian struct {
	Id           string `json:"id"`
	IdwaBotPromo string `json:"idwa_bot_promo"`
	Hari         int64  `json:"hari"` // 1-7. 1 senin 7 minggu
	JamStart     string `json:"jam_start"`
	JamEnd       string `json:"jam_end"`
}
