package model

import (
	"fmt"
	"strconv"
	"time"

	"bitbucket.org/smartlinkid/smartlink-go-base/constant"
	"github.com/graphql-go/graphql"
)

type SnapTransaksi struct {
	Id                  string                `json:"id" gorm:"primary_key;index;not null"`
	Qty                 float64               `json:"qty"`
	Idoutlet            string                `json:"idoutlet" gorm:"index"`
	Idowner             string                `json:"idowner" gorm:"index" swagger:"ignore"`
	IdcustomerGlobal    *string               `json:"idcustomer_global" gorm:"index"`
	Idkaryawan          *string               `json:"idkaryawan" gorm:"index"`
	NamaCustomer        *string               `json:"nama_customer"`
	Jenis               int                   `json:"jenis"`        // 1 self service 2 dropoff
	StatusBayar         int                   `json:"status_bayar"` // 1 sukses 2 gagal 3 pending
	Status              int                   `json:"status"`
	FailedReason        string                `json:"failed_reason"`
	PersenDiskon        float64               `json:"persen_diskon"`
	Diskon              float64               `json:"diskon"`
	PersenPajak         float64               `json:"persen_pajak"`
	Pajak               float64               `json:"pajak"`
	Total               float64               `json:"total"`
	Grandtotal          float64               `json:"grandtotal"`
	Keterangan          *string               `json:"keterangan"`
	Lunas               int                   `json:"lunas" gorm:"default:0"`
	CreatedAtLong       int64                 `json:"created_at_long"`
	FinishedAt          *time.Time            `json:"finished_at"`
	FinishedAtLong      int64                 `json:"finished_at_long"`
	Working             int                   `json:"working"`
	TakenAt             *time.Time            `json:"taken_at" gorm:"taken_at"`
	CustomerGlobal      *CustomerGlobal       `json:"customer_global" gorm:"foreignKey:id;association_foreignkey:idcustomer_global;references:idcustomer_global"`
	SnapTransaksiDetail []SnapTransaksiDetail `json:"snap_transaksi_detail" gorm:"foreignkey:idsnap_transaksi;association_foreignkey:id;references:id"`
	SnapBayar           []SnapBayar           `json:"snap_bayar" gorm:"foreignkey:idsnap_transaksi;association_foreignkey:id;references:id"`
	Outlet              *Outlet               `json:"outlet" gorm:"foreignkey:idoutlet;association_foreignkey:idoutlet;references:idoutlet"`
	Owner               *Owner                `json:"owner" gorm:"foreignkey:idowner;association_foreignkey:idowner;references:idowner"`
	Karyawan            *Karyawan             `json:"karyawan" gorm:"foreignkey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	SnapTransaksiHapus  SnapTransaksiHapus    `json:"snap_transaksi_hapus" gorm:"foreignkey:idsnap_transaksi;association_foreignkey:id;references:id"`
	ExpiredAt           *time.Time            `json:"expired_at"`
	CreatedAt           *time.Time            `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt           *time.Time            `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	Hapus               *int                  `json:"hapus" gorm:"default:0"`
	IsLoyalty           int                   `json:"is_loyalty" gorm:"-"`
}

var SnapTransaksiType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "snap_transaksi",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "id transaksi snap",
			},
			"qty": &graphql.Field{
				Type:        graphql.Float,
				Description: "kuantitas transaksi snap",
			},
			"idoutlet": &graphql.Field{
				Type:        graphql.String,
				Description: "id outlet snap",
			},
			"idowner": &graphql.Field{
				Type:        graphql.String,
				Description: "id owner snap",
			},
			"idcustomer_global": &graphql.Field{
				Type:        graphql.String,
				Description: "id customer global",
			},
			"idkaryawan": &graphql.Field{
				Type:        graphql.String,
				Description: "id karyawan",
			},
			"nama_customer": &graphql.Field{
				Type:        graphql.String,
				Description: "nama customer",
			},
			"jenis": &graphql.Field{
				Type:        graphql.Int,
				Description: "jenis transaksi snap. 1: self service; 2: drop off",
			},
			"status_bayar": &graphql.Field{
				Type:        graphql.Int,
				Description: "status pembayaran transaksi snap. 1: sukses; 2: gagal; 3: gagal(expired/pembatalan transaski)",
			},
			"status": &graphql.Field{
				Type:        graphql.Int,
				Description: "status transaksi snap. 0: ready; 1: progress; 2: selesai; 3: gagal",
			},
			"failed_reason": &graphql.Field{
				Type:        graphql.String,
				Description: "keterangan gagal saat pembuatan transaksi",
			},
			"persen_diskon": &graphql.Field{
				Type:        graphql.Float,
				Description: "diskon dalam bentuk persen",
			},
			"diskon": &graphql.Field{
				Type:        graphql.Float,
				Description: "diskon dalam bentuk rupiah",
			},
			"persen_pajak": &graphql.Field{
				Type:        graphql.Float,
				Description: "pajak dalam bentuk persen",
			},
			"pajak": &graphql.Field{
				Type:        graphql.Float,
				Description: "pajak dalam bentuk rupiah",
			},
			"total": &graphql.Field{
				Type:        graphql.Float,
				Description: "total transaksi snap",
			},
			"grandtotal": &graphql.Field{
				Type:        graphql.Float,
				Description: "grand total transaksi snap",
			},
			"keterangan": &graphql.Field{
				Type:        graphql.String,
				Description: "keterangan transaksi snap",
			},
			"lunas": &graphql.Field{
				Type:        graphql.Int,
				Description: "status lunas transaksi snap",
			},
			"created_at_long": &graphql.Field{
				Type:        graphql.String,
				Description: "created_at_long",
			},
			"finished_at": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"finished_at_long": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"working": &graphql.Field{
				Type:        graphql.Int,
				Description: "0/1, jika salah satu layanan mengandung status 2/3",
			},
			"taken_at": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "pengambilan",
			},
			"expired_at": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "Batas waktu transaksi valid",
			},
			"created_at": &graphql.Field{
				Type:        graphql.String,
				Description: "created_at",
			},
			"customer_global": &graphql.Field{
				Type:        CustomerGlobalType,
				Description: "",
			},
			"snap_transaksi_detail": &graphql.Field{
				Type:        graphql.NewList(SnapTransaksiDetailType),
				Description: "detail transaksi snap",
			},
			"snap_bayar": &graphql.Field{
				Type:        graphql.NewList(SnapBayarType),
				Description: "pembayaran transaksi snap",
			},
			"outlet": &graphql.Field{
				Type:        OutletType,
				Description: "outlet snap",
			},
			"owner": &graphql.Field{
				Type:        OwnerType,
				Description: "owner snap",
			},
			"karyawan": &graphql.Field{
				Type:        KaryawanType,
				Description: "karyawan snap",
			},
		},
	})

var SnapTransaksiInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "snap_transaksi_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id transaksi snap",
			},
			"qty": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "kuantitas transaksi snap",
			},
			"idoutlet": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id outlet snap",
			},
			"idowner": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id owner snap",
			},
			"idcustomer_global": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id customer global",
			},
			"idkaryawan": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id karyawan",
			},
			"nama_customer": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "nama customer",
			},
			"jenis": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "jenis transaksi snap. 1: self service; 2: drop off",
			},
			"status_bayar": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "status pembayaran transaksi snap. 1: sukses; 2: gagal; 3: gagal(expired/pembatalan transaski)",
			},
			"status": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "status transaksi snap. 0: ready; 1: progress; 2: selesai; 3: gagal",
			},
			"persen_diskon": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "diskon dalam bentuk persen",
			},
			"diskon": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "diskon dalam bentuk rupiah",
			},
			"persen_pajak": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "pajak dalam bentuk persen",
			},
			"pajak": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "pajak dalam bentuk rupiah",
			},
			"total": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "total transaksi snap",
			},
			"grandtotal": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "grand total transaksi snap",
			},
			"keterangan": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "keterangan transaksi snap",
			},
			"lunas": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "status lunas transaksi snap",
			},
			"created_at_long": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "created_at_long",
			},
			"finished_at": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "",
			},
			"finished_at_long": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "",
			},
			"expired_at": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "Batas waktu transaksi valid",
			},
			//"customer_global": &graphql.InputObjectFieldConfig{
			//	Type:        CustomerGlobalType,
			//	Description: "",
			//},
			"snap_transaksi_detail": &graphql.InputObjectFieldConfig{
				Type:        graphql.NewList(SnapTransaksiDetailInput),
				Description: "detail transaksi snap",
			},
			"snap_bayar": &graphql.InputObjectFieldConfig{
				Type:        graphql.NewList(SnapBayarInput),
				Description: "pembayaran transaksi snap",
			},
			//"outlet": &graphql.InputObjectFieldConfig{
			//	Type:        OutletType,
			//	Description: "outlet snap",
			//},
			//"owner": &graphql.InputObjectFieldConfig{
			//	Type:        OwnerType,
			//	Description: "owner snap",
			//},
			//"karyawan": &graphql.InputObjectFieldConfig{
			//	Type:        KaryawanType,
			//	Description: "karyawan snap",
			//},
		},
	})

func (t *SnapTransaksi) GetJsonForTemplate() map[string]interface{} {
	j := make(map[string]interface{})
	// transaksi
	j["id"] = t.Id
	j["status_lunas"] = "Belum lunas"
	if t.Lunas == 1 {
		j["status_lunas"] = "Lunas"
	}
	if t.Jenis == 1 {
		j["jenis_transaksi"] = "Self-Service"
	} else if t.Jenis == 2 {
		j["jenis_transaksi"] = "Dropoff"
	}
	bayar := .0
	kembalian := .0
	strBayar := ""
	for _, snapBayar := range t.SnapBayar {
		bayar += snapBayar.Bayar
		kembalian += snapBayar.Kembalian
		strBayar += fmt.Sprintf("> %s %s Rp%.f\n", constant.JENIS_BAYAR[snapBayar.JenisBayar], constant.TIPE_BAYAR[snapBayar.TipeBayar], snapBayar.Bayar)
	}
	sisaBayar := t.Grandtotal - bayar
	if t.Keterangan != nil {
		j["keterangan"] = t.Keterangan
	} else {
		j["keterangan"] = ""
	}
	j["grandtotal"] = fmt.Sprintf("Rp%.f", t.Grandtotal)
	j["total"] = fmt.Sprintf("Rp%.f", t.Total)
	j["bayar"] = fmt.Sprintf("Rp%.f", bayar)
	j["detail_bayar"] = strBayar
	j["kembalian"] = fmt.Sprintf("Rp%.f", kembalian)
	j["sisa_bayar"] = fmt.Sprintf("Rp%.f", sisaBayar)

	j["tanggal_terima"] = t.CreatedAt.Format("02/01/06")
	j["jam_terima"] = t.CreatedAt.Format("15:04")
	if t.FinishedAt != nil {
		j["tanggal_selesai"] = t.FinishedAt.Format("02/01/06")
		j["jam_selesai"] = t.FinishedAt.Format("15:04")
	} else {
		j["tanggal_selesai"] = "-"
		j["jam_selesai"] = "-"
	}

	// customer
	j["nama_customer"] = t.NamaCustomer
	j["telp_customer"] = ""
	if c := t.CustomerGlobal; c != nil {
		j["telp_customer"] = c.FullTelp
		j["sapaan"] = "Ibu"
		if t.CustomerGlobal.JenisKelamin == 1 {
			j["sapaan"] = "Bapak"
		}
		j["alamat_customer"] = t.CustomerGlobal.Alamat
	}

	j["outlet"] = t.Outlet
	// outlet
	//j["nama_outlet"] = ""
	//j["alamat_outlet"] = ""
	//j["kota_outlet"] = ""
	//j["telp_outlet"] = ""
	//if o := t.Outlet; o != nil {
	//	j["nama_outlet"] = o.Nama
	//	j["alamat_outlet"] = o.Alamat
	//	j["kota_outlet"] = o.Kota
	//	j["telp_outlet"] = o.Telp
	//}

	// layanan
	groupLayanan := make(map[string][]SnapTransaksiDetail)
	for _, detail := range t.SnapTransaksiDetail {
		snapLayanan := detail.SnapLayanan
		groupLayanan[snapLayanan.Nama] = append(groupLayanan[snapLayanan.Nama], detail)
	}
	strLayanan := ""
	for s, details := range groupLayanan {
		snapLayanan := details[0].SnapLayanan
		satuan := snapLayanan.Satuan
		strSatuan := satuan.Nama
		if satuan.JenisSatuan == 4 {
			strSatuan = "KG"
		}
		qty := .0
		hargaTotal := .0
		for _, detail := range details {
			qty += detail.Qty
			hargaTotal += detail.Harga
		}
		strLayanan += fmt.Sprintf("- %s %s %s @ Rp%s\n  %dX Load \nTotal Rp%s\n", s, strconv.FormatFloat(qty, 'f', -1, 64), strSatuan, strconv.FormatFloat(snapLayanan.Harga, 'f', -1, 64), len(details), strconv.FormatFloat(hargaTotal, 'f', -1, 64))
	}
	j["layanan"] = strLayanan
	return j
}
