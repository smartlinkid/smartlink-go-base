package model

import (
	"time"
)

type AbsenLog struct {
	Id                   int        `json:"id" gorm:"primary_key"`
	Idkaryawan           string     `json:"idkaryawan"`
	Karyawan             *Karyawan  `json:"karyawan" gorm:"foreignKey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	Idowner              string     `json:"idowner"`
	Idshift              string     `json:"idshift"`
	Lat                  float64    `json:"lat"`
	Lng                  float64    `json:"lng"`
	Lokasi               int        `json:"lokasi"` // outlet, workshop
	Idlokasi             string     `json:"idlokasi"`
	TanggalAbsen         string     `json:"tanggal_absen"`
	WaktuMasuk           *time.Time `json:"waktu_absen"`
	WaktuPulang          *time.Time `json:"waktu_pulang"`
	Status               int        `json:"status"` // 1 masuk, 2 aplha, 3 cuti, 4 sakit, 0 tanpastatus
	MasukShift           string     `json:"masuk_shift"`
	KeluarShift          string     `json:"keluar_shift"`
	ToleransiShiftMasuk  int        `json:"toleransi_shift_masuk"`
	ToleransiShiftKeluar int        `json:"toleransi_shift_keluar"`
	SumberData           int        `json:"sumber_data"` // 1. kasir, 2. workshop, 3. import
	Outlet               *Outlet    `json:"outlet" gorm:"foreignKey:idoutlet;association_foreignkey:idlokasi;references:idlokasi"`
	Workshop             *Workshop  `json:"workshop" gorm:"foreignKey:idworkshop;association_foreignkey:idlokasi;references:idlokasi"`
	BaseModel
}
