package model

import "time"

type SledinSmsReguler struct {
	Idsms       string     `json:"idsms" gorm:"primary_key"`
	Text        string     `json:"text"`
	Number      string     `json:"number"`
	NumberInter string     `json:"number_inter"`
	Idowner     string     `json:"idowner" gorm:"index"`
	Idoutlet    string     `json:"idoutlet" gorm:"index"`
	Idmodem     string     `json:"idmodem" gorm:"index"`
	Status      int        `json:"status"`
	Idqueue     int64      `json:"idqueue"`
	SentAt      *time.Time `json:"sent_at"`
	DeliveredAt *time.Time `json:"delivered_at"`
}
