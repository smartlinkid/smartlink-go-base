package model

import "time"

type MasalTransaksiDaftar struct {
	IdmasalTransaksi string  `json:"idmasal_transaksi"`
	Idtransaksi      string  `json:"idtransaksi"`
	Tagihan          float64 `json:"tagihan"`
	Terbayar         float64 `json:"terbayar"`
	SisaBayar        float64 `json:"sisa_bayar"`
	CreatedAt        time.Time
	UpdatedAt        time.Time
	Transaksi        *Transaksi `json:"transaksi" gorm:"foreignKey:idtransaksi;association_foreignkey:idtransaksi;references:idtransaksi"`
	Hapus            int        `json:"hapus"`
}
