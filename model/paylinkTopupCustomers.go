package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type PaylinkTopupCustomers struct {
	Id              string          `json:"id" gorm:"primary_key;"`
	UserId          string          `json:"user_id" gorm:"index"`
	UserType        int             `json:"user_type" gorm:"index"`
	Idowner         string          `json:"idowner" gorm:"index"`
	Idoutlet        string          `json:"idoutlet" gorm:"index"`
	Qrcode          string          `json:"qrcode" gorm:"index"`
	Nominal         float64         `json:"nominal"`
	Diskon          float64         `json:"diskon"`
	Total           float64         `json:"total"`
	JenisBayar      int             `json:"jenis_bayar"`
	RefId           string          `json:"ref_id" gorm:"index"`
	Bayar           float64         `json:"bayar"`
	Kembalian       float64         `json:"kembalian"`
	Status          int             `json:"status"`
	StatusChangedAt *time.Time      `json:"status_changed_at"`
	ExpiredAt       *time.Time      `json:"expired_at" `
	CreatedAt       *time.Time      `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt       *time.Time      `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	Hapus           *int            `json:"hapus" gorm:"default:0"`
	CustomerGlobal  *CustomerGlobal `json:"customer_global" gorm:"foreignKey:id;association_foreignkey:user_id;references:user_id"`
}

var PaylinkTopupCustomersObject = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "paylink_topup_customers",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "id",
			},
			"user_id": &graphql.Field{
				Type:        graphql.String,
				Description: "user id",
			},
			"user_type": &graphql.Field{
				Type:        graphql.Int,
				Description: "user type : 1=owner, 2 = customer",
			},
			"idowner": &graphql.Field{
				Type:        graphql.String,
				Description: "idowner ",
			},
			"idoutlet": &graphql.Field{
				Type:        graphql.String,
				Description: "idoutlet",
			},
			"qrcode": &graphql.Field{
				Type:        graphql.String,
				Description: "qrcode",
			},
			"nominal": &graphql.Field{
				Type:        graphql.Float,
				Description: "nominal",
			},
			"diskon": &graphql.Field{
				Type:        graphql.Float,
				Description: "diskon",
			},
			"kode_unik": &graphql.Field{
				Type:        graphql.Float,
				Description: "kode_unik",
			},
			"total": &graphql.Field{
				Type:        graphql.Float,
				Description: "total",
			},
			"jenis_bayar": &graphql.Field{
				Type:        graphql.Int,
				Description: "jenis bayar : 1 = cash,2 = transfer",
			},
			"ref_id": &graphql.Field{
				Type:        graphql.String,
				Description: "reference id : jika cash, bisa di isi id owner yang men-topup",
			},
			"bayar": &graphql.Field{
				Type:        graphql.Float,
				Description: "bayar",
			},
			"kembalian": &graphql.Field{
				Type:        graphql.Float,
				Description: "kembalian",
			},
			"status": &graphql.Field{
				Type:        graphql.Int,
				Description: "0=belum dibayar, 1=sudah konfirmasi bayar, 2=sudah diverifikasi, 3 = dibatalkan",
			},
			"expired_at": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "Expired",
			},
			"created_at": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "created_at",
			},
			"status_changed_at": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "status_changed_at",
			},
			"customer_global": &graphql.Field{
				Type:        CustomerGlobalType,
				Description: "",
			},
		},
	})

var PaylinkTopupCustomerInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "paylink_topup_customers_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"user_id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "user id",
			},
			"user_type": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "user type : 1=owner, 2 = customer",
			},
			"qrcode": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "qrcode",
			},
			"nominal": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "nominal",
			},
			"diskon": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "diskon",
			},
			"total": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "total",
			},
		},
	})

var PaylinkTopupCustomerSuccess = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "paylink_topup_customers_success",
		Fields: graphql.InputObjectConfigFieldMap{
			"id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "topup id",
			},
			"idowner": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "idowner",
			},
			"idoutlet": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "idoutlet",
			},

			"jenis_bayar": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "jenis bayar : 1 = cash,2 = transfer",
			},
			"bayar": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "bayar",
			},
			"kembalian": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "kembalian",
			},
		},
	})

var PaylinkTopupCashAcceptorInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "paylink_topup_cash_acceptor_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id contoh : PCA2994391804031020",
			},

			"idcustomer_global": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "customer glibal id",
			},
			"nominal": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "nominal",
			},
			"idowner": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "idowner",
			},
			"idoutlet": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "idoutlet",
			},
		},
	})
