package model

import "time"
import "github.com/graphql-go/graphql"

type Outlet struct {
	Idoutlet                  string            `json:"idoutlet" gorm:"primary_key;index"`
	OwnerIdowner              string            `json:"owner_idowner" gorm:"foreignkey:idowner"`
	WorkshopIdworkshop        *string           `json:"workshop_idworkshop"`
	Nama                      string            `json:"nama"`
	Kota                      string            `json:"kota"`
	Telp                      string            `json:"telp"`
	Alamat                    string            `json:"alamat"`
	LogoUrl                   string            `json:"logo_url"`
	JenisPrinterId            int               `json:"jenis_printer_id"`
	DisplayLogo               int               `json:"display_logo"`
	DisplayBarcode            int               `json:"display_barcode"`
	DisplayQrcode             int               `json:"display_qrcode"`
	NotaTransaksi             string            `json:"nota_transaksi"`
	NotaBeliDeposit           string            `json:"nota_beli_deposit"`
	NotaBeliEmoney            string            `json:"nota_beli_emoney"`
	NotaDaftarMember          string            `json:"nota_daftar_member"`
	NotaSuratJemput           string            `json:"nota_surat_jemput"`
	NotaSuratAntar            string            `json:"nota_surat_antar"`
	NotaSuratJemputWorkshop   string            `json:"nota_surat_jemput_workshop"`
	NotaSuratAntarWorkshop    string            `json:"nota_surat_antar_workshop"`
	NotaTopupPaylink          string            `json:"nota_topup_paylink"`
	NotifWa                   int               `json:"notif_wa"`
	NotifSms                  int               `json:"notif_sms"`
	NotifPrint                int               `json:"notif_print"`
	SecEnableFaktur           int               `json:"sec_enable_faktur"`
	SecEnableSms              int               `json:"sec_enable_sms"`
	SecEnableWa               int               `json:"sec_enable_wa"`
	SecJamBuka                *time.Time        `json:"sec_jam_buka"`
	SecJamTutup               *time.Time        `json:"sec_jam_tutup"`
	SecActive                 int               `json:"sec_active"`
	SecLat                    float64           `json:"sec_lat"`
	SecLng                    float64           `json:"sec_lng"`
	SecSmsPremium             int               `json:"sec_sms_premium"`
	SecPrefixActive           int               `json:"sec_prefix_active"`
	SecPrefixTransaksiReguler string            `json:"sec_prefix_transaksi_reguler"`
	SecPrefixTransaksiDeposit string            `json:"sec_prefix_transaksi_deposit"`
	SecPrefixTopupEmoney      string            `json:"sec_prefix_topup_emoney"`
	SecPrefixTopupDeposit     string            `json:"sec_prefix_topup_deposit"`
	DateActive                *time.Time        `json:"date_active"`
	Diskon                    float64           `json:"diskon"`
	EditableDiskon            float64           `json:"editable_diskon"`
	Pajak                     float64           `json:"pajak"`
	EditablePajak             float64           `json:"editable_pajak"`
	KotaId                    int               `json:"kota_id"`
	PrintAmbil                int               `json:"print_ambil"`
	CountryCode               string            `json:"country_code"`
	Latitude                  float64           `json:"latitude"`
	Longitude                 float64           `json:"longitude"`
	AbsenRadius               float64           `json:"absen_radius"`
	MaxTagihanSnap            float64           `json:"max_tagihan_snap"`
	Biaya                     float64           `json:"biaya"`
	EditableBiaya             float64           `json:"editable_biaya"`
	BiayaTipe                 int               `json:"biaya_tipe"`
	BiayaPengantaran          float64           `json:"biaya_pengantaran"`
	EditableBiayaPengantaran  int               `json:"editable_biaya_pengantaran"`
	RegulerWajibLunas         float64           `json:"reguler_wajib_lunas"`
	DropoffWajibLunas         float64           `json:"dropoff_wajib_lunas"`
	EnableExpress             int               `json:"enable_express"`
	HideCustomer              int               `json:"hide_customer"`
	BisaLunasiInvoice         int               `json:"bisa_lunasi_invoice"` // 0 OFF, 1 Semua Kasir, 2 Kasir Tertentu
	WajibBuktiTf              int               `json:"wajib_bukti_tf"`
	SnapHome                  []SnapHome        `json:"snap_home" gorm:"index;foreignKey:idoutlet;association_foreignkey:idoutlet;references:idoutlet"`
	SnapCredential            *SnapCredential   `json:"snap_credential" gorm:"index;foreignKey:idoutlet;association_foreignkey:idoutlet;references:idoutlet"`
	Workshop                  *Workshop         `json:"workshop" gorm:"index;foreignKey:idworkshop;association_foreignkey:workshop_idworkshop;references:workshop_idworkshop"`
	SnapMesin                 []SnapMesin       `json:"snap_mesin" gorm:"index;foreignKey:idoutlet;association_foreignkey:idoutlet;references:idoutlet"`
	Whitelabel                *WhitelabelOutlet `json:"whitelabel" gorm:"index;foreignKey:idoutlet;association_foreignkey:idoutlet;references:idoutlet"`
	BaseModel
}

var OutletType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "outlet",
		Fields: graphql.Fields{
			"idoutlet": &graphql.Field{
				Type:        graphql.String,
				Description: "id outlet",
			},
			"owner_idowner": &graphql.Field{
				Type:        graphql.String,
				Description: "id owner outlet",
			},
			"workshop_idworkshop": &graphql.Field{
				Type:        graphql.String,
				Description: "id workshop outlet",
			},
			"nama": &graphql.Field{
				Type:        graphql.String,
				Description: "nama outlet",
			},
			"kota": &graphql.Field{
				Type:        graphql.String,
				Description: "kota outlet",
			},
			"telp": &graphql.Field{
				Type:        graphql.String,
				Description: "nomor telepon outlet",
			},
			"alamat": &graphql.Field{
				Type:        graphql.String,
				Description: "alamat lengkap outlet",
			},
			"logo_url": &graphql.Field{
				Type:        graphql.String,
				Description: "gambar logo outlet",
			},
			"jenis_printer_id": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"display_logo": &graphql.Field{
				Type:        graphql.Int,
				Description: "memunculkan logo outlet",
			},
			"display_barcode": &graphql.Field{
				Type:        graphql.Int,
				Description: "memunculkan display barcode",
			},
			"display_qrcode": &graphql.Field{
				Type:        graphql.Int,
				Description: "memunculkan display qrcode",
			},
			"nota_transaksi": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"nota_beli_deposit": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"nota_beli_emoney": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"nota_daftar_member": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"nota_surat_jemput": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"nota_surat_antar": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"nota_surat_jemput_workshop": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"nota_surat_antar_workshop": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"notif_wa": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"notif_sms": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"notif_print": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"sec_enable_faktur": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"sec_enable_sms": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"sec_enable_wa": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"sec_jam_buka": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "",
			},
			"sec_jam_tutup": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "",
			},
			"sec_active": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"sec_lat": &graphql.Field{
				Type:        graphql.Float,
				Description: "",
			},
			"sec_lng": &graphql.Field{
				Type:        graphql.Float,
				Description: "",
			},
			"sec_sms_premium": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"sec_prefix_active": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"sec_prefix_transaksi_reguler": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"sec_prefix_transaksi_deposit": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"sec_prefix_topup_emoney": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"sec_prefix_topup_deposit": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"date_active": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "",
			},
			"diskon": &graphql.Field{
				Type:        graphql.Float,
				Description: "",
			},
			"editable_diskon": &graphql.Field{
				Type:        graphql.Float,
				Description: "",
			},
			"pajak": &graphql.Field{
				Type:        graphql.Float,
				Description: "",
			},
			"editable_pajak": &graphql.Field{
				Type:        graphql.Float,
				Description: "",
			},
			"biaya": &graphql.Field{
				Type:        graphql.Float,
				Description: "",
			},
			"editable_biaya": &graphql.Field{
				Type:        graphql.Float,
				Description: "",
			},
			"kota_id": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"dropoff_wajib_lunas": &graphql.Field{
				Type:        graphql.Float,
				Description: "",
			},

			"reguler_wajib_lunas": &graphql.Field{
				Type:        graphql.Float,
				Description: "",
			},
			"latitude": &graphql.Field{
				Type:        graphql.Float,
				Description: "",
			},
			"longitude": &graphql.Field{
				Type:        graphql.Float,
				Description: "",
			},
			"max_tagihan_snap": &graphql.Field{
				Type:        graphql.Float,
				Description: "",
			},
			"snap_home": &graphql.Field{
				Type:        graphql.NewList(SnapHomeType),
				Description: "",
			},
			"snap_credential": &graphql.Field{
				Type:        SnapCredentialType,
				Description: "",
			},
			"snap_mesin": &graphql.Field{
				Type:        graphql.NewList(SnapMesinType),
				Description: "",
			},
		},
	},
)
