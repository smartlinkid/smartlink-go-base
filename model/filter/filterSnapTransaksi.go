package filter

import "github.com/graphql-go/graphql"

type FilterSnapTransaksi struct {
	CreatedAtStart      string `json:"created_at_start"`
	CreatedAtEnd        string `json:"created_at_end"`
	FinishedAtStart     string `json:"finished_at_start"`
	FinishedAtEnd       string `json:"finished_at_end"`
	TakenAtStart        string `json:"taken_at_start"`
	TakenAtEnd          string `json:"taken_at_end"`
	Jenis               []int  `json:"jenis"`
	Lunas               int    `json:"lunas"`
	Working             int    `json:"working"`
	StatusBayar         []int  `json:"status_bayar"`
	Status              []int  `json:"status"`
	BayarCreatedAtStart string `json:"bayar_created_at_start"`
	BayarCreatedAtEnd   string `json:"bayar_created_at_end"`
	TipeBayar           []int  `json:"tipe_bayar"`
	JenisBayar          []int  `json:"jenis_bayar"`
}

var FilterSnapTransaksiInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "filter_snap_transaksi",
		Fields: graphql.InputObjectConfigFieldMap{
			"created_at_start": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "",
			},
			"created_at_end": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "",
			},
			"finished_at_start": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "",
			},
			"finished_at_end": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "",
			},
			"taken_at_start": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "",
			},
			"taken_at_end": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "",
			},
			"jenis": &graphql.InputObjectFieldConfig{
				Type:        graphql.NewList(graphql.Int),
				Description: "",
			},
			"lunas": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "",
			},
			"working": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "",
			},
			"status_bayar": &graphql.InputObjectFieldConfig{
				Type:        graphql.NewList(graphql.Int),
				Description: "",
			},
			"status": &graphql.InputObjectFieldConfig{
				Type:        graphql.NewList(graphql.Int),
				Description: "",
			},
			"bayar_created_at_start": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "",
			},
			"bayar_created_at_end": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "",
			},
			"tipe_bayar": &graphql.InputObjectFieldConfig{
				Type:        graphql.NewList(graphql.Int),
				Description: "",
			},
			"jenis_bayar": &graphql.InputObjectFieldConfig{
				Type:        graphql.NewList(graphql.Int),
				Description: "",
			},
		},
	},
)
