package model

import "time"

type JadwalJemputBayar struct {
	Id         string     `json:"id" gorm:"primary_key"`
	Idjemput   string     `json:"idjemput"`
	IdOwner    string     `json:"idowner"`
	IdOutlet   string     `json:"idoutlet"`
	Bayar      float64    `json:"bayar"`
	Kembalian  float64    `json:"kembalian"`
	JenisBayar int        `json:"jenis_bayar"`
	TipeBayar  int        `json:"tipe_bayar"`
	Keterangan string     `json:"keterangan"`
	Resi       string     `json:"resi"`
	CreatedAt  *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP"`
	UpdatedAt  *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
