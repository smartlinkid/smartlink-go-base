package model

import "time"

type PembelianEmoneyHapus struct {
	Id                   int              `json:"id" gorm:"primary_key"`
	Idpembelian          string           `json:"idpembelian"`
	TanggalRequest       int              `json:"tanggal_request"`
	Keterangan           string           `json:"keterangan"`
	Status               int              `json:"status"`
	TanggalKonfirmasi    *time.Time       `json:"tanggal_konfirmasi"`
	KeteranganKonfirmasi string           `json:"keterangan_konfirmasi"`
	Idowner              string           `json:"idowner"`
	Idkaryawan           *string          `json:"idkaryawan"`
	Idoutlet             string           `json:"idoutlet"`
	Outlet               *Outlet          `json:"outlet" gorm:"foreignkey:idoutlet;association_foreignkey:idoutlet;references:idoutlet"`
	Owner                *Owner           `json:"owner" gorm:"foreignkey:idowner;association_foreignkey:idowner;references:idowner"`
	Pembelian            *PembelianEmoney `json:"pembelian" gorm:"foreignkey:idpembelian_emoney;association_foreignkey:idpembelian;references:idpembelian"`
	Karyawan             *Karyawan        `json:"karyawan" gorm:"foreignkey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	CreatedAt            *time.Time       `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt            *time.Time       `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
