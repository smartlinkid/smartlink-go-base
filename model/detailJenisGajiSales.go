package model

import "time"

type DetailJenisGajiSales struct {
	Id               int        `json:"id" gorm:"index;primary_key"`
	IdjenisGajiSales int        `json:"idjenis_gaji_sales"`
	TotalOmset       float64    `json:"total_omset"`
	PersentaseKomisi float64    `json:"persentase_komisi"`
	CreatedAt        *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt        *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
