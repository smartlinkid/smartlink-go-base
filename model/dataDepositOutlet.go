package model

type DataDepositOutlet struct {
	OutletIdoutlet           string        `json:"outlet_idoutlet"`
	DataDepositIddataDeposit string        `json:"data_deposit_iddata_deposit"`
	DataDeposit2             *DataDeposit2 `json:"Deposit" gorm:"foreignkey:iddata_deposit;association_foreignkey:data_deposit_iddata_deposit;references:data_deposit_iddata_deposit"`
	Outlet                   *Outlet       `json:"outlet" gorm:"foreignkey:idoutlet;association_foreignkey:outlet_idoutlet;references:outlet_idoutlet"`
	BaseModel
}
