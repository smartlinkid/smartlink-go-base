package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type PaylinkRefunds struct {
	Id            string           `json:"id" gorm:"primary_key;"`
	PaymentId     string           `json:"payment_id" gorm:"index"`
	IdakunDebet   string           `json:"idakun_debet" gorm:"index"`
	IdakunKredit  string           `json:"idakun_kredit" gorm:"index"`
	Amount        float64          `json:"amount"`
	Waktu         int64            `json:"waktu"`
	Idowner       string           `json:"idowner" gorm:"index"`
	Idoutlet      string           `json:"idoutlet" gorm:"index"`
	Idkaryawan    string           `json:"idkaryawan" gorm:"index"`
	NomorPenerima string           `json:"nomor_penerima"`
	NamaPenerima  string           `json:"nama_penerima"`
	Description   string           `json:"description"`
	CreatedAt     time.Time        `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt     *time.Time       `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	Hapus         *int             `json:"hapus" gorm:"default:0"`
	AkunDebet     KeuanganAkunAkun `json:"akun_debet" gorm:"index;foreignKey:idakun_debet;association_foreignkey:idakun;references:idakun"`
	AkunKredit    KeuanganAkunAkun `json:"akun_kredit" gorm:"index;foreignKey:idakun_kredit;association_foreignkey:idakun;references:idakun"`
}

var PaylinkRefundsObject = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "paylink_refund",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "id ",
			},
			"payment_id": &graphql.Field{
				Type:        graphql.String,
				Description: "payment id ",
			},
			"idakun_debet": &graphql.Field{
				Type:        graphql.String,
				Description: "ID akun debet string",
			},
			"idakun_kredit": &graphql.Field{
				Type:        graphql.String,
				Description: "ID akun kredit string",
			},
			"amount": &graphql.Field{
				Type:        graphql.Float,
				Description: "Amount",
			},
			"idowner": &graphql.Field{
				Type:        graphql.String,
				Description: "Id Owner",
			},
			"idoutlet": &graphql.Field{
				Type:        graphql.String,
				Description: "Id Outlet",
			},
			"idkaryawan": &graphql.Field{
				Type:        graphql.String,
				Description: "Id Karyawan",
			},
			"description": &graphql.Field{
				Type:        graphql.String,
				Description: "description or status reason",
			},
			"nomor_penerima": &graphql.Field{
				Type:        graphql.String,
				Description: "nomor penerima",
			},
			"nama_penerima": &graphql.Field{
				Type:        graphql.String,
				Description: "nama penerima",
			},
			"waktu": &graphql.Field{
				Type:        graphql.String,
				Description: "waktu",
			},
			"created_at": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "CreatedAt",
			},
			"akun_debet": &graphql.Field{
				Type:        KeuanganAkunAkunType,
				Description: "AkunDebet",
			},
			"akun_kredit": &graphql.Field{
				Type:        KeuanganAkunAkunType,
				Description: "Akun Kredit",
			},
		},
	})

var PaylinkRefundsInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "paylink_refund_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "Unix ID",
			},
			"payment_id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "payment ID",
			},
			"idakun": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "idakun",
			},
			"idkaryawan": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id karyawan ",
			},
			"waktu": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "Waktu",
			},
			"nomor_penerima": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "nomor penerima",
			},
			"nama_penerima": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "nama penerima",
			},
			"description": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "Description",
			},
		},
	})
