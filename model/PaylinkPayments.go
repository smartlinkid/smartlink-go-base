package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type PaylinkPayments struct {
	Id           string     `json:"id" gorm:"primary_key;"`
	RelatedTable string     `json:"related_table"`
	RelatedId    string     `json:"related_id"`
	Amount       float64    `json:"amount"`
	FeePercent   float64    `json:"fee_percent"`
	Project      int        `json:"project"`
	Action       int        `json:"action"`
	UserId       string     `json:"user_id" gorm:"index"`
	UserType     int        `json:"user_type"`
	Idoutlet     string     `json:"idoutlet" gorm:"index"`
	Status       int        `json:"status"`
	Description  string     `json:"description"`
	RefTable     string     `json:"ref_table"`
	RefId        string     `json:"ref_id"`
	CompleteId   string     `json:"complete_id"`
	Source       string     `json:"source"`
	ReceiptId    string     `json:"receipt_id"`
	Channel      string     `json:"channel"`
	CreatedAt    *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt    *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	Hapus        *int       `json:"hapus" gorm:"default:0"`
}

var PaylinkPaymentsObject = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "paylink_payment",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "id ",
			},
			"related_table": &graphql.Field{
				Type:        graphql.String,
				Description: "related_table string",
			},
			"related_id": &graphql.Field{
				Type:        graphql.String,
				Description: "related_id string",
			},
			"user_id": &graphql.Field{
				Type:        graphql.String,
				Description: "user id",
			},
			"user_type": &graphql.Field{
				Type:        graphql.Int,
				Description: "user type : 1=owner, 2 = customer",
			},
			"idoutlet": &graphql.Field{
				Type:        graphql.String,
				Description: "Id Outlet",
			},
			"amount": &graphql.Field{
				Type:        graphql.Float,
				Description: "Amount",
			},
			"fee_percent": &graphql.Field{
				Type:        graphql.Float,
				Description: "Fee Percent",
			},
			"status": &graphql.Field{
				Type:        graphql.Int,
				Description: "status (1= Active, 2=Inactive)",
			},
			"description": &graphql.Field{
				Type:        graphql.String,
				Description: "description or status reason",
			},
			"project": &graphql.Field{
				Type:        graphql.Int,
				Description: "Project (1=Smartlink, 2=Snapclean, 4=Member Register)",
			},
			"action": &graphql.Field{
				Type:        graphql.Int,
				Description: "Project (11=smartlink kasir, 21=snapclean kasir,  22=snapclean sticker, 41=member register  )",
			},
			"ref_table": &graphql.Field{
				Type:        graphql.String,
				Description: "Ref Tabler",
			},
			"ref_id": &graphql.Field{
				Type:        graphql.String,
				Description: "Ref Tabler",
			},
			"complete_id": &graphql.Field{
				Type:        graphql.String,
				Description: "ref id for complete payment",
			},
			"source": &graphql.Field{
				Type:        graphql.String,
				Description: "Ewallet payment",
			},
			"receipt_id": &graphql.Field{
				Type:        graphql.String,
				Description: "RRN payment",
			},
			"channel": &graphql.Field{
				Type:        graphql.String,
				Description: "Between Nobu,xendit",
			},
			"created_at": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "CreatedAt",
			},
		},
	})
