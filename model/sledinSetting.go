package model

import "time"

type SledinSetting struct {
	Idowner             string     `json:"idowner" gorm:"primary_key"`
	Idoutlet            string     `json:"idoutlet" gorm:"primary_key"`
	SmsNota             int        `json:"sms_nota"`
	SmsAmbil            int        `json:"sms_ambil"`
	SmsSelesai          int        `json:"sms_selesai"`
	DurasiAmbil         string     `json:"durasi_ambil"`
	SmsUltah            int        `json:"sms_ultah"`
	LastReminderAmbil   *time.Time `json:"last_reminder_ambil"`
	ReminderEmoney      int        `json:"reminder_emoney"`
	DurasiEmoney        string     `json:"durasi_emoney"`
	DurasiDeposit       string     `json:"durasi_deposit"`
	LastReminderDeposit *time.Time `json:"last_reminder_deposit"`
}
