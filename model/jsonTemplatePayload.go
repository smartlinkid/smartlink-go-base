package model

type JsonTemplatePayload struct {
	Id         int64  `json:"id" gorm:"primarykey;index;auto_increment"`
	Payload    string `json:"payload"`
	Idpath     string `json:"idpath" gorm:"foreignkey:idpath;association_foreignkey:id;references:id"`
	Keterangan string `json:"keterangan"`
}
