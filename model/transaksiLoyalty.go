package model

import "time"

type TransaksiLoyalty struct {
	Id              string                 `json:"id"`
	Idtransaksi     string                 `json:"idtransaksi"`
	Idjemput        string                 `json:"idjemput"`
	Idcustomer      string                 `json:"idcustomer"`
	Idowner         string                 `json:"idowner"`
	Idoutlet        string                 `json:"idoutlet"`
	Idantar         string                 `json:"idantar"`
	Status          string                 `json:"status"`
	CreatedAt       *time.Time             `json:"created_at" gorm:"default:CURRENT_TIMESTAMP"`
	Hapus           int                    `json:"hapus"`
	Lunas           *bool                  `json:"lunas,omitempty" gorm:"-"`
	Amount          *float64               `json:"amount,omitempty" gorm:"-"`
	Customer        *Customer              `json:"customer,omitempty" gorm:"foreignkey:idcustomer;association_foreignkey:idcustomer;references:idcustomer"`
	JadwalJemput    *JadwalJemput          `json:"jadwal_jemput" gorm:"foreignkey:idjemput;association_foreignkey:idjemput;references:idjemput"`
	AntarExtra      *AntarExtra            `json:"antar_extra" gorm:"foreignkey:id;association_foreignkey:idantar;references:idantar"`
	Outlet          *Outlet                `json:"outlet" gorm:"foreignkey:idoutlet;association_foreignkey:idoutlet;references:idoutlet"`
	Transaksi       *Transaksi             `json:"transaksi" gorm:"foreignkey:idtransaksi;association_foreignkey:idtransaksi;references:idtransaksi"`
	TransaksiFoto   *TransaksiFoto         `json:"transaksi_foto" gorm:"foreignkey:idtransaksi;association_foreignkey:idtransaksi;references:idtransaksi"`
	DetailTransaksi map[string]interface{} `json:"detail_transaksi" gorm:"-"`
}
