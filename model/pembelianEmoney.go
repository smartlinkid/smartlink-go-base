package model

type PembelianEmoney struct {
	IdpembelianEmoney            string                      `json:"idpembelian_emoney" gorm:"primary_key"`
	OutletIdoutlet               string                      `json:"outlet_idoutlet"`
	CustomerIdcustomer           string                      `json:"customer_idcustomer"`
	KaryawanIdkaryawan           string                      `json:"karyawan_idkaryawan"`
	TanggalBeli                  int                         `json:"tanggal_beli"`
	Keterangan                   string                      `json:"keterangan"`
	Total                        float64                     `json:"total"`
	Diskon                       float64                     `json:"diskon"`
	PersenDiskon                 float64                     `json:"persendiskon"`
	Pajak                        float64                     `json:"pajak"`
	PersenPajak                  float64                     `json:"persenpajak"`
	Grandtotal                   float64                     `json:"grandtotal"`
	ShortLink                    string                      `json:"short_link"`
	Quota                        float64                     `json:"quota"`
	SaldoAwal                    float64                     `json:"saldo_awal"`
	SaldoAkhir                   float64                     `json:"saldo_akhir"`
	SaldoHapus                   float64                     `json:"saldo_hapus"`
	MasaAktifAwal                int                         `json:"masa_aktif_awal"`
	MasaAktifAkhir               int                         `json:"masa_aktif_akhir"`
	StatusHapus                  int                         `json:"status_hapus"`
	OldJenisEmoney               int                         `json:"old_jenis_emoney"`
	NewJenisEmoney               int                         `json:"new_jenis_emoney"`
	OldTipePerpanjanganMasaAktif int                         `json:"old_tipe_perpanjangan_masa_aktif"`
	NewTipePerpanjanganMasaAktif int                         `json:"new_tipe_perpanjangan_masa_aktif"`
	OldTipeSaldoExpired          int                         `json:"old_tipe_saldo_expired"`
	NewTipeSaldoExpired          int                         `json:"new_tipe_saldo_expired"`
	Lunas                        int                         `json:"lunas"`
	Outlet                       *Outlet                     `json:"outlet" gorm:"foreignkey:idoutlet;association_foreignkey:outlet_idoutlet;references:outlet_idoutlet"`
	Karyawan                     *Karyawan                   `json:"karyawan" gorm:"foreignkey:idkaryawan;association_foreignkey:karyawan_idkaryawan;references:karyawan_idkaryawan"`
	Customer                     *Customer                   `json:"customer" gorm:"foreignkey:idcustomer;association_foreignkey:customer_idcustomer;references:customer_idcustomer"`
	PembelianEmoneyHapus         *PembelianEmoneyHapus       `json:"pembelian_emoney_hapus" gorm:"foreignkey:idpembelian;association_foreignkey:idpembelian_emoney;references:idpembelian_emoney"`
	Pembayaran                   []PembayaranPembelianEmoney `json:"pembayaran" gorm:"foreignkey:idpembelian_emoney;association_foreignkey:idpembelian_emoney;references:idpembelian_emoney"`
	DetailPembelianEmoney        []DetailPembelianEmoney     `json:"detail_pembelian_emoney" gorm:"foreignkey:pembelian_emoney_idpembelian_emoney;association_foreignkey:idpembelian_emoney;references:idpembelian_emoney"`
	PembelianEmoneyLoyalty       *PembelianEmoneyLoyalty     `json:"pembelian_emoney_loyalty" gorm:"foreignkey:idpembelian_emoney;association_foreignkey:idpembelian_emoney;references:idpembelian_emoney"`
	BaseModel
}
