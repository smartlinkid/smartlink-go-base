package model

import "time"

type WaTemplate struct {
	Idowner   string     `json:"idowner"`
	Template  string     `json:"template"`
	Idpayload string     `json:"idpayload"`
	CreatedAt *time.Time `json:"created_at"`
	UpdatedAt *time.Time `json:"updated_at"`
	Idoutlet  string     `json:"idoutlet"`
}
