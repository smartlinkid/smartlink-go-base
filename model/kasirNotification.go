package model

import "time"

type KasirNotification struct {
	Id             string     `json:"id" gorm:"primary_key;index"`
	Type           string     `json:"type"`
	Idowner        string     `json:"idowner"`
	Idoutlet       *string    `json:"idoutlet"`
	Idkaryawan     *string    `json:"idkaryawan"`
	NotifiableType string     `json:"notifiable_type"`
	Data           string     `json:"data"`
	IsRead         int        `json:"is_read"`
	ReadAt         *time.Time `json:"read_at"`
	CreatedAt      *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt      *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
	RelatedType    string     `json:"related_type"`
	RelatedId      string     `json:"related_id"`
	IsAction       int        `json:"is_action"`
	NeedAction     int        `json:"need_action"`
}
