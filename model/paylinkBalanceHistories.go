package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type PaylinkBalanceHistories struct {
	Id            int64      `json:"id" gorm:"primary_key;index;auto_increment"`
	UserId        string     `json:"user_id" gorm:"index"`
	BalanceType   int        `json:"balance_type"`
	Debit         float64    `json:"debit"`
	Credit        float64    `json:"credit"`
	Balance       float64    `json:"balance"`
	ReferenceType string     `json:"reference_type" gorm:"index"`
	ReferenceId   string     `json:"reference_id" gorm:"index"`
	Description   string     `json:"description"`
	CreatedAt     *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt     *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	Hapus         *int       `json:"hapus" gorm:"default:0"`
}

var PaylinkBalancesHistoryObject = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "paylink_balance_history",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.Int,
				Description: "id",
			},
			"user_id": &graphql.Field{
				Type:        graphql.String,
				Description: "user id",
			},
			"balance_type": &graphql.Field{
				Type:        graphql.Int,
				Description: "balance type",
			},
			"debit": &graphql.Field{
				Type:        graphql.Float,
				Description: "debit",
			},
			"credit": &graphql.Field{
				Type:        graphql.Float,
				Description: "credit",
			},
			"balance": &graphql.Field{
				Type:        graphql.Float,
				Description: "balance",
			},
			"reference_type": &graphql.Field{
				Type:        graphql.String,
				Description: "reference_type",
			},
			"reference_id": &graphql.Field{
				Type:        graphql.String,
				Description: "reference_id",
			},
			"description": &graphql.Field{
				Type:        graphql.String,
				Description: "description",
			},
			"created_at": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "created_at",
			},
		},
	})
