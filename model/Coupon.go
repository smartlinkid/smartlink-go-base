package model

import "time"

type Coupon struct {
	Id                 int `json:"id" gorm:"primary_key;index"`
	Kode               string
	DiskonPersen       float64
	StartDate          time.Time
	EndDate            time.Time
	Kapasitas          int
	Keterangan         string
	UpdatedAt          time.Time
	CreatedAt          time.Time
	Hapus              int
	Type               int
	KapasitasTerpesan  int
	KapasitasTerjurnal int
	KapasitasTerjual   int
	NamaProduk         string
	HargaJualProduk    string
	IdakunProduk       string
	Status             int
	Jenis              string
	KapasitasPerAkun   int
	Cashback           float64
	FreeFitur          string
	FreeFiturUsed      int
	DiskonFitur        float64
	MinTagihan         float64
	MaxDiskon          float64
	MaxDiskonFitur     float64
}
