package model

import "github.com/graphql-go/graphql"

type LayananSnapTag struct {
	Idlayanan string  `json:"idlayanan" gorm:"primary_key;index"`
	Idtahapan int     `json:"idtahapan" gorm:"index"`
	IdsnapTag int     `json:"idsnap_tag" gorm:"index"`
	SnapTag   SnapTag `json:"snap_tag" gorm:"foreignkey:id;association_foreignkey:idsnap_tag;references:idsnap_tag"`
}

var LayananSnapTagType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "layanan_snap_tag_type",
		Fields: graphql.Fields{
			"idlayanan": &graphql.Field{
				Type:        graphql.String,
				Description: "idlayanan",
			},
			"idtahapan": &graphql.Field{
				Type:        graphql.Int,
				Description: "idtahapan",
			},
			"idsnap_tag": &graphql.Field{
				Type:        graphql.Int,
				Description: "idsnap_tag",
			},
		},
	})

var LayananSnapTagInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "layanan_snap_tag_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"idlayanan": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "idlayanan",
			},
			"idtahapan": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "idtahapan",
			},
			"idsnap_tag": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "idsnap_tag",
			},
		},
	})
