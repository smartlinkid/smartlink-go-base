package model

import "time"

type MasalBayar struct {
	IdmasalTransaksi string          `json:"idmasal_transaksi"`
	Idpembayaran     int64           `json:"idpembayaran"`
	Hash             string          `json:"hash"`
	Hapus            int             `json:"hapus"`
	PembayaranUmum   *PembayaranUmum `json:"pembayaranUmum" gorm:"foreignKey:idpembayaran;association_foreignkey:id;references:id"`
	CreatedAt        time.Time
	UpdatedAt        time.Time
}
