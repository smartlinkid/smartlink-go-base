package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type SnapHome struct {
	HomeId    int64      `json:"home_id" gorm:"index"`
	Idoutlet  string     `json:"idoutlet" gorm:"index"`
	Idowner   string     `json:"idowner" gorm:"index"`
	CreatedAt *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	Hapus     *int       `json:"hapus" gorm:"default:0"`
}

var SnapHomeType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "snap_home",
		Fields: graphql.Fields{
			"home_id": &graphql.Field{
				Type:        graphql.String,
				Description: "home id snap",
			},
			"idoutlet": &graphql.Field{
				Type:        graphql.String,
				Description: "id outlet snap",
			},
			"idowner": &graphql.Field{
				Type:        graphql.String,
				Description: "id owner snap",
			},
			//"base": &graphql.Field{
			//	Type:        BaseModelInput,
			//	Description: "base model",
			//},
		},
		Description: "snap home data",
	})

var SnapHomeInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "snap_home_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"home_id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "home id snap",
			},
			"idoutlet": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id outlet snap",
			},
			"idowner": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id owner snap",
			},
		},
	})
