package model

type WhitelabelOutlet struct {
	Idowner         string  `json:"idowner"`
	Idoutlet        string  `json:"idoutlet"`
	IsAntarJemput   int     `json:"is_antar_jemput"`
	IsSelfservice   int     `json:"is_selfservice"`
	IsBukanKeduanya int     `json:"is_bukan_keduanya"`
	Alamat          string  `json:"alamat"`
	AlamatLink      string  `json:"alamat_link"`
	Latitude        float64 `json:"latitude"`
	Longitude       float64 `json:"longitude"`
	IsKurirOutlet   int     `json:"is_kurir_outlet"`
	Aktif           int     `json:"aktif"`
	Tz              string  `json:"tz"`
	Outlet          *Outlet `json:"outlet" gorm:"foreignkey:idoutlet;association_foreignkey:idoutlet"`
}
