package model

import "time"

type WaBotPromo struct {
	Id                string             `json:"id"`
	Idowner           string             `json:"idowner"`
	Idoutlet          string             `json:"idoutlet"`
	IdwaDevice        string             `json:"idwa_device"`
	Nama              string             `json:"nama"` // Nama Promo
	PersentaseDiskon  float64            `json:"persentase_diskon"`
	JumlahKupon       int64              `json:"jumlah_kupon"`
	MasaAktifKupon    int64              `json:"masa_aktif_kupon"` //Masa aktif setelah dibeli. milisecond
	MinTopup          float64            `json:"min_topup"`
	JenisWaktuPromo   int64              `json:"jenis_waktu_promo"` //1: tanggal  2: hari. kalau tanggal pakai kolom wa_bot_promo.promo_tanggal_start * promo_tanggal_end.\nkalau pakai harian pakai tabel wa_bot_promo_harian
	PromoTanggalStart *time.Time         `json:"promo_tanggal_start"`
	PromoTanggalEnd   *time.Time         `json:"promo_tanggal_end"`
	Active            int                `json:"active"` // 0 / 1
	CreatedAt         *time.Time         `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt         *time.Time         `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	WaBotPromoHarian  []WaBotPromoHarian `json:"wa_bot_promo_harian" gorm:"foreignKey:idwa_bot_promo;association_foreignkey:id;references:id"`
}
