package model

import "time"

type AntarExtra struct {
	Id                    string                 `json:"id" gorm:"primary_key"`
	RefId                 string                 `json:"ref_id"`
	RefType               string                 `json:"ref_type"`
	Idkaryawan            string                 `json:"idkaryawan"`
	OldIdalamat           string                 `json:"old_idalamat"`
	OldAlamat             string                 `json:"old_alamat"`
	OldLat                float64                `json:"old_lat"`
	OldLng                float64                `json:"old_lng"`
	OldKm                 float64                `json:"old_km"`
	OldBiaya              float64                `json:"old_biaya"`
	NewBiaya              float64                `json:"new_biaya"`
	NewIdalamat           string                 `json:"new_idalamat"`
	NewAlamat             string                 `json:"new_alamat"`
	NewLat                float64                `json:"new_lat"`
	NewLng                float64                `json:"new_lng"`
	NewKm                 float64                `json:"new_km"`
	AddBiaya              float64                `json:"add_biaya"`
	AddKm                 float64                `json:"add_km"`
	Keterangan            string                 `json:"keterangan"`
	Lunas                 int                    `json:"lunas"`
	Idoutlet              string                 `json:"idoutlet"`
	Idowner               string                 `json:"idowner"`
	Idcustomer            string                 `json:"idcustomer"`
	Diskon                float64                `json:"diskon"`
	Pajak                 float64                `json:"pajak"`
	PersenDiskon          float64                `json:"persen_diskon"`
	PersenPajak           float64                `json:"persen_pajak"`
	Total                 float64                `json:"total"`
	Grantotal             float64                `json:"grantotal"`
	Tarifperkm            float64                `json:"tarifperkm"`
	OldWaktuAntar         *time.Time             `json:"old_waktu_antar"`
	NewWaktuAntar         *time.Time             `json:"new_waktu_antar"`
	CourierType           string                 `json:"courier_type"`
	ExpeditionName        string                 `json:"expedition_name"`
	TransportationName    string                 `json:"transportation_type"`
	CourierCode           string                 `json:"courier_code"`
	Idwaybill             string                 `json:"idwaybill"`
	IdkaryawanKurir       string                 `json:"idkaryawan_kurir"`
	VendorCourierType     string                 `json:"vendor_courier_type"`
	VendorCourierCompany  string                 `json:"vendor_courier_company"`
	VendorCourierId       string                 `json:"vendor_courier_id"`
	ShipmentRawData       string                 `json:"shipment_raw_data"`
	CourierDriverName     string                 `json:"courier_driver_name"`
	ShipmentRawDataObject map[string]interface{} `json:"shipment_raw_data_object" gorm:"-"`
	Karyawan              *Karyawan              `json:"karyawan" gorm:"foreignkey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	KaryawanKurir         *Karyawan              `json:"karyawan_kurir" gorm:"foreignkey:idkaryawan;association_foreignkey:idkaryawan_kurir;references:idkaryawan_kurir"`
	AntarExtraBayar       []AntarExtraBayar      `json:"antar_extra_bayar" gorm:"foreignkey:idantar_extra;association_foreignkey:id;references:id"`
	Customer              *Customer              `json:"customer" gorm:"foreignkey:idcustomer;association_foreignkey:idcustomer;references:idcustomer"`
	Alamat                *Alamat                `json:"alamat" gorm:"foreignkey:idalamat;association_foreignkey:new_idalamat;references:new_idalamat"`
	BaseModel
}
