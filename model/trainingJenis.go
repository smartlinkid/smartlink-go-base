package model

type TrainingJenis struct {
	Id         string  `json:"id" gorm:"primary_key"`
	Nama       string  `json:"nama"`
	Harga      float64 `json:"harga"`
	JumlahSesi int     `json:"jumlah_sesi"` // 1 atau 2
	Keterangan string  `json:"keterangan"`
	BaseModel
}
