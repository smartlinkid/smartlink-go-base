package model

import "github.com/graphql-go/graphql"

type KeuanganAkunAkun struct {
	Idakun               string                `json:"idakun" gorm:"primary_key"`
	Idkategori           string                `json:"idkategori"`
	Idowner              string                `json:"idowner"`
	NamaAkun             string                `json:"nama_akun"`
	Statis               int                   `json:"statis"`
	Keterangan           string                `json:"keterangan"`
	RelatedType          string                `json:"related_type"`
	RelatedId            string                `json:"related_id"`
	SaldoAkhir           float64               `json:"saldo_akhir"`
	Outlet               *Outlet               `json:"outlet" gorm:"foreignkey:idoutlet;association_foreignkey:related_id;references:related_id"`
	KeuanganAkunKategori *KeuanganAkunKategori `json:"keuangan_akun_kategori" gorm:"foreignkey:idkategori;association_foreignkey:idkategori;references:idkategori"`
	BaseModel
}

var KeuanganAkunAkunType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "keuangan_akun_akun",
		Fields: graphql.Fields{
			"idakun": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"idkategori": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"idowner": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"nama_akun": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"statis": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"keterangan": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"related_type": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"related_id": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"saldo_akhir": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
		},
		Description: "",
	})

var KeuanganAkunAkunType2 = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "keuangan_akun_akun2",
		Fields: graphql.Fields{
			"idakun": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"idkategori": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"idowner": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"nama_akun": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"statis": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"keterangan": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"related_type": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"related_id": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"saldo_akhir": &graphql.Field{
				Type:        graphql.Float,
				Description: "",
			},
		},
		Description: "",
	})
