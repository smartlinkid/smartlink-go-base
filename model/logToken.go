package model

import "time"

type LogToken struct {
	Idkaryawan string    `json:"idkaryawan"`
	Idaplikasi int       `json:"idaplikasi"`
	Email      string    `json:"email"`
	Token      string    `json:"token"`
	WaktuLogin int64     `json:"waktu_login"`
	Valid      int       `json:"valid"`
	Hapus      int       `json:"hapus"`
	CreatedAt  time.Time `json:"created_at"`
	UpdatedAt  time.Time `json:"updated_at"`
	FirstLogin int       `json:"first_login"`
	LastLogin  time.Time `json:"last_login"`
	LastLogout time.Time `json:"last_logout"`
	TempToken  string    `json:"temp_token"`
}
