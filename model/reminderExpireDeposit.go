package model

import "time"

type ReminderExpireDeposit struct {
	Id                      int                      `json:"id" gorm:"primary_key"`
	Idowner                 string                   `json:"idowner"`
	IdcountdownMaster       int                      `json:"idcountdown_master"`
	SendingTime             *string                  `json:"sending_time"`
	ReminderCountdownMaster *ReminderCountdownMaster `json:"reminder_countdown_master" gorm:"foreignkey:id;association_foreignkey:idcountdown_master;references:idcountdown_master"`
	CreatedAt               *time.Time               `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt               *time.Time               `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
