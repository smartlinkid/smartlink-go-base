package model

import "time"

type TrainingPembayaran struct {
	Id                 string            `json:"id" gorm:"primary_key"`
	TrainingId         string            `json:"training_id"`
	OwnerIdowner       string            `json:"owner_idowner"`
	KeuanganAkunIdakun string            `json:"keuangan_akun_idakun"`
	Subtotal           float64           `json:"subtotal"`
	Diskon             float64           `json:"diskon"`
	DiskonRupiah       float64           `json:"diskon_rupiah"`
	BiayaAdmin         float64           `json:"biaya_admin"`
	Total              float64           `json:"total"`
	JenisBayar         int               `json:"jenis_bayar"` // 1 Transfer Bank 2 QRIS
	KodePromo          string            `json:"kode_promo"`
	CouponId           int               `json:"coupon_id"`
	ExpiredAt          time.Time         `json:"expired_at"`
	PaylinkQrisId      string            `json:"paylink_qris_id"`
	PaylinkQris        PaylinkQris       `json:"paylink_qris" gorm:"foreignkey:PaylinkQrisId"`
	Training           *Training         `json:"training" gorm:"foreignkey:id;association_foreignkey:training_id;references:training_id"`
	Owner              *Owner            `json:"owner" gorm:"foreignkey:idowner;association_foreignkey:owner_idowner;references:owner_idowner"`
	KeuanganAkunAkun   *KeuanganAkunAkun `json:"keuangan_akun_akun" gorm:"foreignkey:idakun;association_foreignkey:keuangan_akun_idakun;references:keuangan_akun_idakun"`
	BaseModel
}
