package model

type RakKategori struct {
	Idkategori string `json:"idkategori"`
	Idoutlet   string `json:"idoutlet"`
	Nama       string `json:"nama"`
}
