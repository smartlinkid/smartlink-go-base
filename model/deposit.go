package model

type DataDeposit2 struct {
	IddataDeposit             string             `json:"iddata_deposit"`
	Nama                      string             `json:"nama"`
	MasaAktif                 int64              `json:"masa_aktif"`
	Jumlah                    float64            `json:"jumlah"`
	LayananIdlayanan          string             `json:"layanan_idlayanan"`
	Idowner                   string             `json:"idowner"`
	HargaDeposit              float64            `json:"harga_deposit"`
	HargaLayanan              float64            `json:"harga_layanan"`
	OutletIdoutlet            string             `json:"outlet_idoutlet"`
	JenisDeposit              int                `json:"jenis_deposit"`                // 1=limited, 2=unlimited
	TipePerpanjanganMasaAktif *int               `json:"tipe_perpanjangan_masa_aktif"` // 1=akumulasi(pulsa), 2= menggunakan terlama, 3= menggunakan terbaru
	TipeQuotaExpired          *int               `json:"tipe_quota_expired"`           // 1= hangus, 2= akumulasi
	Layanan                   *Layanan           `json:"layanan" gorm:"foreignkey:idlayanan;association_foreignkey:layanan_idlayanan;references:layanan_idlayanan"`
	DataDepositOutlet         *DataDepositOutlet `json:"data_deposit_outlet" gorm:"foreignkey:data_deposit_iddata_deposit;association_foreignkey:iddata_deposit;references:iddata_deposit"`
	BaseModel
}
