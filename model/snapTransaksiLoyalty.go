package model

import (
	"time"
)

type SnapTransaksiLoyalty struct {
	IdsnapTransaksi string     `json:"idsnap_transaksi"`
	Idowner         string     `json:"idowner"`
	Idoutlet        string     `json:"idoutlet"`
	CreatedAt       *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP"`
	UpdatedAt       *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
