package model

import "time"

type PembayaranUmum struct {
	Id                 int64              `json:"id" gorm:"primary_key;auto_increment"`
	Idtransaksi        string             `json:"idtransaksi" gorm:"index"`
	TipeBayar          int                `json:"tipe_bayar"`
	Idjenisbayar       int                `json:"idjenisbayar"`
	Bayar              float64            `json:"bayar"`
	Nominal            float64            `json:"nominal"`
	Kembalian          float64            `json:"kembalian"`
	Keterangan         string             `json:"keterangan"`
	Waktu              int64              `json:"waktu"`
	DNoresi            string             `json:"d_noresi"`
	DNokartu           string             `json:"d_nokartu"`
	DTanggalTransfer   int64              `json:"d_tanggal_transfer"`
	DBank              string             `json:"d_bank"`
	DIdakunbank        string             `json:"d_idakunbank" gorm:"index"`
	DIdowner           string             `json:"d_idowner" gorm:"index"`
	CreatedAt          *time.Time         `json:"created_at" gorm:"index"`
	UpdatedAt          *time.Time         `json:"updated_at"`
	Hash               *string            `json:"hash" gorm:"index"`
	Idkaryawan         string             `json:"idkaryawan" gorm:"index"`
	Idoutlet           string             `json:"idoutlet" gorm:"index"`
	Idowner            string             `json:"idowner" gorm:"index"`
	DateRefund         *time.Time         `json:"date_refund"`
	AkunRefund         string             `json:"akun_refund"`
	JenisPembayaran    *JenisPembayaran   `json:"jenis_pembayaran" gorm:"foreignkey:idjenis_pembayaran;association_foreignkey:idjenisbayar;references:idjenisbayar"`
	KeuanganAkunRefund *KeuanganAkunAkun  `json:"keuangan_akun_refund" gorm:"foreignkey:idakun;association_foreignkey:akun_refund;references:akun_refund"`
	PemakaianEmoney    *PemakaianEmoney   `json:"pemakaian_emoney" gorm:"foreignkey:idpembayaran;association_foreignkey:id;references:id"`
	PemakaianDeposit   []PemakaianDeposit `json:"pemakaian_deposit" gorm:"foreignkey:idtransaksi;association_foreignkey:idtransaksi;references:idtransaksi"`
	EdcPembayaran      *EdcPembayaran     `json:"edc_pembayaran" gorm:"foreignkey:ref_id;association_foreignkey:id;references:id"`
	Karyawan           *Karyawan          `json:"karyawan" gorm:"foreignkey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	TransaksiFoto      []TransaksiFoto    `json:"transaksi_foto" gorm:"foreignkey:idtransaksi;association_foreignkey:hash;references:hash"`
}
