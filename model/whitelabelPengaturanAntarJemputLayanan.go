package model

import "time"

type WhitelabelPengaturanAntarJemputLayanan struct {
	IdwhitelabelPengaturanAntarJemput string      `json:"idwhitelabel_pengaturan_antar_jemput"`
	Idowner                           string      `json:"idowner"`
	Idlayanan                         string      `json:"idlayanan"`
	CreatedAt                         *time.Time  `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	Data                              interface{} `json:"data" gorm:"-"`
}
