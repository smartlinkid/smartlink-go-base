package model

import "time"

type DashboardContent struct {
	Id        int        `json:"id" gorm:"primary_key;index;auto_increment"`
	Type      int        `json:"type"`
	Title     string     `json:"title"`
	Body      string     `json:"body"`
	Image     string     `json:"image"`
	Link      string     `json:"link"`
	StartTime *time.Time `json:"start_time"`
	EndTime   *time.Time `json:"end_time"`
	BaseModel
}
