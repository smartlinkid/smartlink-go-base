package model

import "time"

type TransaksiHapus struct {
	Id                   int            `json:"id"  gorm:"primary_key"`
	Idtransaksi          string         `json:"idtransaksi"`
	TanggalRequest       int            `json:"tanggal_request"`
	Keterangan           string         `json:"keterangan"`
	Status               int            `json:"status"`
	TanggalKonfirmasi    *time.Time     `json:"tanggal_konfirmasi"`
	KeteranganKonfirmasi string         `json:"keterangan_konfirmasi"`
	Idowner              string         `json:"idowner"`
	Idkaryawan           *string        `json:"idkaryawan"`
	Idoutlet             string         `json:"idoutlet"`
	KeteranganStatus     string         `json:"keterangan_status"`
	Outlet               *Outlet        `json:"outlet" gorm:"foreignkey:idoutlet;association_foreignkey:idoutlet;references:idoutlet"`
	Owner                *Owner         `json:"owner" gorm:"foreignkey:idowner;association_foreignkey:idowner;references:idowner"`
	Karyawan             *Karyawan      `json:"karyawan" gorm:"foreignkey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	Transaksi            *Transaksi     `json:"transaksi" gorm:"foreignkey:idtransaksi;association_foreignkey:idtransaksi;references:idtransaksi"`
	TransaksiFoto        *TransaksiFoto `json:"transaksi_foto" gorm:"foreignkey:idtransaksi;association_foreignkey:idtransaksi;references:idtransaksi"`
	CreatedAt            *time.Time     `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt            *time.Time     `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
