package model

import "time"

type NotaKirimWorkshop struct {
	IdsuratKirimWorkshop string              `json:"idsurat_kirim_workshop" gorm:"primary_key"`
	TransaksiIdtransaksi string              `json:"transaksi_idtransaksi"`
	Iddetlayanan         string              `json:"iddetlayanan"`
	Valid                int                 `json:"valid"`
	CreatedAt            *time.Time          `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt            *time.Time          `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
	SuratKirimWorkshop   *SuratKirimWorkshop `json:"surat_kirim_workshop" gorm:"foreignkey:idsurat_kirim_workshop;association_foreignkey:idsurat_kirim_workshop;references:idsurat_kirim_workshop"`
}
