package model

import "time"

type TrainingDetail struct {
	Id                    string    `json:"id" gorm:"primary_key"`
	TrainingId            string    `json:"training_id"`
	JadwalTrainingMulai   time.Time `json:"jadwal_training_mulai"`
	JadwalTrainingSelesai time.Time `json:"jadwal_training_selesai"`
	Sesi                  int       `json:"sesi"`
	LinkZoom              string    `json:"link_zoom"`
	LinkRecord            string    `json:"link_record"`
	Status                string    `json:"status"` // 10_Menunggu konfirmasi CS, 20_Terjadwalkan, 30_Dijadwalkan Ulang, 40_Dibatalkan oleh CS, 50_Selesai CS, 60_Menunggu Pembayaran, 70_Pembayaran Gagal
	Training              *Training `json:"training" gorm:"foreignkey:id;association_foreignkey:training_id;references:training_id"`
	BaseModel
}
