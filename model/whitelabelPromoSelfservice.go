package model

import "time"

type WhitelabelPromoSelfservice struct {
	BaseModel
	ID            string                                `json:"id"`
	Idowner       string                                `json:"idowner"`
	Name          string                                `json:"name"`
	Active        bool                                  `json:"active"`
	StartDate     *time.Time                            `json:"start_date"`
	EndDate       *time.Time                            `json:"end_date"`
	PromoLocation string                                `json:"promo_location"` // all, only, except
	Quota         int                                   `json:"quota"`
	QuotaUsed     int                                   `json:"quota_used"`
	UserPromo     string                                `json:"user_promo"` // all, customer, member
	MinTotal      float64                               `json:"min_total"`  // s&k min total for apply promo
	Deskripsi     string                                `json:"deskripsi"`
	Benefit       *WhitelabelPromoSelfserviceBenefit    `json:"benefit" gorm:"foreignkey:idpromo;association_foreignkey:id;references:id"`
	Locations     []*WhitelabelPromoSelfserviceLocation `json:"locations" gorm:"foreignkey:idpromo;association_foreignkey:id;references:id"`
}
