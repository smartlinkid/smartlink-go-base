package model

import "github.com/graphql-go/graphql"

type LayananSnap struct {
	Idlayanan      string           `json:"idlayanan" gorm:"primary_key;index"`
	Idtahapan      int              `json:"idtahapan" gorm:"primary_key;index"`
	Load           float64          `json:"load"`
	LoadToleransi  float64          `json:"load_toleransi"`
	Durasi         int64            `json:"durasi"`
	Hapus          *int             `json:"hapus"`
	CreatedAt      *string          `json:"created_at"`
	UpdatedAt      *string          `json:"updated_at"`
	Idowner        string           `json:"idowner"`
	Idoutlet       string           `json:"idoutlet"`
	LayananSnapTag []LayananSnapTag `json:"layanan_snap_tag" gorm:"foreignkey:idlayanan;association_foreignkey:idlayanan;references:idlayanan"`
}

var LayananSnapType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "layanan_snap_type",
		Fields: graphql.Fields{
			"idlayanan": &graphql.Field{
				Type:        graphql.String,
				Description: "idlayanan",
			},
			"idtahapan": &graphql.Field{
				Type:        graphql.Int,
				Description: "idtahapan",
			},
			"load": &graphql.Field{
				Type:        graphql.Float,
				Description: "load",
			},
			"load_toleransi": &graphql.Field{
				Type:        graphql.Float,
				Description: "load_toleransi",
			},
			"durasi": &graphql.Field{
				Type:        graphql.String,
				Description: "durasi",
			},
			"hapus": &graphql.Field{
				Type:        graphql.Int,
				Description: "hapus",
			},
			"created_at": &graphql.Field{
				Type:        graphql.String,
				Description: "created_at",
			},
			"updated_at": &graphql.Field{
				Type:        graphql.String,
				Description: "updated_at",
			},
			"idowner": &graphql.Field{
				Type:        graphql.String,
				Description: "idowner",
			},
			"idoutlet": &graphql.Field{
				Type:        graphql.String,
				Description: "idoutlet",
			},
		},
	})

var LayananSnapInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "layanan_snap_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"idlayanan": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "idlayanan",
			},
			"idtahapan": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "idtahapan",
			},
			"load": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "load",
			},
			"load_toleransi": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "load_toleransi",
			},
			"durasi": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "durasi",
			},
			"hapus": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "hapus",
			},
			"created_at": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "created_at",
			},
			"updated_at": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "updated_at",
			},
			"idowner": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "idowner",
			},
			"idoutlet": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "idoutlet",
			},
		},
	})
