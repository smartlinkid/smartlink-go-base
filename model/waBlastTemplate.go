package model

import "time"

type WaBlastTemplate struct {
	Id        int64      `json:"id" gorm:"primary_key;auto_increment"`
	Idowner   string     `json:"idowner" gorm:"index"`
	Nama      string     `json:"nama"`
	Template  string     `json:"template"`
	CreatedAt *time.Time `json:"created_at"`
	UpdatedAt *time.Time `json:"updated_at"`
	Hapus     int        `json:"hapus"`
	Jenis     string     `json:"jenis"`
}
