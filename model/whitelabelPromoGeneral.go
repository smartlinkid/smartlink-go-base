package model

import "time"

type WhitelabelPromoGeneral struct {
	Idowner        string     `json:"idowner"`
	Idoutlet       string     `json:"idoutlet"`
	IdpromoGeneral string     `json:"idpromo_general"`
	Description    string     `json:"description"`
	CreatedAt      *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt      *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
