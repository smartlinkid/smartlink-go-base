package model

import "time"

type KeuanganAkunHide struct {
	Id         int       `json:"id" gorm:"primary_key;auto_increment"`
	Idowner    string    `json:"idowner"`
	Idakun     string    `json:"idakun"`
	Idkategori string    `json:"idkategori"`
	CreatedAt  time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP"`
	UpdatedAt  time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
