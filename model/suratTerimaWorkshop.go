package model

type SuratTerimaWorkshop struct {
	IdsuratTerimaWorkshop string    `json:"idsurat_terima_workshop" gorm:"primary_key"`
	TanggalKirim          int64     `json:"tanggal_kirim"`
	TanggalTerima         int64     `json:"tanggal_terima"`
	Keterangan            string    `json:"keterangan"`
	WorkshopIdworkshop    string    `json:"workshop_idworkshop"`
	OutletIdoutlet        string    `json:"outlet_idoutlet"`
	IdkaryawanKurir       string    `json:"idkaryawan_kurir"`
	IdkaryawanWorkshop    string    `json:"idkaryawan_workshop"`
	IdkaryawanKasir       string    `json:"idkaryawan_kasir"`
	Status                int       `json:"status"`
	Valid                 int       `json:"valid"`
	Validasi              int       `json:"validasi"`
	Outlet                *Outlet   `json:"outlet" gorm:"foreignkey:idoutlet;association_foreignkey:outlet_idoutlet;references:outlet_idoutlet"`
	KaryawanKurir         *Karyawan `json:"karyawan_kurir" gorm:"foreignkey:idkaryawan;association_foreignkey:idkaryawan_kurir;references:idkaryawan_kurir"`
	BaseModel
}
