package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type PaylinkBalanceTransfers struct {
	Id            int64      `json:"id" gorm:"primary_key;index;auto_increment"`
	Type          int        `json:"type"`
	Sender        string     `json:"sender" gorm:"index"`
	SenderType    int        `json:"sender_type"`
	Recipient     string     `json:"recipient" gorm:"index"`
	RecipientType int        `json:"recipent_type" `
	Nominal       float64    `json:"nominal"`
	Idoutlet      string     `json:"idoutlet" gorm:"index"`
	CreatedAt     *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt     *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	Hapus         *int       `json:"hapus" gorm:"default:0"`
}

var PaylinkBalanceTransfersObject = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "paylink_balance_transfer",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.Int,
				Description: "user type",
			},
			"Type": &graphql.Field{
				Type:        graphql.String,
				Description: "1=topup customer & owner,2=pembayaran transaksi",
			},
			"sender": &graphql.Field{
				Type:        graphql.String,
				Description: "sender",
			},
			"sender_type": &graphql.Field{
				Type:        graphql.Int,
				Description: "1=owner, 2=customer",
			},
			"recipient": &graphql.Field{
				Type:        graphql.String,
				Description: "recipient",
			},
			"recipient_type": &graphql.Field{
				Type:        graphql.Int,
				Description: "1=owner, 2=customer",
			},
			"nominal": &graphql.Field{
				Type:        graphql.Float,
				Description: "nominal",
			},
			"idoutlet": &graphql.Field{
				Type:        graphql.String,
				Description: "idoutlet",
			},
		},
	})
