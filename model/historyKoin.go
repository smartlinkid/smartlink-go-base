package model

import "time"

type HistoryKoin struct {
	Id         int64      `json:"id" gorm:"primary_key;auto_increment;index"`
	Idowner    string     `json:"idowner" gorm:"index"`
	Jenis      int        `json:"jenis"`
	Masuk      int        `json:"masuk"`
	Keluar     int        `json:"keluar"`
	Saldo      int        `json:"saldo"`
	RefType    *string    `json:"ref_type" gorm:"index"`
	RefId      *string    `json:"ref_id" gorm:"index"`
	Keterangan string     `json:"keterangan"`
	CreatedAt  *time.Time `json:"created_at"`
	UpdatedAt  *time.Time `json:"updated_at"`
}
