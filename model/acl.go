package model

type (
	AclAuthority struct {
		Idkaryawan    string `json:"idkaryawan"`
		AuthorityTipe string `json:"authority_tipe"`
		AuthorityId   string `json:"authority_id"`
	}

	AclBackend struct {
		Id       string `json:"id"`
		IdAcl    string `json:"id_acl"`
		Endpoint string `json:"endpoint"`
	}

	AclDefinition struct {
		IdaclRoles  string `json:"idacl_roles"`
		IdaclMaster string `json:"idacl_master"`
	}

	AclMaster struct {
		Id     string `json:"id"`
		Nama   string `json:"nama"`
		Jenis  string `json:"jenis"`
		Parent string `json:"parent"`
	}

	AclRoles struct {
		Id        string `json:"id"`
		Tipe      string `json:"tipe"` //'owner, karyawan'
		Nama      string `json:"nama"`
		Idowner   string `json:"idowner"`
		Deskripsi string `json:"deskripsi"`
	}

	AclUser struct {
		Idkaryawan string `json:"idkaryawan"`
		IdaclRoles string `json:"idacl_roles"`
		Authority  int    `json:"authority"`
	}
)
