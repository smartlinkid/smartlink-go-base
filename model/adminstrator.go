package model

import "github.com/graphql-go/graphql"

type Administrator struct {
	Id            int    `json:"id" gorm:"primary_key"`
	Nama          string `json:"nama"`
	Email         string `json:"email"`
	Password      string `json:"-"`
	Telp          string `json:"telp"`
	RememberToken string `json:"remember_token"`
	Level         int    `json:"level"`
	BaseModel
}

var AdministratorType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "administrator",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.Int,
				Description: "id ",
			},
			"nama": &graphql.Field{
				Type:        graphql.String,
				Description: "nama ",
			},
			"email": &graphql.Field{
				Type:        graphql.String,
				Description: "email ",
			},
			"telp": &graphql.Field{
				Type:        graphql.String,
				Description: "nomor handphone owner",
			},
			"level": &graphql.Field{
				Type:        graphql.Int,
				Description: "level ",
			},

			//"base": &graphql.Field{
			//	Type:        BaseModelType,
			//	Description: "base model",
			//},
		},
		Description: "Data Administrator",
	})
