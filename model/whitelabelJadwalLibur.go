package model

import "time"

type WhitelabelJadwalLibur struct {
	ID                          string                         `json:"id"`
	Idowner                     string                         `json:"idowner"`
	Title                       string                         `json:"title"`
	StartDate                   *time.Time                     `json:"start_date"`
	EndDate                     *time.Time                     `json:"end_date"`
	CreatedAt                   *time.Time                     `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt                   *time.Time                     `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
	WhitelabelJadwalLiburOutlet []*WhitelabelJadwalLiburOutlet `json:"whitelabel_jadwal_libur_outlet" gorm:"foreignkey:idwhitelabel_jadwal_libur;association_foreignkey:id;references:idwhitelabel_jadwal_libur"`
}
