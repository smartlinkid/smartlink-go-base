package model

type RekeningOwner struct {
	Id               int               `json:"id" gorm:"primary_key"`
	IdakunKeuangan   string            `json:"idakun_keuangan"`
	Idowner          string            `json:"idowner"`
	Nama             string            `json:"nama"`
	NomorRekening    string            `json:"nomor_rekening"`
	PemilikRekening  string            `json:"pemilik_rekening"`
	Keterangan       string            `json:"keterangan"`
	KeuanganAkunAkun *KeuanganAkunAkun `json:"keuangan_akun_akun" gorm:"foreignKey:idakun;association_foreignkey:idakun_keuangan;references:idakun_keuangan"`
	BaseModel
}
