package model

type Customer struct {
	Idcustomer       string          `json:"idcustomer" gorm:"primary_key"`
	Nama             string          `json:"nama"`
	OwnerIdowner     string          `json:"owner_idowner"`
	Telp             string          `json:"telp"`
	JenisKelamin     *int            `json:"jenis_kelamin"`
	TanggalLahir     int64           `json:"tanggal_lahir"`
	Instansi         string          `json:"instansi"`
	Email            string          `json:"email"`
	Idoutlet         string          `json:"idoutlet"`
	CountryCode      string          `json:"country_code" gorm:"index"`
	Phone            string          `json:"phone" gorm:"index"`
	EmptyPhone       int             `json:"empty_phone"`
	Agama            *int            `json:"agama"`
	Sapaan           int             `json:"sapaan"`
	IdcustomerGlobal string          `json:"idcustomer_global"`
	IsLoyalty        int             `json:"is_loyalty"` // whitelabel
	RememberToken    string          `json:"remember_token"`
	Transaksi        []Transaksi     `json:"transaksi" gorm:"foreignkey:customer_idcustomer;association_foreignkey:idcustomer;references:idcustomer"`
	Alamat           []Alamat        `json:"alamat" gorm:"foreignkey:customer_idcustomer;association_foreignkey:idcustomer;references:idcustomer"`
	ResumeEmoney     *ResumeEmoney   `json:"quota_emoney" gorm:"foreignkey:customer_idcustomer;association_foreignkey:idcustomer;references:idcustomer"`
	ResumeDeposit    []ResumeDeposit `json:"quota_deposit" gorm:"foreignkey:customer_idcustomer;association_foreignkey:idcustomer;references:idcustomer"`
	Outlet           *Outlet         `json:"outlet" gorm:"foreignkey:idoutlet;association_foreignkey:idoutlet;references:idoutlet"`
	DataSapaan       *Sapaan         `json:"data_sapaan" gorm:"foreignkey:id;association_foreignkey:sapaan;references:sapaan"`
	DataAgama        *Agama          `json:"data_agama" gorm:"foreignkey:id;association_foreignkey:agama;references:agama"`
	Owner            Owner           `json:"owner" gorm:"foreignkey:idowner;association_foreignkey:owner_idowner;references:owner_idowner"`
	TransaksiFoto    *TransaksiFoto  `json:"transaksi_foto" gorm:"foreignkey:idtransaksi;association_foreignkey:idcustomer;references:idcustomer"`
	BaseModel
}
