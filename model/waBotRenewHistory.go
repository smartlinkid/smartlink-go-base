package model

import "time"

type WaBotRenewHistory struct {
	Id               string            `json:"id"`
	Idowner          string            `json:"idowner"`
	IdwaDevice       string            `json:"idwa_device"`
	Biaya            int               `json:"biaya"`
	Status           int               `json:"status"` // 1 gagal 2 sukses
	Message          string            `json:"message"`
	CreatedAt        *time.Time        `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt        *time.Time        `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	WaBotPromoHarian *WaBotPromoHarian `json:"wa_bot_promo_harian"`
}
