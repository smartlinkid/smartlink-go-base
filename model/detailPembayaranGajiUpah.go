package model

import "time"

type DetailPembayaranGajiUpah struct {
	Id               int        `json:"id" gorm:"primary_key;index;not null;auto_increment"`
	IdpembayaranGaji string     `json:"idpembayaran_gaji"`
	TahapanIdtahapan int        `json:"tahapan_idtahapan"`
	TotalUpah        float64    `json:"total_upah"`
	Tahapan          *Tahapan   `json:"tahapan" gorm:"index;foreignKey:idtahapan;association_foreignkey:tahapan_idtahapan;references:tahapan_idtahapan"`
	CreatedAt        *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt        *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
