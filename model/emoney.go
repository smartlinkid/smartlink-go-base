package model

type DataEmoney struct {
	IddataEmoney     string               `json:"iddata_emoney" gorm:"primary_key"`
	Nama             string               `json:"nama"`
	Saldo            float64              `json:"saldo"`
	MasaAktif        int                  `json:"masa_aktif"`
	Idowner          string               `json:"idowner"`
	OutletIdoutlet   string               `json:"outlet_idoutlet"`
	DataEmoneyOutlet *DataEmoneyOutlet    `json:"data_emoney_outlet" gorm:"foreignkey:data_emoney_iddata_emoney;association_foreignkey:iddata_emoney;references:iddata_emoney"`
	PengaturanUmum   *OwnerPengaturanUmum `json:"pengaturan_umum" gorm:"foreignkey:idowner;association_foreignkey:idowner;references:idowner"`
	BaseModel
}
