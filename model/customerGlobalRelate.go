package model

import "github.com/graphql-go/graphql"

type CustomerGlobalRelate struct {
	IdcustomerGlobal string `json:"idcustomer_global" gorm:"index;primary_key"`
	Idowner          string `json:"idowner" gorm:"index;primary_key"`
	Idoutlet         string `json:"idoutlet" gorm:"index;primary_key"`
	Owner            Owner  `json:"owner" gorm:"index;foreignKey:idowner;association_foreignkey:idowner;references:idowner"`
}

var CustomerGlobalRelateType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "customer_global_relate",
		Fields: graphql.Fields{
			"idcustomer_global": &graphql.Field{
				Type:        graphql.String,
				Description: "id custotomer global",
			},
			"idowner": &graphql.Field{
				Type:        graphql.String,
				Description: "id owner",
			},
			"idoutlet": &graphql.Field{
				Type:        graphql.String,
				Description: "id outlet",
			},
			"owner": &graphql.Field{
				Type:        OwnerType,
				Description: "owner",
			},
		},
	})
