package model

import "time"

type OwnerPremium struct {
	Id             string           `json:"id" gorm:"primary_key"`
	Idowner        string           `json:"idowner" gorm:"index"`
	NamaLengkap    string           `json:"nama_lengkap"`
	Telp           string           `json:"telp"`
	Ktp            string           `json:"ktp"`
	Npwp           string           `json:"npwp"`
	Alamat         string           `json:"alamat"`
	FotoKtp        string           `json:"foto_ktp"`
	FotoSelfieKtp  string           `json:"foto_selfie_ktp"`
	FotoNpwp       string           `json:"foto_npwp"`
	FotoTtd        string           `json:"foto_ttd"`
	JenisKelamin   int              `json:"jenis_kelamin"`
	TempatLahir    string           `json:"tempat_lahir"`
	TanggalLahir   string           `json:"tanggal_lahir"`
	NamaIbuKandung string           `json:"nama_ibu_kandung"`
	Approved       int              `json:"approved"`
	ApprovedBy     int              `json:"approved_by"`
	Reason         string           `json:"reason"`
	CreatedAt      *time.Time       `json:"created_at"`
	UpdatedAt      *time.Time       `json:"updated_at"`
	Hapus          int              `json:"hapus"`
	Owner          *Owner           `json:"owner" gorm:"foreignKey:idowner;association_foreignkey:idowner;references:idowner"`
	OwnerTabungan  *[]OwnerTabungan `json:"owner_tabungan" gorm:"foreignKey:idowner_premium;association_foreignkey:id;references:id"`
	Administrator  *Administrator   `json:"administrator" gorm:"foreignKey:approved_by;association_foreignkey:id;references:id"`
}
