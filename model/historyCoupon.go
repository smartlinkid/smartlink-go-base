package model

import "time"

type HistoryCoupon struct {
	Id           int64      `json:"id"`
	CouponId     int64      `json:"coupon_id"`
	Idowner      string     `json:"idowner"`
	RelatedType  string     `json:"related_type"`
	Nominal      float64    `json:"nominal"`
	DiskonPersen float64    `json:"diskon_persen"`
	RelatedId    string     `json:"related_id"`
	CreatedAt    *time.Time `json:"created_at"`
	UpdatedAt    *time.Time `json:"updated_at"`
}
