package model

import "time"

type WhitelabelPromoMember struct {
	Idowner       string     `json:"idowner"`
	Idoutlet      string     `json:"idoutlet"`
	IdpromoMember string     `json:"idpromo_member"`
	Description   string     `json:"description"`
	CreatedAt     *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt     *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
