package model

import "time"

type WaBlastUsedTemplate struct {
	IdwaBlastTemplate int64           `json:"idwa_blast_template" gorm:"primarykey"`
	IdwaBlast         int64           `json:"idwa_blast" gorm:"index"`
	Idowner           string          `json:"idowner" gorm:"index"`
	LastUsed          *time.Time      `json:"last_used"`
	CreatedAt         *time.Time      `json:"created_at"`
	UpdatedAt         *time.Time      `json:"updated_at"`
	Hapus             int             `json:"hapus"`
	WaBlastTemplate   WaBlastTemplate `json:"wa_blast_template" gorm:"foreignkey:idwa_blast_template;association:id"`
}
