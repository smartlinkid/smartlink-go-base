package model

type TrainingUlasan struct {
	Id              string    `json:"id" gorm:"primary_key"`
	NilaiInformatif int       `json:"nilai_informatif"`
	NilaiKesesuaian int       `json:"nilai_kesesuaian"`
	Solusi          string    `json:"solusi"`
	KritikSaran     string    `json:"kritik_saran"`
	TrainingId      string    `json:"training_id"`
	OwnerIdowner    string    `json:"owner_idowner"`
	Training        *Training `json:"training" gorm:"foreignkey:id;association_foreignkey:training_id;references:training_id"`
	Owner           *Owner    `json:"owner" gorm:"foreignkey:idowner;association_foreignkey:owner_idowner;references:owner_idowner"`
	BaseModel
}
