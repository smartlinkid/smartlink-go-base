package model

type WaAttachment struct {
	Id       string `json:"id" gorm:"primary_key;"`
	IdwaData int    `json:"idwa_data"`
	Mime     string `json:"mime"`
	Url      string `json:"url"`
	Filename string `json:"filename"`
}
