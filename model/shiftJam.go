package model

import "time"

type ShiftJam struct {
	Id              string    `json:"id" gorm:"primary_key"`
	Nama            string    `json:"nama"`
	MasukShift      time.Time `json:"masuk_shift"`
	KeluarShift     time.Time `json:"keluar_shift"`
	ToleransiMasuk  int       `json:"toleransi_masuk"`
	ToleransiKeluar int       `json:"toleransi_keluar"`
	Lokasi          string    `json:"lokasi"`
	Idlokasi        string    `json:"idlokasi"`
	Hari            int       `json:"hari"` // 1 minggu, 2 senin, 3 selasa, 4 rabu, 5 kamis, 6 jumat, 7 sabtu
}
