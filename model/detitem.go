package model

import "time"

type Detitem struct {
	ItemIditem           string     `json:"item_iditem" gorm:"index"`
	LayananIdlayanan     string     `json:"layanan_idlayanan" gorm:"index"`
	JumlahItem           int        `json:"jumlah_item"`
	TransaksiIdtransaksi string     `json:"transaksi_idtransaksi"`
	Id                   string     `json:"id" gorm:"primary_key;index"`
	Iddetlayanan         string     `json:"iddetlayanan" gorm:"index"`
	CreatedAt            *time.Time `json:"created_at"`
	UpdatedAt            *time.Time `json:"updated_at"`
	Hapus                *int       `json:"hapus"`
	Item                 *Item      `json:"item" gorm:"foreignkey:iditem;association_foreignkey:item_iditem;references:item_iditem"`
}
