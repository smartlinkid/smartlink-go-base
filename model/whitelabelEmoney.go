package model

import "time"

type WhitelabelEmoney struct {
	Idowner   string     `json:"idowner"`
	Idoutlet  string     `json:"idoutlet"`
	Idemoney  string     `json:"idemoney"`
	CreatedAt *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
}
