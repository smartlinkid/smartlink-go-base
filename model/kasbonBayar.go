package model

import "time"

type KasbonBayar struct {
	Id               int        `json:"id" gorm:"primary_key;index;not null;auto_increment"`
	Idkasbon         int        `json:"idkasbon"`
	Idkaryawan       string     `json:"idkaryawan"`
	IdpembayaranGaji string     `json:"idpembayaran_gaji"`
	Idowner          string     `json:"idowner"`
	Nominal          float64    `json:"nominal"`
	SisaSaldo        float64    `json:"sisa_saldo"`
	WaktuPembayaran  *time.Time `json:"waktu_pembayaran"`
	Kasbon           *Kasbon    `json:"potongan" gorm:"index;foreignKey:id;association_foreignkey:idkasbon;references:idkasbon"`
	BaseModel
}
