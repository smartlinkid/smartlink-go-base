package model

import "time"

type OwnerChangeEmail struct {
	Id                int64          `json:"id" grom:"primary_key;auto_increment"`
	Idowner           string         `json:"idowner" gorm:"index"`
	FromEmail         string         `json:"from_email" gorm:"index"`
	ToEmail           string         `json:"to_email" gorm:"index"`
	PindahKepemilikan int            `json:"pindah_kepemilikan"`
	Ktp               string         `json:"ktp"`
	BukuTabungan      string         `json:"buku_tabungan"`
	Apporved          int            `json:"apporved"`
	ApprovedBy        int            `json:"approved_by"`
	Reason            string         `json:"reason"`
	ScheduledAt       *time.Time     `json:"scheduled_at" gorm:"index"`
	CreatedAt         *time.Time     `json:"created_at" gorm:"index"`
	UpdatedAt         *time.Time     `json:"updated_at"`
	Hapus             int            `json:"hapus"`
	Owner             *Owner         `json:"owner" gorm:"foreignKey:idowner;association_foreignkey:idowner;references:idowner"`
	Administrator     *Administrator `json:"administrator" gorm:"foreignKey:approved_by;association_foreignkey:id;references:id"`
}
