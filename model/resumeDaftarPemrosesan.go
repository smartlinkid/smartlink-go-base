package model

import "time"

type ResumeDaftarPemrosesan struct {
	TransaksiIdtransaksi string             `json:"transaksi_idtransaksi" gorm:"primary_key"`
	KaryawanIdkaryawan   string             `json:"karyawan_idkaryawan"`
	TahapanIdtahapan     int                `json:"tahapan_idtahapan"`
	Iddetlayanan         string             `json:"iddetlayanan" gorm:"primary_key"`
	Waktu                int                `json:"waktu"`
	CreatedAt            *time.Time         `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt            *time.Time         `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
	DaftarPemrosesan     []DaftarPemrosesan `json:"daftar_pemrosesan" gorm:"foreignkey:iddetlayanan;association_foreignkey:iddetlayanan;references:iddetlayanan"`
	Detlayanan           *Detlayanan        `json:"detlayanan" gorm:"foreignkey:id;association_foreignkey:iddetlayanan;references:iddetlayanan"`
}
