package model

import "time"

type OwnerToken struct {
	Id            int        `json:"id"`
	Idkaryawan    string     `json:"idkaryawan"`
	Idowner       string     `json:"idowner"`
	TokenJwt      string     `json:"token_jwt"`
	TokenFirebase string     `json:"token_firebase"`
	LastLogin     *time.Time `json:"last_login"`
	IpNetwork     string     `json:"ip_network"`
	UserAgent     string     `json:"user_agent"`
}
