package model

import "time"

type AfiliasiOwner struct {
	Id        string     `json:"id" gorm:"index;primary_key;not null"`
	Idowner   string     `json:"idowner" gorm:"index"`
	Saldo     int64      `json:"saldo"`
	CreatedAt *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	Owner     Owner      `json:"owner" gorm:"foreignKey:idowner;association_foreignkey:idowner;references:idowner"`
}
