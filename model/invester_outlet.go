package model

type InvestorOutlet struct {
	Id                int     `json:"id" gorm:"primary_key;index;auto_increment"`
	Idinvestor        string  `json:"idinvestor" gorm:"index"`
	Idoutlet          string  `json:"idoutlet" gorm:"index"`
	Idowner           string  `json:"idowner" gorm:"index"`
	PersentaseDeviden float64 `json:"persentase_deviden"`
	FileKontrak       string  `json:"file_kontrak"`
	BaseModel
}
