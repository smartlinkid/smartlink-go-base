package model

import "time"

type WaHistory struct {
	Id         int64      `json:"id" gorm:"primary_key;auto_increment"`
	Masuk      int        `json:"masuk"`
	Keluar     int        `json:"keluar"`
	CreatedAt  *time.Time `json:"created_at"`
	UpdatedAt  *time.Time `json:"updated_at"`
	Kuota      int        `json:"kuota"`
	Keterangan string     `json:"keterangan"`
	Iddevice   string     `json:"iddevice" gorm:"index"`
	Idowner    string     `json:"idowner" gorm:"index"`
	RefId      string     `json:"ref_id" grom:"index"`
	RefType    string     `json:"ref_type" gorm:"index"`
	Jenis      int        `json:"jenis"`
}
