package model

type Resgen struct {
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}
type Resgen2 struct {
	Msg    string      `json:"msg"`
	Reason interface{} `json:"reason"`
	Data   interface{} `json:"data"`
}
type AndroidResult struct {
	Msg    string `json:"msg"`
	Reason string `json:"reason"`
}
type OtpResult struct {
	Code   int         `json:"code"`
	Reason string      `json:"reason"`
	Msg    string      `json:"msg"`
	Data   interface{} `json:"data"`
}
type AuthResult struct {
	Code   int         `json:"code"`
	Reason string      `json:"reason"`
	Msg    string      `json:"msg"`
	Data   interface{} `json:"data"`
}
type DeployJobConfiguration struct {
	JobName  string `json:"job_name"`
	Image    string `json:"image"`
	Type     string `json:"type"`
	Id       string `json:"id"`
	Idowner  string `json:"idowner"`
	Argument string `json:"argument"`
}
