package model

import "time"

type OwnerChangeTelp struct {
	Id                int64          `json:"id" gorm:"primary_key;auto_increment"`
	Idowner           string         `json:"idowner"`
	Idsuperowner      string         `json:"idsuperowner"`
	FromTelp          string         `json:"from_telp" gorm:"index"`
	ToTelp            string         `json:"to_telp" gorm:"index"`
	PindahKepemilikan int            `json:"pindah_kepemilikan"`
	NomorAktif        int            `json:"nomor_aktif"`
	Ktp               string         `json:"ktp"`
	BukuTabungan      string         `json:"buku_tabungan"`
	Approved          int            `json:"approved"`
	ApprovedBy        int            `json:"approved_by"`
	Reason            string         `json:"reason"`
	ScheduledAt       *time.Time     `json:"scheduled_at" gorm:"index"`
	CreatedAt         *time.Time     `json:"created_at" gorm:"index"`
	UpdatedAt         *time.Time     `json:"updated_at"`
	Hapus             int            `json:"hapus"`
	Owner             *Owner         `json:"owner" gorm:"foreignKey:idowner;association_foreignkey:idowner;references:idowner"`
	Superowner        *Superowner    `json:"superowner" gorm:"foreignKey:idsuperowner;association_foreignkey:id;references:id"`
	Administrator     *Administrator `json:"administrator" gorm:"foreignKey:approved_by;association_foreignkey:id;references:id"`
}
