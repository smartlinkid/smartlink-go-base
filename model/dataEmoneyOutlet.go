package model

type DataEmoneyOutlet struct {
	DataEmoneyIddataEmoney string     `json:"data_emoney_iddata_emoney" gorm:"primary_key"`
	OutletIdoutlet         string     `json:"outlet_idoutlet"`
	HargaEmoney            float64    `json:"harga_emoney"`
	DataEmoney             DataEmoney `json:"emoney" gorm:"foreignkey:iddata_emoney;association_foreignkey:data_emoney_iddata_emoney;references:data_emoney_iddata_emoney"`
	Outlet                 *Outlet    `json:"outlet" gorm:"foreignkey:idoutlet;association_foreignkey:outlet_idoutlet;references:outlet_idoutlet"`
	BaseModel
}
