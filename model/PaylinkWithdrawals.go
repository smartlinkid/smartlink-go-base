package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type PaylinkWithdrawals struct {
	Id              string        `json:"id" gorm:"primary_key;"`
	TabunganId      string        `json:"tabungan_id"`
	IdakunBank      string        `json:"idakun_bank"`
	Idoutlet        string        `json:"idoutlet"`
	Amount          float64       `json:"amount"`
	Fee             float64       `json:"fee"`
	Total           float64       `json:"total"`
	Status          int           `json:"status"`
	Reason          string        `json:"reason"`
	AdministratorId int           `json:"administrator_id"`
	UserId          string        `json:"user_id" gorm:"index"`
	UserType        int           `json:"user_type"`
	LastBalance     float64       `json:"last_balance"`
	CreatedAt       *time.Time    `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt       *time.Time    `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	Hapus           *int          `json:"hapus" gorm:"default:0"`
	Owner           Owner         `json:"owner" gorm:"index;foreignKey:user_id;association_foreignkey:idowner;references:idowner"`
	Administrator   Administrator `json:"administrator" gorm:"index;foreignKey:administrator_id;association_foreignkey:id;references:id"`
	Tabungan        OwnerTabungan `json:"tabungan" gorm:"index;foreignKey:tabungan_id;association_foreignkey:id;references:id"`
	Service          string        `json:"service"`
	AccountNumber          string        `json:"account_number"`
	AccountName          string        `json:"account_name"`
	BankCode          string        `json:"bank_code"`
	CallbackUrl          string        `json:"callback_url"`
}

var PaylinkWithdrawalsObject = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "paylink_withdrawal",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "id ",
			},
			"tabungan_id": &graphql.Field{
				Type:        graphql.String,
				Description: "tabungan_id",
			},
			"idoutlet": &graphql.Field{
				Type:        graphql.String,
				Description: "idoutlet",
			},
			"idakun_bank": &graphql.Field{
				Type:        graphql.String,
				Description: "idakun_bank",
			},
			"user_id": &graphql.Field{
				Type:        graphql.String,
				Description: "user id",
			},
			"user_type": &graphql.Field{
				Type:        graphql.Int,
				Description: "user type : 1=owner, 2 = customer",
			},
			"amount": &graphql.Field{
				Type:        graphql.Float,
				Description: "Amount",
			},
			"fee": &graphql.Field{
				Type:        graphql.Float,
				Description: "fee",
			},
			"total": &graphql.Field{
				Type:        graphql.Float,
				Description: "total",
			},
			"last_balance": &graphql.Field{
				Type:        graphql.Float,
				Description: "last_balance",
			},
			"status": &graphql.Field{
				Type:        graphql.Int,
				Description: "status (0=menunggu 1= onprogress, 2=success, 3=failed)",
			},
			"reason": &graphql.Field{
				Type:        graphql.String,
				Description: "reason",
			},
			"administrator_id": &graphql.Field{
				Type:        graphql.Int,
				Description: "administrator_id",
			},
			"administrator": &graphql.Field{
				Type:        AdministratorType,
				Description: "administrator",
			},
			"owner": &graphql.Field{
				Type:        OwnerType,
				Description: "owner",
			},
			"tabungan": &graphql.Field{
				Type:        OwnerTabunganObject,
				Description: "owner",
			},
			"created_at": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "CreatedAt",
			},
		},
	})

var PaylinkWithdrawalsInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "paylink_withdrawal_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "ID must be unique generated (contoh: PYW20072416080400002)",
			},
			"amount": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "Amount (Required ) ",
			},
			"tabungan_id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "tabungan_id",
			},
			"idakun_bank": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "idakun_bank",
			},
			"idoutlet": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "idoutlet",
			},
			"user_id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "user id",
			},
			"user_type": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "user type : 1=owner, 2 = customer",
			},
		},
	})

var PaylinkWithdrawalPaymentInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "paylink_withdrawal_payment_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "withdrawal id",
			},
			"password_withdrawal": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "password_withdrawal",
			},
			"action": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "2=accept, 3=rejected",
			},
			"reason": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "Reason",
			},
			"administrator_id": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "id admin",
			},
		},
	})
