package model

import "time"

type MasalCronHistory struct {
	Id               string     `json:"id"`
	Idcustomer       string     `json:"idcustomer"`
	IdmasalTransaksi string     `json:"idmasal_transaksi"`
	Status           string     `json:"status"`
	StatusInfo       string     `json:"status_info"`
	CreatedAt        *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt        *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
