package model

import "time"

type UpahTahapanLayanan struct {
	LayananIdlayanan string     `json:"layanan_idlayanan"`
	TahapanIdtahapan int        `json:"tahapan_idtahapan"`
	Upah             float64    `json:"upah"`
	Idowner          string     `json:"idowner"`
	Tahapan          *Tahapan   `json:"tags" gorm:"foreignkey:idtahapan;association_foreignkey:tahapan_idtahapan;references:tahapan_idtahapan"`
	CreatedAt        *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP"`
	UpdatedAt        *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
