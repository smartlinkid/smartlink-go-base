package model

import "time"

type Notifications struct {
	Id             string     `json:"id" gorm:"primary_key;index"`
	Type           string     `json:"type"`
	NotifiableId   string     `json:"notifiable_id"`
	NotifiableType string     `json:"notifiable_type"`
	Data           string     `json:"data"`
	IsRead         int        `json:"is_read"`
	IsAction       int        `json:"is_action"`
	NeedAction     int        `json:"need_action"`
	RelatedType    string     `json:"related_type"`
	RelatedId      string     `json:"related_id"`
	CreatedAt      *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt      *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
