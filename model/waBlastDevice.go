package model

import "time"

type WaBlastDevice struct {
	IdwaBlast int64      `json:"idwa_blast" gorm:"primary_key;index"`
	Iddevice  string     `json:"iddevice" gorm:"primary_key;index"`
	LastSend  *time.Time `json:"last_send"`
	CreatedAt *time.Time `json:"created_at"`
	UpdatedAt *time.Time `json:"updated_at"`
}
