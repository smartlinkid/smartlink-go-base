package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type SnapLayanan struct {
	Id                 string           `json:"id" gorm:"primary_key"`
	Nama               string           `json:"nama"`
	Idowner            string           `json:"idowner" gorm:"index"`
	Harga              float64          `json:"harga"`
	Idoutlet           string           `json:"idoutlet" gorm:"index"`
	Durasi             int64            `json:"durasi" gorm:"default:0"` // second
	Idsatuan           int              `json:"idsatuan" gorm:"index"`
	Jenis              int              `json:"jenis"` // 1 cuci 2 kering 3 lain
	Tipe               int              `json:"tipe"`  // 1  self service 2 dropoff
	Load               float64          `json:"load" gorm:"default:0"`
	StatusWajibLunas   int              `json:"status_wajib_lunas"`   // 0 non-aktif , 1 aktif
	StatusKomisiHelper int              `json:"status_komisi_helper"` // 0 non-aktif , 1 aktif
	KomisiHelper       float64          `json:"komisi_helper"`
	DurasiLayanan      int64            `json:"durasi_layanan"`
	LoadToleransi      float64          `json:"load_toleransi"`
	Satuan             DataSatuan       `json:"satuan" gorm:"foreignkey:idsatuan;association_foreignkey:iddata_satuan;references:iddata_satuan"`
	SnapLayananTag     []SnapLayananTag `json:"snap_layanan_tag" gorm:"foreignKey:idsnap_layanan;association_foreignkey:id;references:id"`
	LayananTags        []LayananTags    `json:"layanan_tags" gorm:"foreignKey:layanan_idlayanan;association_foreignkey:id;references:id"`
	CreatedAt          *time.Time       `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt          *time.Time       `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	Hapus              *int             `json:"hapus" gorm:"default:0"`
	Mesinable          int              `json:"mesinable"`
	JamTertentuAktif   int              `json:"jam_tertentu_aktif"`
	JamTertentuAwal    string           `json:"jam_tertentu_awal"`
	JamTertentuAkhir   string           `json:"jam_tertentu_akhir"`
}

var SnapLayananType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "snap_layanan",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "id layanan snap",
			},
			"nama": &graphql.Field{
				Type:        graphql.String,
				Description: "nama layanan snap",
			},
			"idowner": &graphql.Field{
				Type:        graphql.String,
				Description: "id owner snap",
			},
			"harga": &graphql.Field{
				Type:        graphql.Float,
				Description: "harga layanan snap",
			},
			"idoutlet": &graphql.Field{
				Type:        graphql.String,
				Description: "id outlet snap",
			},
			"durasi": &graphql.Field{
				Type:        graphql.Int,
				Description: "durasi layanan snap (dalam satuan detik)",
			},
			"idsatuan": &graphql.Field{
				Type:        graphql.Int,
				Description: "id satuan layanan snap",
			},
			"jenis": &graphql.Field{
				Type:        graphql.Int,
				Description: "jenis layanan snap. 1: cuci 2: kering 3: non-mesin",
			},
			"tipe": &graphql.Field{
				Type:        graphql.Int,
				Description: "tipe layanan snap. 1: self service 2: dropoff",
			},
			"load": &graphql.Field{
				Type:        graphql.Float,
				Description: "kapasitas load layanan snap",
			},
			"durasi_layanan": &graphql.Field{
				Type:        graphql.String,
				Description: "durasi layanan in second",
			},
			"load_toleransi": &graphql.Field{
				Type:        graphql.Float,
				Description: "load toleransi",
			},
			"satuan": &graphql.Field{
				Type:        DataSatuanType,
				Description: "",
			},
			//"base": &graphql.Field{
			//	Type:        BaseModelType,
			//	Description: "base model",
			//},
		},
		Description: "snap layanan data",
	})

func init() {
	SnapLayananType.AddFieldConfig("snap_layanan_tag", &graphql.Field{Type: graphql.NewList(SnapLayananTagType)})
}
