package model

type JenisGajiSales struct {
	Id                   int                    `json:"id" gorm:"index;primary_key"`
	Idowner              string                 `json:"idowner"`
	Nama                 string                 `json:"nama"`
	DihitungDariNota     int                    `json:"dihitung_dari_nota"`
	DetailJenisGajiSales []DetailJenisGajiSales `json:"detail_jenisgaji_sales" gorm:"foreignkey:idjenis_gaji_sales;association_foreignkey:id;references:id"`
	BaseModel
}
