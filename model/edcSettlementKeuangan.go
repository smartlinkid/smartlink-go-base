package model

import (
	"time"
)

type EdcSettlementKeuangan struct {
	Id                   string     `json:"id" gorm:"primary_key"`
	Idowner              string     `json:"idowner"`
	Idoutlet             string     `json:"idoutlet"`
	IdakunKeuanganDebit  string     `json:"idakun_keuagan_debit"`
	IdakunKeuanganKredit string     `json:"idakun_keuangan_kredit"`
	Idedc                string     `json:"idedc"`
	Idkaryawan           *string    `json:"idkaryawan"`
	Nominal              float64    `json:"nominal"`
	BiayaMdr             float64    `json:"biaya_mdr"`
	Total                float64    `json:"total"`
	StatusKonfirmasi     int        `json:"status_konfirmasi"`
	WaktuKonfirmasi      *time.Time `json:"waktu_konfirmasi"`
	SaldoAwal            float64    `json:"saldo_awal"`
	SaldoAkhir           float64    `json:"saldo_akhir"`
	WaktuTransfer        *time.Time `json:"waktu_transfer"`
	CatatanKonfirmasi    string     `json:"catatan_konfirmasi"`
	Keterangan           string     `json:"keterangan"`
	IdrekeningOwner      int        `json:"idrekening_owner"`
	NamaAkunRekening     string     `json:"nama_akun_rekening"`
	PemilikRekening      string     `json:"pemilik_rekening"`
	WaktuSettlement      *time.Time `json:"waktu_settlement"`
	NomorRekening        string     `json:"nomor_rekening"`
	BaseModel
	EdcMesin      *EdcMesin       `json:"edc_mesin" gorm:"foreignkey:id;association_foreignkey:idedc;references:idedc"`
	Karyawan      *Karyawan       `json:"karyawan" gorm:"foreignkey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	RekeningOwner *RekeningOwner  `json:"rekening_owner" gorm:"foreignkey:id;association_foreignkey:idrekening_owner;references:idrekening_owner"`
	Outlet        *Outlet         `json:"outlet" gorm:"foreignkey:idoutlet;association_foreignkey:idoutlet;references:idoutlet"`
	EdcPembayaran []EdcPembayaran `json:"edc_pembayaran" gorm:"foreignkey:idsettlement;association_foreignkey:id;references:id"`
	TransaksiFoto []TransaksiFoto `json:"transaksi_foto" gorm:"foreignkey:idtransaksi;association_foreignkey:id;references:id"`
}
