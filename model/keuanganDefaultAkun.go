package model

type KeuanganDefaultAkun struct {
	IdkeuanganAkun string `json:"idkeuangan_akun" gorm:"primary_key"`
	Nama           string `json:"nama"`
	IdkategoriAkun string `json:"idkategori_akun"`
	Keterangan     string `json:"keterangan"`
	BaseModel
}
