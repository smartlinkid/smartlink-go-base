package model

import "time"

type PembelianKoin struct {
	IdpembelianKoin    string    `json:"idpembelian_koin" gorm:"primary_key;index"`
	OwnerIdowner       string    `json:"owner_idowner"`
	Waktu              int64     `json:"waktu"`
	Subtotal           float64   `json:"subtotal"`
	Diskon             float64   `json:"diskon"`
	KodeUnik           float64   `json:"kode_unik"`
	Total              float64   `json:"total"`
	TotalKoinUnlimited int       `json:"total_koin_unlimited"`
	TotalKoinLimited   int       `json:"total_koin_limited"`
	TotalMasaAktif     int64     `json:"total_masa_aktif"`
	JenisBayar         int       `json:"jenis_bayar"`
	Status             int       `json:"status"`
	Expired            int64     `json:"expired"`
	Catatan            string    `json:"catatan"`
	CreatedAt          time.Time `json:"created_at"`
	UpdatedAt          time.Time `json:"updated_at"`
	WaktuStatus        int64     `json:"waktu_status"`
	KeteranganStatus   string    `json:"keterangan_status"`
	RelatedType        string    `json:"related_type"`
	RelatedId          string    `json:"related_id"`
	Idcoupon           int       `json:"idcoupon"`
	KodePromo          string    `json:"kode_promo"`
}
