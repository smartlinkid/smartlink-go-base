package model

import "time"

type PekerjaanKaryawan struct {
	TahapanIdtahapan   int        `json:"tahapan_idtahapan" gorm:"primary_key"`
	KaryawanIdkaryawan string     `json:"karyawan_idkaryawan" gorm:"primary_key"`
	Dibayar            int        `json:"dibayar"`
	CreatedAt          *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP"`
	UpdatedAt          *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
	Tahapan            *Tahapan   `json:"tahapan" gorm:"foreignkey:idtahapan;association_foreignkey:tahapan_idtahapan;references:tahapan_idtahapan"`
}
