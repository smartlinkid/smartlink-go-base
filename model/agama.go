package model

import "time"

type Agama struct {
	Id        int        `json:"id" gorm:"primary_key"`
	NamaAgama string     `json:"nama_agama"`
	CreatedAt *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
