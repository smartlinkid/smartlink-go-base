package model

import "time"

type WaBlastData struct {
	Id          int64      `json:"id" gorm:"primary_key;auto_increment"`
	IdwaBlast   int64      `json:"idwa_blast" gorm:"index"`
	Idwassenger *string    `json:"idwassenger" gorm:"index"`
	Idcustomer  string     `json:"idcustomer" gorm:"index"`
	Nomor       string     `json:"nomor" gorm:"index"`
	Status      int        `json:"status"`
	Idtemplate  *int64     `json:"idtemplate"`
	Text        *string    `json:"text"`
	Iddevice    *string    `json:"iddevice" gorm:"index"`
	Idowner     string     `json:"idowner" gorm:"index"`
	Trial       int        `json:"trial"`
	CreatedAt   *time.Time `json:"created_at"`
	UpdatedAt   *time.Time `json:"updated_at"`
	Hapus       int        `json:"hapus"`
	StatusText  string     `json:"status_text"`
	Idoutlet    string     `json:"idoutlet"`
	WaDevice    *WaDevice  `json:"wa_device" gorm:"foreignkey:iddevice; association_foreignkey:iddevice;references:iddevice"`
}
