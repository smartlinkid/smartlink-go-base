package model

import "time"

type WhitelabelExpress struct {
	Idowner        string        `json:"idowner"`
	Idoutlet       string        `json:"idoutlet"`
	IdpaketExpress int           `json:"idpaket_express"`
	CreatedAt      *time.Time    `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	PaketExpress   *PaketExpress `json:"paket_express" gorm:"foreignkey:id;association_foreignkey:idpaket_express;references:idpaket_express"`
}
