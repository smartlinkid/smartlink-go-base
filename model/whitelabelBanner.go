package model

import "time"

type WhitelabelBanner struct {
	Id                     string                   `json:"id"`
	Idowner                string                   `json:"idowner"`
	Headline               string                   `json:"headline"`
	JenisBanner            int                      `json:"jenis_banner"`
	Idpromo                string                   `json:"idpromo"`
	Link                   string                   `json:"link"`
	LinkName               string                   `json:"link_name"`
	StartDate              time.Time                `json:"start_date"`
	EndDate                time.Time                `json:"end_date"`
	Deskripsi              string                   `json:"deskripsi"`
	CreatedAt              time.Time                `json:"created_at"`
	UpdatedAt              time.Time                `json:"updated_at"`
	Hapus                  int                      `json:"hapus"`
	WhitelabelBannerOutlet []WhitelabelBannerOutlet `json:"whitelabel_banner_outlet,omitempty" gorm:"foreignKey:idbanner;association_foreignkey:id;references:id"`
}

type WhitelabelBannerOutlet struct {
	Idbanner string  `json:"idbanner"`
	Idoutlet string  `json:"idoutlet"`
	Idowner  string  `json:"idowner"`
	Outlet   *Outlet `json:"outlet" gorm:"foreignkey:idoutlet;association_foreignkey:idoutlet;references:idoutlet"`
}
