package model

type JsonpathUsage struct {
	JenisData string `json:"jenis_data"`
	Usage     string `json:"usage"`
}
