package model

type PembelianDeposit struct {
	IdpembelianDeposit     string                       `json:"idpembelian_deposit" gorm:"primary_key"`
	OutletIdoutlet         string                       `json:"outlet_idoutlet"`
	CustomerIdcustomer     string                       `json:"customer_idcustomer"`
	KaryawanIdkaryawan     string                       `json:"karyawan_idkaryawan"`
	TanggalBeli            int64                        `json:"tanggal_beli"`
	Keterangan             string                       `json:"keterangan"`
	Total                  float64                      `json:"total"`
	Diskon                 float64                      `json:"diskon"`
	PersenDiskon           float64                      `json:"persendiskon"`
	Pajak                  float64                      `json:"pajak"`
	PersenPajak            float64                      `json:"persenpajak"`
	Grandtotal             float64                      `json:"grandtotal"`
	ShortLink              string                       `json:"short_link"`
	StatusHapus            int                          `json:"status_hapus"`
	Lunas                  int                          `json:"lunas"`
	Outlet                 *Outlet                      `json:"outlet" gorm:"foreignkey:idoutlet;association_foreignkey:outlet_idoutlet;references:outlet_idoutlet"`
	Karyawan               *Karyawan                    `json:"karyawan" gorm:"foreignkey:idkaryawan;association_foreignkey:karyawan_idkaryawan;references:karyawan_idkaryawan"`
	Customer               *Customer                    `json:"customer" gorm:"foreignkey:idcustomer;association_foreignkey:customer_idcustomer;references:customer_idcustomer"`
	PembelianDepositHapus  *PembelianDepositHapus       `json:"pembelian_deposit_hapus" gorm:"foreignkey:idpembelian;association_foreignkey:idpembelian_deposit;references:idpembelian_deposit"`
	Pembayaran             []PembayaranPembelianDeposit `json:"pembayaran" gorm:"foreignkey:idpembelian_deposit;association_foreignkey:idpembelian_deposit;references:idpembelian_deposit"`
	DetailPembelianDeposit []DetailPembelianDeposit     `json:"detail_pembelian_deposit" gorm:"foreignkey:pembelian_deposit_idpembelian_deposit;association_foreignkey:idpembelian_deposit;references:idpembelian_deposit"`
	BaseModel
}
