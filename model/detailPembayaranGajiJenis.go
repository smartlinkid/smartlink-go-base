package model

import "time"

type DetailPembayaranGajiJenis struct {
	Id               int        `json:"id" gorm:"primary_key;index;not null;auto_increment"`
	Idjenis          int        `json:"idjenis"`
	IdpembayaranGaji string     `json:"idpembayaran_gaji"`
	Nominal          float64    `json:"nominal"`
	Jumlah           float64    `json:"jumlah"`
	TotalGaji        float64    `json:"total_gaji"`
	JenisGaji        *JenisGaji `json:"jenis_gaji" gorm:"index;foreignKey:id;association_foreignkey:idjenis;references:idjenis"`
	CreatedAt        *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt        *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
