package model

type ReminderCountdownMaster struct {
	Id            int    `json:"id" gorm:"primary_key"`
	Nama          string `json:"nama"`
	DaysCountdown int    `json:"days_countdown"`
	EditableTime  int    `json:"editable_time"`
	BaseModel
}
