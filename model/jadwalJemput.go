package model

import "time"

type JadwalJemput struct {
	Id                    int                    `json:"id" gorm:"primary_key"`
	Idjemput              string                 `json:"idjemput"`
	Idcustomer            string                 `json:"idcustomer"`
	Idalamat              string                 `json:"idalamat"`
	TanggalJemput         int64                  `json:"tanggal_jemput"`
	Keterangan            string                 `json:"keterangan"`
	StatusJemput          int                    `json:"status_jemput"`
	GambarJemput          string                 `json:"gambar_jemput"`
	JenisKilo             int                    `json:"jenis_kilo"`
	JenisSatuan           int                    `json:"jenis_satuan"`
	JenisLuas             int                    `json:"jenis_luas"`
	Idoutlet              string                 `json:"idoutlet"`
	JenisKurir            string                 `json:"jenis_kurir"`
	NamaKurir             string                 `json:"nama_kurir"`
	JenisKendaraan        string                 `json:"jenis_kendaraan"`
	Idkurir               string                 `json:"idkurir"`
	UangTitip             float64                `json:"uang_titip"`
	KeteranganJemput      string                 `json:"keterangan_jemput"`
	Deskripsi             string                 `json:"deskripsi"`
	KeteranganStatus      string                 `json:"keterangan_status"`
	DateTanggalJemput     *time.Time             `json:"date_tanggal_jemput"`
	Jarak                 float64                `json:"jarak"`
	MaksJarakGratis       float64                `json:"maks_jarak_gratis"`
	BiayaPerkm            float64                `json:"biaya_perkm"`
	BiayaTotal            float64                `json:"biaya_total"`
	DataTransaksi         string                 `json:"data_transaksi"`
	DataTransaksiObject   map[string]interface{} `json:"data_transaksi_object" gorm:"-"`
	IdkaryawanKasir       string                 `json:"idkaryawan_kasir"`
	ReturnedBalance       float64                `json:"returned_balace"`
	CourierType           string                 `json:"courier_type"`
	ShipmentOrderId       string                 `json:"shipment_order_id"`
	ShipmentRawData       string                 `json:"shipment_raw_data"`
	CourierDriverName     string                 `json:"courier_driver_name"`
	ShipmentRawDataObject map[string]interface{} `json:"shipment_raw_data_object" gorm:"-"`
	Outlet                *Outlet                `json:"outlet" gorm:"foreignkey:idoutlet;association_foreignkey:idoutlet;references:idoutlet"`
	Karyawan              *Karyawan              `json:"karyawan_kurir" gorm:"foreignkey:idkaryawan;association_foreignkey:idkurir;references:idkurir"`
	Kasir                 *Karyawan              `json:"karyawan_kasir" gorm:"foreignkey:idkaryawan;association_foreignkey:idkaryawan_kasir;references:idkaryawan_kasir"`
	Customer              *Customer              `json:"customer" gorm:"foreignkey:idcustomer;association_foreignkey:idcustomer;references:idcustomer"`
	Alamat                *Alamat                `json:"alamat" gorm:"foreignkey:idalamat;association_foreignkey:idalamat;references:idalamat"`
	TransaksiLoyalty      *TransaksiLoyalty      `json:"transaksi_loyalty" gorm:"foreignkey:idjemput;association_foreignkey:idjemput;references:idjemput"`
	JadwalJemputHapus     *JadwalJemputHapus     `json:"jadwal_jemput_hapus" gorm:"foreignkey:idjemput;association_foreignkey:idjemput;references:idjemput"`
	JadwalJemputBayar     *JadwalJemputBayar     `json:"jadwal_jemput_bayar" gorm:"foreignkey:idjemput;association_foreignkey:idjemput;references:idjemput"`
	BaseModel
}
