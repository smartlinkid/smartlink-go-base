package model

import "time"

type OwnerPengaturanUmum struct {
	Id                        int        `json:"id" gorm:"index;primary_key;not null"`
	Idowner                   string     `json:"idowner" gorm:"index"`
	JenisEmoney               int        `json:"jenis_emoney"`                 //1 limited, 2 unlimited
	TipePerpanjanganMasaAktif int        `json:"tipe_perpanjangan_masa_aktif"` //1 akumulasi (pulsa), 2 menggunakan terlama, 3 menggunakan terbaru
	TipeSaldoExpired          int        `json:"tipe_saldo_expired"`           //1 hangus, 2 akumulasi
	EnableReminderDeposit     int        `json:"enable_reminder_deposit"`
	EnableReminderEmoney      int        `json:"enable_reminder_emoney"`
	IddeviceReminderDeposit   *string    `json:"iddevice_reminder_deposit"`
	IddeviceReminderEmoney    *string    `json:"iddevice_reminder_emoney"`
	AmbilTanpaLunasi          int        `json:"ambil_tanpa_lunasi"`
	BisaLunasiInvoice         int        `json:"bisa_lunasi_invoice"` //0 OFF, 1 Semua Outlet & Kasir, 2 Outlet & Kasir Tertentu
	BisaMembuatKasbon         int        `json:"bisa_membuat_kasbon"` //0 OFF, 1 Semua Kasir, 2 Kasir Tertentu
	AutoGenerateQrisInvoice   int        `json:"auto_generate_qris_invoice"`
	EnableNotaPdf             int        `json:"enable_nota_pdf"`
	EnableNotaText            int        `json:"enable_nota_text"`
	EnableRekomendasiLayanan  int        `json:"enable_rekomendasi_layanan"`
	EnablePredictionAi        int        `json:"enable_prediction_ai"`
	CreatedAt                 *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt                 *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
}
