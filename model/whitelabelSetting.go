package model

import "time"

type WhitelabelSetting struct {
	Idowner         string    `json:"idowner"`
	Nama            string    `json:"nama"`
	Idpackage       string    `json:"idpackage"`
	Warna           string    `json:"warna"`
	Versi           string    `json:"versi"`
	LogoCloudBucket string    `json:"logo_cloud_bucket"`
	LogoCloudUrl    string    `json:"logo_cloud_url"`
	EnableWaOtp     int       `json:"enable_wa_otp"`
	FcmApiKey       string    `json:"fcm_api_key"`
	CreatedAt       time.Time `json:"created_at"`
	UpdatedAt       time.Time `json:"updated_at"`
}
