package model

import "time"

type ParfumOutlet struct {
	Idoutlet  string     `json:"idoutlet"`
	Idparfum  int        `json:"idparfum"`
	Idowner   string     `json:"idowner"`
	CreatedAt *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
	Parfum    Parfum     `json:"parfum" gorm:"foreignkey:idparfum;association_foreignkey:idparfum;references:idparfum"`
}
