package model

import "time"

type TransaksiFoto struct {
	Id               string     `json:"id" gorm:"primary_key;index`
	Idowner          string     `json:"idowner"`
	Idtransaksi      string     `json:"idtransaksi"`
	Idfoto           string     `json:"idfoto"`
	Idkaryawan       string     `json:"idkaryawan"`
	Lat              float64    `json:"lat"`
	Lng              float64    `json:"lng"`
	Lokasi           string     `json:"lokasi"`
	Device           string     `json:"device"`
	Keterangan       string     `json:"keterangan"`
	UploadedAt       *time.Time `json:"uploaded_at"`
	UploadedAtLong   int64      `json:"uploaded_at_long"`
	Size             int64      `json:"size"`
	RefTable         string     `json:"ref_table"`
	Format           string     `json:"format"`
	Width            int        `json:"width"`
	Height           int        `json:"height"`
	Type             string     `json:"type"`
	CloudBucket      string     `json:"cloud_bucket"`
	CloudName        string     `json:"cloud_name"`
	CloudContentType string     `json:"cloud_content_type"`
	CloudRawInfo     string     `json:"cloud_raw_info"`
	CloudThumb       *string    `json:"cloud_thumb"`
	CloudThumbSize   int64      `json:"cloud_thumb_size"`
	BaseModel
}
