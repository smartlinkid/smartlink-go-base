package model

import "time"

type WaStorySchedule struct {
	Id        string    `json:"id" gorm:"primary_key"`
	Iddevice  string    `json:"iddevice" gorm:"index"`
	Idowner   string    `json:"idowner"`
	ThumbName *string   `json:"thumb_name"`
	MediaName *string   `json:"media_name"`
	Caption   *string   `json:"caption"`
	TypeMedia int       `json:"type_media"`
	PostAt    time.Time `json:"post_at"`
	Enable    int       `json:"enable"`
	Repeat    int       `json:"repeat"`
	Senin     int       `json:"senin"`
	Selasa    int       `json:"selasa"`
	Rabu      int       `json:"rabu"`
	Kamis     int       `json:"kamis"`
	Jumat     int       `json:"jumat"`
	Sabtu     int       `json:"sabtu"`
	Minggu    int       `json:"minggu"`
	Jam       *string   `json:"jam"`
	Hapus     int       `json:"hapus"`
	CreatedAt time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP"`
	UpdatedAt time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
	WaDevice  *WaDevice `json:"wa_device" gorm:"foreignkey:iddevice;association_foreignkey:iddevice;references:iddevice"`
}
