package model

import "time"

type KeuanganJurnal struct {
	Idjurnal    int64      `json:"idjurnal" gorm:"primary_key;index;auto_increment"`
	Idakun      string     `json:"idakun" gorm:"index"`
	Debet       float64    `json:"debet"`
	Kredit      float64    `json:"kredit"`
	SaldoAkhir  float64    `json:"saldo_akhir"`
	Keterangan  string     `json:"keterangan"`
	RelatedType string     `json:"related_type"`
	RelatedId   string     `json:"related_id" gorm:"index"`
	Idkaryawan  string     `json:"idkaryawan" gorm:"index"`
	Idowner     string     `json:"idowner" gorm:"index"`
	Waktu       int64      `json:"waktu"`
	RefType     string     `json:"ref_type"`
	RefId       string     `json:"ref_id" gorm:"index"`
	Rev         int        `json:"rev"`
	CreatedAt   *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt   *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
