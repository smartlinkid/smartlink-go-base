package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type AcceptorHistory struct {
	Id               string     `json:"id" gorm:"primary_key;index"`
	CreatedAt        *time.Time `json:"created_at"`
	UpdatedAt        *time.Time `json:"updated_at"`
	Masuk            float64    `json:"masuk"`
	Keluar           float64    `json:"keluar"`
	SaldoAkhir       float64    `json:"saldo_akhir"`
	RefType          string     `json:"ref_type" gorm:"index"`
	RefId            string     `json:"ref_id" gorm:"index"`
	IdacceptorDevice string     `json:"idacceptor_device" gorm:"index"`
	Idowner          string     `json:"idowner" gorm:"index"`
	Idoutlet         string     `json:"idoutlet" gorm:"index"`
}

var AcceptorHistoryType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "acceptor_mesin",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "id",
			},
			"created_at": &graphql.Field{
				Type:        graphql.String,
				Description: "created at",
			},
			"updated_at": &graphql.Field{
				Type:        graphql.String,
				Description: "updated at",
			},
			"masuk": &graphql.Field{
				Type:        graphql.Float,
				Description: "uang masuk",
			},
			"keluar": &graphql.Field{
				Type:        graphql.Float,
				Description: "uang keluar",
			},
			"saldo_akhir": &graphql.Field{
				Type:        graphql.Float,
				Description: "saldo akhir",
			},
			"ref_type": &graphql.Field{
				Type:        graphql.String,
				Description: "ref type",
			},
			"ref_id": &graphql.Field{
				Type:        graphql.String,
				Description: "ref id",
			},
			"idowner": &graphql.Field{
				Type:        graphql.String,
				Description: "id owner",
			},
			"idoutlet": &graphql.Field{
				Type:        graphql.String,
				Description: "id outlet",
			},
		},
	})

var AcceptorHistoryInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "acceptor_history_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id",
			},
			"masuk": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "uang masuk",
			},
			"keluar": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "uang keluar",
			},
			"saldo_akhir": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "saldo akhir",
			},
			"ref_type": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "ref type",
			},
			"ref_id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "ref id",
			},
			"idowner": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id owner",
			},
			"idoutlet": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id outlet",
			},
		},
	})
