package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type WaData struct {
	Idwa          int64      `json:"idwa" gorm:"primary_key; auto_increment"`
	Idwassenger   *string    `json:"idwassenger" gorm:"index"`
	Text          string     `json:"text"`
	Number        string     `json:"number" gorm:"index"`
	NumberInter   string     `json:"number_inter" gorm:"index"`
	Processed     int        `json:"processed"`
	Queued        int        `json:"queued"`
	Status        int        `json:"status"`
	StatusAt      *time.Time `json:"status_at"`
	StatusMessage string     `json:"status_message"`
	RefTable      string     `json:"ref_table" gorm:"index"`
	RefId         string     `json:"ref_id" gorm:"index"`
	Idowner       string     `json:"idowner" gorm:"index"`
	DebugMessage  *string    `json:"debug_message"`
	Idpayload     string     `json:"idpayload"`
	ScheduledAt   *time.Time `json:"scheduled_at"`
	Iddevice      *string    `json:"iddevice" gorm:"index"`
	RelatedTable  string     `json:"related_table" gorm:"index"`
	RelatedId     string     `json:"related_id" gorm:"index"`
	Attempt       int        `json:"attempt"`
	LastTry       *time.Time `json:"last_try"`
	HasAttachment int        `json:"has_attachment"`
	CreatedAt     *time.Time `json:"created_at" gorm:"index"`
	UpdatedAt     *time.Time `json:"updated_at"`
	Hapus         int        `json:"hapus"`
	Customer      Customer   `json:"customer" gorm:"foreignkey:telp;association_foreignkey:number;references:number"`
	Outlet        Outlet     `json:"outlet" gorm:"foreignkey:idoutlet;association_foreignkey:related_id;references:related_id"`
}

var WaDataType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "wa_data",
		Fields: graphql.Fields{
			"idwa": &graphql.Field{
				Type:        graphql.String,
				Description: "idwa",
			},
			"idwassenger": &graphql.Field{
				Type:        graphql.String,
				Description: "idwassenger",
			},
			"text": &graphql.Field{
				Type:        graphql.String,
				Description: "text",
			},
			"number": &graphql.Field{
				Type:        graphql.String,
				Description: "number",
			},
			"number_inter": &graphql.Field{
				Type:        graphql.String,
				Description: "number_inter",
			},
			"processed": &graphql.Field{
				Type:        graphql.Int,
				Description: "processed",
			},
			"queued": &graphql.Field{
				Type:        graphql.Int,
				Description: "queued",
			},
			"status": &graphql.Field{
				Type:        graphql.Int,
				Description: "status",
			},
			"status_at": &graphql.Field{
				Type:        graphql.String,
				Description: "status_at",
			},
			"status_message": &graphql.Field{
				Type:        graphql.String,
				Description: "status_message",
			},
			"ref_table": &graphql.Field{
				Type:        graphql.String,
				Description: "ref_table",
			},
			"ref_id": &graphql.Field{
				Type:        graphql.String,
				Description: "ref_id",
			},
			"idowner": &graphql.Field{
				Type:        graphql.String,
				Description: "idowner",
			},
			"debug_message": &graphql.Field{
				Type:        graphql.String,
				Description: "debug_message",
			},
			"idpayload": &graphql.Field{
				Type:        graphql.String,
				Description: "idpayload",
			},
			"scheduled_at": &graphql.Field{
				Type:        graphql.String,
				Description: "scheduled_at",
			},
			"iddevice": &graphql.Field{
				Type:        graphql.String,
				Description: "iddevice",
			},
			"related_table": &graphql.Field{
				Type:        graphql.String,
				Description: "related_table",
			},
			"related_id": &graphql.Field{
				Type:        graphql.String,
				Description: "related_id",
			},
			"attempt": &graphql.Field{
				Type:        graphql.String,
				Description: "attempt",
			},
			"last_try": &graphql.Field{
				Type:        graphql.String,
				Description: "last_try",
			},
			"created_at": &graphql.Field{
				Type:        graphql.String,
				Description: "created_at",
			},
			"updated_at": &graphql.Field{
				Type:        graphql.String,
				Description: "updated_at",
			},
			"hapus": &graphql.Field{
				Type:        graphql.Int,
				Description: "updated_at",
			},
		},
	})

var WaDataInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "wa_data",
		Fields: graphql.InputObjectConfigFieldMap{
			"idwa": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "idwa",
			},
			"idwassenger": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "idwassenger",
			},
			"text": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "text",
			},
			"number": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "number",
			},
			"number_inter": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "number_inter",
			},
			"processed": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "processed",
			},
			"queued": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "queued",
			},
			"status": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "status",
			},
			"status_at": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "status_at",
			},
			"status_message": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "status_message",
			},
			"ref_table": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "ref_table",
			},
			"ref_id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "ref_id",
			},
			"idowner": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "idowner",
			},
			"debug_message": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "debug_message",
			},
			"idpayload": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "idpayload",
			},
			"scheduled_at": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "scheduled_at",
			},
			"iddevice": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "iddevice",
			},
			"related_table": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "related_table",
			},
			"related_id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "related_id",
			},
			"attempt": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "attempt",
			},
			"last_try": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "last_try",
			},
			"created_at": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "created_at",
			},
			"updated_at": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "updated_at",
			},
			"hapus": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "updated_at",
			},
		},
	})
