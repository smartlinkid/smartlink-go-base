package model

import "time"

type AntarExtraBayar struct {
	Id               string     `json:"id" gorm:"primary_key"`
	IdantarExtra     string     `json:"idantar_extra"`
	JenisBayar       int        `json:"jenis_bayar"`
	TipeBayar        int        `json:"tipe_bayar"`
	Bayar            float64    `json:"bayar"`
	Kembalian        float64    `json:"kembalian"`
	IdakunDebit      string     `json:"idakun_debit"`
	TransfederedAt   *time.Time `json:"transadered_at"`
	TransferedAtLong int64      `json:"transfered_at_long"`
	Keterangan       string     `json:"keterangan"`
	NoResi           string     `json:"no_resi"`
	Idoutlet         string     `json:"idoutlet"`
	Idowner          string     `json:"idowner"`
	CreatedAt        *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt        *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
