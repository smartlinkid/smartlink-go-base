package model

type CouponFiturUsage struct {
	Id            string `json:"id"`
	IdcouponFitur string `json:"idcoupon_fitur"`
	Idowner       string `json:"idowner"`
	Usage         int64  `json:"usage"`
	Kode          string `json:"kode"`
}
