package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type AcceptorIn struct {
	Id               string     `json:"id" gorm:"primary_key;index"`
	Nominal          float64    `json:"nominal"`
	IdacceptorDevice string     `json:"idacceptor_device" gorm:"index"`
	Idowner          string     `json:"idowner" gorm:"index"`
	Idoutlet         string     `json:"idoutlet" gorm:"index"`
	IdcustomerGlobal string     `json:"idcustomer_global" gorm:"index"`
	CreatedAt        *time.Time `json:"created_at"`
	UpdatedAt        *time.Time `json:"updated_at"`
	Hapus            *int       `json:"hapus"`
}

var AcceptorInType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "acceptor_in",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "id",
			},
			"nominal": &graphql.Field{
				Type:        graphql.String,
				Description: "nominal",
			},
			"idacceptor_device": &graphql.Field{
				Type:        graphql.String,
				Description: "id acceptor device",
			},
			"idowner": &graphql.Field{
				Type:        graphql.String,
				Description: "id owner",
			},
			"idoutlet": &graphql.Field{
				Type:        graphql.String,
				Description: "id outlet",
			},
			"idcustomer_global": &graphql.Field{
				Type:        graphql.String,
				Description: "idcustomer global",
			},
			"created_at": &graphql.Field{
				Type:        graphql.String,
				Description: "created at",
			},
			"updated_at": &graphql.Field{
				Type:        graphql.String,
				Description: "updated at",
			},
			"hapus": &graphql.Field{
				Type:        graphql.Int,
				Description: "hapus",
			},
		},
	})

var AcceptorInInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "acceptor_in_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id",
			},
			"nominal": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "nominal",
			},
			"idacceptor_device": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id acceptor device",
			},
			"idowner": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id owner",
			},
			"idoutlet": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id outlet",
			},
			"idcustomer_global": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "idcustomer global",
			},
			"hapus": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "hapus",
			},
		},
	})
