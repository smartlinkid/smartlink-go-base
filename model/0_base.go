package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type BaseModel struct {
	CreatedAt *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
	Hapus     *int       `json:"hapus" gorm:"default:0"`
}

// base model for all

var BaseModelType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "0_base",
		Fields: graphql.Fields{
			"created_at": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "created_at",
			},
			"updated_at": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "updated_at",
			},
			"hapus": &graphql.Field{
				Type:        graphql.Int,
				Description: "hapus",
			},
		},
		Description: "basemodel data",
	})

var BaseModelInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "ownerInput",
		Fields: graphql.InputObjectConfigFieldMap{
			"created_at": &graphql.InputObjectFieldConfig{
				Type:        graphql.DateTime,
				Description: "created_at",
			},
			"updated_at": &graphql.InputObjectFieldConfig{
				Type:        graphql.DateTime,
				Description: "updated_at",
			},
			"hapus": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "hapus",
			},
		},
	})
