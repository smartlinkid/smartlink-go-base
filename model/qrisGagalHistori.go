package model

import "time"

type QrisGagalHistori struct {
	IdqrisGagal int        `json:"idqris_gagal"`
	Idowner     string     `json:"idowner"`
	Status      string     `json:"status"`
	Nama        string     `json:"nama"`
	CreatedAt   *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
}
