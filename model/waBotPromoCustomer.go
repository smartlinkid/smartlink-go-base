package model

import "time"

type WaBotPromoCustomer struct {
	Kode                    string     `json:"kode"`
	Idowner                 string     `json:"idowner"`
	Idoutlet                string     `json:"idoutlet"`
	IdcustomerGlobal        string     `json:"idcustomer_global"`
	IdcustomerGlobalReferal string     `json:"idcustomer_global_referal"`
	IdwaBotPromo            string     `json:"idwa_bot_promo"`
	MasaAktif               int64      `json:"masa_aktif"`
	ExpiredAt               *time.Time `json:"expired_at"`
	Used                    int        `json:"used"`
	UsedAt                  *time.Time `json:"used_at"`
	UsedBy                  string     `json:"used_by"`
	Diskon                  float64    `json:"diskon"`
	IdpaylinkTopupCustomer  string     `json:"idpaylink_topup_customer"`
	CreatedAt               *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt               *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
}
