package model

type DetailPembayaranGajiKomisi struct {
	Id               int     `json:"id" gorm:"primary_key;index;not null;auto_increment"`
	IdpembayaranGaji string  `json:"idpembayaran_gaji"`
	Nama             string  `json:"nama"`
	Nominal          float64 `json:"nominal"`
}
