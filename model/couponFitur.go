package model

import "time"

type CouponFitur struct {
	Id        string     `json:"id"`
	Idcoupon  string     `json:"idcoupon"`
	Fitur     string     `json:"fitur"`
	Diskon    float64    `json:"diskon"`
	Quota     int64      `json:"quota"`
	Kode      string     `json:"kode"`
	Expired   *time.Time `json:"expired"`
	MaxDiskon float64    `json:"max_diskon"`
	IsGlobal  int64      `json:"is_global"` //0 dipakai masal, 1 tiap owner dapat jatah
}
