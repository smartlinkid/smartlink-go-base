package model

import "time"

type JenisPembayaran struct {
	IdjenisPembayaran int        `json:"idjenis_pembayaran" gorm:"primary_key;auto_increment"`
	Nama              string     `json:"nama"`
	CreatedAt         *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt         *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
