package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type SnapTarif struct {
	Id         int        `json:"id" gorm:"primary_key;index;auto_increment"`
	Nama       string     `json:"nama"`
	Harga      float64    `json:"harga"`
	Tarif      float64    `json:"tarif"`
	DurasiSewa int        `json:"durasi_sewa"`
	CreatedAt  *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt  *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	Hapus      *int       `json:"hapus" gorm:"default:0"`
}

var SnapTarifType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "snap_tarif",
		Fields: graphql.Fields{
			"Id": &graphql.Field{
				Type:        graphql.Int,
				Description: "id snap tarif",
			},
			"tarif": &graphql.Field{
				Type:        graphql.Float,
				Description: "tarif snap in percent",
			},
		},
		Description: "",
	})

var SnapTarifInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "snap_tarif_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"tarif": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "",
			},
		},
		Description: "",
	})
