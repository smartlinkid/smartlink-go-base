package model

import "time"

type NotaTerimaWorkshop struct {
	IdsuratTerimaWorkshop string               `json:"idsurat_terima_workshop" gorm:"primary_key"`
	TransaksiIdtransaksi  string               `json:"transaksi_idtransaksi"`
	Iddetlayanan          string               `json:"iddetlayanan"`
	Valid                 int                  `json:"valid"`
	CreatedAt             *time.Time           `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt             *time.Time           `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
	SuratTerimaWorkshop   *SuratTerimaWorkshop `json:"surat_terima_workshop" gorm:"foreignkey:idsurat_terima_workshop;association_foreignkey:idsurat_terima_workshop;references:idsurat_terima_workshop"`
}
