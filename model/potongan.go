package model

import (
	"time"
)

type Potongan struct {
	Id              int             `json:"id" gorm:"primary_key;index;not null;auto_increment"`
	Idkaryawan      string          `json:"idkaryawan"`
	Karyawan        *Karyawan       `json:"karyawan" gorm:"foreignKey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	Idowner         string          `json:"idowner"`
	Waktu           *time.Time      `json:"waktu"`
	Keterangan      string          `json:"keterangan"`
	Nominal         float64         `json:"nominal"`
	Saldo           float64         `json:"saldo"`
	RefId           int             `json:"ref_id"`
	StatusPelunasan int             `json:"status_pelunasan"`
	PotonganBayar   []PotonganBayar `json:"potongan_bayar" gorm:"foreignKey:idpotongan;association_foreignkey:id;references:id"`
	BaseModel
}
