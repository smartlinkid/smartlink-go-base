package model

type Training struct {
	Id                 string              `json:"id" gorm:"primary_key"`
	OwnerIdowner       string              `json:"owner_idowner"`
	TrainingJenisId    string              `json:"training_jenis_id"`
	TelpOwner          string              `json:"telp_owner"`
	NamaOwner          string              `json:"nama_owner"`
	Status             string              `json:"status_training"` // 10_Menunggu konfirmasi CS, 20_Terjadwalkan, 30_Dijadwalkan Ulang, 40_Dibatalkan oleh CS, 50_Selesai CS, 60_Menunggu Pembayaran, 70_Pembayaran Gagal
	AlasanPembatalan   string              `json:"alasan_pembatalan"`
	TrainingPembayaran *TrainingPembayaran `json:"training_pembayaran" gorm:"foreignkey:training_id;association_foreignkey:id;references:id"`
	TrainingDetail     []TrainingDetail    `json:"training_detail" gorm:"foreignkey:training_id;association_foreignkey:id;references:id"`
	TrainingUlasan     *TrainingUlasan     `json:"training_ulasan" gorm:"foreignKey:training_id;association_foreignkey:id;references:id"`
	TraningJenis       *TrainingJenis      `json:"training_jenis" gorm:"foreignKey:id;association_foreignkey:training_jenis_id;references:training_jenis_id"`
	Owner              *Owner              `json:"owner" gorm:"foreignKey:idowner;association_foreignkey:owner_idowner;references:owner_idowner"`
	BaseModel
}
