package model

type BiayaItem struct {
	Id              int            `json:"id" gorm:"primary_key;index;not null;auto_increment"`
	IdbiayaKategori int            `json:"idbiaya_kategori"`
	Nama            string         `json:"nama"`
	Idowner         *string        `json:"idowner"`
	BiayaKategori   *BiayaKategori `json:"biaya_kategori" gorm:"foreignkey:id;association_foreignkey:idbiaya_kategori;references:idbiaya_kategori"`
	BaseModel
}
