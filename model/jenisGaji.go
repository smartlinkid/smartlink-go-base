package model

type JenisGaji struct {
	Id           int     `json:"id" gorm:"primary_key"`
	Nama         string  `json:"nama"`
	Type         int8    `json:"type"`
	OwnerIdowner *string `json:"owner_idowner"`
	BaseModel
}
