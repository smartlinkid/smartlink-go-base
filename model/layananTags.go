package model

type LayananTags struct {
	Id               int    `json:"id" gorm:"AUTO_INCREMENT"`
	LayananIdlayanan string `json:"layanan_idlayanan"`
	TagsIdtags       string `json:"tags_idtags"`
	Tipe             int    `json:"tipe" gorm:"default:0"` // 0 =  tipe layanan reguler, 1 = tipe layanan snap
	Tags             *Tags  `json:"tags" gorm:"foreignkey:idtags;association_foreignkey:tags_idtags;references:tags_idtags"`
}
