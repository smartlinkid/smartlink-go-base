package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type PaylinkQris struct {
	Id          string     `json:"id" gorm:"primary_key;"`
	Qrcode      string     `json:"qrcode"`
	Type        int        `json:"type"`
	Amount      float64    `json:"amount"`
	Status      int        `json:"status"`
	Project     int        `json:"project"`
	Action      int        `json:"action"`
	UserId      string     `json:"user_id" gorm:"index"`
	UserType    int        `json:"user_type"`
	Idoutlet    string     `json:"idoutlet" gorm:"index"`
	CallbackUrl string     `json:"callback_url"`
	Channel     string     `json:"channel"`
	CreatedAt   *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt   *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	Hapus       *int       `json:"hapus" gorm:"default:0"`
}

var PaylinkQrisObject = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "paylink_qris",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "id ",
			},
			"qrcode": &graphql.Field{
				Type:        graphql.String,
				Description: "qrcode string",
			},
			"user_id": &graphql.Field{
				Type:        graphql.String,
				Description: "user id",
			},
			"user_type": &graphql.Field{
				Type:        graphql.Int,
				Description: "user type : 1=owner, 2 = customer",
			},
			"idoutlet": &graphql.Field{
				Type:        graphql.String,
				Description: "id outlet",
			},
			"amount": &graphql.Field{
				Type:        graphql.Float,
				Description: "Amount",
			},
			"status": &graphql.Field{
				Type:        graphql.Int,
				Description: "status (1= Active, 2=Inactive)",
			},
			"project": &graphql.Field{
				Type:        graphql.Int,
				Description: "Project (1=Smartlink, 2=Snapclean, 4=Member Register)",
			},
			"action": &graphql.Field{
				Type:        graphql.Int,
				Description: "Project (11=smartlink kasir, 21=snapclean kasir,  22=snapclean sticker, 41=member register  )",
			},
			"callback_url": &graphql.Field{
				Type:        graphql.String,
				Description: "Callback URL if empty go to default",
			},
			"channel": &graphql.Field{
				Type:        graphql.String,
				Description: "Channel choose between nobu,xendit",
			},
			"created_at": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "CreatedAt",
			},
		},
	})

var PaylinkQrisInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "paylink_qris_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "ID must be unique, as reference of qris generated",
			},
			"type": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "Qris Type (1= Static, 2= Dynamic) ",
			},
			"amount": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "Amount (Required for Dynamic Type) ",
			},
			"project": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "1=Smartlink, 2=Snapclean ",
			},
			"action": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "11=smartlink kasir, 21=snapclean kasir,  22=snapclean sticker,",
			},
			"user_id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "user id",
			},
			"user_type": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "user type : 1=owner, 2 = customer",
			},
			"idoutlet": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id outlet",
			},
			"callback_url": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "Callback url (ex https://dev-central-kube.smartlink.id/mobile/node-sl-ws/v2)",
			},
			"channel": &graphql.InputObjectFieldConfig{
				Type:         graphql.String,
				DefaultValue: "xendit",
				Description:  "choose between Nobu, xendit",
			},
		},
	})
