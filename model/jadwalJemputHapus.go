package model

import "time"

type JadwalJemputHapus struct {
	Id               string        `json:"id" gorm:"primary_key"`
	IdOwner          string        `json:"idowner"`
	IdCustomer       string        `json:"idcustomer"`
	Idjemput         string        `json:"idjemput"`
	Status           int           `json:"status"`
	Keterangan       string        `json:"keterangan"`
	KeteranganStatus string        `json:"keterangan_status"`
	ConfirmedAt      *time.Time    `json:"confirmed_at"`
	CreatedAt        *time.Time    `json:"created_at" gorm:"default:CURRENT_TIMESTAMP"`
	UpdatedAt        *time.Time    `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
	JadwalJemput     *JadwalJemput `json:"jadwal_jemput" gorm:"foreignkey:idjemput;association_foreignkey:idjemput;references:idjemput"`
}
