package model

import "time"

type TahapanWorkshop struct {
	Idtahapan  int        `json:"idtahapan"`
	Idworkshop string     `json:"idworkshop"`
	CreatedAt  *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP"`
	UpdatedAt  *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
