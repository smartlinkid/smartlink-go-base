package model

import "time"

type DaftarPemrosesanGaji struct {
	Id           int        `json:"id" gorm:"primary_key;index;not null;auto_increment"`
	Idtransaksi  string     `json:"idtransaksi"`
	Idkaryawan   string     `json:"idkaryawan"`
	Idtahapan    int        `json:"idtahapan"`
	Iddetlayanan string     `json:"iddetlayanan"`
	Idlayanan    string     `json:"idlayanan"`
	Idowner      string     `json:"idowner"`
	GajiLama     float64    `json:"gaji_lama"`
	Gaji         float64    `json:"gaji"`
	Idsatuan     int        `json:"idsatuan"`
	Kuantitas    float32    `json:"kuantitas"`
	LongWaktu    int        `json:"long_waktu"`
	Tahapan      *Tahapan   `json:"tahapan" gorm:"foreignkey:idtahapan;association_foreignkey:idtahapan;references:idtahapan"`
	Waktu        *time.Time `json:"waktu"`
	CreatedAt    *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt    *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
