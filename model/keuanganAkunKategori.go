package model

type KeuanganAkunKategori struct {
	Idkategori       string             `json:"idkategori" gorm:"primary_key"`
	Idkelompok       string             `json:"idkelompok"`
	Idowner          string             `json:"idowner" gorm:"primary_key"`
	NamaKategori     string             `json:"nama_kategori"`
	Statis           int                `json:"statis"`
	KeuanganAkunAkun []KeuanganAkunAkun `json:"keuangan_akun_akun" gorm:"foreignkey:idkategori;association_foreignkey:idkategori;references:idkategori"`
	BaseModel
}
