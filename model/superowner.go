package model

import "github.com/graphql-go/graphql"

type Superowner struct {
	Id       string  `json:"id" gorm:"primary_key"`
	Telp     string  `json:"telp"`
	Password string  `json:"password"`
	Owner    []Owner `json:"owner" gorm:"ForeignKey:telp;AssociationForeignKey:telp"`
	BaseModel
}

var SuperownerType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "superowner",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "id superowner",
			},
			"telp": &graphql.Field{
				Type:        graphql.String,
				Description: "telp superowner",
			},
			"owner": &graphql.Field{
				Type:        graphql.NewList(OwnerType),
				Description: "",
			},
		},
		Description: "superowner data",
	})

var SuperownerInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "superowner_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"id": &graphql.InputObjectFieldConfig{
				Type: graphql.String,
			},
			"telp": &graphql.InputObjectFieldConfig{
				Type: graphql.String,
			},
			"owner": &graphql.InputObjectFieldConfig{
				Type:        graphql.NewList(OwnerInput),
				Description: "",
			},
		},
	},
)
