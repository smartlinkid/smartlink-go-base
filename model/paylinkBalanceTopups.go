package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type PaylinkBalanceTopups struct {
	Id                   string     `json:"id" gorm:"primary_key;"`
	UserId               string     `json:"user_id" gorm:"index"`
	UserType             int        `json:"user_type"`
	Qrcode               string     `json:"qrcode" gorm:"index"`
	Nominal              float64    `json:"nominal"`
	Diskon               float64    `json:"diskon"`
	KodeUnik             float64    `json:"kode_unik" gorm:"index"`
	Total                float64    `json:"total"`
	JenisBayar           int        `json:"jenis_bayar"`
	IdkasOwner           string     `json:"idkas_owner" gorm:"index"`
	RekeningPerusahaanId string     `json:"rekening_perusahaan_id" gorm:"index"`
	KeteranganBayar      string     `json:"keterangan_bayar"`
	Status               int        `json:"status"`
	StatusChangedAt      *time.Time `json:"status_changed_at"`
	ExpiredAt            *time.Time `json:"expired_at" `
	CreatedAt            *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt            *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	Hapus                *int       `json:"hapus" gorm:"default:0"`
	Owner                *Owner     `json:"owner" gorm:"foreignKey:idowner;association_foreignkey:user_id;references:user_id"`
}

var PaylinkBalanceTopupsObject = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "topup",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "id",
			},
			"user_id": &graphql.Field{
				Type:        graphql.String,
				Description: "user id",
			},
			"user_type": &graphql.Field{
				Type:        graphql.Int,
				Description: "user type : 1=owner, 2 = customer",
			},
			"qrcode": &graphql.Field{
				Type:        graphql.String,
				Description: "qrcode",
			},
			"nominal": &graphql.Field{
				Type:        graphql.Float,
				Description: "nominal",
			},
			"diskon": &graphql.Field{
				Type:        graphql.Float,
				Description: "diskon",
			},
			"kode_unik": &graphql.Field{
				Type:        graphql.Float,
				Description: "kode_unik",
			},
			"total": &graphql.Field{
				Type:        graphql.Float,
				Description: "total",
			},
			"jenis_bayar": &graphql.Field{
				Type:        graphql.Int,
				Description: "jenis bayar : 1 = cash,2 = transfer",
			},
			"idkas_owner": &graphql.Field{
				Type:        graphql.String,
				Description: "id akun kas",
			},
			"RekeningPerusahaanId": &graphql.Field{
				Type:        graphql.String,
				Description: "RekeningPerusahaanId",
			},
			"keterangan_bayar": &graphql.Field{
				Type:        graphql.String,
				Description: "keterangan_bayar",
			},
			"expired_at": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "Expired",
			},
			"created_at": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "created_at",
			},
			"status": &graphql.Field{
				Type:        graphql.Int,
				Description: "0=belum dibayar, 1=sudah konfirmasi bayar, 2=sudah diverifikasi, 3 = dibatalkan",
			},
			"owner": &graphql.Field{
				Type:        OwnerType,
				Description: "0=belum dibayar, 1=sudah konfirmasi bayar, 2=sudah diverifikasi, 3 = dibatalkan",
			},
		},
	})

var PaylinkBalanceTopupInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "topup_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"user_id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "user id",
			},
			"user_type": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "user type : 1=owner, 2 = customer",
			},
			"qrcode": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "qrcode",
			},
			"nominal": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "nominal",
			},
			"diskon": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "diskon",
			},
			"kode_unik": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "kode_unik",
			},
			"total": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "total",
			},
			"jenis_bayar": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "jenis bayar : 1 = cash,2 = transfer",
			},
			"idkas_owner": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id akun kas owner",
			},
			"status": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "0=belum dibayar, 1=sudah konfirmasi bayar, 2=sudah diverifikasi, 3 = dibatalkan",
			},
		},
	})

var PaylinkBalanceTopupSuccess = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "paylink_balance_topup_success",
		Fields: graphql.InputObjectConfigFieldMap{
			"id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "topup id",
			},
			"mode": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "string mode ignore-expired",
			},
			"rekening_perusahaan_id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "rekening_perusahaan_id",
			},
			"keterangan_bayar": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "keterangan_bayar",
			},
		},
	})
