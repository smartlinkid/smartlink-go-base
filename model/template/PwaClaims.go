package template

import (
	"bitbucket.org/smartlinkid/smartlink-go-base/model"
	"github.com/dgrijalva/jwt-go"
)

type PwaClaims struct {
	model.CustomerGlobal
	jwt.StandardClaims
}
