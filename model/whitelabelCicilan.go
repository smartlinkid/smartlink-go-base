package model

import "time"

type WhitelabelCicilan struct {
	Id             string    `json:"id" gorm:"primary_key"`
	OwnerIdowner   string    `json:"owner_idowner"`
	PesananId      string    `json:"pesanan_id"`
	CicilanKe      int       `json:"cicilan_ke"`
	CicilanNominal float64   `json:"cicilan_nominal"`
	Status         string    `json:"status"` // 10_lunas 20_belum_bayar 30_pembayaran_expired
	ExpiredAt      time.Time `json:"expired_at"`
	CicilanTagihan float64   `json:"cicilan_tagihan"`
	Owner          *Owner    `json:"owner" gorm:"foreignkey:idowner;association_foreignkey:owner_idowner;references:owner_idowner"`
	BaseModel
}
