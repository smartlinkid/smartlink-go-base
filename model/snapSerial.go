package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type SnapSerial struct {
	Serial      string     `json:"serial" gorm:"primary_key;index;not null"`
	IdsnapMesin string     `json:"idsnap_mesin" gorm:"index"`
	RegAt       *time.Time `json:"reg_at"`
	IdsnapTarif int        `json:"idsnap_tarif" gorm:"index"`
	TotalClaim  int        `json:"total_claim"`
	Garansi     int        `json:"garansi"`
	Harga       float64    `json:"harga"`
	ActivatedAt *time.Time `json:"activated_at"`
	Idowner     string     `json:"idowner"`
	IdsnapBeli  string     `json:"idsnap_beli"`
	ExpiredSewa *time.Time `json:"expired_sewa"`
	SnapMesin   SnapMesin  `json:"snap_mesin" gorm:"foreignKey:idsnap_mesin;association_foreignkey:id;references:id"`
	SnapTarif   SnapTarif  `json:"snap_tarif" gorm:"foreignKey:idsnap_tarif;association_foreignkey:id;references:id"`
	CreatedAt   *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt   *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	Hapus       *int       `json:"hapus" gorm:"default:0"`
}

var SnapSerialType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "snap_serial",
		Fields: graphql.Fields{
			"serial": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"idsnap_mesin": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"reg_at": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"idsnap_tarif": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"total_claim": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"garansi": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"activated_at": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "",
			},
			"idowner": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"snap_mesin": &graphql.Field{
				Type:        SnapMesinType,
				Description: "",
			},
			"snap_tarif": &graphql.Field{
				Type:        SnapTarifType,
				Description: "",
			},
		},
		Description: "",
	})

var SnapSerialInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "snap_serial_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"serial": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "",
			},
			"idsnap_mesin": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "",
			},
			"reg_at": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "",
			},
			"idsnap_tarif": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "",
			},
			"total_claim": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "",
			},
			"garansi": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "",
			},
			"activated_at": &graphql.InputObjectFieldConfig{
				Type:        graphql.DateTime,
				Description: "",
			},
			"idowner": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "",
			},
			"snap_mesin": &graphql.InputObjectFieldConfig{
				Type:        SnapMesinInput,
				Description: "",
			},
			"snap_tarif": &graphql.InputObjectFieldConfig{
				Type:        SnapTarifInput,
				Description: "",
			},
		},
		Description: "",
	})
