package model

type Tags struct {
	Idtags       string `json:"idtags" gorm:"primary_key"`
	OwnerIdowner string `json:"owner_idowner"`
	Nama         string `json:"nama"`
}
