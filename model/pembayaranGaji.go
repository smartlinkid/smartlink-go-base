package model

import "time"

type PembayaranGaji struct {
	Id                         string                       `json:"id" gorm:"primary_key;index;not null"`
	Idowner                    string                       `json:"idowner"`
	Idkaryawan                 string                       `json:"idkaryawan"`
	Judul                      string                       `json:"judul"`
	TanggalAwal                *time.Time                   `json:"tanggal_awal"`
	TanggalAkhir               *time.Time                   `json:"tanggal_akhir"`
	TanggalBayar               *time.Time                   `json:"tanggal_bayar"`
	Keterangan                 string                       `json:"keterangan"`
	JumlahKehadiran            float64                      `json:"jumlah_kehadiran"`
	JumlahLembur               float64                      `json:"jumlah_lembur"`
	JumlahTerlambat            float64                      `json:"jumlah_terlambat"`
	NominalTerlambat           float64                      `json:"nominal_terlambat"`
	SubtotalGaji               float64                      `json:"subtotal_gaji"`
	Komisi                     float64                      `json:"komisi"`
	KomisiSales                float64                      `json:"komisi_sales"`
	Kasbon                     float64                      `json:"kasbon"`
	Potongan                   float64                      `json:"potongan"`
	Total                      float64                      `json:"total"`
	Idakun                     string                       `json:"idakun"`
	Idoutlet                   string                       `json:"idoutlet"`
	RefType                    string                       `json:"ref_type"` // outlet atau workshop
	Karyawan                   *Karyawan                    `json:"karyawan" gorm:"index;foreignKey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	DetailPembayaranGajiKomisi []DetailPembayaranGajiKomisi `json:"detail_pembayaran_gaji_komisi" gorm:"index;foreignKey:idpembayaran_gaji;association_foreignkey:id;references:id"`
	DetailPembayaranGajiJenis  []DetailPembayaranGajiJenis  `json:"detail_pembayaran_gaji_jenis" gorm:"index;foreignKey:idpembayaran_gaji;association_foreignkey:id;references:id"`
	PotonganBayar              []PotonganBayar              `json:"potongan_bayar" gorm:"index;foreignKey:idpembayaran_gaji;association_foreignkey:id;references:id"`
	KasbonBayar                []KasbonBayar                `json:"kasbon_bayar" gorm:"index;foreignKey:idpembayaran_gaji;association_foreignkey:id;references:id"`
	DetailPembayaranGajiUpah   []DetailPembayaranGajiUpah   `json:"detail_pembayaran_gaji_upah" gorm:"index;foreignKey:idpembayaran_gaji;association_foreignkey:id;references:id"`
	DetailPembayaranGajiSales  *DetailPembayaranGajiSales   `json:"detail_pembayaran_gaji_sales" gorm:"index;foreignKey:idpembayaran_gaji;association_foreignkey:id;references:id"`
	KeuanganAkunAkun           *KeuanganAkunAkun            `json:"keuangan_akun_akun" gorm:"index;foreignKey:idakun;association_foreignkey:idakun;references:idakun"`
	BaseModel
}
