package model

type BiayaKategori struct {
	Id        int         `json:"id" gorm:"primary_key;index;not null;auto_increment"`
	Idakun    string      `json:"idakun"`
	Nama      string      `json:"nama"`
	BiayaItem []BiayaItem `json:"biaya_item" gorm:"foreignkey:idbiaya_kategori;association_foreignkey:id;references:id"`
	BaseModel
}
