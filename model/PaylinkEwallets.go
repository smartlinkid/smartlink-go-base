package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type PaymentRequests struct {
	ReferenceId  string     `json:"reference_id" gorm:"primary_key;"`
	PaymentChannel  string     `json:"payment_channel"`
	PaymentType  string     `json:"payment_type"`
	Status       string     `json:"status"`
	Amount       float64     `json:"amount"`
	CheckoutUrl  string     `json:"checkout_url"`
	RedirectUrl  string     `json:"redirect_url"`
	FailedReason string     `json:"failed_reason"`
	Phone        string     `json:"phone"`
	UserId       string     `json:"user_id" gorm:"index"`
	UserType     int        `json:"user_type"`
	CreatedAt    *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt    *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	Hapus        *int       `json:"hapus" gorm:"default:0"`
}

var PaylinkEwalletsObject = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "paylink_request",
		Fields: graphql.Fields{
			"reference_id": &graphql.Field{
				Type:        graphql.String,
				Description: "reference_id ",
			},
			"user_id": &graphql.Field{
				Type:        graphql.String,
				Description: "user id",
			},
			"user_type": &graphql.Field{
				Type:        graphql.Int,
				Description: "user type : 1=owner, 2 = customer",
			},
			"payment_type": &graphql.Field{
				Type:        graphql.String,
				Description: "payment type",
			},
			"status": &graphql.Field{
				Type:        graphql.String,
				Description: "status",
			},
			"amount": &graphql.Field{
				Type:        graphql.Float,
				Description: "Amount",
			},
			"phone": &graphql.Field{
				Type:        graphql.String,
				Description: "phone",
			},
			"checkout_url": &graphql.Field{
				Type:        graphql.String,
				Description: "checkout_url",
			},
			"failed_reason": &graphql.Field{
				Type:        graphql.String,
				Description: "failed_reason",
			},
			"created_at": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "CreatedAt",
			},
		},
	})


var PaylinkEwalletInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "checkout_ewallet_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"reference_id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "Reference id from transaction must be unique",
			},
			"user_id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "User Id ",
			},
			"user_type": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "User Type , 1= owner, 2= customer ",
			},
			"amount": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "Amount ",
			},
			"payment_type": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "choose between OVO, DANA, LINKAJA ",
			},
		},
	})


