package model

type DetailPembelianDeposit struct {
	PembelianDepositIdpembelianDeposit string        `json:"pembelian_deposit_idpembelian_deposit" gorm:"primary_key"`
	Id                                 string        `json:"id" gorm:"primary_key"`
	DataDepositIddataDeposit           string        `json:"data_deposit_iddata_deposit" gorm:"primary_key"`
	Jumlah                             int           `json:"jumlah"`
	Harga                              float64       `json:"harga"`
	HargaTotal                         float64       `json:"harga_total"`
	Keterangan                         string        `json:"keterangan"`
	QuotaAwal                          float64       `json:"quota_awal"`
	QuotaAkhir                         float64       `json:"quota_akhir"`
	QuotaHapus                         float64       `json:"quota_hapus"`
	MasaAktifAwal                      int           `json:"masa_aktif_awal"`
	MasaAktifAkhir                     int           `json:"masa_aktif_akhir"`
	Deposit                            *DataDeposit2 `json:"outlet" gorm:"foreignkey:iddata_deposit;association_foreignkey:data_deposit_iddata_deposit;references:data_deposit_iddata_deposit"`
	BaseModel
}
