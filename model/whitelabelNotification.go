package model

import "time"

type WhitelabelNotification struct {
	Id               string     `json:"id"`
	Idowner          string     `json:"idowner"`
	Idcustomer       string     `json:"idcustomer"`
	Title            string     `json:"title"`
	Body             string     `json:"body"`
	NotificationType string     `json:"notification_type"`
	RelatedId        string     `json:"related_id"`
	RelatedTable     string     `json:"related_table"`
	ReadAt           *time.Time `json:"read_at"`
	CreatedAt        *time.Time `json:"created_at"`
	UpdatedAt        *time.Time `json:"updated_at"`
}
