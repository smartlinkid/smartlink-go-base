package model

import "time"

type WaBlast struct {
	Id                  int64                 `json:"id" gorm:"primary_key;auto_increment"`
	Idowner             string                `json:"idowner" gorm:"index"`
	Text                string                `json:"text"`
	Total               int                   `json:"total"`
	Interval            int                   `json:"interval"`
	FinishedAt          *time.Time            `json:"finished_at"`
	Running             int                   `json:"running"`
	CreatedAt           *time.Time            `json:"created_at"`
	UpdatedAt           *time.Time            `json:"updated_at"`
	Hapus               int                   `json:"hapus"`
	Idoutlet            string                `json:"idoutlet"`
	WaBlastData         []WaBlastData         `json:"wa_blast_data" gorm:"foreignkey:idwa_blast;association:id"`
	WaBlastUsedTemplate []WaBlastUsedTemplate `json:"wa_blast_used_template" gorm:"foreignkey:idwa_blast;association:id"`
	WaBlastDevice       []WaBlastDevice       `json:"wa_blast_device" gorm:"foreignkey:idwa_blast;association:id"`
	StopMsg             string                `json:"stop_msg"`
}
