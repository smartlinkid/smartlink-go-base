package model

import "time"

type PengaturanGaji struct {
	Id         int        `json:"id" gorm:"auto_increment;primary_key"`
	Idjenis    int        `json:"idjenis"`
	Idowner    string     `json:"idowner"`
	Idkaryawan string     `json:"idkaryawan"`
	Nominal    float64    `json:"nominal"`
	Karyawan   *Karyawan  `json:"karyawan" gorm:"foreignkey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	JenisGaji  *JenisGaji `json:"jenis_gaji" gorm:"foreignkey:id;association_foreignkey:idjenis;references:idjenis"`
	CreatedAt  *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP"`
	UpdatedAt  *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
