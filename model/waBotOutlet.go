package model

import "time"

type WaBotOutlet struct {
	Idowner    string      `json:"idowner"`
	Idoutlet   string      `json:"idoutlet"`
	IdwaDevice string      `json:"idwa_device"`
	WaBotPromo *WaBotPromo `json:"wa_bot_promo" gorm:"foreignKey:idoutlet;association_foreignkey:idoutlet;references:idoutlet"`
	CreatedAt  *time.Time  `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt  *time.Time  `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
}
