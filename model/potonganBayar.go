package model

import "time"

type PotonganBayar struct {
	Id               int        `json:"id" gorm:"primary_key;index;not null;auto_increment"`
	Idpotongan       int        `json:"idpotongan"`
	Idkaryawan       string     `json:"idkaryawan"`
	IdpembayaranGaji string     `json:"idpembayaran_gaji"`
	Idowner          string     `json:"idowner"`
	Nominal          float64    `json:"nominal"`
	SisaSaldo        float64    `json:"sisa_saldo"`
	Potongan         *Potongan  `json:"potongan" gorm:"index;foreignKey:id;association_foreignkey:idpotongan;references:idpotongan"`
	WaktuPembayaran  *time.Time `json:"waktu_pembayaran"`
	BaseModel
}
