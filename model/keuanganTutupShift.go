package model

import "time"

type KeuanganTutupShift struct {
	Idtransfer       int        `json:"idtransfer" gorm:"primary_key"`
	Idowner          string     `json:"idowner"`
	Idkaryawan       string     `json:"idkaryawan"`
	CreatedAt        *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	CreatedAtLong    int64      `json:"created_at_long"`
	TransferDate     *time.Time `json:"transfer_date"`
	TransferDateLong int64      `json:"transfer_date_long"`
	UpdatedAt        *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
	Data             string     `json:"data"`
}
