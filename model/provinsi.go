package model

import "github.com/graphql-go/graphql"

type Provinsi struct {
	Id       int    `json:"id" gorm:"primary_key;index"`
	Nama     string `json:"nama"`
	NegaraId int    `json:"negara_id"`
}

var ProvinsiType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "provinsi",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"nama": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"negara_id": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
		},
	})
