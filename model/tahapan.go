package model

type Tahapan struct {
	Idtahapan int    `json:"idtahapan" gorm:"primary_key"`
	Nama      string `json:"nama"`
	Statis    int    `json:"statis"`
	Setelah   int    `json:"setelah"`
	BaseModel
}
