package model

import "time"

type HistorySaldoAfiliasi struct {
	Id          int       `json:"id" gorm:"index;primary_key;not null"`
	Debet       float64   `json:"debet"`
	Kredit      float64   `json:"kredit"`
	SaldoAkhir  float64   `json:"saldo_akhir"`
	Keterangan  string    `json:"keterangan"`
	Idowner     string    `json:"idowner"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
	RelatedType string    `json:"related_type"`
	RelatedId   string    `json:"related_id"`
}
