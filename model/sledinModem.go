package model

type SledinModem struct {
	Idmodem    string `json:"idmodem" gorm:"primary_key"`
	Nama       string `json:"nama"`
	Nomor      string `json:"nomor"`
	Keterangan string `json:"keterangan"`
	Lick       int64  `json:"lick"`
	Active     int    `json:"active"`
	Touch      int64  `json:"touch"`
	BaseModel
}
