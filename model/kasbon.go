package model

import "time"

type Kasbon struct {
	Id                   int               `json:"id" gorm:"primary_key;index;not null;auto_increment"`
	Idowner              string            `json:"idowner"`
	Idkaryawan           string            `json:"idkaryawan"`
	Idakun               string            `json:"idakun"`
	Waktu                *time.Time        `json:"waktu"`
	Nominal              float64           `json:"nominal"`
	Keterangan           string            `json:"keterangan"`
	RefId                string            `json:"ref_id"`
	Saldo                float64           `json:"saldo"`
	StatusPelunasan      int               `json:"status_pelunasan"` //0 belum dibayar, 1 lunas, 2 cicil
	StatusPengajuan      int               `json:"status_pengajuan"`
	TanggalKonfirmasi    *time.Time        `json:"tanggal_konfirmasi"`
	KeteranganKonfirmasi string            `json:"keterangan_konfirmasi"`
	CreatedBy            string            `json:"created_by"`
	RelatedType          string            `json:"related_type"`
	RelatedId            string            `json:"related_id"`
	KeuanganAkunAkun     *KeuanganAkunAkun `json:"keuangan_akun_akun" gorm:"foreignKey:idakun;association_foreignkey:idakun;references:idakun"`
	Karyawan             *Karyawan         `json:"karyawan" gorm:"foreignKey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	KaryawanCreated      *Karyawan         `json:"karyawan_created" gorm:"foreignKey:idkaryawan;association_foreignkey:created_by;references:created_by"`
	KasbonBayar          []KasbonBayar     `json:"kasbon_bayar" gorm:"foreignKey:idkasbon;association_foreignkey:id;references:id"`
	BaseModel
}
