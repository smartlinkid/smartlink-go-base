package model

import "time"

type ResumePotongan struct {
	Id         int        `json:"id" gorm:"primary_key;index;not null;auto_increment"`
	Idkaryawan string     `json:"idkaryawan"`
	Idowner    string     `json:"idowner"`
	Nominal    float64    `json:"nominal"`
	Potongan   []Potongan `json:"potongan" gorm:"foreignkey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	Karyawan   *Karyawan  `json:"karyawan" gorm:"foreignkey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	CreatedAt  *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt  *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
