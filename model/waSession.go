package model

type WaSession struct {
	Idsession int    `json:"idsession" gorm:"primary_key;index;auto_increment"`
	Iddevice  string `json:"iddevice" gorm:"index"`
	Idowner   string `json:"idowner"`
	Session   string `json:"session"`
	Reason    string `json:"reason"`
	BaseModel
}
