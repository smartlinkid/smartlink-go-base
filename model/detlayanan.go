package model

import "time"

type Detlayanan struct {
	TransaksiIdtransaksi string           `json:"transaksi_idtransaksi" gorm:"column:transaksi_idtransaksi" gorm:"index"`
	LayananIdlayanan     string           `json:"layanan_idlayanan" gorm:"index"`
	NamaLayanan          string           `json:"nama_layanan"`
	Id                   string           `json:"id" gorm:"id;primary_key"`
	JumlahBeli           float32          `gorm:"column:jumlah_beli"`
	Harga                float64          `json:"harga"`
	Hargatotal           float64          `json:"hargatotal"`
	LuasP                float64          `json:"luas_p"`
	LuasL                float64          `json:"luas_l"`
	LuasQ                float64          `json:"luas_q"`
	Keterangan           string           `json:"keterangan"`
	Iddeposit            string           `json:"iddeposit" gorm:"index"`
	Posisi               int              `json:"posisi"`
	Idworkshop           string           `json:"idworkshop" gorm:"index"`
	IsWsot               int              `json:"is_wsot"`
	CreatedAt            *time.Time       `json:"created_at"`
	UpdatedAt            *time.Time       `json:"updated_at"`
	TotalBersih          string           `json:"total_bersih"`
	TotalMinOrder        float64          `json:"total_min_order"`
	KondisiBarang        string           `json:"kondisi_barang"`
	TotalLoad            int              `json:"total_load"`
	Layanan              *Layanan         `json:"data_layanan" gorm:"foreignkey:idlayanan;association_foreignkey:layanan_idlayanan;references:layanan_idlayanan"`
	HargaLayanan         *HargaLayanan    `json:"harga_layanan" gorm:"foreignkey:layanan_idlayanan;association_foreignkey:layanan_idlayanan;references:layanan_idlayanan"`
	DetlayananSnap       []DetlayananSnap `json:"detlayanan_snap" gorm:"foreignkey:iddetlayanan;association_foreignkey:id;references:id"`
	Detitem              []Detitem        `json:"detitem" gorm:"foreignkey:iddetlayanan;association_foreignkey:id;references:id"`
}
