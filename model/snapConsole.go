package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type SnapConsole struct {
	Id               string             `json:"id" gorm:"index;primary_key;not null"`
	Idowner          string             `json:"idowner" gorm:"index"`
	Idoutlet         string             `json:"idoutlet" gorm:"index"`
	Pin              string             `json:"pin"`
	Jenis            int                `json:"jenis"` // 1 device 2 stack 3 group
	SnapConsoleMesin []SnapConsoleMesin `json:"snap_console_mesin" gorm:"foreignKey:idsnap_console;association_foreignkey:id;references:id"`
	Outlet           Outlet             `json:"outlet" gorm:"foreignKey:idoutlet;association_foreignkey:idoutlet;references:idoutlet"`
	CreatedAt        *time.Time         `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt        *time.Time         `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	Hapus            *int               `json:"hapus" gorm:"default:0"`
}

var SnapConsoleType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "snap_console",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "id mesin snap",
			},
			"idowner": &graphql.Field{
				Type:        graphql.String,
				Description: "id owner snap",
			},
			"idoutlet": &graphql.Field{
				Type:        graphql.String,
				Description: "id outlet snap",
			},
			"jenis": &graphql.Field{
				Type:        graphql.Int,
				Description: "jenis mesin cosnole. 1: device 2: stack 3:group",
			},
			"outlet": &graphql.Field{
				Type:        OutletType,
				Description: "outlet",
			},
			"snap_console_mesin": &graphql.Field{
				Type:        SnapConsoleMesinType,
				Description: "outlet",
			},

			//"base": &graphql.Field{
			//	Type:        BaseModelType,
			//	Description: "base model",
			//},
		},
		Description: "snap mesin data",
	})

var SnapConsoleInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "snap_console_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id console",
			},
			"idowner": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id owner snap",
			},
			"idoutlet": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id outlet snap",
			},
			"pin": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "pin",
			},
			"jenis": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "jenis mesin cosnole. 1: device 2: stack 3:group",
			},
			"mesins": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "List Mesin",
			},
		},
	})
