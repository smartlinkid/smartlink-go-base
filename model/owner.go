package model

import "time"
import "github.com/graphql-go/graphql"

type Owner struct {
	Idowner             string          `json:"idowner" gorm:"primary_key"`
	Nama                string          `json:"nama"`
	Email               string          `json:"email"`
	Password            string          `json:"-"`
	Kota                string          `json:"kota"`
	Telp                string          `json:"telp"`
	Salt                string          `json:"salt"`
	RememberToken       string          `json:"remember_token"`
	VerifiedEmail       int             `json:"verified_email"`
	VerifiedTelp        int             `json:"verified_telp"`
	KodeVerifikasiTelp  string          `json:"kode_verifikasi_telp"`
	LevelAfiliasi       int             `json:"level_afiliasi"`
	KodeAfiliasi        string          `json:"kode_afiliasi"`
	KodeAfiliator       string          `json:"kode_afiliator"`
	PersentaseAfiliator int             `json:"persentase_afiliator"`
	SaldoAfiliator      float32         `json:"saldo_afiliator"`
	TutorNumber         int             `json:"tutor_number"`
	DataGenerated       int             `json:"data_generated"`
	ExpiryTrialDate     *time.Time      `json:"expiry_trial_date"`
	Active              int             `json:"active"`
	LastLogin           *time.Time      `json:"last_login"`
	EmailReminders      string          `json:"email_reminders"`
	Versi               string          `json:"versi"`
	DateReset           *time.Time      `json:"date_reset"`
	SmsPremium          int             `json:"sms_premium"`
	UploadToken         string          `json:"upload_token"`
	LastKoinNotif       int             `json:"last_koin_notif"`
	NamaUsaha           string          `json:"nama_usaha"`
	Idfotoprofil        string          `json:"idfotoprofil"`
	LogoUrl             string          `json:"logo_url"`
	IsPremium           int             `json:"is_premium"`
	Walkthrough         int             `json:"walkthrough"`
	Outlet              []Outlet        `json:"outlet" gorm:"foreignKey:owner_idowner;association_foreignkey:idowner;references:idowner"`
	KotaName            Kota            `json:"kota_name" gorm:"foreignKey:kota;association_foreignkey:id;references:id"`
	OwnerPremium        *OwnerPremium   `json:"owner_premium" gorm:"foreignKey:idowner;association_foreignkey:idowner;references:idowner"`
	Superowner          *Superowner     `json:"superowner" gorm:"foreignKey:telp;association_foreignkey:telp;references:telp"`
	OwnerTabungan       []OwnerTabungan `json:"owner_tabungan" gorm:"foreignKey:idowner;association_foreignkey:idowner;references:idowner"`
	BaseModel
}

var OwnerType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "owner",
		Fields: graphql.Fields{
			"idowner": &graphql.Field{
				Type:        graphql.String,
				Description: "id owner",
			},
			"nama": &graphql.Field{
				Type:        graphql.String,
				Description: "nama owner",
			},
			"email": &graphql.Field{
				Type:        graphql.String,
				Description: "email owner",
			},
			"kota": &graphql.Field{
				Type:        graphql.String,
				Description: "kota owner",
			},
			"telp": &graphql.Field{
				Type:        graphql.String,
				Description: "nomor handphone owner",
			},
			"verified_email": &graphql.Field{
				Type:        graphql.Int,
				Description: "0: email belum terverifikasi 1: email telah terverifikasi",
			},
			"verified_telp": &graphql.Field{
				Type:        graphql.Int,
				Description: "0: nomor hanphone belum terverifikasi 1: nomor hanphone telah terverifikasi",
			},
			"kode_verifikasi_telp": &graphql.Field{
				Type:        graphql.String,
				Description: "kode verifikasi nomor hanphone",
			},
			"level_afiliasi": &graphql.Field{
				Type:        graphql.Int,
				Description: "level afiliasi owner",
			},
			"kode_afiliator": &graphql.Field{
				Type:        graphql.String,
				Description: "kode afiliator owner",
			},
			"persentase_afiliator": &graphql.Field{
				Type:        graphql.Int,
				Description: "presentase affiliator owner",
			},
			"saldo_afiliator": &graphql.Field{
				Type:        graphql.Float,
				Description: "saldo affiliator owner",
			},
			"tutor_number": &graphql.Field{
				Type:        graphql.Int,
				Description: "step terakhir yang dijalankan owner pada tutorial bos",
			},
			"data_generated": &graphql.Field{
				Type:        graphql.Int,
				Description: "0: data belum digenerate 1: data telah digenerate",
			},
			"expiry_trial_date": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "tanggal trial bos berakhir",
			},
			"active": &graphql.Field{
				Type:        graphql.Int,
				Description: "status aktivasi bos",
			},
			"last_login": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "waktu bos terakhir login",
			},
			"email_reminders": &graphql.Field{
				Type:        graphql.String,
				Description: "email reminders owner",
			},
			"versi": &graphql.Field{
				Type:        graphql.String,
				Description: "versi bos yang dipakai owner",
			},
			"date_reset": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "tanggal reset bos",
			},
			"sms_premium": &graphql.Field{
				Type:        graphql.Int,
				Description: "status aktivasi sms premium",
			},
			"upload_token": &graphql.Field{
				Type:        graphql.String,
				Description: "upload token",
			},
			"last_koin_notif": &graphql.Field{
				Type:        graphql.Int,
				Description: "last koin notif",
			},
			"idfotoprofil": &graphql.Field{
				Type:        graphql.String,
				Description: "link foto profil owner",
			},
			"is_premium": &graphql.Field{
				Type:        graphql.Int,
				Description: "premium",
			},
			"outlet": &graphql.Field{
				Type: graphql.NewList(OutletType),
			},
			//"base": &graphql.Field{
			//	Type:        BaseModelType,
			//	Description: "base model",
			//},
		},
		Description: "Data owner",
	})

var OwnerInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "owner_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"idowner": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id owner",
			},
			"nama": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "nama owner",
			},
			"email": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "email owner",
			},
			"kota": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "kota owner",
			},
			"telp": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "nomor handphone owner",
			},
			"verified_email": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "0: email belum terverifikasi 1: email telah terverifikasi",
			},
			"verified_telp": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "0: nomor hanphone belum terverifikasi 1: nomor hanphone telah terverifikasi",
			},
			"kode_verifikasi_telp": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "kode verifikasi nomor hanphone",
			},
			"level_afiliasi": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "level afiliasi owner",
			},
			"kode_afiliator": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "kode afiliator owner",
			},
			"persentase_afiliator": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "presentase affiliator owner",
			},
			"saldo_afiliator": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "saldo affiliator owner",
			},
			"tutor_number": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "step terakhir yang dijalankan owner pada tutorial bos",
			},
			"data_generated": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "0: data belum digenerate 1: data telah digenerate",
			},
			"expiry_trial_date": &graphql.InputObjectFieldConfig{
				Type:        graphql.DateTime,
				Description: "tanggal trial bos berakhir",
			},
			"active": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "status aktivasi bos",
			},
			"last_login": &graphql.InputObjectFieldConfig{
				Type:        graphql.DateTime,
				Description: "waktu bos terakhir login",
			},
			"email_reminders": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "email reminders owner",
			},
			"versi": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "versi bos yang dipakai owner",
			},
			"date_reset": &graphql.InputObjectFieldConfig{
				Type:        graphql.DateTime,
				Description: "tanggal reset bos",
			},
			"sms_premium": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "status aktivasi sms premium",
			},
			"upload_token": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "upload token",
			},
			"last_koin_notif": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "last koin notif",
			},
			"idfotoprofil": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "link foto profil owner",
			},
		},
	},
)
