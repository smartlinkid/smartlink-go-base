package model

import "time"

type Sapaan struct {
	Id         int        `json:"id" gorm:"primary_key"`
	Sapaan     string     `json:"sapaan"`
	TipeSapaan int        `json:"tipe_sapaan"`
	CreatedAt  *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt  *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
