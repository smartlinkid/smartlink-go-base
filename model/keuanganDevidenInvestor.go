package model

import "time"

type KeuanganDevidenInvestor struct {
	Id           int64      `json:"id" gorm:"primary_key;index"`
	Idinvestor   string     `json:"idinvestor" gorm:"index"`
	IdakunDebet  string     `json:"idakun_debet" gorm:"index"`
	IdakunKredit string     `json:"idakun_kredit" gorm:"index"`
	Nominal      float64    `json:"nominal"`
	Waktu        int64      `json:"waktu" gorm:"index"`
	Keterangan   string     `json:"keterangan"`
	Idoutlet     string     `json:"idoutlet" gorm:"index"`
	Idowner      string     `json:"idowner" gorm:"index"`
	PeriodeAwal  *time.Time `json:"periode_awal" gorm:"index"`
	PeriodeAkhir *time.Time `json:"periode_akhir" gorm:"index"`
	Outlet       Outlet     `json:"outlet" gorm:"foreignkey:idoutlet;association_foreignkey:idoutlet;references:idoutlet"`
	Owner        Owner      `json:"owner" gorm:"foreignkey:idowner;association_foreignkey:idowner;references:idowner"`
	Investor     Investor   `json:"investor" gorm:"foreignkey:idinvestor;association_foreignkey:id;references:id"`
	CreatedAt    *time.Time `json:"created_at" gorm:"index"`
	UpdatedAt    *time.Time `json:"updated_at"`
	Hapus        int        `json:"hapus"`
}
