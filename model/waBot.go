package model

import "time"

type WaBot struct {
	IdwaDevice       *string    `json:"idwa_device"`
	Idowner          string     `json:"idowner"`
	Keyword          string     `json:"keyword"`
	WelcomeTemplate  string     `json:"welcome_template"`
	PromoTemplate    string     `json:"promo_template"`
	AutoRenew        int64      `json:"auto_renew"` // 0 tidak aktif, 1 aktif
	LastRenew        *time.Time `json:"last_renew"`
	Active           int64      `json:"active"` // 0 tidak aktif, 1 aktif
	ExpiredAt        *time.Time `json:"expired_at"`
	CreatedAt        *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt        *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	CountWaBotOutlet int        `json:"count_wa_bot_outlet"`
}
