package model

import "time"

type QrisGagal struct {
	Id              int             `json:"id" gorm:"primary_key"`
	RefTransaksi    string          `json:"ref_transaksi"`
	IdTransaksi     string          `json:"id_transaksi"`
	Idowner         string          `json:"idowner"`
	Idoutlet        string          `json:"idoutlet"`
	Idkaryawan      string          `json:"idkaryawan"`
	Total           float64         `json:"total"`
	Keterangan      string          `json:"keterangan"`
	Idcustomer      string          `json:"idcustomer"`
	NamaCustomer    string          `json:"nama_customer"`
	JenisCustomer   int             `json:"jenis_customer"`
	Telp            string          `json:"telp"`
	WaktuPembayaran *time.Time      `json:"waktu_pembayaran"`
	IdakunRefund    string          `json:"idakun_refund"`
	WaktuRefund     *time.Time      `json:"waktu_refund"`
	Status          string          `json:"status"`
	RecordId        string          `json:"record_id"`
	RrnFormat       string          `json:"rrn_format"`
	RrnFull         string          `json:"rrn_full"`
	TanggalInject   *time.Time      `json:"tanggal_inject"`
	AlasanTolak     string          `json:"alasan_tolak"`
	CustomerGlobal  *CustomerGlobal `json:"customer_global" gorm:"foreignkey:id;association_foreignkey:idcustomer;references:idcustomer"`
	SnapMesin       *SnapMesin      `json:"snap_mesin" gorm:"foreignkey:id;association_foreignkey:id_transaksi;references:id_transaksi"`
	Outlet          *Outlet         `json:"outlet" gorm:"foreignkey:idoutlet;association_foreignkey:idoutlet;references:idoutlet"`
	Owner           *Owner          `json:"owner" gorm:"foreignkey:idowner;association_foreignkey:idowner;references:idowner"`
	BaseModel
}
