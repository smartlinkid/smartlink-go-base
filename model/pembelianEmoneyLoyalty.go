package model

import "time"

type PembelianEmoneyLoyalty struct {
	IdpembelianEmoney string     `json:"idpembelian_emoney"`
	Idowner           string     `json:"idowner"`
	Idoutlet          string     `json:"idoutlet"`
	Hapus             int        `json:"hapus" gorm:"default:0"`
	CreatedAt         *time.Time `json:"created_at"`
}
