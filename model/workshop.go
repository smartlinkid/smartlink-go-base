package model

type Workshop struct {
	Idworkshop      string            `json:"idworkshop" gorm:"primary_key"`
	OwnerIdowner    string            `json:"owner_idowner"`
	Nama            string            `json:"nama"`
	Alamat          string            `json:"alamat"`
	Kota            string            `json:"kota"`
	Telp            string            `json:"telp"`
	LogoUrl         string            `json:"logo_url"`
	KotaId          int               `json:"kota_id"`
	Outlet          *Outlet           `json:"outlet" gorm:"foreignkey:workshop_idworkshop;association_foreignkey:idworkshop;references:idworkshop"`
	TahapanWorkshop []TahapanWorkshop `json:"tahapan_workshop" gorm:"foreignkey:idworkshop;association_foreignkey:idworkshop;references:idworkshop"`
	BaseModel
}
