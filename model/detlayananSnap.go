package model

import (
	"time"

	"github.com/graphql-go/graphql"
)

type DetlayananSnap struct {
	Id           string     `json:"id" gorm:"primary_key;index"`
	Iddetlayanan string     `json:"iddetlayanan" gorm:"index"`
	Idtransaksi  string     `json:"idtransaksi"`
	Idmesin      string     `json:"idmesin" gorm:"index"`
	Idkaryawan   string     `json:"idkaryawan" gorm:"index"`
	Idoutlet     string     `json:"idoutlet" gorm:"index"`
	Idowner      string     `json:"idowner" gorm:"index"`
	Status       int        `json:"status"`
	StartAt      *time.Time `json:"start_at"`
	StopAt       *time.Time `json:"stop_at"`
	UpdatedAt    *time.Time `json:"updated_at"`
	CreatedAt    *time.Time `json:"created_at"`
	Keterangan   string     `json:"keterangan"`
	Qty          int        `json:"qty"`
	Durasi       int64      `json:"durasi"`
	TimeLeft     int        `json:"time_left"`
	Idlayanan    string     `json:"idlayanan" gorm:"index"`
	Idtahapan    int        `json:"idtahapan" gorm:"index"`
	Hapus        *int       `json:"hapus"`
	SnapMesin    SnapMesin  `json:"snap_mesin" gorm:"foreignKey:idmesin;association_foreignkey:id;references:id"`
	Layanan      Layanan    `json:"layanan" gorm:"foreignKey:idlayanan;association_foreignkey:idlayanan;references:idlayanan"`
	Karyawan     *Karyawan  `json:"karyawan" gorm:"foreignKey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
}

var DetlayananSnapType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "detlayanan_snap_type",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "id",
			},
			"iddetlayanan": &graphql.Field{
				Type:        graphql.String,
				Description: "iddetlayanan",
			},
			"idtransaksi": &graphql.Field{
				Type:        graphql.String,
				Description: "idtransaksi",
			},
			"idmesin": &graphql.Field{
				Type:        graphql.String,
				Description: "idmesin",
			},
			"idkaryawan": &graphql.Field{
				Type:        graphql.String,
				Description: "idkaryawan",
			},
			"idoutlet": &graphql.Field{
				Type:        graphql.String,
				Description: "idoutlet",
			},
			"idowner": &graphql.Field{
				Type:        graphql.String,
				Description: "idowner",
			},
			"status": &graphql.Field{
				Type:        graphql.Int,
				Description: "status",
			},
			"start_at": &graphql.Field{
				Type:        graphql.String,
				Description: "start_at",
			},
			"stop_at": &graphql.Field{
				Type:        graphql.String,
				Description: "stop_at",
			},
			"updated_at": &graphql.Field{
				Type:        graphql.String,
				Description: "updated_at",
			},
			"created_at": &graphql.Field{
				Type:        graphql.String,
				Description: "created_at",
			},
			"keterangan": &graphql.Field{
				Type:        graphql.String,
				Description: "keterangan",
			},
			"qty": &graphql.Field{
				Type:        graphql.Int,
				Description: "qty",
			},
			"durasi": &graphql.Field{
				Type:        graphql.Int,
				Description: "durasi",
			},
			"time_left": &graphql.Field{
				Type:        graphql.Int,
				Description: "time_left",
			},
			"idlayanan": &graphql.Field{
				Type:        graphql.String,
				Description: "idlayanan",
			},
			"idtahapan": &graphql.Field{
				Type:        graphql.Int,
				Description: "idtahapan",
			},
			"snap_mesin": &graphql.Field{
				Type:        SnapMesinType,
				Description: "snap_mesin",
			},
		},
	})

var DetlayananSnapInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "detlayanan_snap_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id",
			},
			"iddetlayanan": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "iddetlayanan",
			},
			"idtransaksi": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "idtransaksi",
			},
			"idmesin": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "idmesin",
			},
			"idkaryawan": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "idkaryawan",
			},
			"idoutlet": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "idoutlet",
			},
			"idowner": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "idowner",
			},
			"status": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "status",
			},
			"start_at": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "start_at",
			},
			"stop_at": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "stop_at",
			},
			"updated_at": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "updated_at",
			},
			"created_at": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "created_at",
			},
			"keterangan": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "keterangan",
			},
			"qty": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "qty",
			},
			"durasi": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "durasi",
			},
			"time_left": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "time_left",
			},
			"idlayanan": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "idlayanan",
			},
			"idtahapan": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "idtahapan",
			},
		},
	})
