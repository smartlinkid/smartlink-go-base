package model

import "time"

type CouponFiturHistory struct {
	Id            string     `json:"id"`
	Idowner       string     `json:"idowner"`
	IdcouponFitur string     `json:"idcoupon_fitur"`
	Idcoupon      string     `json:"idcoupon"`
	Kode          string     `json:"kode"`
	CreatedAt     *time.Time `json:"created_at"`
	RefId         string     `json:"ref_id"`
	RefTable      string     `json:"ref_table"`
}
