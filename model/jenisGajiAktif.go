package model

type JenisGajiAktif struct {
	Id                 int            `json:"id" gorm"primary_key"`
	Idjenisgaji        int            `json:"idjenisgaji"`
	OwnerIdowner       string         `json:"owner_idowner"`
	KaryawanIdkaryawan string         `json:"karyawan_idkaryawan"`
	PengaturanGaji     PengaturanGaji `json:"pengaturan_gaji" gorm:"foreignkey:idjenis;association_foreignkey:idjenisgaji;references:idjenisgaji"`
	JenisGaji          JenisGaji      `json:"jenis_gaji" gorm:"foreignkey:id;association_foreignkey:idjenisgaji;references:idjenisgaji"`
	Karyawan           Karyawan       `json:"karyawan" gorm:"foreignkey:idkaryawan;association_foreignkey:karyawan_idkaryawan;references:karyawan_idkaryawan"`
}
