package model

import "time"

type DaftarKaryawanKurir struct {
	OwnerIdowner    string     `json:"owner_idowner" gorm:"primary_key"`
	KaryawanIdkurir string     `json:"karyawan_idkurir" gorm:"primary_key"`
	Status          int8       `json:"status"`
	Lat             float64    `json:"lat"`
	Lng             float64    `json:"lng"`
	CreatedAt       *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP"`
	UpdatedAt       *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
