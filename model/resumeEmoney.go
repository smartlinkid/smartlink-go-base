package model

import "time"

type ResumeEmoney struct {
	CustomerIdcustomer        string     `json:"customer_idcustomer"`
	Saldo                     float64    `json:"saldo"`
	MasaAktif                 int64      `json:"masa_aktif"`
	JenisEmoney               int        `json:"jenis_emoney"`
	TipePerpanjanganMasaAktif int        `json:"tipe_perpanjangan_masa_aktif"`
	TipeSaldoExpired          int        `json:"tipe_saldo_expired"`
	CreatedAt                 *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt                 *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
	Customer                  *Customer  `json:"cusomer" gorm:"foreignkey:idcustomer;association_foreignkey:customer_idcustomer;references:customer_idcustomer"`
}
