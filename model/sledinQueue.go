package model

import (
	"encoding/json"
	"time"
)

type SledinQueue struct {
	Id        int64      `json:"id" gorm:"primary_key;auto_increment"`
	RefId     string     `json:"ref_id" gorm:"index"`
	Idpayload string     `json:"idpayload" gorm:"index"`
	Idowner   string     `json:"idowner"`
	Idoutlet  string     `json:"idoutlet"`
	SendAt    *time.Time `json:"send_at"`
	Status    int        `json:"status"`
	UpdatedAt *time.Time `json:"updated_at"`
	CreatedAt *time.Time `json:"created_at"`
}

func (d *SledinQueue) GetCustom1() map[string]interface{} {
	if d != nil {
		var x map[string]interface{}
		b, _ := json.Marshal(d)
		e := json.Unmarshal(b, &x)
		if e != nil {
			return nil
		}
		return x
	}
	return nil
}
