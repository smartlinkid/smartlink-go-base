package model

import "github.com/graphql-go/graphql"

type Negara struct {
	Id   int    `json:"id" gorm:"primary_key;index"`
	Nama string `json:"nama"`
}

var NegaraType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "negara",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"nama": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
		},
	})
