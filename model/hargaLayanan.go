package model

type HargaLayanan struct {
	OutletIdoutlet   string        `json:"outlet_idoutlet"`
	Harga            float32       `json:"harga"`
	LayananIdlayanan string        `json:"layanan_idlayanan" gorm:"primary_key"`
	MinOrderReg      float32       `json:"min_order_reg"`
	MinOrderDepo     float32       `json:"min_order_depo"`
	UseInventory     int           `json:"use_inventory"`
	Layanan          *Layanan      `json:"layanan" gorm:"foreignkey:idlayanan;association_foreignkey:layanan_idlayanan;references:layanan_idlayanan"`
	LayananSnap      []LayananSnap `json:"layanan_snap" gorm:"foreignkey:idlayanan;association_foreignkey:layanan_idlayanan;references:layanan_idlayanan"`
	Snap             Snap          `json:"snap" gorm:"-"`
	BaseModel
}

type Snap struct {
	UseSnapbrige bool   `json:"use_snapbrige"`
	MesinUse     string `json:"mesin_use"`
}
