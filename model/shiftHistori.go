package model

import "time"

type ShiftHistori struct {
	Id              string    `json:"id" gorm:"primary_key"`
	Idshift         int       `json:"idshift"`
	MasukShift      time.Time `json:"masuk_shift"`
	KeluarShift     time.Time `json:"keluar_shift"`
	ToleransiMasuk  int       `json:"toleransi_masuk"`
	ToleransiKeluar int       `json:"toleransi_keluar"`
}
