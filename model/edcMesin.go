package model

import (
	"time"
)

type EdcMesin struct {
	Id                      string            `json:"id" gorm:"primary_key"`
	Idowner                 string            `json:"idowner"`
	Nama                    string            `json:"nama"`
	Idoutlet                string            `json:"idoutlet"`
	IdakunKeuanganEdc       string            `json:"idakun_keuangan_edc"`
	IdakunKeuanganTemp      string            `json:"idakun_keuangan_temp"`
	IdrekeningOwner         int               `json:"idrekening_owner"`
	IdedcProvider           int               `json:"idedc_provider"`
	PersenBiayaSesama       float32           `json:"persen_biaya_sesama"`
	PersenBiayaBerbeda      float32           `json:"persen_biaya_berbeda"`
	PersenBiayaKartuKredit  float32           `json:"persen_biaya_kartu_kredit"`
	Active                  int               `json:"active"`
	Saldo                   float64           `json:"saldo"`
	WaktuTerakhirSettlement *time.Time        `json:"waktu_terakhir_settlement"`
	Outlet                  *Outlet           `json:"outlet" gorm:"foreignKey:idoutlet;association_foreignkey:idoutlet;references:idoutlet"`
	RekeningOwner           *RekeningOwner    `json:"rekening_owner" gorm:"foreignKey:id;association_foreignkey:idrekening_owner;references:idrekening_owner"`
	Provider                *EdcProvider      `json:"provider" gorm:"foreignKey:id;association_foreignkey:idedc_provider;references:idedc_provider"`
	KeuanganEdc             *KeuanganAkunAkun `json:"provider" gorm:"foreignKey:idakun;association_foreignkey:idakun_keuangan_edc;references:idakun_keuangan_edc"`
	BaseModel
}
