package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type SnapConsoleMesin struct {
	IdsnapMesin   string     `json:"idsnap_mesin" gorm:"primary_key;index"`
	IdsnapConsole string     `json:"idsnap_console" gorm:"index"`
	SnapMesin     SnapMesin  `json:"snap_mesin" gorm:"foreignkey:idsnap_mesin;association_foreignkey:id;references:id"`
	CreatedAt     *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
}

var SnapConsoleMesinType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "snap_console_mesin",
		Fields: graphql.Fields{
			"idsnap_console": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"idsnap_mesin": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},

			"snap_mesin": &graphql.Field{
				Type:        SnapMesinType,
				Description: "",
			},
		},
		Description: "",
	})
