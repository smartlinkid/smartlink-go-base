package model

import "time"

type WaBill struct {
	Idbill          int64     `json:"idbill" gorm:"primary_key;index"`
	Idowner         string    `json:"idowner"`
	Iddevice        string    `json:"iddevice"`
	CreatedAt       time.Time `json:"created_at"`
	UpdatedAt       time.Time `json:"updated_at"`
	Hapus           int       `json:"hapus"`
	NextDue         time.Time `json:"next_due"`
	KuotaAwal       int       `json:"kuota_awal"`
	KuotaAkhir      int       `json:"kuota_akhir"`
	Kuota           int       `json:"kuota"`
	TipeBill        int       `json:"tipe_bill"`
	Idpaket         int       `json:"idpaket"`
	Idkupon         int       `json:"idkupon"`
	KodeKupon       string    `json:"kode_kupon"`
	Diskon          float64   `json:"diskon"`
	MetodeBayar     int       `json:"metode_bayar"`
	HargaRegistrasi float64   `json:"harga_registrasi"`
	HargaPaket      float64   `json:"harga_paket"`
	BiayaAdmin      float64   `json:"biaya_admin"`
	Total           float64   `json:"total"`
	ExpiredAt       time.Time `json:"expired_at"`
	NomorBill       string    `json:"nomor_bill"`
	Status          int       `json:"status"`
	Reason          string    `json:"reason"`
	WaDevice        WaDevice  `json:"wa_device" gorm:"foreignkey:iddevice;association_foreignkey:iddevice;references:iddevice"`
	WaPaket         WaPaket   `json:"wa_paket" gorm:"foreignkey:id;association_foreignkey:idpaket;references:idpaket"`
	Coupon          Coupon    `json:"coupon" gorm:"foreignkey:id;association_foreignkey:idkupon;references:idkupon"`
}
