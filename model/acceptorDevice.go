package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type AcceptorDevice struct {
	Id            string          `json:"id" gorm:"primary_key;index"`
	Nama          string          `json:"nama"`
	Idowner       string          `json:"idowner" gorm:"index"`
	Idoutlet      string          `json:"idoutlet" gorm:"index"`
	Keterangan    string          `json:"keterangan"`
	CreatedAt     *time.Time      `json:"created_at"`
	UpdatedAt     *time.Time      `json:"updated_at"`
	IsLogin       *int            `json:"is_login"`
	Saldo         float64         `json:"saldo"`
	Token         *string         `json:"token"`
	AcceptorMesin []AcceptorMesin `json:"acceptor_mesin" gorm:"foreignKey:idacceptor_device;association_foreignkey:id;references:id"`
	Outlet        Outlet          `json:"outlet" gorm:"foreignKey:idoutlet;association_foreignkey:idoutlet;references:idoutlet"`
}

var AcceptorDeviceType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "acceptor_device",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "id acceptor",
			},
			"nama": &graphql.Field{
				Type:        graphql.String,
				Description: "nama acceptor",
			},
			"idowner": &graphql.Field{
				Type:        graphql.String,
				Description: "id owner",
			},
			"idoutlet": &graphql.Field{
				Type:        graphql.String,
				Description: "id outlet",
			},
			"keterangan": &graphql.Field{
				Type:        graphql.String,
				Description: "keterangan",
			},
			"created_at": &graphql.Field{
				Type:        graphql.String,
				Description: "created at",
			},
			"updated_at": &graphql.Field{
				Type:        graphql.String,
				Description: "updated at",
			},
			"is_login": &graphql.Field{
				Type:        graphql.Int,
				Description: "check acceptor is logged in or not",
			},
			"saldo": &graphql.Field{
				Type:        graphql.Float,
				Description: "Saldo of cash acceptor",
			},
			"token": &graphql.Field{
				Type:        graphql.String,
				Description: "acceptor token",
			},
			"acceptor_mesin": &graphql.Field{
				Type:        graphql.NewList(AcceptorMesinType),
				Description: "acceptor token",
			},
			"outlet": &graphql.Field{
				Type:        OutletType,
				Description: "Outlet",
			},
		},
	})

var AcceptorDeviceInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "acceptor_device_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id acceptor",
			},
			"nama": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "nama acceptor",
			},
			"idowner": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id owner",
			},
			"idoutlet": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id outlet",
			},
			"keterangan": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "keterangan",
			},
			"is_login": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "check acceptor is logged in or not",
			},
			"saldo": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "Saldo of cash acceptor",
			},
			"token": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "acceptor token",
			},
			"acceptor_mesin": &graphql.InputObjectFieldConfig{
				Type:        graphql.NewList(AcceptorMesinInput),
				Description: "list of id snap mesin",
			},
		},
	})
