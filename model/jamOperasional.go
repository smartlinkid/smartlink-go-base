package model

import "time"

type JamOperasional struct {
	Id       int       `json:"id" gorm:"primary_key"`
	Lokasi   string    `json:"lokasi"` // outlet, workhshop
	Idlokasi string    `json:"idlokasi"`
	Idowner  string    `json:"idowner"`
	Buka     time.Time `json:"buka"`
	Tutup    time.Time `json:"tutup"`
}
