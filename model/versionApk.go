package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type VersionApk struct {
	VersionCode  int       `json:"version_code" gorm:"primary_key;index;not null"`
	AppType      int       `json:"app_type" gorm:"primary_key;index;not null"`
	ForceUpdate  int       `json:"force_update"`
	VersionName  string    `json:"version_name"`
	VersionMajor int       `json:"version_major"`
	VersionMinor int       `json:"version_minor"`
	VersionPoint int       `json:"version_point"`
	VersionMicro int       `json:"version_micro"`
	Changelog    string    `json:"changelog"`
	ReleaseTime  time.Time `json:"release_time"`
	Note         string    `json:"note"`
	Title        string    `json:"title"`
}

var VersionApkType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "version_apk",
		Fields: graphql.Fields{
			"version_code": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"app_type": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"force_update": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"version_name": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"version_major": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"version_minor": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"version_point": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"version_micro": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"changelog": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"release_time": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "",
			},
			"note": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"title": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
		},
	})
