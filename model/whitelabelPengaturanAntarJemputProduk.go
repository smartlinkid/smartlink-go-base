package model

import "time"

type WhitelabelPengaturanAntarJemputProduk struct {
	IdwhitelabelPengaturanAntarJemput string      `json:"idwhitelabel_pengaturan_antar_jemput"`
	Idowner                           string      `json:"idowner"`
	Idproduk                          string      `json:"idproduk"`
	CreatedAt                         *time.Time  `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	Data                              interface{} `json:"data" gorm:"-"`
}
