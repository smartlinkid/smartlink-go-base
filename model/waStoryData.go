package model

import "time"

type WaStoryData struct {
	Id         int64     `json:"id" gorm:"primary_key;auto_increment"`
	Iddevice   string    `json:"iddevice"`
	Idowner    string    `json:"idowner"`
	Idmessage  string    `json:"idmessage"`
	ThumbName  *string   `json:"thumb_name"`
	Idschedule *string   `json:"idschedule"`
	MediaName  *string   `json:"media_name"`
	Caption    *string   `json:"caption"`
	TypeMedia  int       `json:"type_media"`
	Status     int       `json:"status"`
	Reason     *string   `json:"reason"`
	Hapus      int       `json:"hapus"`
	EndAt      time.Time `json:"end_at"`
	CreatedAt  time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP"`
	UpdatedAt  time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
