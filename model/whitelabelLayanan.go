package model

import "time"

type WhitelabelLayanan struct {
	Id       string `json:"id"`
	Idowner  string `json:"idowner"`
	Idoutlet string `json:"idoutlet"`
	// Idkategori         string                         `json:"idkategori"`
	Idlayanan          string                             `json:"idlayanan"`
	Description        string                             `json:"description"`
	Weight             float64                            `json:"weight"`
	CreatedAt          *time.Time                         `json:"created_at"`
	UpdatedAt          *time.Time                         `json:"updated_at"`
	Layanan            *Layanan                           `json:"layanan" gorm:"foreignkey:idlayanan;association_foreignkey:idlayanan"`
	KategoriLayanan    []WhitelabelLayananKategoriLayanan `json:"kategori_layanan" gorm:"foreignkey:id_wl_layanan;association_foreignkey:id"`
	SpesifikasiLayanan []WhitelabelSpesifikasiLayanan     `json:"spesifikasi_layanan" gorm:"foreignkey:idlayanan;association_foreignkey:idlayanan"`
	TransaksiFoto      []TransaksiFoto                    `json:"transaksi_foto" gorm:"foreignkey:idtransaksi;association_foreignkey:idlayanan"`
}

type WhitelabelLayananKategoriLayanan struct {
	IdWlLayanan     string                     `json:"id_wl_layanan"`
	IdWlKategori    string                     `json:"id_wl_kategori"`
	CreatedAt       *time.Time                 `json:"created_at"`
	UpdatedAt       *time.Time                 `json:"updated_at"`
	KategoriLayanan *WhitelabelKategoriLayanan `json:"kategori" gorm:"foreignkey:id_wl_kategori;association_foreignkey:id"`
}

type WhitelabelKategoriLayanan struct {
	Id        string     `json:"id"`
	Idowner   string     `json:"idowner"`
	Nama      string     `json:"nama"`
	Icon      string     `json:"icon"`
	CreatedAt *time.Time `json:"created_at"`
	UpdatedAt *time.Time `json:"updated_at"`
}

type WhitelabelSpesifikasiLayanan struct {
	Id        string     `json:"id"`
	Idowner   string     `json:"idowner"`
	Idoutlet  string     `json:"idoutlet"`
	Idlayanan string     `json:"idlayanan"`
	Nama      string     `json:"nama"`
	Konten    string     `json:"konten"`
	CreatedAt *time.Time `json:"created_at"`
	UpdatedAt *time.Time `json:"updated_at"`
}
