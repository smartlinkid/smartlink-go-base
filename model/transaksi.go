package model

import "time"

type Transaksi struct {
	Idtransaksi            string                   `json:"idtransaksi" gorm:"primary_key"`
	CustomerIdcustomer     string                   `json:"customer_idcustomer" gorm:"column:customer_idcustomer"`
	OutletIdoutlet         string                   `json:"outlet_idoutlet" gorm:"index"`
	WorkshopIdworkshop     string                   `json:"workshop_idworkshop" gorm:"index"`
	KaryawanIdkaryawan     string                   `json:"karyawan_idkaryawan" gorm:"index"`
	ParfumIdparfum         *int64                   `json:"parfum_idparfum" gorm:"index"`
	Waktu                  int64                    `json:"waktu"	gorm:"index"`
	TanggalSelesai         int64                    `json:"tanggal_selesai" gorm:"index"`
	TanggalTerima          int64                    `json:"tanggal_terima" gorm:"index"`
	TanggalPengantaran     int64                    `json:"tanggal_pengantaran" gorm:"index"`
	IdalamatAntar          string                   `json:"idalamat_antar" gorm:"index"`
	Total                  float64                  `json:"total"`
	Pajak                  float64                  `json:"pajak"`
	Diskon                 float64                  `json:"diskon"`
	Persendiskon           float64                  `json:"persendiskon"`
	Persenpajak            float64                  `json:"persenpajak"`
	TarifKali              float64                  `json:"tarif_kali"`
	TarifTambahan          float64                  `json:"tarif_tambahan"`
	Grandtotal             float64                  `json:"grandtotal"`
	Keterangan             string                   `json:"keterangan"`
	Hapus                  *int                     `json:"hapus"`
	Lunas                  int                      `json:"lunas"`
	StatusHapus            *int                     `json:"status_hapus"`
	IsAntar                int                      `json:"is_antar"`
	IsJemput               int                      `json:"is_jemput"`
	JenisTransaksi         int                      `json:"jenis_transaksi"`
	ShortLink              string                   `json:"short_link"`
	Posisi                 int                      `json:"posisi"`
	StatusAntar            int                      `json:"status_antar"`
	GambarAntar            string                   `json:"gambar_antar"`
	KeteranganAntar        string                   `json:"keterangan_antar"`
	Idjemput               int64                    `json:"idjemput"`
	IsWsot                 int                      `json:"is_wsot"`
	TipeBayar              int                      `json:"tipe_bayar"`
	CreatedAt              time.Time                `json:"created_at"`
	UpdatedAt              time.Time                `json:"updated_at"`
	StatusAmbil            int                      `json:"status_ambil"`
	StatusSelesai          int                      `json:"status_selesai"`
	PackGantung            int                      `json:"pack_gantung"`
	PackLipat              int                      `json:"pack_lipat"`
	PackGulung             int                      `json:"pack_gulung"`
	RakGantung             int                      `json:"rak_gantung"`
	RakLipat               int                      `json:"rak_lipat"`
	RakGulung              int                      `json:"rak_gulung"`
	Kerjacepat             int                      `json:"kerjacepat"`
	ReminderAmbil          int                      `json:"reminder_ambil"`
	BiayaKirim             float64                  `json:"biaya_kirim"`
	BiayaService           float64                  `json:"biaya_service"`
	BiayaTambah            float64                  `json:"biaya_tambah"`
	IdtransaksiLama        string                   `json:"idtransaksi_lama" gorm:"index"`
	ReviewKecepatan        float32                  `json:"review_kecepatan"`
	ReviewLayanan          float32                  `json:"review_layanan"`
	ReviewKualitas         float32                  `json:"review_kualitas"`
	ReviewAvg              float32                  `json:"review_avg"`
	ReviewText             string                   `json:"review_text"`
	DateTanggalSelesai     *time.Time               `json:"date_tanggal_selesai"`
	DateTanggalAntar       *time.Time               `json:"date_tanggal_antar"`
	IdkaryawanKurir        string                   `json:"idkaryawan_kurir"`
	LokasiKerja            int                      `json:"lokasi_kerja"`
	IdmasalTransaksi       *string                  `json:"idmasal_transaksi"`
	Parfum                 *Parfum                  `json:"parfum" gorm:"foreignkey:idparfum;association_foreignkey:parfum_idparfum;references:parfum_idparfum"`
	Outlet                 *Outlet                  `json:"outlet" gorm:"foreignkey:idoutlet;association_foreignkey:outlet_idoutlet;references:outlet_idoutlet"`
	Customer               *Customer                `json:"customer" gorm:"foreignkey:idcustomer;association_foreignkey:customer_idcustomer;references:customer_idcustomer"`
	Workshop               *Workshop                `json:"workshop" gorm:"foreignkey:idworkshop;association_foreignkey:workshop_idworkshop;references:workshop_idworkshop"`
	Karyawan               *Karyawan                `json:"karyawan" gorm:"foreignkey:idkaryawan;association_foreignkey:karyawan_idkaryawan;references:karyawan_idkaryawan"`
	Kurir                  *Karyawan                `json:"kurir" gorm:"foreignkey:idkaryawan;association_foreignkey:idkaryawan_kurir;references:idkaryawan_kurir"`
	Alamat                 *Alamat                  `json:"alamat" gorm:"foreignkey:idalamat;association_foreignkey:idalamat_antar;references:idalamat_antar"`
	AntarExtra             *AntarExtra              `json:"antar_extra" gorm:"foreignkey:idtransaksi;association_foreignkey:ref_id;references:ref_id"`
	Detlayanan             []Detlayanan             `json:"detlayanan" gorm:"foreignkey:transaksi_idtransaksi;association_foreignkey:idtransaksi;references:idtransaksi"`
	Bayar                  []PembayaranUmum         `json:"bayar" gorm:"foreignkey:idtransaksi;association_foreignkey:idtransaksi;references:idtransaksi"`
	PemakaianDeposit       []PemakaianDeposit       `json:"pemakaian_deposit" gorm:"foreignkey:idtransaksi;association_foreignkey:idtransaksi;references:idtransaksi"`
	PemakaianEmoney        []PemakaianEmoney        `json:"pemakaian_emoney" gorm:"foreignkey:idtransaksi;association_foreignkey:idtransaksi;references:idtransaksi"`
	DaftarPemrosesan       []DaftarPemrosesan       `json:"daftar_pemrosesan" gorm:"foreignkey:transaksi_idtransaksi;association_foreignkey:idtransaksi;references:idtransaksi"`
	ResumeDaftarPemrosesan []ResumeDaftarPemrosesan `json:"resume_daftar_pemrosesan" gorm:"foreignkey:transaksi_idtransaksi;association_foreignkey:idtransaksi;references:idtransaksi"`
	TransaksiHapus         *TransaksiHapus          `json:"transaksi_hapus"gorm:"foreignkey:idtransaksi;association_foreignkey:idtransaksi;references:idtransaksi"`
	JadwalJemput           *JadwalJemput            `json:"jadwal_jemput" gorm:"foreignkey:id;association_foreignkey:idjemput;references:idjemput"`
	TransaksiLoyalty       *TransaksiLoyalty        `json:"transaksi_loyalty" gorm:"foreignkey:idtransaksi;association_foreignkey:idtransaksi;references:idtransaksi"`
}

func (t *Transaksi) GetJsonForTemplate() map[string]interface{} {
	j := make(map[string]interface{})

	return j
}
