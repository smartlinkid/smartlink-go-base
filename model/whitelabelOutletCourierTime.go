package model

type WhitelabelOutletCourierTime struct {
	Id                    string `json:"id"`
	Idowner               string `json:"idowner"`
	Idoutlet              string `json:"idoutlet"`
	Tipe                  int    `json:"tipe"`
	Hari                  string `json:"hari"`
	StartTime             string `json:"start_time"`
	EndTime               string `json:"end_time"`
	Aktif                 int    `json:"aktif"`
	MengikutiJadwalOutlet int    `json:"mengikuti_jadwal_outlet"`
}
