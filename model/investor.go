package model

type Investor struct {
	Id            string `json:"id" gorm:"primary_key;index;not null"`
	Email         string `json:"email" gorm:"index"`
	CountryCode   string `json:"country_code" gorm:"index"`
	Telp          string `json:"telp" gorm:"index"`
	FullTelp      string `json:"full_telp" gorm:"index"`
	Password      string `json:"password"`
	Nama          string `json:"nama"`
	Idkota        int    `json:"idkota" gorm:"index"`
	Alamat        string `json:"alamat"`
	NomorRekening string `json:"nomor_rekening"`
	IddataBank    int    `json:"iddata_bank"`
	BankAtasNama  string `json:"bank_atas_nama"`
	RememberToken string `json:"remember_token"`
	BaseModel
}
