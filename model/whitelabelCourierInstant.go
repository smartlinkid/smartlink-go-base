package model

type WhitelabelCourierInstant struct {
	Id                   string                         `json:"id"`
	Name                 string                         `json:"name"`
	IconUrl              string                         `json:"icon_url"`
	OutletCourierInstant WhitelabelOutletCourierInstant `json:"outlet_courier_instant" gorm:"foreignkey:IdcourierInstant;references:Id"`
}

type WhitelabelOutletCourierInstant struct {
	Idowner          string `json:"idowner"`
	Idoutlet         string `json:"idoutlet"`
	IdcourierInstant string `json:"idcourier_instant"`
	Aktif            bool   `json:"aktif"`
}
