package model

import "time"

type WhitelabelPromoSelfserviceLayanan struct {
	Id            string     `json:"id"`
	Idowner       string     `json:"idowner"`
	Idpromo       string     `json:"idpromo"`
	IdsnapLayanan string     `json:"idsnap_layanan"`
	CreatedAt     *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
}
