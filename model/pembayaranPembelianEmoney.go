package model

import "time"

type PembayaranPembelianEmoney struct {
	IdpembelianEmoney  string            `json:"idpembelian_emoney"`
	Id                 int               `json:"id" gorm:"primary_key"`
	Bayar              float64           `json:"bayar"`
	Nominal            float64           `json:"nominal"`
	Kembalian          float64           `json:"kembalian"`
	Waktu              int64             `json:"waktu"`
	Keterangan         string            `json:"keterangan"`
	Idjenisbayar       int               `json:"idjenisbayar"`
	TipeBayar          int               `json:"tipe_bayar"`
	DNoresi            string            `json:"d_noresi"`
	DNokartu           string            `json:"d_nokartu"`
	DTanggalTransfer   int64             `json:"d_tanggal_transfer"`
	DBank              string            `json:"d_bank"`
	DIdakunbank        string            `json:"d_idakunbank"`
	DIdowner           string            `json:"d_idowner"`
	Idkaryawan         string            `json:"idkaryawan"`
	Idoutlet           string            `json:"idoutlet"`
	Idowner            string            `json:"idowner"`
	DateRefund         *time.Time        `json:"date_refund"`
	AkunRefund         string            `json:"akun_refund"`
	Hash               string            `json:"hash"`
	KeuanganAkunRefund *KeuanganAkunAkun `json:"keuangan_akun_refund" gorm:"foreignkey:idakun;association_foreignkey:akun_refund;references:akun_refund"`
	CreatedAt          *time.Time        `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt          *time.Time        `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
	//DataEmoney         DataEmoney        `json:"data_emoney" gorm:"foreignkey:iddata_emoney;association_foreignkey:data_emoney_iddata_emoney;references:data_emoney_iddata_emoney"`
	EdcPembayaran *EdcPembayaran  `json:"edc_pembayaran" gorm:"foreignkey:ref_id;association_foreignkey:id;references:id"`
	Karyawan      *Karyawan       `json:"karyawan" gorm:"foreignkey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	TransaksiFoto []TransaksiFoto `json:"transaksi_foto" gorm:"foreignkey:idtransaksi;association_foreignkey:hash;references:hash"`
}
