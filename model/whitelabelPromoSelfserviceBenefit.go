package model

import "time"

type WhitelabelPromoSelfserviceBenefit struct {
	Id                 string                               `json:"id"`
	Idowner            string                               `json:"idowner"`
	Idpromo            string                               `json:"idpromo"`
	BenefitType        string                               `json:"benefit_type"`  // diskon
	BenefitUsage       string                               `json:"benefit_usage"` // total_tagihan, layanan_tertentu, semua_layanan
	AmountType         string                               `json:"amount_type"`   // persen, nominal
	Amount             float64                              `json:"amount"`
	MaxAmount          float64                              `json:"max_amount"`
	CreatedAt          *time.Time                           `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	SelfserviceLayanan []*WhitelabelPromoSelfserviceLayanan `json:"selfservice_layanan" gorm:"foreignkey:idpromo;association_foreignkey:idpromo;references:idpromo"`
}
