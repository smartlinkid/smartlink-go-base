package model

import "github.com/graphql-go/graphql"

type DataSatuan struct {
	IddataSatuan int    `json:"iddata_satuan" gorm:"primary_key"`
	Nama         string `json:"nama"`
	JenisSatuan  int    `json:"jenis_satuan"`
	BaseModel
}

var DataSatuanType = graphql.NewObject(graphql.ObjectConfig{
	Name: "data_satuan",
	Fields: graphql.Fields{
		"iddata_satuan": &graphql.Field{
			Type:        graphql.Int,
			Description: "",
		},
		"nama": &graphql.Field{
			Type:        graphql.String,
			Description: "",
		},
		"jenis_satuan": &graphql.Field{
			Type:        graphql.Int,
			Description: "",
		},
	},
})
