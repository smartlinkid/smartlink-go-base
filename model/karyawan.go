package model

import (
	"time"

	"github.com/graphql-go/graphql"
)

type Karyawan struct {
	Idkaryawan             string                  `json:"idkaryawan" gorm:"index;primary_key"`
	OwnerIdowner           string                  `json:"owner_idowner"`
	Nama                   string                  `json:"nama"`
	Telp                   string                  `json:"telp"`
	Email                  string                  `json:"email"`
	Alamat                 string                  `json:"alamat"`
	KasKurir               float64                 `json:"kas_kurir"`
	Lat                    float64                 `json:"lat"`
	Lng                    float64                 `json:"lng"`
	Foto                   string                  `json:"foto"`
	IdjenisGajiSales       int                     `json:"idjenis_gaji_sales"`
	Device                 string                  `json:"device"`
	LastOpen               *time.Time              `json:"last_open"`
	LastLocation           string                  `json:"last_location"`
	Temperor               int                     `json:"temperor"`
	Password               string                  `json:"-"`
	BisaLunasiInvoice      int                     `json:"bisa_lunasi_invoice"` //0 OFF, 1 ON
	BisaMembuatKasbon      int                     `json:"bisa_membuat_kasbon"`
	DaftarKaryawanOutlet   *DaftarKaryawanOutlet   `json:"daftar_karyawan_outlet" gorm:"foreignkey:karyawan_idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	DaftarKaryawanWorkshop *DaftarKaryawanWorkshop `json:"daftar_karyawan_workshop" gorm:"foreignkey:karyawan_idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	DaftarKaryawanKurir    *DaftarKaryawanKurir    `json:"daftar_karyawan_kurir" gorm:"foreignkey:karyawan_idkurir;association_foreignkey:idkaryawan;references:idkaryawan"`
	ResumeKasbon           *ResumeKasbon           `json:"resume_kasbon" gorm:"foreignkey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	ResumePotongan         *ResumePotongan         `json:"resume_potongan" gorm:"foreignkey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	JenisGajiSales         *JenisGajiSales         `json:"jenis_gaji_sales" gorm:"foreignkey:id;association_foreignkey:idjenis_gaji_sales;references:idjenis_gaji_sales"`
	AclUser                *AclUser                `json:"acl_user" gorm:"foreignkey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	TransaksiFoto          *TransaksiFoto          `json:"transaksi_foto" gorm:"foreignkey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	AclAuthority           []AclAuthority          `json:"acl_authority" gorm:"foreignkey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	PekerjaanKaryawan      []PekerjaanKaryawan     `json:"pekerjaan_karyawan" gorm:"foreignkey:karyawan_idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	LogToken               []LogToken              `json:"log_token" gorm:"foreignkey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	BaseModel
}

var KaryawanType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "karyawan",
		Fields: graphql.Fields{
			"idkaryawan": &graphql.Field{
				Type:        graphql.String,
				Description: "id karyawan snap",
			},
			"owner_idowner": &graphql.Field{
				Type:        graphql.String,
				Description: "id owner snap",
			},
			"nama": &graphql.Field{
				Type:        graphql.String,
				Description: "nama karyawan snap",
			},
			"telp": &graphql.Field{
				Type:        graphql.String,
				Description: "nomor telepon karyawan snap",
			},
			"email": &graphql.Field{
				Type:        graphql.String,
				Description: "email karyawan snap",
			},
			"alamat": &graphql.Field{
				Type:        graphql.String,
				Description: "alamat karyawan snap",
			},
			"kas_kurir": &graphql.Field{
				Type:        graphql.Int,
				Description: "kas kurir snap",
			},
			"lat": &graphql.Field{
				Type:        graphql.Float,
				Description: "latitude",
			},
			"lng": &graphql.Field{
				Type:        graphql.Float,
				Description: "longitude",
			},
			"foto": &graphql.Field{
				Type:        graphql.String,
				Description: "foto karyawan snap",
			},
			"idjenis_gaji_sales": &graphql.Field{
				Type:        graphql.Int,
				Description: "id jenis gaji sales",
			},
			"device": &graphql.Field{
				Type:        graphql.String,
				Description: "device karyawan snap",
			},
			"last_open": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "terakhir kali membuka aplikasi",
			},
			"last_location": &graphql.Field{
				Type:        graphql.String,
				Description: "lokasi terakhir karyawan snap",
			},
			"temperor": &graphql.Field{
				Type:        graphql.String,
				Description: "temperor",
			},
			"daftar_karyawan_outlet": &graphql.Field{
				Type:        DaftarKaryawanOutletType,
				Description: "daftar_karyawan_outlet",
			},
		},
	})
