package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type SnapLayananTag struct {
	IdsnapLayanan string       `json:"id" gorm:"primary_key;index"`
	IdsnapTag     int64        `json:"idsnap_tag" gorm:"index"` // add primary key and index manually
	SnapLayanan   *SnapLayanan `json:"snap_layanan" gorm:"foreignKey:idsnap_layanan;association_foreignkey:id;references:id"`
	SnapTag       *SnapTag     `json:"snap_tag" gorm:"foreignKey:id;association_foreignkey:idsnap_tag;references:idsnap_tag"`
	SnapMesin     []SnapMesin  `json:"snap_mesin" gorm:"foreignkey:idsnap_tag;association_foreignkey:idsnap_tag;references:idsnap_tag"`
	CreatedAt     *time.Time   `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt     *time.Time   `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	Hapus         *int         `json:"hapus" gorm:"default:0"`
}

var SnapLayananTagType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "snap_layanan_tag",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "id layanan snap",
			},
			"idsnap_tag": &graphql.Field{
				Type:        graphql.Int,
				Description: "id tag snap",
			},
			//"snap_layanan": &graphql.Field{
			//	Type:        SnapLayananType,
			//	Description: "",
			//},
			"snap_tag": &graphql.Field{
				Type:        SnapTagType,
				Description: "",
			},
			"snap_mesin": &graphql.Field{
				Type:        graphql.NewList(SnapMesinType),
				Description: "",
			},
			//"base": &graphql.Field{
			//	Type:        BaseModelType,
			//	Description: "base model",
			//},
		},
		Description: "snap layanan tag data",
	})

func init() {
	SnapLayananTagType.AddFieldConfig("snap_layanan", &graphql.Field{Type: SnapLayananType})
}
