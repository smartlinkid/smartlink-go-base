package model

import "time"

type WhitelabelUlasan struct {
	Id                string          `json:"id"`
	RefTable          string          `json:"ref_table"`
	RefId             string          `json:"ref_id"`
	Idcustomer        string          `json:"idcustomer"`
	Idoutlet          string          `json:"idoutlet"`
	OutletTestimoni   string          `json:"outlet_testimoni"`
	OutletRating      int             `json:"outlet_rating"`
	OutletMark        string          `json:"outlet_mark"`
	Idkaryawan        string          `json:"idkaryawan"`
	KaryawanTestimoni string          `json:"karyawan_testimoni"`
	KaryawanRating    int             `json:"karyawan_rating"`
	KaryawanMark      string          `json:"karyawan_mark"`
	CreatedAt         time.Time       `json:"created_at"`
	Idowner           string          `json:"idowner"`
	Outlet            *Outlet         `json:"outlet" gorm:"foreignkey:idoutlet;association_foreignkey:idoutlet;references:idoutlet"`
	Customer          *Customer       `json:"customer" gorm:"foreignkey:idcustomer;association_foreignkey:idcustomer;references:idcustomer"`
	Karyawan          *Karyawan       `json:"karyawan" gorm:"foreignkey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	FotoUlasan        []TransaksiFoto `json:"foto_ulasan" gorm:"foreignkey:idtransaksi;association_foreignkey:id;references:id"`
}
