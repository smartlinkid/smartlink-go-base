package model

type WaPaket struct {
	Id        int     `json:"id" gorm:"primary_key;index"`
	Nama      string  `json:"nama"`
	Quota     int     `json:"quota"`
	MasaAktif int     `json:"masa_aktif"`
	Harga     float64 `json:"harga"`
	BaseModel
}
