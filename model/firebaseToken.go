package model

import "time"

type FirebaseToken struct {
	Idaplikasi int        `json:"idaplikasi" gorm:"primary_key"` // 1 kasir 2 workshop 3 kurir
	Idkaryawan string     `json:"idkaryawan"`
	Waktu      string     `json:"waktu"`
	Token      string     `json:"token"`
	CreatedAt  *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt  *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
