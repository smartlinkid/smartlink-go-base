package model

type PemakaianEmoney struct {
	Id             int     `json:"id" gorm:"primary_key"`
	Idpembayaran   int64   `json:"idpembayaran"`
	Idtransaksi    string  `json:"idtransaksi"`
	Nominal        float64 `json:"nominal"`
	SaldoAwal      float64 `json:"saldo_awal"`
	SaldoAkhir     float64 `json:"saldo_akhir"`
	MasaAktifAwal  int     `json:"masa_aktif_awal"`
	MasaAktifAkhir int     `json:"masa_aktif_akhir"`
	SaldoHapus     float64 `json:"saldo_hapus"`
	BaseModel
}
