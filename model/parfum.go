package model

type Parfum struct {
	Idparfum       int    `json:"idparfum" gorm:"primary_key;auto_increment"`
	NamaParfum     string `json:"nama_parfum"`
	OwnerIdowner   string `json:"owner_idowner"`
	OutletIdoutlet string `json:"outlet_idoutlet"`
	ParfumDefault  int    `json:"parfum_default"`
	BaseModel
}
