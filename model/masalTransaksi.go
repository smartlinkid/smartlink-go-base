package model

import "time"

type MasalTransaksi struct {
	IdmasalTransaksi     string                 `json:"idmasal_transaksi" gorm:"primary_key;"`
	Idowner              string                 `json:"idowner"`
	Idkaryawan           string                 `json:"idkaryawan"`
	Idakun               string                 `json:"idakun"`
	Idoutlet             string                 `json:"idoutlet"`
	Tanggal              *time.Time             `json:"tanggal"`
	Tagihan              float32                `json:"tagihan"`
	Terbayar             float32                `json:"terbayar"`
	SisaBayar            float32                `json:"sisa_bayar"`
	JumlahTransaksi      int64                  `json:"jumlah_transaksi"`
	Lunas                int32                  `json:"lunas"`
	Penerima             string                 `json:"penerima"`
	TanggalBayar         *time.Time             `json:"tanggal_bayar"`
	PenerimaWa           string                 `json:"penerima_wa"`
	ShowLogoOutlet       int32                  `json:"show_logo_outlet"`
	ShowDocumentation    int32                  `json:"show_documentation"`
	JsonReminder         string                 `json:"json_reminder"`
	Keterangan           string                 `json:"keterangan"`
	StatusPembatalan     int32                  `json:"status_pembatalan"`
	TanggalJatuhTempo    *time.Time             `json:"tanggal_jatuh_tempo"`
	AlasanPemabatalan    string                 `json:"alasan_pemabatalan"`
	KeteranganKonfirmasi string                 `json:"keterangan_konfirmasi"`
	TipeKelompok         int32                  `json:"tipe_kelompok"`
	Hapus                int64                  `json:"hapus"`
	UrlInvoice           *string                `json:"url_invoice"`
	SendWa               int                    `json:"send_wa"`
	MasalTransaksiDaftar []MasalTransaksiDaftar `json:"masal_transaksi_daftar" gorm:"foreignkey:idmasal_transaksi;association_foreignkey:idmasal_transaksi;references:idmasal_transaksi"`
	MasalBayar           []MasalBayar           `json:"masal_bayar" gorm:"foreignkey:idmasal_transaksi;association_foreignkey:idmasal_transaksi;references:idmasal_transaksi"`
	Owner                *Owner                 `json:"owner" gorm:"foreignKey:idowner;association_foreignkey:idowner;references:idowner"`
	Outlet               *Outlet                `json:"outlet" gorm:"foreignKey:idoutlet;association_foreignkey:idoutlet;references:idoutlet"`
	CreatedAt            *time.Time             `json:"created_at" gorm:"default:CURRENT_TIMESTAMP"`
	UpdatedAt            *time.Time             `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
