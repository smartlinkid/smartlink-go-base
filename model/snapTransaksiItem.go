package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type SnapTransaksiItem struct {
	Id                    string     `json:"id" gorm:"primary_key;index;not null"`
	IdsnapTransaksi       string     `json:"idsnap_transaksi" gorm:"index"`
	IdsnapTransaksiDetail string     `json:"idsnap_transaksi_detail" gorm:"index"`
	Idoutlet              string     `json:"idoutlet" gorm:"index"`
	Iditem                string     `json:"iditem" gorm:"index"`
	CreatedAt             *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt             *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	Hapus                 *int       `json:"hapus" gorm:"default:0"`
}

//
//func (d *SnapTransaksiDetail) getSlotStatus() {
//	module := pwa.GetModuleByIdMesin(*d.IdsnapMesin)
//	module.InfoSlot(module.SingleSlot.Id)
//	d.Misc = module
//}

var SnapTransaksiItemType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "snap_transaksi_item",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "id transaksi item snap",
			},
			"idsnap_transaksi": &graphql.Field{
				Type:        graphql.String,
				Description: "id transaksi snap",
			},
			"idsnap_transaksi_detail": &graphql.Field{
				Type:        graphql.String,
				Description: "id transaksi detail snap",
			},
			"idoutlet": &graphql.Field{
				Type:        graphql.String,
				Description: "id outlet snap",
			},
			"iditem": &graphql.Field{
				Type:        graphql.String,
				Description: "id item snap",
			},
		},
	})
