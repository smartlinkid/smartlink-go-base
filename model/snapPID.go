package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type SnapPid struct {
	Pid       string     `json:"pid"`
	CreatedAt *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	Hapus     *int       `json:"hapus" gorm:"default:0"`
}

var SnapPidType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "snap_pid",
		Fields: graphql.Fields{
			"pid": &graphql.Field{
				Type:        graphql.String,
				Description: "pid snap",
			},
		},
	})

var SnapPidInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "snap_pid_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"pid": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "pid snap",
			},
		},
	})
