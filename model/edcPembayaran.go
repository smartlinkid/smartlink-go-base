package model

type EdcPembayaran struct {
	Id                         string                      `json:"id" gorm:"primary_key"`
	Idedc                      string                      `json:"idedc"`
	Idowner                    string                      `json:"idowner"`
	Idoutlet                   string                      `json:"idoutlet"`
	Idkaryawan                 string                      `json:"idkaryawan"`
	JenisProviderEdc           int                         `json:"jenis_provider_edc"`
	Nominal                    float64                     `json:"nominal"`
	BiayaPersen                float32                     `json:"biaya_persen"`
	BiayaNominal               float64                     `json:"biaya_nominal"`
	Total                      float64                     `json:"total"`
	NomorKartu                 string                      `json:"nomor_kartu"`
	NomorReferensi             string                      `json:"nomor_referensi"`
	Keterangan                 string                      `json:"keterangan"`
	RefTable                   string                      `json:"ref_table"`
	RefId                      string                      `json:"ref_id"`
	RefTransaksi               string                      `json:"ref_transaksi"`
	TipeBayar                  int                         `json:"tipe_bayar"`
	StatusSettlement           int                         `json:"status_settlement"`
	Idsettlement               string                      `json:"idsettlement"`
	IdedcProvider              int                         `json:"idedc_provider"`
	Transaksi                  *Transaksi                  `json:"transaksi" gorm:"foreignkey:idtransaksi;association_foreignkey:ref_transaksi;references:ref_transaksi"`
	Karyawan                   *Karyawan                   `json:"karyawan" gorm:"foreignkey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	Outlet                     *Outlet                     `json:"outlet" gorm:"foreignkey:idoutlet;association_foreignkey:idoutlet;references:idoutlet"`
	EdcMesin                   *EdcMesin                   `json:"edc_mesin" gorm:"foreignkey:id;association_foreignkey:idedc;references:idedc"`
	ProviderKonsumen           *EdcProvider                `json:"provider_konsumen" gorm:"foreignkey:id;association_foreignkey:idedc_provider;references:idedc_provider"`
	PembayaranUmum             *PembayaranUmum             `json:"pembayaran_umum" gorm:"foreignkey:id;association_foreignkey:ref_id;references:ref_id"`
	PembayaranPembelianDeposit *PembayaranPembelianDeposit `json:"pembayaran_pembelian_deposit" gorm:"foreignkey:id;association_foreignkey:ref_id;references:ref_id"`
	PembayaranPembelianEmoney  *PembayaranPembelianEmoney  `json:"pembayaran_pembelian_emoney" gorm:"foreignkey:id;association_foreignkey:ref_id;references:ref_id"`
	BaseModel
}
