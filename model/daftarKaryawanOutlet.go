package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type DaftarKaryawanOutlet struct {
	OutletIdoutlet     string     `json:"outlet_idoutlet" gorm:"primary_key"`
	KaryawanIdkaryawan string     `json:"karyawan_idkaryawan" gorm:"primary_key"`
	CreatedAt          *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP"`
	UpdatedAt          *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
	Outlet             *Outlet    `json:"outlet" gorm:"foreignkey:idoutlet;association_foreignkey:outlet_idoutlet;references:outlet_idoutlet"`
}

var DaftarKaryawanOutletType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "daftar_karyawan_outlet_type",
		Fields: graphql.Fields{
			"outlet_idoutlet": &graphql.Field{
				Type:        graphql.String,
				Description: "id owner",
			},
			"karyawan_idkaryawan": &graphql.Field{
				Type:        graphql.String,
				Description: "karyawan idkaryawan",
			},
		},
		Description: "daftar karyawan outlet type",
	})
