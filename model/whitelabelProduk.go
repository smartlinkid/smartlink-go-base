package model

import "time"

type WhitelabelProduk struct {
	Id       string `json:"id"`
	Idowner  string `json:"idowner"`
	Idoutlet string `json:"idoutlet"`
	// Idkategori        string                           `json:"idkategori"`
	Idproduk          string                           `json:"idproduk"`
	Description       string                           `json:"description"`
	Weight            float64                          `json:"weight"`
	CreatedAt         *time.Time                       `json:"created_at"`
	UpdatedAt         *time.Time                       `json:"updated_at"`
	Produk            map[string]interface{}           `json:"produk" gorm:"-"`
	KategoriProduk    []WhitelabelProdukKategoriProduk `json:"kategori_produk" gorm:"foreignkey:id_wl_produk;association_foreignkey:id"`
	SpesifikasiProduk []WhitelabelSpesifikasiProduk    `json:"spesifikasi_produk" gorm:"foreignkey:idproduk;association_foreignkey:idproduk"`
	TransaksiFoto     []TransaksiFoto                  `json:"transaksi_foto" gorm:"foreignkey:idtransaksi;association_foreignkey:idproduk"`
}

type WhitelabelProdukKategoriProduk struct {
	IdWlProduk     string                   `json:"id_wl_produk"`
	IdWlKategori   string                   `json:"id_wl_kategori"`
	CreatedAt      *time.Time               `json:"created_at"`
	UpdatedAt      *time.Time               `json:"updated_at"`
	KategoriProduk WhitelabelKategoriProduk `json:"kategori" gorm:"foreignkey:id;association_foreignkey:id_wl_kategori"`
}

type WhitelabelKategoriProduk struct {
	Id        string     `json:"id"`
	Idowner   string     `json:"idowner"`
	Nama      string     `json:"nama"`
	Icon      string     `json:"icon"`
	CreatedAt *time.Time `json:"created_at"`
	UpdatedAt *time.Time `json:"updated_at"`
}

type WhitelabelSpesifikasiProduk struct {
	Id        string     `json:"id"`
	Idowner   string     `json:"idowner"`
	Idoutlet  string     `json:"idoutlet"`
	Idproduk  string     `json:"idproduk"`
	Nama      string     `json:"nama"`
	Konten    string     `json:"konten"`
	CreatedAt *time.Time `json:"created_at"`
	UpdatedAt *time.Time `json:"updated_at"`
}
