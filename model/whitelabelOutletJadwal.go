package model

import "time"

type WhitelabelOutletJadwal struct {
	Idowner     string     `json:"idowner"`
	Idoutlet    string     `json:"idoutlet" binding:"required"`
	SeninAktif  int        `json:"senin_aktif"`
	SeninBuka   string     `json:"senin_buka"`
	SeninTutup  string     `json:"senin_tutup"`
	SelasaAktif int        `json:"selasa_aktif"`
	SelasaBuka  string     `json:"selasa_buka"`
	SelasaTutup string     `json:"selasa_tutup"`
	RabuAktif   int        `json:"rabu_aktif"`
	RabuBuka    string     `json:"rabu_buka"`
	RabuTutup   string     `json:"rabu_tutup"`
	KamisAktif  int        `json:"kamis_aktif"`
	KamisBuka   string     `json:"kamis_buka"`
	KamisTutup  string     `json:"kamis_tutup"`
	JumatAktif  int        `json:"jumat_aktif"`
	JumatBuka   string     `json:"jumat_buka"`
	JumatTutup  string     `json:"jumat_tutup"`
	SabtuAktif  int        `json:"sabtu_aktif"`
	SabtuBuka   string     `json:"sabtu_buka"`
	SabtuTutup  string     `json:"sabtu_tutup"`
	MingguAktif int        `json:"minggu_aktif"`
	MingguBuka  string     `json:"minggu_buka"`
	MingguTutup string     `json:"minggu_tutup"`
	CreatedAt   *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt   *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
