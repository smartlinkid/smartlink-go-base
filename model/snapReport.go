package model

import (
	"github.com/graphql-go/graphql"
)

type SnapReportDevice struct {
	Id   string  `json:"id" gorm:"-"`
	Sw   bool    `json:"sw" gorm:"-"`
	St   float64 `json:"st" gorm:"-"`
	Aid  string  `json:"aid" gorm:"-"`
	Tl   float64 `json:"tl" gorm:"-"`
	Ol   bool    `json:"ol" gorm:"-"`
	Door bool    `json:"door" gorm:"-"`
	Dur  float64 `json:"dur" gorm:"-"`
	Ver  float64 `json:"ver" gorm:"-"`
	Cur  float64 `json:"cur" gorm:"-"`
	Pow  float64 `json:"pow" gorm:"-"`
	Vol  float64 `json:"vol" gorm:"-"`
	Ssid string  `json:"ssid" gorm:"-"`
	Rssi float64 `json:"rssi" gorm:"-"`
	Ip   string  `json:"ip" gorm:"_"`
}

var SnapReportDeviceType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "snap_report_device",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"sw": &graphql.Field{
				Type:        graphql.Boolean,
				Description: "",
			},
			"st": &graphql.Field{
				Type:        graphql.Float,
				Description: "",
			},
			"aid": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"tl": &graphql.Field{
				Type:        graphql.Float,
				Description: "",
			},
			"ol": &graphql.Field{
				Type:        graphql.Boolean,
				Description: "",
			},
			"door": &graphql.Field{
				Type:        graphql.Boolean,
				Description: "",
			},
			"dur": &graphql.Field{
				Type:        graphql.Float,
				Description: "",
			},
			"ver": &graphql.Field{
				Type:        graphql.Float,
				Description: "",
			},
			"cur": &graphql.Field{
				Type:        graphql.Float,
				Description: "",
			},
			"pow": &graphql.Field{
				Type:        graphql.Float,
				Description: "",
			},
			"vol": &graphql.Field{
				Type:        graphql.Float,
				Description: "",
			},
			"ssid": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"rssi": &graphql.Field{
				Type:        graphql.Float,
				Description: "",
			},
		},
		Description: "",
	})
