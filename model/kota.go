package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type Kota struct {
	Id         int        `json:"id" gorm:"primary_key"`
	NegaraId   int        `json:"negara_id"`
	ProvinsiId int        `json:"provinsi_id"`
	Nama       string     `json:"nama"`
	Status     int        `json:"status"`
	CreatedBy  string     `json:"created_by"`
	UpdatedBy  string     `json:"updated_by"`
	Type       int        `json:"type"`
	DeletedAt  *time.Time `json:"deleted_at"`
	Negara     Negara     `json:"negara" gorm:"index;foreignKey:id;association_foreignkey:negara_id;references:negara_id"`
	Provinsi   Provinsi   `json:"provinsi" gorm:"index;foreignKey:id;association_foreignkey:provinsi_id;references:provinsi_id"`
	BaseModel
}

var KotaType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "kota",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"nama": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"provinsi_id": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"provinsi": &graphql.Field{
				Type:        ProvinsiType,
				Description: "",
			},
			"negara_id": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"negara": &graphql.Field{
				Type:        NegaraType,
				Description: "",
			},
		},
	})
