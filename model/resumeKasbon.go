package model

import "time"

type ResumeKasbon struct {
	Id         int64      `json:"id" gorm:"primary_key;index;not null;auto_increment"`
	Idowner    string     `json:"idowner"`
	Idkaryawan string     `json:"idkaryawan"`
	Nominal    float64    `json:"nominal"`
	Kasbon     []Kasbon   `json:"kasbon" gorm:"foreignKey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	Karyawan   *Karyawan  `json:"karyawan" gorm:"foreignKey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	CreatedAt  *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt  *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
