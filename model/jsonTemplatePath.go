package model

import "time"

type JsonTemplatePath struct {
	Id         string    `json:"id" grom:"primarykey;index"`
	Code       string    `json:"code"`
	Path       string    `json:"path"`
	CreatedAt  time.Time `json:"created_at"`
	UpdatedAt  time.Time `json:"updated_at"`
	Keterangan string    `json:"keterangan"`
}
