package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type AcceptorMesin struct {
	IdacceptorDevice string     `json:"idacceptor_device" gorm:"primary_key;index"`
	IdsnapMesin      string     `json:"idsnap_mesin" gorm:"index"`
	CreatedAt        *time.Time `json:"created_at"`
	UpdatedAt        *time.Time `json:"updated_at"`
	SnapMesin        SnapMesin  `json:"snap_mesin" gorm:"foreignKey:id;association_foreignkey:idsnap_mesin;references:idsnap_mesin"`
}

var AcceptorMesinType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "acceptor_mesin",
		Fields: graphql.Fields{
			"idacceptor_device": &graphql.Field{
				Type:        graphql.String,
				Description: "id accpetor device",
			},
			"idsnap_mesin": &graphql.Field{
				Type:        graphql.String,
				Description: "id snap mesin",
			},
			"created_at": &graphql.Field{
				Type:        graphql.String,
				Description: "created at",
			},
			"updated_at": &graphql.Field{
				Type:        graphql.String,
				Description: "updated at",
			},
			"snap_mesin": &graphql.Field{
				Type:        SnapMesinType,
				Description: "list snap mesin",
			},
		},
	})

var AcceptorMesinInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "acceptor_mesin_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"idacceptor_device": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id accpetor device",
			},
			"idsnap_mesin": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id snap mesin",
			},
			//"snap_mesin": &graphql.InputObjectFieldConfig{
			//	Type:        graphql.NewList(SnapMesinType),
			//	Description: "list snap mesin",
			//},
		},
	})
