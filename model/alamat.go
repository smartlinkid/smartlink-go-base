package model

type Alamat struct {
	Idalamat           string  `json:"idalamat" gorm:"primary_key"`
	CustomerIdcustomer string  `json:"customer_idcustomer"`
	Lokasi             string  `json:"lokasi"`
	Tetap              int     `json:"tetap"`
	Judul              string  `json:"judul"`
	DetailAlamat       string  `json:"detail_alamat"`
	Latitude           float64 `json:"latitude"`
	Longitude          float64 `json:"longitude"`
	Idprovinsi         int     `json:"idprovinsi"`
	Idkota             int     `json:"idkota"`
	Idkecamatan        int     `json:"idkecamatan"`
	Idkelurahan        int     `json:"idkelurahan"`
	BaseModel
}
