package model

import "time"

type DetailPembayaranGajiKuantitas struct {
	Id                         int        `json:"id" gorm:"primary_key;index;not null;auto_increment"`
	IddetailPembayaranGajiUpah int        `json:"iddetail_pembayaran_gaji_upah"`
	Idsatuan                   int        `json:"idsatuan"`
	Kuantitas                  float64    `json:"kuantitas"`
	CreatedAt                  *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt                  *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
