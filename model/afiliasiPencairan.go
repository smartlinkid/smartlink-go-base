package model

import "time"

type AfiliasiPencairan struct {
	Id                   int        `json:"id" gorm:"index;primary_key;not null"`
	Idowner              string     `json:"idowner" gorm:"index"`
	WaktuTransfer        time.Time  `json:"waktu_transfer"`
	Status               int        `json:"status"`
	Amount               float64    `json:"amount"`
	DataBankId           int        `json:"data_bank_id"`
	NoRekening           string     `json:"no_rekening"`
	NamaPemilik          string     `json:"nama_pemilik"`
	Keterangan           string     `json:"keterangan"`
	BuktiTransfer        string     `json:"bukti_transfer"`
	IdrekeningPerusahaan string     `json:"idrekening_perusahaan"`
	TransferedBy         string     `json:"transfered_by"`
	Hapus                int        `json:"hapus"`
	CreatedAt            *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt            *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	Bank                 DataBank   `json:"bank" gorm:"foreignKey:data_bank_id;association_foreignkey:id;references:id"`
}
