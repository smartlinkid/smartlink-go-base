package model

type TrainingTime struct {
	IdJenisTraining string `json:"id_jenis_training" gorm:"primary_key"`
	JamMulai        []byte `json:"jam_mulai" gorm:"primary_key"`
	JamSelesai      []byte `json:"jam_selesai" gorm:"primary_key"`
	BaseModel
}
