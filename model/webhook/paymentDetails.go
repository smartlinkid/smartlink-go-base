package webhook

type PaymentDetails struct {
	ReceiptId string `json:"receipt_id"`
	Source    string `json:"source"`
}
