package webhook

import "github.com/graphql-go/graphql"

type ResponseWebhookPayment struct {
	Status    int    `json:"status"`
	PaymentId string `json:"payment_id"`
	Message   string `json:"message"`
	RefId     string `json:"ref_id"`
	RefTable  string `json:"ref_table"`
}

type ResponseWebhookPaymentJson struct {
	WebhookPayment ResponseWebhookPayment `json:"webhookPayment"`
}

var ResponseWebhookPaymentObject = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "webhook_payment",
		Fields: graphql.Fields{
			"status": &graphql.Field{
				Type:        graphql.Int,
				Description: "Status ",
			},
			"payment_id": &graphql.Field{
				Type:        graphql.String,
				Description: "payment_id string",
			},
			"message": &graphql.Field{
				Type:        graphql.String,
				Description: "message string",
			},
			"ref_table": &graphql.Field{
				Type:        graphql.String,
				Description: "Ref Tabler",
			},
			"ref_id": &graphql.Field{
				Type:        graphql.String,
				Description: "Ref Tabler",
			},
		},
	})
