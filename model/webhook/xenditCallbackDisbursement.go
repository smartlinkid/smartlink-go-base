package webhook

type XenditCallbackDisbursement struct {
	Id                      string  `json:"id"`
	Status                  string  `json:"status"`
	UserId                  string  `json:"user_id"`
	ExternalId              string  `json:"external_id"`
	BankCode                string  `json:"bank_code"`
	AccountHolderName       string  `json:"account_holder_name"`
	DisbursementDescription string  `json:"disbursement_description"`
	Amount                  float64 `json:"amount"`
	Created                 string  `json:"created"`
	FailureCode             string  `json:"failure_code"`
}
