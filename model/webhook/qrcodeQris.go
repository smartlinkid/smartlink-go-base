package webhook

type QrcodeQris struct {
	Id         string `json:"id"`
	ExternalId string `json:"external_id"`
	QrString   string `json:"qr_string"`
	Type       string `json:"type"`
}
