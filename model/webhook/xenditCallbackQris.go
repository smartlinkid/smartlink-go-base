package webhook

type XenditCallbackQris struct {
	Event          string         `json:"event"`
	Id             string         `json:"id"`
	Amount         float64        `json:"amount"`
	Created        string         `json:"created"`
	Status         string         `json:"status"`
	Qrcode         QrcodeQris     `json:"qr_code"`
	PaymentDetails PaymentDetails `json:"payment_details"`
}
