package model

import "time"

type RekeningPerusahaan struct {
	Id          int       `json:"id" gorm:"primary_key"`
	DataBankId  int       `json:"data_bank_id"`
	NoRekening  string    `json:"no_rekening"`
	NamaPemilik string    `json:"nama_pemilik"`
	Type        string    `json:"type"`
	Hapus       int       `json:"hapus"`
	NamaCabang  string    `json:"nama_cabang"`
	CreatedAt   time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt   time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
	Idakun      string    `json:"idakun"`
	DataBank    DataBank  `json:"data_bank"`
}
