package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type SnapTransaksiDetail struct {
	Id               string      `json:"id" gorm:"primary_key;index;not null"`
	IdsnapTransaksi  string      `json:"idsnap_transaksi" gorm:"index"`
	IdsnapLayanan    string      `json:"idsnap_layanan" gorm:"index"`
	IdcustomerGlobal *string     `json:"idcustomer_global" gorm:"index"`
	Idkaryawan       *string     `json:"idkaryawan" gorm:"index"`
	Idoutlet         string      `json:"idoutlet" gorm:"index"`
	Idowner          string      `json:"idowner" gorm:"index"`
	Idmesin          string      `json:"idmesin" gorm:"index"`
	TimeLeft         int64       `json:"time_left"`
	Status           int         `json:"status"`
	StartAt          *time.Time  `json:"start_at"`
	StopAt           *time.Time  `json:"stop_at"`
	Qty              float64     `json:"qty"`
	Harga            float64     `json:"harga"`
	HargaTotal       float64     `json:"harga_total"`
	Durasi           int64       `json:"durasi"`
	Tipe             int         `json:"tipe"`
	SnapLayanan      SnapLayanan `json:"snap_layanan" gorm:"foreignKey:idsnap_layanan;association_foreignkey:id;references:id"`
	SnapMesin        SnapMesin   `json:"snap_mesin" gorm:"foreignKey:idmesin;association_foreignkey:id;references:id"`
	SnapMesinRelated *SnapMesin  `json:"snap_mesin_related" gorm:"foreignKey:idmesin;association_foreignkey:id;references:id"`
	Karyawan         *Karyawan   `json:"karyawan" gorm:"foreignkey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	CreatedAt        *time.Time  `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt        *time.Time  `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
	Hapus            *int        `json:"hapus" gorm:"default:0"`
}

//
//func (d *SnapTransaksiDetail) getSlotStatus() {
//	module := pwa.GetModuleByIdMesin(*d.IdsnapMesin)
//	module.InfoSlot(module.SingleSlot.Id)
//	d.Misc = module
//}

var SnapTransaksiDetailType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "snap_transaksi_detail",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "id detail transaksi snap",
			},
			"idsnap_transaksi": &graphql.Field{
				Type:        graphql.String,
				Description: "id transaksi snap",
			},
			"idsnap_layanan": &graphql.Field{
				Type:        graphql.String,
				Description: "id layanan snap",
			},
			"idcustomer_global": &graphql.Field{
				Type:        graphql.String,
				Description: "id customer global",
			},
			"idkaryawan": &graphql.Field{
				Type:        graphql.String,
				Description: "id karyawan",
			},
			"idoutlet": &graphql.Field{
				Type:        graphql.String,
				Description: "id outlet snap",
			},
			"idmesin": &graphql.Field{
				Type:        graphql.String,
				Description: "id mesin snap",
			},
			"time_left": &graphql.Field{
				Type:        graphql.Int,
				Description: "time left",
			},
			"status": &graphql.Field{
				Type:        graphql.Int,
				Description: "status. 1: off; 2: on for trial; 3 on; 4: finish; 5 intterrupt",
			},
			"start_at": &graphql.Field{
				Type:        graphql.String,
				Description: "start at",
			},
			"stop_at": &graphql.Field{
				Type:        graphql.String,
				Description: "stop at",
			},
			"qty": &graphql.Field{
				Type:        graphql.Float,
				Description: "kuantitas",
			},
			"harga": &graphql.Field{
				Type:        graphql.Float,
				Description: "harga ",
			},
			"harga_total": &graphql.Field{
				Type:        graphql.Float,
				Description: "harga total",
			},
			"durasi": &graphql.Field{
				Type:        graphql.Int,
				Description: "durasi",
			},
			"tipe": &graphql.Field{
				Type:        graphql.Int,
				Description: "tipe, 1: self service; 2: drop off",
			},
			"snap_layanan": &graphql.Field{
				Type:        SnapLayananType,
				Description: "",
			},
			"snap_mesin": &graphql.Field{
				Type:        SnapMesinType,
				Description: "",
			},
			"karyawan": &graphql.Field{
				Type:        KaryawanType,
				Description: "",
			},
		},
	})

var SnapTransaksiDetailInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "snap_transaksi_detail_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id detail transaksi snap",
			},
			//"idsnap_transaksi": &graphql.InputObjectFieldConfig{
			//	Type:        graphql.String,
			//	Description: "id transaksi snap",
			//},
			"idsnap_layanan": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id layanan snap",
			},
			//"idcustomer_global": &graphql.InputObjectFieldConfig{
			//	Type:        graphql.String,
			//	Description: "id customer global",
			//},
			//"idkaryawan": &graphql.InputObjectFieldConfig{
			//	Type:        graphql.String,
			//	Description: "id karyawan",
			//},
			//"idoutlet": &graphql.InputObjectFieldConfig{
			//	Type:        graphql.String,
			//	Description: "id outlet snap",
			//},
			"idmesin": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id mesin snap",
			},
			//"time_left": &graphql.InputObjectFieldConfig{
			//	Type:        graphql.Int,
			//	Description: "time left",
			//},
			"status": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "status. 1: off; 2: on for trial; 3 on; 4: finish; 5 intterrupt",
			},
			//"start_at": &graphql.InputObjectFieldConfig{
			//	Type:        graphql.String,
			//	Description: "start at",
			//},
			//"stop_at": &graphql.InputObjectFieldConfig{
			//	Type:        graphql.String,
			//	Description: "stop at",
			//},
			"qty": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "kuantitas",
			},
			"harga": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "harga ",
			},
			"harga_total": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "harga total",
			},
			"durasi": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "durasi",
			},
			"tipe": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "tipe, 1: self service; 2: drop off",
			},
			//"snap_layanan": &graphql.InputObjectFieldConfig{
			//	Type:        SnapLayananType,
			//	Description: "",
			//},
		},
	})
