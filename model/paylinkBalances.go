package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type PaylinkBalances struct {
	UserId    string     `json:"user_id" gorm:"primary_key;"`
	UserType  int        `json:"user_type"`
	Total     float64    `json:"total"`
	CreatedAt *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	Hapus     *int       `json:"hapus" gorm:"default:0"`
}

var PaylinkBalancesObject = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "paylink_balance",
		Fields: graphql.Fields{
			"user_id": &graphql.Field{
				Type:        graphql.String,
				Description: "user id",
			},
			"user_type": &graphql.Field{
				Type:        graphql.Int,
				Description: "user type",
			},
			"total": &graphql.Field{
				Type:        graphql.Float,
				Description: "total balance",
			},
		},
	})

var PaylinkBalanceInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "balance_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"user_id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "User Id ",
			},
			"user_type": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "User Type ",
			},
			"amount": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "Amount ",
			},
			"reference_type": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "Reference Type (nama table yang ikut memproses)",
			},
			"reference_id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "Reference ID , contoh id transaksi ",
			},
			"description": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "Deskripsi, contoh digunakan untuk transaksi MHQYRU213002992020",
			},
			"idowner": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "idowner",
			},
			"idoutlet": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "idoutlet",
			},
			"fee_transaksi": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "Fee Transaksi ",
			},

		},
	})
