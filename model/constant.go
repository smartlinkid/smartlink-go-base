package model

type Constant struct {
	Key        string `json:"key"`
	Val        string `json:"value"`
	Keterangan string `json:"keterangan"`
}
