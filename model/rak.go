package model

type Rak struct {
	Idrak          int            `json:"idrak" gorm:"primary_key"`
	IdrakKategori  string         `json:"idrak_kategori"`
	OutletIdoutlet string         `json:"outlet_idoutlet" gorm:"primary_key"`
	Nama           string         `json:"nama"`
	IsiLipat       int            `json:"isi_lipat"`
	IsiGantung     int            `json:"isi_gantung"`
	IsiGulung      int            `json:"isi_gulung"`
	Jenis          int            `json:"jenis"`
	RakKategori    *RakKategori   `json:"rak_kategori" gorm:"foreignkey:idkategori;association_foreignkey:idrak_kategori;references:idrak_kategori"`
	TransaksiRak   []TransaksiRak `json:"transaksi_rak" gorm:"foreignkey:idrak;association_foreignkey:idrak;references:idrak"`
	BaseModel
}
