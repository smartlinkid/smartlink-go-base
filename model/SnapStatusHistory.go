package model

import "time"

type SnapStatusHistory struct {
	Id                    int64  `json:"id" gorm:"primary_key;index;auto_increment"`
	IdsnapMesin           string `json:"idsnap_mesin" gorm:"index"`
	IdsnapTransaksiDetail string `json:"idsnap_transaksi_detail" gorm:"index"`
	Status                int
	CreatedAt             *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt             *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	Hapus                 *int       `json:"hapus" gorm:"default:0"`
}
