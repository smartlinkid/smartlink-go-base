package model

import (
	"time"

	"github.com/jinzhu/gorm"
)

type HistoryDeposit struct {
	Id         int64      `json:"id" gorm:"primary_key;index;auto_increment"`
	Iddeposit  string     `json:"iddeposit" gorm:"index"`
	Idowner    string     `json:"idowner" gorm:"index"`
	Idcustomer string     `json:"idcustomer" gorm:"index"`
	RefType    string     `json:"ref_type" gorm:"index"`
	RefId      string     `json:"ref_id" gorm:"index"`
	DetailType string     `json:"detail_type" gorm:"index"`
	DetailId   string     `json:"detail_id" gorm:"index"`
	Masuk      float64    `json:"masuk"`
	Keluar     float64    `json:"keluar"`
	SaldoAkhir float64    `json:"saldo_akhir"`
	CreatedAt  *time.Time `json:"created_at" gorm:"index"`
	UpdatedAt  *time.Time `json:"updated_at"`
}

func (receiver *HistoryDeposit) Save(db *gorm.DB) error {
	e := db.Create(receiver).Error
	return e
}

type ESHistoryDeposit struct {
	Id         string     `json:"id"`
	Iddeposit  string     `json:"iddeposit:"`
	Idowner    string     `json:"idowner"`
	Idcustomer string     `json:"idcustomer"`
	RefType    string     `json:"ref_type"`
	RefId      string     `json:"ref_id"`
	DetailType string     `json:"detail_type"`
	DetailId   string     `json:"detail_id"`
	Masuk      float64    `json:"masuk"`
	Keluar     float64    `json:"keluar"`
	SaldoAkhir float64    `json:"saldo_akhir"`
	CreatedAt  time.Time  `json:"created_at"`
	UpdatedAt  *time.Time `json:"updated_at"`
}
