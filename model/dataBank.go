package model

import (
	"time"

	"github.com/graphql-go/graphql"
)

type DataBank struct {
	Id         string     `json:"id" gorm:"primary_key"`
	Nama       string     `json:"nama"`
	Kode       string     `json:"kode"`
	SwiftCode  string     `json:"swift_code"`
	Keterangan string     `json:"keterangan"`
	Logo       string     `json:"logo"`
	Type       string     `json:"type"`
	CreatedAt  *time.Time `json:"created_at"`
}

var DataBankObject = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "databank_object",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "id ",
			},
			"nama": &graphql.Field{
				Type:        graphql.String,
				Description: "nama",
			},
			"kode": &graphql.Field{
				Type:        graphql.String,
				Description: "kode",
			},
			"swift_code": &graphql.Field{
				Type:        graphql.String,
				Description: "swiftCode",
			},
			"keterangan": &graphql.Field{
				Type:        graphql.String,
				Description: "keterangan",
			},
			"logo": &graphql.Field{
				Type:        graphql.String,
				Description: "logo",
			},
			"created_at": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "CreatedAt",
			},
		},
	})
