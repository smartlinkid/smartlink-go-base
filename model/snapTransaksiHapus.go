package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type SnapTransaksiHapus struct {
	Id                   int64          `json:"id" gorm:"primary_key;index"`
	IdsnapTransaksi      string         `json:"idsnap_transaksi" gorm:"index"`
	TanggalRequest       int64          `json:"tanggal_request"`
	Keterangan           string         `json:"keterangan"`
	Status               int            `json:"status"`
	TanggalKonfirmasi    *time.Time     `json:"tanggal_konfirmasi"`
	KeteranganKonfirmasi string         `json:"keterangan_konfirmasi"`
	Idowner              string         `json:"idowner" gorm:"index"`
	Idkaryawan           *string        `json:"idkaryawan" gorm:"index"`
	Idoutlet             string         `json:"idoutlet" gorm:"index"`
	KeteranganStatus     string         `json:"keterangan_status"`
	IdakunRefund         *string        `json:"idakun_refund"`
	CreatedAt            *time.Time     `json:"created_at" gorm:"index"`
	UpdatedAt            *time.Time     `json:"updated_at"`
	Hapus                int            `json:"hapus"`
	Outlet               *Outlet        `json:"outlet" gorm:"foreignkey:idoutlet;association_foreignkey:idoutlet;references:idoutlet"`
	Owner                *Owner         `json:"owner" gorm:"foreignkey:idowner;association_foreignkey:idowner;references:idowner"`
	Karyawan             *Karyawan      `json:"karyawan" gorm:"foreignkey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	SnapTransaksi        *SnapTransaksi `json:"snap_transaksi" gorm:"foreignkey:id;association_foreignkey:idsnap_transaksi;references:idsnap_transaksi"`
}

var SnapTransaksiHapusType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "snap_transaksi_hapus",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "id (bigint)",
			},
			"idsnap_transaksi": &graphql.Field{
				Type:        graphql.String,
				Description: "idsnap_transaksi",
			},
			"tanggal_request": &graphql.Field{
				Type:        graphql.String,
				Description: "tanggal_request (bigint)",
			},
			"keterangan": &graphql.Field{
				Type:        graphql.String,
				Description: "keterangan",
			},
			"status": &graphql.Field{
				Type:        graphql.Int,
				Description: "status: 0 pending; 1 penghapusan diterima/benar benar dihapus; 2 penghapusan ditolak",
			},
			"tanggal_konfirmasi": &graphql.Field{
				Type:        graphql.String,
				Description: "tanggal_konfirmasi",
			},
			"keterangan_konfirmasi": &graphql.Field{
				Type:        graphql.String,
				Description: "keterangan_konfirmasi",
			},
			"idowner": &graphql.Field{
				Type:        graphql.String,
				Description: "idowner",
			},
			"idkaryawan": &graphql.Field{
				Type:        graphql.String,
				Description: "idkaryawan",
			},
			"idoutlet": &graphql.Field{
				Type:        graphql.String,
				Description: "idoutlet",
			},
			"keterangan_status": &graphql.Field{
				Type:        graphql.String,
				Description: "keterangan_status",
			},
			"created_at": &graphql.Field{
				Type:        graphql.String,
				Description: "created_at",
			},
			"updated_at": &graphql.Field{
				Type:        graphql.String,
				Description: "updated_at",
			},
			"hapus": &graphql.Field{
				Type:        graphql.Int,
				Description: "hapus",
			},
		},
	})
