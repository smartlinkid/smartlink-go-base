package model

type LayananTahapan struct {
	Idtahapan int    `json:"idtahapan" gorm:"primary_key;index"`
	Idlayanan string `json:"idlayanan" gorm:"primary_key;index"`
}
