package model

type SuratKirimWorkshop struct {
	IdsuratKirimWorkshop string    `json:"idsurat_kirim_workshop" gorm:"primary_key"`
	IdkaryawanKurir      string    `json:"idkaryawan_kurir"`
	IdkaryawanKasir      string    `json:"idkaryawan_kasir"`
	IdkaryawanWorkshop   string    `json:"idkaryawan_workshop"`
	OutletIdoutlet       string    `json:"outlet_idoutlet"`
	WorkshopIdworkshop   string    `json:"workshop_idworkshop"`
	TanggalKirim         int64     `json:"tanggal_kirim"`
	TanggalTerima        int64     `json:"tanggal_terima"`
	Keterangan           string    `json:"keterangan"`
	Valid                int       `json:"valid"`
	Status               int       `json:"status"`
	Validasi             int       `json:"validasi"`
	Workshop             *Workshop `json:"workshop" gorm:"foreignkey:idworkshop;association_foreignkey:workshop_idworkshop;references:workshop_idworkshop"`
	KaryawanKurir        *Karyawan `json:"karyawan_kurir" gorm:"foreignkey:idkaryawan;association_foreignkey:idkaryawan_kurir;references:idkaryawan_kurir"`
	BaseModel
}
