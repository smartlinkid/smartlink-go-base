package model

type WhitelabelPesanan struct {
	Id                string              `json:"id" gorm:"primary_key"`
	WhitelabelJenisId string              `json:"whitelabel_jenis_id"`
	OwnerIdowner      string              `json:"owner_idowner"`
	Terbayar          float64             `json:"terbayar"`
	SisaCicilan       int                 `json:"sisa_cicilan"`
	Tagihan           float64             `json:"tagihan"`
	WhitelabelCicilan []WhitelabelCicilan `json:"whitelabel_cicilan" gorm:"foreignkey:pesanan_id;association_foreignkey:id;references:id"`
	WhitelabelJenis   *WhitelabelJenis    `json:"whitelabel_jenis" gorm:"foreignkey:id;association_foreignkey:whitelabel_jenis_id;references:whitelabel_jenis_id"`
	Owner             *Owner              `json:"owner" gorm:"foreignkey:idowner;association_foreignkey:owner_idowner;references:owner_idowner"`
	BaseModel
}
