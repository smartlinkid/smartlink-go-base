package model

import "time"

type DataKoinOwner struct {
	Idowner   string     `json:"idowner" gorm:"primary_key;index"`
	Jumlah    int        `json:"jumlah"`
	MasaAktif int64      `json:"masa_aktif"`
	Jenis     int        `json:"jenis"`
	CreatedAt *time.Time `json:"created_at"`
	UpdatedAt *time.Time `json:"updated_at"`
}
