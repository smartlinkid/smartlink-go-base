package model

type DetailPembelianEmoney struct {
	PembelianEmoneyIdpembelianEmoney string     `json:"pembelian_emoney_idpembelian_emoney" gorm:"primary_key"`
	DataEmoneyIddataEmoney           string     `json:"data_emoney_iddata_emoney"`
	Jumlah                           int        `json:"jumlah"`
	HargaTotal                       float64    `json:"harga_total"`
	Harga                            float64    `json:"harga"`
	Keterangan                       string     `json:"keterangan"`
	DataEmoney                       DataEmoney `json:"data_emoney" gorm:"foreignkey:iddata_emoney;association_foreignkey:data_emoney_iddata_emoney;references:data_emoney_iddata_emoney"`
	BaseModel
}
