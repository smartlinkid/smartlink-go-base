package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type SnapMesinHapus struct {
	Id          string     `json:"id" gorm:"index;primary_key;not null"`
	IdsnapMesin string     `json:"idsnap_mesin" gorm:"index;primary_key;not null"`
	Idowner     string     `json:"idowner" gorm:"index"`
	Idoutlet    string     `json:"idoutlet" gorm:"index"`
	Nama        string     `json:"nama"`
	Serial      string     `json:"serial"`
	Jenis       int        `json:"jenis"` // 1 cuci 2 kering
	Keterangan  string     `json:"keterangan"`
	IdsnapTag   int        `json:"idsnap_tag" gorm:"index"`
	IdsnapTarif int        `json:"idsnap_tarif" gorm:"index"`
	Tarif       float64    `json:"tarif"`
	Gen         int        `json:"gen"`
	Door        int        `json:"door"`
	CreatedAt   *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt   *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
}

var SnapMesinHapusType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "snap_mesin",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "id mesin snap",
			},
			"idowner": &graphql.Field{
				Type:        graphql.String,
				Description: "id owner snap",
			},
			"idoutlet": &graphql.Field{
				Type:        graphql.String,
				Description: "id outlet snap",
			},
			"nama": &graphql.Field{
				Type:        graphql.String,
				Description: "nama mesin snap",
			},
			"jenis": &graphql.Field{
				Type:        graphql.Int,
				Description: "jenis mesin snap. 1: cuci 2: kering",
			},
			"keterangan": &graphql.Field{
				Type:        graphql.String,
				Description: "keterangan mesin snap",
			},
			"idsnap_tag": &graphql.Field{
				Type:        graphql.Int,
				Description: "id tag snap",
			},
			"serial": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"idsnap_tarif": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"tarif": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"gen": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"door": &graphql.Field{
				Type:        graphql.Int,
				Description: "Support untuk mesin. 1 support 0 ga",
			},
		},
		Description: "snap mesin data",
	},
)
