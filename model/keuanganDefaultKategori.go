package model

type KeuanganDefaultKategori struct {
	IdkategoriAkun string `json:"idkategori_akun" gorm:"primary_key"`
	NamaKategori   string `json:"nama_kategori"`
	IdkelompokAkun string `json:"idkelompok_akun"`
	BaseModel
}
