package model

import "time"

type PaketExpress struct {
	Id        int        `json:"id" gorm:"primary_key"`
	Idowner   string     `json:"idowner"`
	Idoutlet  string     `json:"idoutlet"`
	Nama      string     `json:"nama"`
	Durasi    float64    `json:"durasi"`
	Tarif     float64    `json:"tarif"`
	CreatedAt *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
	Hapus     *int       `json:"hapus" gorm:"default:0"`
}
