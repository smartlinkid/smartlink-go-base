package model

import "time"

type WhitelabelJenis struct {
	Id           string    `json:"id" gorm:"primary_key"`
	Nama         string    `json:"nama"`
	Harga        float64   `json:"harga"`
	Keterangan   string    `json:"keterangan"`
	CicilanKali  int       `json:"cicilan_kali"`
	PeriodeAwal  time.Time `json:"periode_awal"`
	PeriodeAkhir time.Time `json:"periode_akhir"`
	BaseModel
}
