package model

type PemakaianDeposit struct {
	Id             int          `json:"id" gorm:"primary_key"`
	Idtransaksi    string       `json:"idtransaksi"`
	Iddeposit      string       `json:"iddeposit"`
	Jumlah         float64      `json:"jumlah"`
	Iddetlayanan   string       `json:"iddetlayanan"`
	QuotaAwal      float64      `json:"quota_awal"`
	QuotaAkhir     float64      `json:"quota_akhir"`
	QuotaHapus     float64      `json:"quota_hapus"`
	MasaAktifAwal  int          `json:"masa_aktif_awal"`
	MasaAktifAkhir int          `json:"masa_aktif_akhir"`
	Deposit        DataDeposit2 `json:"deposit"  gorm:"foreignkey:iddata_deposit;association_foreignkey:iddeposit;references:iddeposit"`
	Detlayanan     Detlayanan   `json:"detlayanan"  gorm:"foreignkey:id;association_foreignkey:iddetlayanan;references:iddetlayanan"`
	BaseModel
}
