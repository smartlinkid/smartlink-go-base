package model

import "time"

type WaBlastStatus struct {
	IdwaBlast     int64      `json:"idwa_blast" gorm:"primary_key;index"`
	Idowner       string     `json:"idowner" gorm:"index"`
	StartId       int64      `json:"start_id" gorm:"index"`
	EndId         int64      `json:"end_id" gorm:"index"`
	LastProcessed int64      `json:"last_processed" gorm:"index"`
	LastStatus    int        `json:"last_status"`
	LastTrial     int        `json:"last_trial"`
	CreatedAt     *time.Time `json:"created_at"`
	UpdatedAt     *time.Time `json:"updated_at"`
	Hapus         int        `json:"hapus"`
	WaBlast       WaBlast    `json:"wa_blast" gorm:"foreignkey:id;association_foreignkey:idwa_blast;references:idwa_blast"`
}
