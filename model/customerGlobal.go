package model

import (
	"fmt"
	"time"

	"github.com/graphql-go/graphql"
)

type CustomerGlobal struct {
	Id                     string                 `json:"id" gorm:"primary_key;index;not null"`
	CountryCode            string                 `json:"country_code" gorm:"index"`
	Email                  string                 `json:"email" gorm:"index"`
	Telp                   string                 `json:"telp" gorm:"index"`
	Saldo                  float64                `json:"saldo"`
	Idowner                string                 `json:"idowner" gorm:"index"`
	Idoutlet               string                 `json:"idoutlet" gorm:"index"`
	NamaAwal               string                 `json:"nama_awal"`
	NamaAkhir              string                 `json:"nama_akhir"`
	JenisKelamin           int                    `json:"jenis_kelamin"`
	Ttl                    time.Time              `json:"ttl"`
	Alamat                 string                 `json:"alamat"`
	Kota                   string                 `json:"kota"`
	Idkota                 int                    `json:"idkota" gorm:"index"`
	Provinsi               string                 `json:"provinsi"`
	Idprovinsi             int                    `json:"idprovinsi" gorm:"index"`
	Negara                 string                 `json:"negara"`
	Idnegara               int                    `json:"idnegara" gorm:"index"`
	Pass                   string                 `json:"-"`
	FullTelp               string                 `json:"full_telp"`
	DataLengkap            int                    `json:"data_lengkap" gorm:"default:0"`
	Verified               int                    `json:"verified"`
	VerifiedAt             time.Time              `json:"verified_at"`
	LastLogin              time.Time              `json:"last_login"`
	Otp                    *string                `json:"-"`
	ExpiredOtp             time.Time              `json:"-"`
	Link                   *string                `json:"-"`
	ExpiredLink            time.Time              `json:"-"`
	OtpGlobal              *string                `json:"-"`
	ExpiredOtpGlobal       time.Time              `json:"-"`
	UsePass                bool                   `json:"use_pass" gorm:"-"`
	Customer               []Customer             `json:"customer" gorm:"foreignkey:idcustomer_global;association_foreignkey:id;references:id"`
	FirebaseRegistrationId string                 `json:"firebase_registration_id"`
	Outlet                 *Outlet                `json:"outlet" gorm:"foreignkey:idoutlet;association_foreignkey:idoutlet;references:idoutlet"`
	CustomerGlobalRelate   []CustomerGlobalRelate `json:"customer_global_relate" gorm:"index;foreignKey:idcustomer_global;association_foreignkey:id;references:id"`
	CreatedAt              *time.Time             `json:"created_at"`
	UpdatedAt              *time.Time             `json:"updated_at"`
	//Hapus                  *int                   `json:"hapus"`
	BaseModel
}

func (c CustomerGlobal) SendableTelp() string {
	return fmt.Sprintf("+%s%s", c.CountryCode, c.Telp)
}

func (c *CustomerGlobal) AssignFullTelp() {
	c.FullTelp = c.CountryCode + c.Telp
}

var CustomerGlobalType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "customer_global",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "id customer global",
			},
			"country_code": &graphql.Field{
				Type:        graphql.String,
				Description: "kode negara",
			},
			"email": &graphql.Field{
				Type:        graphql.String,
				Description: "email customer global",
			},
			"telp": &graphql.Field{
				Type:        graphql.String,
				Description: "nomor handphone customer global",
			},
			"saldo": &graphql.Field{
				Type:        graphql.Float,
				Description: "saldo customer global",
			},
			"idowner": &graphql.Field{
				Type:        graphql.String,
				Description: "id owner customer global",
			},
			"idoutlet": &graphql.Field{
				Type:        graphql.String,
				Description: "id outlet customer global",
			},
			"nama_awal": &graphql.Field{
				Type:        graphql.String,
				Description: "nama awal customer global",
			},
			"nama_akhir": &graphql.Field{
				Type:        graphql.String,
				Description: "nama akhir customer global",
			},
			"jenis_kelamin": &graphql.Field{
				Type:        graphql.Int,
				Description: "jenis kelamin customer global",
			},
			"ttl": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "ttl customer global",
			},
			"alamat": &graphql.Field{
				Type:        graphql.String,
				Description: "alamat customer global",
			},
			"kota": &graphql.Field{
				Type:        graphql.String,
				Description: "kota customer global",
			},
			"idkota": &graphql.Field{
				Type:        graphql.Int,
				Description: "id kota customer global",
			},
			"provinsi": &graphql.Field{
				Type:        graphql.String,
				Description: "provinsi customer global",
			},
			"idprovinsi": &graphql.Field{
				Type:        graphql.Int,
				Description: "id provinsi customer global",
			},
			"negara": &graphql.Field{
				Type:        graphql.String,
				Description: "negara customer global",
			},
			"idnegara": &graphql.Field{
				Type:        graphql.Int,
				Description: "id negara customer global",
			},
			"full_telp": &graphql.Field{
				Type:        graphql.String,
				Description: "nomor handphone lengkap customer global",
			},
			"data_lengkap": &graphql.Field{
				Type:        graphql.Int,
				Description: "status kelengkapan data customer global",
			},
			"verified": &graphql.Field{
				Type:        graphql.Int,
				Description: "status verifikasi data customer global",
			},
			"verified_at": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "tanggal verifikasi customer global",
			},
			"last_login": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "tanggal terakhir kali login customer global",
			},
			"firebase_registration_id": &graphql.Field{
				Type:        graphql.String,
				Description: "firebase registration id",
			},
			"use_pass": &graphql.Field{
				Type:        graphql.String,
				Description: "check customer global using pass or not",
			},
			"customer_global_relate": &graphql.Field{
				Type:        graphql.NewList(CustomerGlobalRelateType),
				Description: "",
			},
			"created_at": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"updated_at": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			//"hapus": &graphql.Field{
			//	Type:        graphql.Int,
			//	Description: "",
			//},
		},
	})

var CustomerGlobalInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "customer_global_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id customer global",
			},
			"country_code": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "kode negara",
			},
			"email": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "email customer global",
			},
			"telp": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "nomor handphone customer global",
			},
			"saldo": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "saldo customer global",
			},
			"idowner": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id owner customer global",
			},
			"idoutlet": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id outlet customer global",
			},
			"nama_awal": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "nama awal customer global",
			},
			"nama_akhir": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "nama akhir customer global",
			},
			"jenis_kelamin": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "jenis kelamin customer global",
			},
			"ttl": &graphql.InputObjectFieldConfig{
				Type:        graphql.DateTime,
				Description: "ttl customer global",
			},
			"alamat": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "alamat customer global",
			},
			"kota": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "kota customer global",
			},
			"idkota": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "id kota customer global",
			},
			"provinsi": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "provinsi customer global",
			},
			"idprovinsi": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "id provinsi customer global",
			},
			"negara": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "negara customer global",
			},
			"idnegara": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "id negara customer global",
			},
			"full_telp": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "nomor handphone lengkap customer global",
			},
			"data_lengkap": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "status kelengkapan data customer global",
			},
			"verified": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "status verifikasi data customer global",
			},
			"verified_at": &graphql.InputObjectFieldConfig{
				Type:        graphql.DateTime,
				Description: "tanggal verifikasi customer global",
			},
			"last_login": &graphql.InputObjectFieldConfig{
				Type:        graphql.DateTime,
				Description: "tanggal terakhir kali login customer global",
			},
			"pass": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "password customer global",
			},
			//"customer_global_relate": &graphql.InputObjectFieldConfig{
			//	Type:        CustomerGlobalRelateType,
			//	Description: "",
			//},
		},
	})
