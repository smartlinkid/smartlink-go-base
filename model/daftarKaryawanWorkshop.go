package model

import "time"

type DaftarKaryawanWorkshop struct {
	WorkshopIdworkshop string              `json:"workshop_idworkshop" gorm:"primary_key"`
	KaryawanIdkaryawan string              `json:"karyawan_idkaryawan" gorm:"primary_key"`
	CreatedAt          *time.Time          `json:"created_at" gorm:"default:CURRENT_TIMESTAMP"`
	UpdatedAt          *time.Time          `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
	Workshop           *Workshop           `json:"workshop" gorm:"foreignkey:idworkshop;association_foreignkey:workshop_idworkshop;references:workshop_idworkshop"`
	PekerjaanKaryawan  []PekerjaanKaryawan `json:"pekerjaan_karyawan" gorm:"foreignkey:karyawan_idkaryawan;association_foreignkey:karyawan_idkaryawan;references:karyawan_idkaryawan"`
}
