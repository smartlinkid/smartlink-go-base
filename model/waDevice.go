package model

import "time"

type WaDevice struct {
	Iddevice          string      `json:"iddevice" gorm:"primary_key;index"`
	Nama              string      `json:"nama"`
	Keterangan        string      `json:"keterangan"`
	Hapus             int         `json:"hapus"`
	CreatedAt         *time.Time  `json:"created_at"`
	UpdatedAt         *time.Time  `json:"updated_at"`
	Quota             int         `json:"quota"`
	LastConnect       *time.Time  `json:"last_connect"`
	Ip                string      `json:"ip"`
	Port              int         `json:"port"`
	Token             string      `json:"token" gorm:"index"`
	CountryCode       string      `json:"country_code"`
	Number            string      `json:"number" gorm:"index"`
	Idowner           string      `json:"idowner" gorm:"index"`
	NextDue           *time.Time  `json:"next_due"`
	AutoRenewal       int         `json:"auto_renewal"`
	KeteranganRenewal string      `json:"keterangan_renewal"`
	ActiveAt          *time.Time  `json:"active_at"`
	Active            int         `json:"active"`
	LastRenewTry      *time.Time  `json:"last_renew_try"`
	IpUp              int         `json:"ip_up"`
	TrialUntil        *time.Time  `json:"trial_until"`
	Lunas             int         `json:"lunas"`
	RenewalDebug      string      `json:"renewal_debug"`
	Idpaket           int         `json:"idpaket"`
	Status            int         `json:"status"`
	WaSetting         []WaSetting `json:"wa_setting" gorm:"foreignkey:iddevice;association_foreignkey:iddevice;references:iddevice"`
	WaPaket           WaPaket     `json:"wa_paket" gorm:"foreignkey:id;association_foreignkey:idpaket;references:idpaket"`
	Versi             int         `json:"versi"`
	Multi             int         `json:"multi"`
	Idoutlet          string      `json:"idoutlet"`
	Outlet            *Outlet     `json:"outlet" gorm:"foreignkey:idoutlet;association_foreignkey:idoutlet;references:idoutlet"`
	DisableWaManual   int         `json:"disable_wa_manual"`
}
