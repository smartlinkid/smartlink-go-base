package model

import "time"

type WhitelabelStatusPesanan struct {
	Id                 string            `json:"id" gorm:"primary_key"`
	IdtransaksiLoyalty string            `json:"idtransaksi_loyalty"`
	Idtransaksi        string            `json:"idtransaksi"`
	Idjemput           *string           `json:"idjemput"`
	Idantar            string            `json:"idantar"`
	Idowner            string            `json:"idowner"`
	Idoutlet           string            `json:"idoutlet"`
	Idkaryawan         *string           `json:"idkaryawan"`
	Status             string            `json:"status"`
	Keterangan         *string           `json:"keterangan"`
	Url                *string           `json:"url"`
	CreatedAt          *time.Time        `json:"created_at" gorm:"default:CURRENT_TIMESTAMP"`
	TransaksiLoyalty   *TransaksiLoyalty `json:"transaksi_loyalty" gorm:"foreignkey:id;association_foreignkey:idtransaksi_loyalty;references:idtransaksi_loyalty"`
	TransaksiFoto      []TransaksiFoto   `json:"transaksi_foto" gorm:"foreignkey:idtransaksi;association_foreignkey:id;references:id"`
	Karyawan           *Karyawan         `json:"karyawan" gorm:"foreignkey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
}
