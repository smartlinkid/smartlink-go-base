package model

import "time"

type AfiliasiRekening struct {
	Id          int        `json:"id" gorm:"index;primary_key;not null"`
	DataBankId  int        `json:"data_bank_id"`
	Idowner     string     `json:"idowner" gorm:"index"`
	NoRekening  string     `json:"no_rekening"`
	NamaPemilik string     `json:"nama_pemilik"`
	Hapus       int        `json:"hapus"`
	DataBank    DataBank   `json:"data_bank" gorm:"index;foreignKey:id;association_foreignkey:data_bank_id;references:data_bank_id"`
	CreatedAt   *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt   *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
}
