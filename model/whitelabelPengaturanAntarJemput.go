package model

import "time"

type WhitelabelPengaturanAntarJemput struct {
	ID                  string                                    `json:"id"`
	Idowner             string                                    `json:"idowner"`
	Idoutlet            string                                    `json:"idoutlet"`
	TipeKendaraan       string                                    `json:"tipe_kendaraan"`
	MaksimalJarakGratis float64                                   `json:"maksimal_jarak_gratis"`
	OngkosKirim         float64                                   `json:"ongkos_kirim"`
	PengaturanLayanan   string                                    `json:"pengaturan_layanan"`
	PengaturanProduk    string                                    `json:"pengaturan_produk"`
	MinBerat            float64                                   `json:"min_berat"`
	MaxBerat            float64                                   `json:"max_berat"`
	CreatedAt           *time.Time                                `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt           *time.Time                                `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
	DataLayanan         []*WhitelabelPengaturanAntarJemputLayanan `json:"data_layanan" gorm:"foreignkey:idwhitelabel_pengaturan_antar_jemput;association_foreignkey:id"`
	DataProduk          []*WhitelabelPengaturanAntarJemputProduk  `json:"data_produk" gorm:"foreignkey:idwhitelabel_pengaturan_antar_jemput;association_foreignkey:id"`
}
