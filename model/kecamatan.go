package model

import "time"

type Kecamatan struct {
	Id         int        `json:"id" gorm:"primary_key;index"`
	NegaraId   int        `json:"negara_id"`
	ProvinsiId int        `json:"provinsi_id"`
	KotaId     int        `json:"kota_id"`
	Nama       string     `json:"nama"`
	Status     int        `json:"status"`
	CreatedBy  int        `json:"created_by"`
	UpdatedBy  int        `json:"updated_by"`
	DeletedAt  *time.Time `json:"deleted_at"`
	CreatedAt  *time.Time `json:"created_at"`
	UpdatedAt  *time.Time `json:"updated_at"`
}
