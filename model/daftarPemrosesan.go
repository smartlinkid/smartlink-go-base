package model

import "time"

type DaftarPemrosesan struct {
	TransaksiIdtransaksi string     `json:"transaksi_idtransaksi"`
	KaryawanIdkaryawan   string     `json:"karyawan_idkaryawan"`
	TahapanIdtahapan     int        `json:"tahapan_idtahapan"`
	Iddetlayanan         string     `json:"iddetlayanan"`
	Waktu                int64      `json:"waktu"`
	Gaji                 float64    `json:"gaji"`
	Keterangan           string     `json:"keterangan"`
	Tahapan              *Tahapan   `json:"tahapan" gorm:"foreignkey:idtahapan;association_foreignkey:tahapan_idtahapan;references:tahapan_idtahapan"`
	Karyawan             *Karyawan  `json:"karyawan" gorm:"foreignkey:idkaryawan;association_foreignkey:karyawan_idkaryawan;references:karyawan_idkaryawan"`
	CreatedAt            *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP"`
	UpdatedAt            *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
