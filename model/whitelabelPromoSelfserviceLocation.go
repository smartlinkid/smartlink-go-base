package model

import "time"

type WhitelabelPromoSelfserviceLocation struct {
	Id        string     `json:"id"`
	Idowner   string     `json:"idowner"`
	Idoutlet  string     `json:"idoutlet"`
	Idpromo   string     `json:"idpromo"`
	CreatedAt *time.Time `json:"created_at"`
}
