package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type SnapBayar struct {
	Id               string     `json:"id" gorm:"primary_key"`
	IdsnapTransaksi  string     `json:"idsnap_transaksi" gorm:"index"`
	Bayar            float64    `json:"bayar"`
	Kembalian        float64    `json:"kembalian"`
	JenisBayar       int        `json:"jenis_bayar"`
	Status           int        `json:"status"`
	TipeBayar        int        `json:"tipe_bayar"`
	FailedReason     string     `json:"failed_reason"`
	Idoutlet         string     `json:"idoutlet" gorm:"index"`
	Idowner          string     `json:"idowner" gorm:"index"`
	Keterangan       string     `json:"keterangan"`
	IdakunDebit      string     `json:"idakun_debit" gorm:"index"`
	TransferedAt     *time.Time `json:"transfered_at" gorm:"index"`
	TransferedAtLong int64      `json:"transfered_at_long"`
	Resi             string     `json:"resi"`
	CreatedAtLong    int64      `json:"created_at_long"`
	CreatedAt        *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt        *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	Hapus            *int       `json:"hapus" gorm:"default:0"`
}

var SnapBayarType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "snap_bayar",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "id snapbayar",
			},
			"idsnap_transaksi": &graphql.Field{
				Type:        graphql.String,
				Description: "id snap transaksi",
			},
			"bayar": &graphql.Field{
				Type:        graphql.Float,
				Description: "bayar snap",
			},
			"kembalian": &graphql.Field{
				Type:        graphql.Float,
				Description: "kembalian snap",
			},
			"jenis_bayar": &graphql.Field{
				Type:        graphql.Int,
				Description: "jenis bayar snap. 1: tunai; 2: kredit; 3: debit; 4: e-payment; 5: deposit; 6: paylink; 7: ovo; 8: dana; 9: linkaja: 10: QRIS",
			},
			"status": &graphql.Field{
				Type:        graphql.Int,
				Description: "status pembayaran. 0: menunggu bayar, 1: sukses, 2: gagal",
			},
			"tipe_bayar": &graphql.Field{
				Type:        graphql.Int,
				Description: "tipe bayar. 1 = lunas; 2 = dp; 3 = pelunasan",
			},
			"failed_reason": &graphql.Field{
				Type:        graphql.String,
				Description: "keterangan gagal saat pembuatan transaksi",
			},
			"idoutlet": &graphql.Field{
				Type:        graphql.String,
				Description: "id outlet snap",
			},
			"idowner": &graphql.Field{
				Type:        graphql.String,
				Description: "id owner snap",
			},
			"keterangan": &graphql.Field{
				Type:        graphql.String,
				Description: "keterangan snap",
			},
			"idakun_debit": &graphql.Field{
				Type:        graphql.String,
				Description: "id akun debit snap",
			},
			"transfered_at": &graphql.Field{
				Type:        graphql.String,
				Description: "tanggal transfer snap",
			},
			"transfered_at_long": &graphql.Field{
				Type:        graphql.String,
				Description: "unix time transfer snap",
			},
			"resi": &graphql.Field{
				Type:        graphql.String,
				Description: "resi",
			},
			"created_at_long": &graphql.Field{
				Type:        graphql.String,
				Description: "unix time created_at snap",
			},
			"created_at": &graphql.Field{
				Type:        graphql.String,
				Description: "unix time created_at snap",
			},
			//"base": &graphql.Field{
			//	Type:        BaseModelType,
			//	Description: "base model",
			//},
		},
		Description: "snapbayar data",
	})

var SnapBayarInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "snap_bayar_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id snapbayar",
			},
			"idsnap_transaksi": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id snap transaksi",
			},
			"bayar": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "bayar snap",
			},
			"kembalian": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "kembalian snap",
			},
			"jenis_bayar": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "jenis bayar snap. 1: tunai; 2: kredit; 3: debit; 4: e-payment; 5: deposit; 6: paylink; 7: ovo; 8: dana; 9: linkaja: 10: QRIS",
			},
			"status": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "status pembayaran. 0: menunggu bayar, 1: sukses, 2: gagal",
			},
			"tipe_bayar": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "tipe bayar. 1 = lunas; 2 = dp; 3 = pelunasan",
			},
			"idoutlet": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id outlet snap",
			},
			"idowner": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id owner snap",
			},
			"keterangan": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "keterangan snap",
			},
			"idakun_debit": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id akun debit snap",
			},
			"transfered_at": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "tanggal transfer snap",
			},
			"transfered_at_long": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "unix time transfer snap",
			},
			"resi": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "resi",
			},
			"created_at_long": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "unix time created_at snap",
			},
		},
		Description: "snapbayar data",
	})
