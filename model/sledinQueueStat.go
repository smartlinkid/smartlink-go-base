package model

import (
	"time"
)

type SledinQueueStat struct {
	Id          time.Time `json:"id" gorm:"primary_key"`
	Created     float64   `json:"created"`
	Speed       float64   `json:"speed"`
	Performance float64   `json:"performance"`
}
