package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type SnapMesin struct {
	Id               string           `json:"id" gorm:"index;primary_key;not null"`
	Idowner          string           `json:"idowner" gorm:"index"`
	Idoutlet         string           `json:"idoutlet" gorm:"index"`
	HomeId           int64            `json:"home_id" gorm:"index"`
	Nama             string           `json:"nama"`
	Jenis            int              `json:"jenis"` // 1 cuci 2 kering
	Keterangan       string           `json:"keterangan"`
	IdsnapTag        int              `json:"idsnap_tag" gorm:"index"`
	VirtualId        string           `json:"virtual_id" gorm:"index"`
	DpSwitch         string           `json:"dp_switch"`
	DpCountdown      string           `json:"dp_countdown"`
	DpVoltage        string           `json:"dp_voltage"`
	DpCurrent        string           `json:"dp_current"`
	DpPower          string           `json:"dp_power"`
	Pid              string           `json:"pid" gorm:"index"`
	UsedBy           string           `json:"used_by"`
	IdsnapTarif      int              `json:"idsnap_tarif" gorm:"index"`
	Gen              int              `json:"gen"`
	Door             int              `json:"door"` // 1 pakai 0 tidak pakai
	Urutan           string           `json:"urutan"`
	SnapTag          SnapTag          `json:"snap_tag" gorm:"foreignKey:idsnap_tag;association_foreignkey:id;references:id"`
	SnapLayananTag   []SnapLayananTag `json:"snap_layanan_tag" gorm:"foreignKey:idsnap_tag;association_foreignkey:idsnap_tag;references:idsnap_tag"`
	SnapTarif        SnapTarif        `json:"snap_tarif" gorm:"foreignKey:idsnap_tarif;association_foreignkey:id;references:id"`
	SnapReportDevice SnapReportDevice `json:"snap_report_device" gorm:"-"`
	PaylinkQris      PaylinkQris      `json:"paylink_qris" gorm:"foreignKey:id;association_foreignkey:id;references:id"`
	SnapSerial       *SnapSerial      `json:"snap_serial" gorm:"foreignKey:id;association_foreignkey:idsnap_mesin;references:idsnap_mesin"`
	CreatedAt        *time.Time       `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt        *time.Time       `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	Hapus            *int             `json:"hapus" gorm:"default:0"`
}

var SnapMesinType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "snap_mesin",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "id mesin snap",
			},
			"idowner": &graphql.Field{
				Type:        graphql.String,
				Description: "id owner snap",
			},
			"idoutlet": &graphql.Field{
				Type:        graphql.String,
				Description: "id outlet snap",
			},
			"home_id": &graphql.Field{
				Type:        graphql.String,
				Description: "home id snap",
			},
			"nama": &graphql.Field{
				Type:        graphql.String,
				Description: "nama mesin snap",
			},
			"jenis": &graphql.Field{
				Type:        graphql.Int,
				Description: "jenis mesin snap. 1: cuci 2: kering",
			},
			"keterangan": &graphql.Field{
				Type:        graphql.String,
				Description: "keterangan mesin snap",
			},
			"idsnap_tag": &graphql.Field{
				Type:        graphql.Int,
				Description: "id tag snap",
			},
			"virtual_id": &graphql.Field{
				Type:        graphql.String,
				Description: "virtual id snap",
			},
			"dp_switch": &graphql.Field{
				Type:        graphql.String,
				Description: "Command function for switching device",
			},
			"dp_countdown": &graphql.Field{
				Type:        graphql.String,
				Description: "Command function to set the countdown timer in second",
			},
			"dp_current": &graphql.Field{
				Type:        graphql.String,
				Description: "Command function for getting current from device",
			},
			"dp_power": &graphql.Field{
				Type:        graphql.String,
				Description: "Command function for getting power from device",
			},
			"dp_voltage": &graphql.Field{
				Type:        graphql.String,
				Description: "Command function for getting voltage",
			},
			"pid": &graphql.Field{
				Type:        graphql.String,
				Description: "PID",
			},
			"snap_tag": &graphql.Field{
				Type:        SnapTagType,
				Description: "",
			},
			"used_by": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"idsnap_tarif": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			//"snap_layanan_tag": &graphql.Field{
			//	Type:        graphql.NewList(SnapLayananTagType),
			//	Description: "",
			//},
			"snap_tarif": &graphql.Field{
				Type:        SnapTarifType,
				Description: "",
			},
			"gen": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"door": &graphql.Field{
				Type:        graphql.Int,
				Description: "Support untuk mesin. 1 support 0 ga",
			},
			"snap_report_device": &graphql.Field{
				Type:        SnapReportDeviceType,
				Description: "Dari redis",
			},
			"paylink_qris": &graphql.Field{
				Type:        PaylinkQrisObject,
				Description: "Qris paylink",
			},
			//"base": &graphql.Field{
			//	Type:        BaseModelType,
			//	Description: "base model",
			//},
		},
		Description: "snap mesin data",
	})

func init() {
	SnapMesinType.AddFieldConfig("snap_layanan_tag", &graphql.Field{Type: graphql.NewList(SnapLayananTagType)})
}

var SnapMesinInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "snap_mesin_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id mesin snap",
			},
			"idowner": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id owner snap",
			},
			"idoutlet": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id outlet snap",
			},
			"home_id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "home id snap",
			},
			"nama": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "nama mesin snap",
			},
			"jenis": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "jenis mesin snap. 1: cuci 2: kering",
			},
			"keterangan": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "keterangan mesin snap",
			},
			"idsnap_tag": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "id tag snap",
			},
			"virtual_id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "virtual id snap",
			},
			"dp_switch": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "Command function for switching device",
			},
			"dp_countdown": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "Command function to set the countdown timer in second",
			},
			"dp_current": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "Command function for getting current from device",
			},
			"dp_power": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "Command function for getting power from device",
			},
			"dp_voltage": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "Command function for getting voltage",
			},
			"pid": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "PID",
			},
			"idsnap_tarif": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "",
			},
			"serial": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "",
			},
		},
	})
