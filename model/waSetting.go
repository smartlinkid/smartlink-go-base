package model

import "time"

type WaSetting struct {
	Idowner            string     `json:"idowner"`
	Idoutlet           string     `json:"idoutlet"`
	WaNota             int        `json:"wa_nota"`
	WaSelesai          int        `json:"wa_selesai"`
	WaAmbil            int        `json:"wa_ambil"`
	WaSnapNota         int        `json:"wa_snap_nota"`
	WaNotaReguler      int        `json:"wa_nota_reguler"`
	WaNotaSnapclean    int        `json:"wa_nota_snapclean"`
	WaNotaTopupDeposit int        `json:"wa_nota_topup_deposit"`
	WaNotaTopupEmoney  int        `json:"wa_nota_topup_emoney"`
	WaBuktiAmbil       int        `json:"wa_bukti_ambil"`
	DurasiAmbil        string     `json:"durasi_ambil"`
	RedirectNota       int        `json:"redirect_nota"`
	RedirectSelesai    int        `json:"redirect_selesai"`
	RedirectAmbil      int        `json:"redirect_ambil"`
	RedirectSnapNota   int        `json:"redirect_snap_nota"`
	Iddevice           string     `json:"iddevice"`
	CreatedAt          *time.Time `json:"created_at"`
	UpdatedAt          *time.Time `json:"updated_at"`
	Hapus              int        `json:"hapus"`
	Outlet             Outlet     `json:"outlet" gorm:"foreignkey:idoutlet;association_foreignkey:idoutlet;references:idoutlet"`
	WaPelunasan        int        `json:"wa_pelunasan"`
}
