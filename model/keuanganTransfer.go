package model

type KeuanganTransfer struct {
	Idtransfer         int                 `json:"idtransfer" gorm:"primary_key"`
	IdakunDebet        string              `json:"idakun_debet"`
	IdakunKredit       string              `json:"idakun_kredit"`
	Nominal            float64             `json:"nominal"`
	Waktu              int64               `json:"waktu"`
	WaktuTransfer      int64               `json:"waktu_transfer"`
	WaktuStatus        int64               `json:"waktu_status"`
	Jenis              int                 `json:"jenis"`
	Status             int                 `json:"status"`
	RelatedType        string              `json:"related_type"`
	RelatedId          string              `json:"related_id"`
	Idowner            string              `json:"idowner"`
	Idkaryawan         string              `json:"idkaryawan"`
	Keterangan         string              `json:"keterangan"`
	KeteranganStatus   string              `json:"keterangan_status"`
	Foto               string              `json:"foto"`
	Uuid               string              `json:"uuid"`
	ParentIdtransfer   *int64              `json:"parent_idtransfer"`
	Hash               *string             `json:"hash"`
	NewFeature         int                 `json:"new_feature"`
	Outlet             *Outlet             `json:"outlet" gorm:"foreignkey:idoutlet;association_foreignkey:related_id;references:related_id"`
	Workshop           *Workshop           `json:"workshop" gorm:"foreignkey:idworkshop;association_foreignkey:related_id;references:related_id"`
	Karyawan           *Karyawan           `json:"karyawan" gorm:"foreignkey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	AkunDebet          *KeuanganAkunAkun   `json:"akun_debit" gorm:"foreignkey:idakun;association_foreignkey:idakun_debet;references:idakun_debet"`
	AkunKredit         *KeuanganAkunAkun   `json:"akun_kredit" gorm:"foreignkey:idakun;association_foreignkey:idakun_kredit;references:idakun_kredit"`
	KeuanganTutupShift *KeuanganTutupShift `json:"keuangan_tutup_shift" gorm:"foreignkey:idtransfer;association_foreignkey:idtransfer;references:idtransfer"`
	OtherTransfer      []KeuanganTransfer  `json:"other_transfer" gorm:"foreignkey:parent_idtransfer;association_foreignkey:idtransfer;references:idtransfer"`
	BaseModel
}
