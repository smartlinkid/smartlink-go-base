package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type SnapConsoleInvoice struct {
	Id            string      `json:"id" gorm:"primary_key;index"`
	IdsnapMesin   string      `json:"idsnap_mesin" gorm:"index"`
	IdsnapConsole string      `json:"idsnap_console" gorm:"index"`
	IdsnapLayanan string      `json:"idsnap_layanan" gorm:"index"`
	Status        int         `json:"status"` // 0 belum dibayar, 1 sudah dibayar
	Nominal       float64     `json:"nominal"`
	SnapMesin     SnapMesin   `json:"snap_mesin" gorm:"foreignkey:idsnap_mesin;association_foreignkey:id;references:id"`
	SnapLayanan   SnapLayanan `json:"snap_layanan" gorm:"foreignkey:idsnap_layanan;association_foreignkey:id;references:id"`
	PaylinkQris   PaylinkQris `json:"paylink_qris" gorm:"foreignkey:id;association_foreignkey:id;references:id"`
	CreatedAt     *time.Time  `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	Qrcode        string      `json:"qrcode" gorm:"-"`
}

var SnapConsoleInvoiceType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "snap_console_invoice",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"idsnap_console": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"idsnap_mesin": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"idsnap_layanan": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},
			"status": &graphql.Field{
				Type:        graphql.Int,
				Description: "",
			},
			"qrcode": &graphql.Field{
				Type:        graphql.String,
				Description: "",
			},

			"snap_mesin": &graphql.Field{
				Type:        SnapMesinType,
				Description: "",
			},
			"snap_layanan": &graphql.Field{
				Type:        SnapLayananType,
				Description: "",
			},
			"paylink_qris": &graphql.Field{
				Type:        PaylinkQrisObject,
				Description: "",
			},
		},
		Description: "",
	})
