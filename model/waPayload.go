package model

type WaPayload struct {
	Idpayload  string `json:"idpayload" gorm:"primary_key;index"`
	Code       string `json:"code" gorm:"index"`
	Payload    string `json:"payload"`
	Keterangan string `json:"keterangan"`
	Sample     string `json:"sample"`
}
