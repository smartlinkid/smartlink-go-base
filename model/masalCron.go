package model

import "time"

type MasalCron struct {
	Idcustomer             string     `json:"idcustomer"`
	JenisPeriode           string     `json:"jenis_periode"`
	Waktu                  int        `json:"waktu"`
	PeriodeAwal            *time.Time `json:"periode_awal"`
	LastGenerate           *time.Time `json:"last_generate"`
	NextGenerate           *time.Time `json:"next_generate"`
	LastGenerateStatus     *time.Time `json:"last_generate_status"`
	LastGenerateStatusInfo string     `json:"last_generate_status_info"`
	Idoutlet               string     `json:"idoutlet"`
	Idowner                string     `json:"idowner"`
	KirimWaCustomer        string     `json:"kirim_wa_customer"`
	OtherTelp              string     `json:"other_telp"`
	CreatedAt              *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt              *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
