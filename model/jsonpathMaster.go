package model

type JsonpathMaster struct {
	JenisData  string `json:"jenis_data"`
	Kode       string `json:"kode"`
	Jsonpath   string `json:"jsonpath"`
	Keterangan string `json:"keterangan"`
	Sample     string `json:"sample"`
}
