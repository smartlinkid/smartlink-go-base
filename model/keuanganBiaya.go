package model

import "time"

type KeuanganBiaya struct {
	Idbiaya          int64             `json:"idbiaya" gorm:"primary_key;index;auto_increment"`
	IdakunDebet      string            `json:"idakun_debet" gorm:"index"`
	IdakunKredit     string            `json:"idakun_kredit" gorm:"index"`
	Nominal          float64           `json:"nominal"`
	Waktu            int64             `json:"waktu"`
	WaktuPakai       int64             `json:"waktu_pakai"`
	WaktuStatus      int64             `json:"waktu_status"`
	RelatedType      string            `json:"related_type"`
	RelatedId        string            `json:"related_id" gorm:"index"`
	Status           int               `json:"status"`
	Idowner          string            `json:"idowner" gorm:"index"`
	Idkaryawan       string            `json:"idkaryawan" gorm:"index"`
	Keterangan       string            `json:"keterangan"`
	KeteranganStatus string            `json:"keterangan_status"`
	Foto             string            `json:"foto"`
	IdbiayaItem      int               `json:"idbiaya_item" gorm:"index"`
	CorrectedAt      *time.Time        `json:"corrected_at"`
	OldIdakunDebet   string            `json:"old_idakun_debet"`
	OldIdbiayaItem   string            `json:"old_idbiaya_item"`
	NewFeature       int               `json:"new_feature"`
	Karyawan         *Karyawan         `json:"karyawan" gorm:"foreignkey:idkaryawan;association_foreignkey:idkaryawan;references:idkaryawan"`
	Outlet           *Outlet           `json:"outlet" gorm:"foreignkey:idoutlet;association_foreignkey:related_id;references:related_id"`
	Workshop         *Workshop         `json:"workshop" gorm:"foreignkey:idworkshop;association_foreignkey:related_id;references:related_id"`
	AkunDebet        *KeuanganAkunAkun `json:"akun_debet" gorm:"foreignkey:idakun;association_foreignkey:idakun_debet;references:idakun_debet"`
	AkunKredit       *KeuanganAkunAkun `json:"akun_kredit" gorm:"foreignkey:idakun;association_foreignkey:idakun_kredit;references:idakun_kredit"`
	BiayaItem        *BiayaItem        `json:"biaya_item" gorm:"foreignkey:id;association_foreignkey:idbiaya_item;references:idbiaya_item"`
	RefTable         string            `json:"ref_table"`
	RefId            string            `json:"ref_id"`
	BaseModel
}
