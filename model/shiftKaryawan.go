package model

type ShiftKaraywan struct {
	Idkaryawan string `json:"idkaryawan" gorm:"primary_key"`
	Idshift    string `json:"idshift" gorm:"primary_key"`
	Tanggal    string `json:"tanggal" gorm:"primary_key"`
}
