package model

import "time"

type WhitelabelPromoSelfserviceTransaksi struct {
	Idtransaksi     string     `json:"idtransaksi"`
	Idowner         string     `json:"idowner"`
	Idoutlet        string     `json:"idoutlet"`
	Idcustomer      string     `json:"idcustomer"`
	Idpromo         string     `json:"idpromo"`
	AmountTransaksi float64    `json:"amount_transaksi"`
	AmountPromo     float64    `json:"amount_promo"`
	GrandTotal      float64    `json:"grand_total"`
	CreatedAt       *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
}
