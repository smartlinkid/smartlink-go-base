package model

import "time"

type TransaksiRak struct {
	Id              int         `json:"id"  gorm:"primary_key"`
	Idrak           int         `json:"idrak"`
	Idtransaksi     string      `json:"idtransaksi"`
	Gantung         int         `json:"gantung"`
	Lipat           int         `json:"lipat"`
	Gulung          int         `json:"gulung"`
	Diambil         int         `json:"diambil"`
	RefIddetlayanan string      `json:"ref_iddetlayanan"`
	Transaksi       *Transaksi  `json:"transaksi" gorm:"foreignkey:idtransaksi;association_foreignkey:idtransaksi;references:idtransaksi"`
	Detlayanan      *Detlayanan `json:"detlayanan" gorm:"foreignkey:ref_iddetlayanan;association_foreignkey:id;references:id"`
	CreatedAt       *time.Time  `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt       *time.Time  `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
