package model

import "time"

type EdcPembayaranHapus struct {
	Id                   int        `json:"id" gorm:"primary_key"`
	EdcPembayaranId      string     `json:"edc_pembayaran_id"`
	Tipe                 int        `json:"tipe"`
	IdakunKeuanganRefund string     `json:"idakun_keuangan_refund"`
	Nominal              float64    `json:"nominal"`
	RefTable             string     `json:"ref_table"`
	RefId                string     `json:"ref_id"`
	RefTransaksi         string     `json:"ref_transaksi"`
	Idkaryawan           string     `json:"idkaryawan"`
	Idowner              string     `json:"idowner"`
	Idoutlet             string     `json:"idoutlet"`
	CreatedAt            *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt            *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
