package model

import "time"

type DetailPembayaranGajiSales struct {
	Id               int             `json:"id" gorm:"primary_key;index;not null;auto_increment"`
	IdpembayaranGaji string          `json:"idpembayaran_gaji"`
	TotalOmset       float64         `json:"total_omset"`
	PersentaseKomisi float64         `json:"persentase_komisi"`
	TotalGaji        float64         `json:"total_gaji"`
	IdjenisSales     int             `json:"idjenis_sales"`
	JenisGajiSales   *JenisGajiSales `json:"jenis_gaji_sales" gorm:"index;foreignKey:id;association_foreignkey:idjenis_sales;references:idjenis_sales"`
	CreatedAt        *time.Time      `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt        *time.Time      `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
