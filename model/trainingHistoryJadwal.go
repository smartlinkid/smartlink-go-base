package model

import "time"

type TrainingHistoryJadwalTraining struct {
	Id                string          `json:"id" gorm:"primary_key"`
	TrainingDetailId  string          `json:"training_detail_id"`
	TrainingId        string          `json:"training_id"`
	SebelumnyaMulai   time.Time       `json:"sebelumnya_mulai"`
	SebelumnyaSelesai time.Time       `json:"sebelumnya_selesai"`
	SekarangMulai     time.Time       `json:"sekarang_mulai"`
	SekarangSelesai   time.Time       `json:"sekarang_selesai"`
	UpdatedBy         string          `json:"updated_by"`
	Training          *Training       `json:"training" gorm:"foreignkey:id;association_foreignkey:training_id;references:training_id"`
	TrainingDetail    *TrainingDetail `json:"training_detail" gorm:"foreignkey:id;association_foreignkey:training_detail_id;references:training_detail_id"`
	BaseModel
}
