package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type OwnerTabungan struct {
	Id               string         `json:"id" gorm:"primary_key"`
	IdownerPremium   string         `json:"idowner_premium" gorm:"index"`
	IddataBank       int            `json:"iddata_bank" gorm:"index"`
	Idowner          string         `json:"idowner" gorm:"index"`
	Tipe             int            `json:"tipe"`
	Norek            string         `json:"norek"`
	NamaBukuTabungan string         `json:"nama_buku_tabungan"`
	FotoBukuTabungan string         `json:"foto_buku_tabungan"`
	Approved         int            `json:"approved"`
	ApprovedBy       int            `json:"approved_by"`
	Reason           string         `json:"reason"`
	CreatedAt        *time.Time     `json:"created_at"`
	UpdatedAt        *time.Time     `json:"updated_at"`
	Hapus            int            `json:"hapus"`
	Owner            Owner          `json:"owner" gorm:"foreignKey:idowner;association_foreignkey:idowner;references:idowner"`
	DataBank         DataBank       `json:"data_bank" gorm:"foreignKey:iddata_bank;association_foreignkey:id;references:id"`
	Administrator    *Administrator `json:"administrator" gorm:"foreignKey:approved_by;association_foreignkey:id;references:id"`
}

var OwnerTabunganObject = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "owner_tabungan_object",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "id ",
			},
			"idowner_premium": &graphql.Field{
				Type:        graphql.String,
				Description: "idowner_premium ",
			},
			"iddata_bank": &graphql.Field{
				Type:        graphql.Int,
				Description: "iddata_bank ",
			},
			"idowner": &graphql.Field{
				Type:        graphql.String,
				Description: "idowner",
			},
			"norek": &graphql.Field{
				Type:        graphql.String,
				Description: "norek",
			},
			"nama_buku_tabungan": &graphql.Field{
				Type:        graphql.String,
				Description: "nama_buku_tabungan",
			},
			"foto_buku_tabungan": &graphql.Field{
				Type:        graphql.String,
				Description: "foto_buku_tabungan",
			},
			"approved": &graphql.Field{
				Type:        graphql.Int,
				Description: "approved",
			},
			"reason": &graphql.Field{
				Type:        graphql.String,
				Description: "reason",
			},
			"created_at": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "CreatedAt",
			},
			"data_bank": &graphql.Field{
				Type:        DataBankObject,
				Description: "bank ",
			},
		},
	})
