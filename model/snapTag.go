package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type SnapTag struct {
	Id        int64      `json:"id" gorm:"primary_key;auto_increment"`
	Nama      string     `json:"nama"`
	Idowner   string     `json:"idowner" gorm:"index"`
	Idoutlet  string     `json:"idoutlet" gorm:"index"`
	CreatedAt *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	Hapus     *int       `json:"hapus" gorm:"default:0"`
}

var SnapTagType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "snap_tag",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.Int,
				Description: "id tag snap",
			},
			"nama": &graphql.Field{
				Type:        graphql.String,
				Description: "nama tag snap",
			},
			"idowner": &graphql.Field{
				Type:        graphql.String,
				Description: "id owner snap",
			},
			"idoutlet": &graphql.Field{
				Type:        graphql.String,
				Description: "idoutlet snap tag",
			},
			//"base": &graphql.Field{
			//	Type:        BaseModelType,
			//	Description: "base model",
			//},
		},
		Description: "snap tag data",
	})

var SnapTagInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "snap_tag_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"id": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "id tag snap",
			},
			"nama": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "nama tag snap",
			},
			"idowner": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id owner snap",
			},
			"idoutlet": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "idoutlet snap tag",
			},
		},
		Description: "snap tag data",
	})
