package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type SnapCredential struct {
	Idowner   string     `json:"idowner" gorm:"primary_key;index;not null"`
	UserId    string     `json:"user_id" gorm:"index"`
	Password  string     `json:"password"`
	Idoutlet  string     `json:"idoutlet" gorm:"index"`
	SnapHome  []SnapHome `json:"snap_home" gorm:"foreignKey:idoutlet;association_foreignkey:idoutlet;references:idoutlet"`
	CreatedAt *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	Hapus     *int       `json:"hapus" gorm:"default:0"`
}

var SnapCredentialType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "snap_credential",
		Fields: graphql.Fields{
			"idowner": &graphql.Field{
				Type:        graphql.String,
				Description: "id owner snap",
			},
			"user_id": &graphql.Field{
				Type:        graphql.String,
				Description: "user id credential snap",
			},
			"idoutlet": &graphql.Field{
				Type:        graphql.String,
				Description: "id outlet snap",
			},
			"snap_home": &graphql.Field{
				Type:        graphql.NewList(SnapHomeType),
				Description: "",
			},
			"password": &graphql.Field{
				Type:        graphql.String,
				Description: "password snap",
			},
			//"base": &graphql.Field{
			//	Type:        BaseModelType,
			//	Description: "base model",
			//},
		},
		Description: "snap credentian data",
	})

var SnapCredentialInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "snap_credential_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"idowner": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id owner snap",
			},
			"user_id": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "user id credential snap",
			},
			"password": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "password snap",
			},
			"idoutlet": &graphql.InputObjectFieldConfig{
				Type:        graphql.String,
				Description: "id outlet snap",
			},
		},
	})
