package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type PaylinkPaket struct {
	Id        int        `json:"id" gorm:"primary_key;auto_increment"`
	Nominal   float64    `json:"nominal"`
	CreatedAt *time.Time `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt *time.Time `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	Hapus     *int       `json:"hapus" gorm:"default:0"`
}

var PaylinkPaketObject = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "paket_list",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.Int,
				Description: "Nominal Float",
			},
			"nominal": &graphql.Field{
				Type:        graphql.Float,
				Description: "Nominal Float",
			},
		},
	})

var PaylinkPaketInput = graphql.NewInputObject(
	graphql.InputObjectConfig{
		Name: "paket_input",
		Fields: graphql.InputObjectConfigFieldMap{
			"id": &graphql.InputObjectFieldConfig{
				Type:        graphql.Int,
				Description: "Id ",
			},
			"nominal": &graphql.InputObjectFieldConfig{
				Type:        graphql.Float,
				Description: "Nominal",
			},
		},
	})
