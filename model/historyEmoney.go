package model

import (
	"time"

	"github.com/jinzhu/gorm"
)

type HistoryEmoney struct {
	Id                           int64      `json:"id" gorm:"primary_key;index;auto_increment"`
	Idowner                      string     `json:"idowner" gorm:"index"`
	Idcustomer                   string     `json:"idcustomer" gorm:"index"`
	RefType                      string     `json:"ref_type" gorm:"index"`
	RefId                        string     `json:"ref_id" gorm:"index"`
	DetailType                   string     `json:"detail_type" gorm:"index"`
	DetailId                     string     `json:"detail_id" gorm:"index"`
	Masuk                        float64    `json:"masuk"`
	Keluar                       float64    `json:"keluar"`
	SaldoAwal                    float64    `json:"saldo_awal"`
	SaldoAkhir                   float64    `json:"saldo_akhir"`
	MasaAktifAwal                int64      `json:"masa_aktif_awal"`
	MasaAktifAkhir               int64      `json:"masa_aktif_akhir"`
	SaldoHapus                   float64    `json:"saldo_hapus"`
	NewJenisEmoney               int        `json:"new_jenis_emoney"`
	OldJenisEmoney               int        `json:"old_jenis_emoney"`
	NewTipePerpanjanganMasaAktif int        `json:"new_tipe_perpanjangan_masa_aktif"`
	OldTipePerpanjanganMasaAktif int        `json:"old_tipe_perpanjangan_masa_aktif"`
	NewTipeSaldoExpired          int        `json:"new_tipe_saldo_expired"`
	OldTipeSaldoExpired          int        `json:"old_tipe_saldo_expired"`
	CreatedAt                    *time.Time `json:"created_at" gorm:"index"`
	UpdatedAt                    *time.Time `json:"updated_at"`
}

func (receiver *HistoryEmoney) Save(db *gorm.DB) error {
	e := db.Create(receiver).Error
	return e
}

// tes tes

type ESHistoryEmoney struct {
	Id         string     `json:"id"`
	Idcustomer string     `json:"idcustomer"`
	Idowner    string     `json:"idowner"`
	RefId      string     `json:"ref_id"`
	RefType    string     `json:"ref_type"`
	DetailType string     `json:"detail_type"`
	DetailId   string     `json:"detail_id"`
	Masuk      float64    `json:"masuk"`
	SaldoAkhir float64    `json:"saldo_akhir"`
	SaldoAwal  float64    `json:"saldo_awal"`
	UpdatedAt  *time.Time `json:"updated_at"`
	CreatedAt  time.Time  `json:"created_at"`
}
