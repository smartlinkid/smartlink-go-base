package model

type JsonTemplatePathPayload struct {
	Id         int64  `json:"id" gorm:"primarykey;index;auto_increment"`
	Payload    string `json:"payload"`
	Idpath     string `json:"idpath"`
	Path       string `json:"path"`
	Code       string `json:"code"`
	Keterangan string `json:"keterangan"`
}
