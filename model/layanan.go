package model

type Layanan struct {
	Idlayanan              string               `json:"idlayanan" gorm:"primary_key"`
	NamaLayanan            string               `json:"nama_layanan"`
	OwnerIdowner           string               `json:"owner_idowner"`
	OutletIdoutlet         string               `json:"outlet_idoutlet"`
	Jumlah                 float32              `json:"jumlah"`
	DurasiPenyelesaian     int64                `json:"durasi_penyelesaian"`
	DataSatuanIddataSatuan int                  `json:"data_satuan_iddata_satuan"`
	Satuan                 *DataSatuan          `json:"satuan" gorm:"foreignkey:data_satuan_iddata_satuan;association_foreignkey:iddata_satuan;references:iddata_satuan"`
	HargaLayanan           *HargaLayanan        `json:"harga_layanan" gorm:"foreignkey:layanan_idlayanan;association_foreignkey:idlayanan;references:idlayanan"`
	WhitelabelLayanan      *WhitelabelLayanan   `json:"whitelabel_layanan" gorm:"foreignkey:idlayanan;association_foreignkey:idlayanan;references:idlayanan"`
	UpahTahapanLayanan     []UpahTahapanLayanan `json:"upah_tahapan_layanan" gorm:"foreignkey:layanan_idlayanan;association_foreignkey:idlayanan;references:idlayanan"`
	LayananTags            []LayananTags        `json:"layanan_tags" gorm:"foreignkey:layanan_idlayanan;association_foreignkey:idlayanan;references:idlayanan"`
	BaseModel
}
