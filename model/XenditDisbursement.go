package model

import (
	"github.com/graphql-go/graphql"
	"time"
)

type XenditDisbursements struct {
	ID                      string   `json:"id" gorm:"primary_key;"`
	UserID                  string   `json:"user_id"`
	ExternalID              string   `json:"external_id"`
	Amount                  float64  `json:"amount"`
	BankCode                string   `json:"bank_code"`
	AccountHolderName       string   `json:"account_holder_name"`
	AccountNumber     		string   `json:"account_number"`
	DisbursementDescription string   `json:"disbursement_description"`
	Status                  string   `json:"status"`
	FailureCode                  string   `json:"failure_code"`
	Payload                  string   `json:"payload"`
	CreatedAt       *time.Time    `json:"created_at" gorm:"default:CURRENT_TIMESTAMP;INDEX"`
	UpdatedAt       *time.Time    `json:"updated_at" gorm:"default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;INDEX"`
	Hapus           *int          `json:"hapus" gorm:"default:0"`
}

var XenditDisbursementsObject = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "xendit_disbursements_object",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type:        graphql.String,
				Description: "id ",
			},
			"user_id": &graphql.Field{
				Type:        graphql.String,
				Description: "user_id",
			},
			"external_id": &graphql.Field{
				Type:        graphql.String,
				Description: "external id",
			},
			"amount": &graphql.Field{
				Type:        graphql.Float,
				Description: "Amount",
			},
			"bank_code": &graphql.Field{
				Type:        graphql.String,
				Description: "bank_code",
			},
			"account_holder_name": &graphql.Field{
				Type:        graphql.String,
				Description: "account_holder_name",
			},
			"disbursement_description": &graphql.Field{
				Type:        graphql.Float,
				Description: "disbursement_description",
			},
			"status": &graphql.Field{
				Type:        graphql.String,
				Description: "status",
			},
			"created_at": &graphql.Field{
				Type:        graphql.DateTime,
				Description: "CreatedAt",
			},
		},
	})
