package helper

import (
	"bitbucket.org/smartlinkid/smartlink-go-base/model"
	"github.com/jinzhu/gorm"
	"time"
)

func Jurnaler(
	db *gorm.DB,
	idAkun string,
	debet float64,
	kredit float64,
	keterangan string,
	relatedType string,
	relatedId string,
	idKaryawan string,
	idOwner string,
	waktu int64,
	refType string,
	refId string) error {
	var jurnal model.KeuanganJurnal
	jurnal = model.KeuanganJurnal{
		Idakun:      idAkun,
		Debet:       debet,
		Kredit:      kredit,
		Keterangan:  keterangan,
		RelatedType: relatedType,
		RelatedId:   relatedId,
		Idkaryawan:  idKaryawan,
		Idowner:     idOwner,
		Waktu:       waktu,
		RefType:     refType,
		RefId:       refId,
		Rev:         0,
	}

	err := db.Create(&jurnal).Error
	if err != nil {
		return err
	}
	return nil
}

type Jurnalers struct {
	IdAkun      string
	Debet       float64
	Kredit      float64
	Keterangan  string
	RelatedType string
	RelatedId   string
	Idkaryawan  string
	Idowner     string
	Waktu       int64
	RefType     string
	RefId       string
	CreatedAt   time.Time
}

type JurnalersBuilder struct {
	Jurnalers
}

func (j *JurnalersBuilder) Save(tx *gorm.DB) error {
	var err error
	var jurnal model.KeuanganJurnal

	jurnal = model.KeuanganJurnal{
		Idakun:      j.Jurnalers.IdAkun,
		Debet:       j.Jurnalers.Debet,
		Kredit:      j.Jurnalers.Kredit,
		Keterangan:  j.Jurnalers.Keterangan,
		RelatedType: j.Jurnalers.RelatedType,
		RelatedId:   j.Jurnalers.RelatedId,
		Idkaryawan:  j.Jurnalers.Idkaryawan,
		Idowner:     j.Jurnalers.Idowner,
		Waktu:       j.Jurnalers.Waktu,
		RefType:     j.Jurnalers.RefType,
		RefId:       j.Jurnalers.RefId,
		Rev:         0,
		CreatedAt:   &j.Jurnalers.CreatedAt,
	}
	err = tx.Save(&jurnal).Error
	if err != nil {
		return err
	}

	return nil
}

func (j *JurnalersBuilder) IdAkun(idakun string) *JurnalersBuilder {
	j.Jurnalers.IdAkun = idakun
	return j
}

func (j *JurnalersBuilder) Debet(debet float64) *JurnalersBuilder {
	j.Jurnalers.Debet = debet
	return j
}

func (j *JurnalersBuilder) Kredit(kredit float64) *JurnalersBuilder {
	j.Jurnalers.Kredit = kredit
	return j
}

func (j *JurnalersBuilder) Keterangan(keterangan string) *JurnalersBuilder {
	j.Jurnalers.Keterangan = keterangan
	return j
}

func (j *JurnalersBuilder) RelatedType(relatedType string) *JurnalersBuilder {
	j.Jurnalers.RelatedType = relatedType
	return j
}

func (j *JurnalersBuilder) RelatedId(relatedId string) *JurnalersBuilder {
	j.Jurnalers.RelatedId = relatedId
	return j
}

func (j *JurnalersBuilder) Idkaryawan(idkaryawan string) *JurnalersBuilder {
	j.Jurnalers.Idkaryawan = idkaryawan
	return j
}

func (j *JurnalersBuilder) Idowner(idowner string) *JurnalersBuilder {
	j.Jurnalers.Idowner = idowner
	return j
}

func (j *JurnalersBuilder) Waktu(waktu int64) *JurnalersBuilder {
	j.Jurnalers.Waktu = waktu
	return j
}

func (j *JurnalersBuilder) RefType(refType string) *JurnalersBuilder {
	j.Jurnalers.RefType = refType
	return j
}

func (j *JurnalersBuilder) RefId(refId string) *JurnalersBuilder {
	j.Jurnalers.RefId = refId
	return j
}

func (j *JurnalersBuilder) CreatedAt(createdAt time.Time) *JurnalersBuilder {
	j.Jurnalers.CreatedAt = createdAt
	return j
}
