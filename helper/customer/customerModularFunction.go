package customer

import (
	"fmt"
	"log"

	"github.com/jinzhu/gorm"
	"gopkg.in/errgo.v2/fmt/errors"
)

type CustomerScanners struct {
	Idcustomer  string `json:"idcustomer"`
	Nama        string `json:"nama"`
	Telp        string `json:"telp"`
	Phone       string `json:"phone"`
	CountryCode string `json:"country_code"`
	IsLoyalty   int    `json:"is_loyalty"`
	Sapaan      string `json:"sapaan"`
}

type CustomerModulars struct {
	Idowner           string        `json:"idowner"`
	Key               string        `json:"key"`
	SortBy            string        `json:"sort_by"`
	OrderBy           string        `json:"order_by"`
	IsContact         *bool         `json:"is_contact"`
	StatusDeposit     []interface{} `json:"status_deposit"`
	FilterDateBy      string        `json:"filter_date_by"`
	FilterDateColumn  string        `json:"filter_date_column"`
	FirstDate         string        `json:"first_date"`
	LastDate          string        `json:"last_date"`
	Month             string        `json:"month"`
	Year              string        `json:"year"`
	Limit             int           `json:"limit"`
	Offset            int           `json:"offset"`
	Sapaan            []interface{} `json:"sapaan"`
	IdmasterMember    []interface{} `json:"idmaster_member"`
	Idcustomer        []string      `json:"idcustomer"`
	Agama             []interface{} `json:"agama"`
	Idoutlet          []interface{} `json:"idoutlet"`
	FilterLayananBy   string        `json:"filter_layanan_by"`
	Idlayanan         []interface{} `json:"idlayanan"`
	Iddeposit         []interface{} `json:"iddeposit"`
	IdoutletTransaksi []string      `json:"idoutlet_transaksi"`
	MinNominalEmoney  float64       `json:"min_nominal_emoney"`
	MaxNominalEmoney  float64       `json:"max_nominal_emoney"`
	CustomSelection   []string      `json:"custom_selection"`
	CustomerType      []interface{} `json:"customer_type"`
}

type MetaCustomerGetter struct {
	CustomerModulars
}

func (u *CustomerModulars) Get(tx *gorm.DB) ([]CustomerScanners, int64, error) {
	var err error
	var dataSelectQuery []string
	var isJoinTransaksi bool

	// error ada customer loyalty
	dataSelectQuery = append(dataSelectQuery, "customer.nama", "customer.telp", "customer.phone", "customer.country_code", "customer.idcustomer", "customer.is_loyalty", "sapaan.sapaan")
	//dataSelectQuery = append(dataSelectQuery, "customer.nama", "customer.telp", "customer.phone", "customer.country_code", "customer.idcustomer", "sapaan.sapaan")

	if IsNotEmptyString(u.Idowner) {
		tx = GetQueryIdowner(tx, u.Idowner)
	} else {
		return nil, 0, errors.New("idowner is not valid !")
	}

	if IsNotEmptyString(u.Key) {
		tx = GetQueryKey(tx, u.Key)
	}

	if IsNotEmptyString(u.FilterDateBy) {
		fmt.Println(u.FilterDateBy)
		tx, err, isJoinTransaksi = GetQueryPeriods(tx, u.FilterDateBy, u.FilterDateColumn, u.Year, u.Month, u.FirstDate, u.LastDate, u.Idowner, isJoinTransaksi)
		if err != nil {
			return nil, 0, err
		}
	}

	if IsNotEmptyString(u.FilterLayananBy) {
		tx, err = GetQueryFilterLayanan(tx, u.Idowner, u.FilterLayananBy, u.Idlayanan)
		if err != nil {
			return nil, 0, err
		}
	}

	if len(u.Iddeposit) > 0 || len(u.StatusDeposit) > 0 {
		tx = GetQueryPaketDeposit(tx, u.Iddeposit, u.StatusDeposit)
	}

	if len(u.Idoutlet) > 0 {
		tx = GetQueryOutlets(tx, u.Idoutlet)
	}

	if u.IsContact != nil {
		isContact := *u.IsContact
		tx = GetQueryContact(tx, isContact)
	}

	if len(u.Idoutlet) > 0 {
		tx = GetQueryOutlets(tx, u.Idoutlet)
	}

	if len(u.Agama) > 0 {
		tx = tx.Where("customer.agama IN (?)", u.Agama)
	}

	if len(u.CustomerType) > 0 {
		tx = tx.Where("customer.is_loyalty IN (?)", u.CustomerType)
	}

	if u.MinNominalEmoney > 0 || u.MaxNominalEmoney > 0 {
		tx = GetQueryEmoney(tx, u.MinNominalEmoney, u.MaxNominalEmoney)
	}

	if IsNotEmptyString(u.SortBy) {
		tx, dataSelectQuery, isJoinTransaksi = GetQuerySorting(tx, u.SortBy, u.OrderBy, u.Idowner, dataSelectQuery, isJoinTransaksi, u.Idoutlet)
	}

	tx = tx.Where("customer.hapus = 0").Group("customer.idcustomer")

	var countDataCustomer int64
	err = tx.Table("customer").Count(&countDataCustomer).Error
	if err != nil {
		log.Println(err.Error())
		return nil, 0, err
	}

	if u.Limit > 0 {
		tx = tx.Limit(u.Limit)
	} else if u.Limit == 0 {
		tx = tx.Limit(20)
	} else {
		tx = tx
	}

	if u.Offset > 0 {
		tx = tx.Offset(u.Offset)
	} else {
		tx = tx.Offset(0)
	}

	if len(u.Idcustomer) > 0 {
		tx = tx.Where("customer.idcustomer IN (?)", u.Idcustomer)
	}

	var customerModel []CustomerScanners
	err = tx.Table("customer").Select(dataSelectQuery).
		Joins("LEFT JOIN sapaan ON customer.sapaan = sapaan.id").Find(&customerModel).Error
	if err != nil {
		log.Println(err.Error())
		return customerModel, 0, err
	}

	return customerModel, countDataCustomer, nil
}

func (u *CustomerModulars) GetDbOnly(tx *gorm.DB) (*gorm.DB, error) {
	var err error
	var dataSelectQuery []string
	var isJoinTransaksi bool

	// Error karena ada loyalty
	dataSelectQuery = append(dataSelectQuery, "customer.idoutlet", "customer.idcustomer", "sapaan.sapaan", "customer.nama", "customer.telp", "customer.jenis_kelamin", "customer.tanggal_lahir", "customer.email", "agama.nama_agama AS agama", "customer.instansi", "customer.created_at", "customer.is_loyalty")
	//dataSelectQuery = append(dataSelectQuery, "customer.idoutlet", "customer.idcustomer", "sapaan.sapaan", "customer.nama", "customer.telp", "customer.jenis_kelamin", "customer.tanggal_lahir", "customer.email", "agama.nama_agama AS agama", "customer.instansi", "customer.created_at")

	if IsNotEmptyString(u.Idowner) {
		tx = GetQueryIdowner(tx, u.Idowner)
	} else {
		return tx, errors.New("idowner is not valid !")
	}

	if IsNotEmptyString(u.Key) {
		tx = GetQueryKey(tx, u.Key)
	}

	if IsNotEmptyString(u.FilterDateBy) {
		tx, err, isJoinTransaksi = GetQueryPeriods(tx, u.FilterDateBy, u.FilterDateColumn, u.Year, u.Month, u.FirstDate, u.LastDate, u.Idowner, isJoinTransaksi)
		if err != nil {
			return tx, err
		}
	}

	if IsNotEmptyString(u.FilterLayananBy) {
		tx, err = GetQueryFilterLayanan(tx, u.Idowner, u.FilterLayananBy, u.Idlayanan)
		if err != nil {
			return tx, err
		}
	}

	if len(u.Iddeposit) > 0 || len(u.StatusDeposit) > 0 {
		tx = GetQueryPaketDeposit(tx, u.Iddeposit, u.StatusDeposit)
	}

	if len(u.Idoutlet) > 0 {
		tx = GetQueryOutlets(tx, u.Idoutlet)
	}

	if u.IsContact != nil {
		isContact := *u.IsContact
		tx = GetQueryContact(tx, isContact)
	}

	if len(u.Idoutlet) > 0 {
		tx = GetQueryOutlets(tx, u.Idoutlet)
	}

	if len(u.Agama) > 0 {
		tx = tx.Where("customer.agama IN (?)", u.Agama)
	}

	if len(u.CustomerType) > 0 {
		tx = tx.Where("customer.is_loyalty IN (?)", u.CustomerType)
	}

	if u.MinNominalEmoney > 0 || u.MaxNominalEmoney > 0 {
		tx = GetQueryEmoney(tx, u.MinNominalEmoney, u.MaxNominalEmoney)
	}

	if IsNotEmptyString(u.SortBy) {
		tx, dataSelectQuery, _ = GetQuerySorting(tx, u.SortBy, u.OrderBy, u.Idowner, dataSelectQuery, isJoinTransaksi, u.Idoutlet)
	}

	if len(u.Idcustomer) > 0 {
		tx = tx.Where("customer.idcustomer IN (?)", u.Idcustomer)
	}

	if isJoinTransaksi {
		tx = tx.Where("transaksi.outlet_idoutlet IN (?)", u.IdoutletTransaksi)
	}

	tx = tx.Table("customer").Select(dataSelectQuery).Where("customer.hapus = 0").Group("customer.idcustomer")
	return tx, nil
}

func (u *CustomerModulars) OnlyCount(tx *gorm.DB) (int64, error) {
	var err error
	var dataSelectQuery []string
	var isJoinTransaksi bool

	// Error karena ada is_loyalty
	dataSelectQuery = append(dataSelectQuery, "customer.nama", "customer.telp", "customer.phone", "customer.country_code", "customer.idcustomer", "customer.is_loyalty")
	//dataSelectQuery = append(dataSelectQuery, "customer.nama", "customer.telp", "customer.phone", "customer.country_code", "customer.idcustomer")

	if IsNotEmptyString(u.Idowner) {
		tx = GetQueryIdowner(tx, u.Idowner)
	} else {
		return 0, errors.New("idowner is not valid !")
	}

	if IsNotEmptyString(u.Key) {
		tx = GetQueryKey(tx, u.Key)
	}

	if IsNotEmptyString(u.FilterDateBy) {
		fmt.Println(u.FilterDateBy)
		tx, err, isJoinTransaksi = GetQueryPeriods(tx, u.FilterDateBy, u.FilterDateColumn, u.Year, u.Month, u.FirstDate, u.LastDate, u.Idowner, isJoinTransaksi)
		if err != nil {
			return 0, err
		}
	}

	if IsNotEmptyString(u.FilterLayananBy) {
		tx, err = GetQueryFilterLayanan(tx, u.Idowner, u.FilterLayananBy, u.Idlayanan)
		if err != nil {
			return 0, err
		}
	}

	if len(u.Iddeposit) > 0 || len(u.StatusDeposit) > 0 {
		tx = GetQueryPaketDeposit(tx, u.Iddeposit, u.StatusDeposit)
	}

	if len(u.Idoutlet) > 0 {
		tx = GetQueryOutlets(tx, u.Idoutlet)
	}

	if u.IsContact != nil {
		isContact := *u.IsContact
		tx = GetQueryContact(tx, isContact)
	}

	if len(u.Agama) > 0 {
		tx = tx.Where("customer.agama IN (?)", u.Agama)
	}

	if len(u.CustomerType) > 0 {
		tx = tx.Where("customer.is_loyalty IN (?)", u.CustomerType)
	}

	if len(u.Idcustomer) > 0 {
		tx = tx.Where("customer.idcustomer IN (?)", u.Idcustomer)
	}

	// if IsNotEmptyString(u.SortBy) {
	// 	tx, dataSelectQuery, isJoinTransaksi = GetQuerySorting(tx, u.SortBy, u.OrderBy, dataSelectQuery, isJoinTransaksi)
	// }

	if u.MinNominalEmoney > 0 || u.MaxNominalEmoney > 0 {
		tx = GetQueryEmoney(tx, u.MinNominalEmoney, u.MaxNominalEmoney)
	}

	tx = tx.Where("customer.hapus = 0").Group("customer.idcustomer")

	var countDataCustomer int64
	err = tx.Table("customer").Count(&countDataCustomer).Error
	if err != nil {
		log.Println(err.Error())
		return 0, err
	}

	return countDataCustomer, nil
}

func (u *CustomerModulars) UIdwoner(data string) *CustomerModulars {
	u.Idowner = data
	return u
}

func (u *CustomerModulars) Ukey(data string) *CustomerModulars {
	u.Key = data
	return u
}

func (u *CustomerModulars) USortBy(data string) *CustomerModulars {
	u.SortBy = data
	return u
}

func (u *CustomerModulars) UOrderBy(data string) *CustomerModulars {
	u.OrderBy = data
	return u
}

func (u *CustomerModulars) UIsContact(data *bool) *CustomerModulars {
	u.IsContact = data
	return u
}

func (u *CustomerModulars) UStatusDeposit(data []interface{}) *CustomerModulars {
	u.StatusDeposit = data
	return u
}

func (u *CustomerModulars) UFilterDateBy(data string) *CustomerModulars {
	u.FilterDateBy = data
	return u
}

func (u *CustomerModulars) UFilterDateColumn(data string) *CustomerModulars {
	u.FilterDateColumn = data
	return u
}

func (u *CustomerModulars) UFirstDate(data string) *CustomerModulars {
	u.FirstDate = data
	return u
}

func (u *CustomerModulars) ULastDate(data string) *CustomerModulars {
	u.LastDate = data
	return u
}

func (u *CustomerModulars) UMonth(data string) *CustomerModulars {
	u.Month = data
	return u
}

func (u *CustomerModulars) UYear(data string) *CustomerModulars {
	u.Year = data
	return u
}

func (u *CustomerModulars) ULimit(data int) *CustomerModulars {
	u.Limit = data
	return u
}

func (u *CustomerModulars) UAgama(data []interface{}) *CustomerModulars {
	u.Agama = data
	return u
}

func (u *CustomerModulars) UMember(data []interface{}) *CustomerModulars {
	u.IdmasterMember = data
	return u
}

func (u *CustomerModulars) UIdcustomer(data []string) *CustomerModulars {
	u.Idcustomer = append(u.Idcustomer, data...)
	return u
}

func (u *CustomerModulars) UOffset(data int) *CustomerModulars {
	u.Offset = data
	return u
}

func (u *CustomerModulars) UIdoutlet(data []interface{}) *CustomerModulars {
	u.Idoutlet = data
	return u
}

func (u *CustomerModulars) UFilterLayananBy(data string) *CustomerModulars {
	u.FilterLayananBy = data
	return u
}

func (u *CustomerModulars) UIdlayanan(data []interface{}) *CustomerModulars {
	u.Idlayanan = data
	return u
}

func (u *CustomerModulars) UIddeposit(data []interface{}) *CustomerModulars {
	u.Iddeposit = data
	return u
}

func (u *CustomerModulars) UMinNominalEmoney(data float64) *CustomerModulars {
	u.MinNominalEmoney = data
	return u
}

func (u *CustomerModulars) UMaxNominalEmoney(data float64) *CustomerModulars {
	u.MaxNominalEmoney = data
	return u
}

func (u *CustomerModulars) UCustomSelect(data []string) *CustomerModulars {
	u.CustomSelection = append(u.CustomSelection, data...)
	return u
}

func (u *CustomerModulars) UIdoutletTransaksi(data []string) *CustomerModulars {
	u.IdoutletTransaksi = data
	return u
}

func (u *CustomerModulars) UCustomerType(data []interface{}) *CustomerModulars {
	u.CustomerType = data
	return u
}
