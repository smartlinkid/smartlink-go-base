package customer

import (
	"errors"
	"fmt"
	"strings"
	"time"
)

func FilterPeriodsByData(filter_by, year, month, firstDate, lastDate string) (string, string, error) {
	fmt.Println(filter_by)
	switch filter_by {
	case "tahun":
		tahunTime, err := time.Parse("2006", year)
		if err != nil {
			return "", "", err
		}

		tanggalAwal := BeginningOfYear(tahunTime).Format("2006-01-02") + " 00:00:00"
		tanggalAkhir := EndOfYear(tahunTime).Format("2006-01-02") + " 23:59:59"
		return tanggalAwal, tanggalAkhir, nil
	case "bulan":
		bulanTime, err := time.Parse("2006-01", month)
		if err != nil {
			return "", "", err
		}

		tanggalAwal := BeginningOfMonth(bulanTime).Format("2006-01-02") + " 00:00:00"
		tanggalAkhir := EndOfMonth(bulanTime).Format("2006-01-02") + " 23:59:59"
		return tanggalAwal, tanggalAkhir, nil
		break
	case "periode":
		if firstDate == "" || lastDate == "" {
			return "", "", errors.New("Tanggal Awal atau Tanggal Akhir tidak valid.")
		}

		tanggalAwal := firstDate + " 00:00:00"
		tanggalAkhir := lastDate + " 23:59:59"
		return tanggalAwal, tanggalAkhir, nil
	default:
		if firstDate == "" || lastDate == "" {
			return "", "", errors.New("Tanggal Awal atau Tanggal Akhir tidak valid.")
		}

		tanggalAwal := firstDate + " 00:00:00"
		tanggalAkhir := lastDate + " 23:59:59"
		return tanggalAwal, tanggalAkhir, nil
	}

	return "", "", nil
}

func BeginningOfYear(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), 1, 0, 0, 0, 0, t.Location())
}
func EndOfYear(t time.Time) time.Time {
	return BeginningOfYear(t).AddDate(1, 0, 0).Add(-time.Second)
}
func BeginningOfMonth(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), 1, 0, 0, 0, 0, t.Location())
}
func EndOfMonth(t time.Time) time.Time {
	return BeginningOfMonth(t).AddDate(0, 1, 0).Add(-time.Second)
}

func IsNotEmptyString(data string) bool {
	if strings.TrimSpace(data) != "" {
		return true
	}
	return false
}
