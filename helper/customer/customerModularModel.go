package customer

import (
	"errors"
	"fmt"
	"log"
	"strings"
	"time"

	"bitbucket.org/smartlinkid/smartlink-go-base/constant"
	"bitbucket.org/smartlinkid/smartlink-go-base/helper"
	"bitbucket.org/smartlinkid/smartlink-go-base/model"

	"github.com/jinzhu/gorm"
)

func GetQueryIdowner(tx *gorm.DB, data string) *gorm.DB {
	tx = tx.Where("customer.owner_idowner = ?", data)
	return tx
}

func GetQueryKey(tx *gorm.DB, data string) *gorm.DB {
	tx = tx.Where("customer.nama LIKE ? OR customer.telp LIKE ? ", "%"+data+"%", "%"+data+"%")
	return tx
}

func GetQueryPeriods(tx *gorm.DB, filterDateBy, filterDateColumn, year, month, firstDate, lastDate, idowner string, isJoinTransaksi bool) (*gorm.DB, error, bool) {

	fmt.Println("Helper : ", filterDateBy)
	if filterDateBy != "" {
		if !IsNotEmptyString(filterDateColumn) {
			filterDateColumn = "created_at"
		}

		tanggalAwal, tanggalAkhir, err := FilterPeriodsByData(filterDateBy, year, month, firstDate, lastDate)
		if err != nil {
			return nil, err, isJoinTransaksi
		}

		if tanggalAwal != "" && tanggalAkhir != "" {
			switch filterDateColumn {
			case constant.FILTER_TERDAFTAR_PADA_PERIODE:
				column := "customer.created_at"
				//tx = tx.Where(column+" >= ? AND "+column+" <= ?", tanggalAwal, tanggalAkhir)
				tx = tx.Where(column+" BETWEEN ? AND ?", tanggalAwal, tanggalAkhir)
				return tx, nil, isJoinTransaksi
			case constant.FILTER_TRANSAKSI_PADA_PERIODE:
				column := "transaksi.created_at"
				if isJoinTransaksi != true {
					isJoinTransaksi = true
					tx = tx.Joins("JOIN transaksi ON customer.idcustomer = transaksi.customer_idcustomer AND transaksi.hapus = 0  ")
				}
				//tx = tx.Where(column+" >= ? AND "+column+" <= ?", tanggalAwal, tanggalAkhir)
				tx = tx.Where(column+" BETWEEN ? AND ?", tanggalAwal, tanggalAkhir)
				return tx, nil, isJoinTransaksi
			case constant.FILTER_TIDAK_TRANSAKSI_PADA_PERIODE:
				tx = tx.Where("customer.idcustomer NOT IN ( " +
					"SELECT transaksi.customer_idcustomer FROM `transaksi` " +
					`JOIN customer ON transaksi.customer_idcustomer = customer.idcustomer AND transaksi.hapus = 0 ` +
					"WHERE transaksi.hapus = '0' " +
					//"AND ( transaksi.created_at >= '" + tanggalAwal + "' ) AND " +
					//"( transaksi.created_at <= '" + tanggalAkhir + "' ) AND " +
					" AND (transaksi.created_at BETWEEN '" + tanggalAwal + "' AND '" + tanggalAkhir + "' ) " +
					" AND ( customer.owner_idowner = '" + idowner + "') " +
					"GROUP BY transaksi.customer_idcustomer) ")
				return tx, nil, isJoinTransaksi
			default:
				//tx = tx.Where("customer."+filterDateColumn+" >= ? AND customer."+filterDateColumn+" <= ?", tanggalAwal, tanggalAkhir)
				tx = tx.Where("customer."+filterDateColumn+" BETWEEN ? AND ?", tanggalAwal, tanggalAkhir)
				return tx, nil, isJoinTransaksi
			}
		} else {
			return nil, errors.New("filter periods is not valid !"), isJoinTransaksi
		}
	}

	return tx, nil, isJoinTransaksi
}

func GetQueryContact(tx *gorm.DB, data bool) *gorm.DB {
	if data == true {
		tx = tx.Where("customer.empty_phone = 0")
	} else {
		tx = tx.Where("customer.empty_phone = 1")
	}
	return tx
}

func GetQueryOutlets(tx *gorm.DB, data []interface{}) *gorm.DB {
	tx = tx.Where("customer.idoutlet IN (?)", data)
	return tx
}

func GetQueryEmoney(tx *gorm.DB, minEmoney float64, maxEmoney float64) *gorm.DB {
	tx = tx.Joins("JOIN resume_emoney ON resume_emoney.customer_idcustomer =  customer.idcustomer AND (resume_emoney.masa_aktif > ? OR resume_emoney.masa_aktif IS NULL)", time.Now().Unix())
	if maxEmoney > 0 {
		tx = tx.Where("resume_emoney.saldo >= ? AND resume_emoney.saldo <= ?", minEmoney, maxEmoney)
	} else {
		tx = tx.Where("resume_emoney.saldo >= ?", minEmoney)
	}
	return tx
}

func GetQueryPaketDeposit(tx *gorm.DB, data []interface{}, statusDeposit []interface{}) *gorm.DB {
	tx = tx.Joins("JOIN resume_deposit ON resume_deposit.customer_idcustomer =  customer.idcustomer")
	if len(data) > 0 {
		tx = tx.Where("resume_deposit.data_deposit_iddata_deposit IN (?)", data)
	}

	if len(statusDeposit) > 0 {
		now := time.Now()
		unixNow := now.Unix() * 1000
		if len(statusDeposit) == 2 {
			tx = tx.Where("resume_deposit.masa_aktif >= ? OR resume_deposit.masa_aktif IS NULL OR resume_deposit.masa_aktif < ?", unixNow, unixNow)
		} else {
			status := statusDeposit[0]
			if status == "1" {
				tx = tx.Where("resume_deposit.masa_aktif >= ? OR resume_deposit.masa_aktif IS NULL", unixNow)
			} else if status == "0" {
				tx = tx.Where("resume_deposit.masa_aktif < ?", unixNow)
			}
		}
	}
	return tx
}

func GetQueryFilterLayanan(tx *gorm.DB, idowner, layananBy string, idlayanan []interface{}) (*gorm.DB, error) {
	filterLayananBy := layananBy
	layananArray := idlayanan
	if len(layananArray) > 0 {
		if filterLayananBy == "" {
			filterLayananBy = constant.FILTER_TRANSAKSI_DENGAN_SEMUA_LAYANAN
		}
		switch layananBy {
		case constant.FILTER_TRANSAKSI_DENGAN_SEMUA_LAYANAN:
			dataLayanan := []string{}
			if len(layananArray) > 0 {
				for _, each := range layananArray {
					dataStr := each.(string)
					dataLayanan = append(dataLayanan, "'"+dataStr+"'")
				}
				tx = tx.Where("customer.idcustomer IN ( " +
					"SELECT transaksi.customer_idcustomer FROM `transaksi` " +
					"JOIN customer ON transaksi.customer_idcustomer = customer.idcustomer " +
					"JOIN detlayanan ON detlayanan.transaksi_idtransaksi = transaksi.idtransaksi " +
					"WHERE transaksi.hapus = '0' AND ( detlayanan.layanan_idlayanan IN (" + strings.Join(dataLayanan, ", ") + ") ) " +
					"AND ( customer.owner_idowner = '" + idowner + "') " +
					"GROUP BY transaksi.customer_idcustomer) ")

				return tx, nil
			}
			break
		case constant.FILTER_TRANSAKSI_DENGAN_SALAH_SATU_LAYANAN:
			dataLayanan := []string{}
			if len(layananArray) > 0 {
				for _, each := range layananArray {
					dataStr := each.(string)
					dataLayanan = append(dataLayanan, "detlayanan.layanan_idlayanan = '"+dataStr+"'")
				}

				tx = tx.Where("customer.idcustomer IN ( " +
					"SELECT transaksi.customer_idcustomer FROM `transaksi` " +
					"JOIN customer ON transaksi.customer_idcustomer = customer.idcustomer " +
					"JOIN detlayanan ON detlayanan.transaksi_idtransaksi = transaksi.idtransaksi " +
					"WHERE transaksi.hapus = '0' AND ( " + strings.Join(dataLayanan, " OR ") + " ) " +
					"AND ( customer.owner_idowner = '" + idowner + "') " +
					"GROUP BY transaksi.customer_idcustomer) ")

				return tx, nil
			}
			break
		case constant.FILTER_TRANSAKSI_TANPA_LAYANAN:
			dataLayanan := []string{}
			if len(layananArray) > 0 {
				for _, each := range layananArray {
					dataStr := each.(string)
					dataLayanan = append(dataLayanan, "'"+dataStr+"'")
				}
				tx = tx.Where("customer.idcustomer NOT IN ( " +
					"SELECT transaksi.customer_idcustomer FROM `transaksi` " +
					"JOIN customer ON transaksi.customer_idcustomer = customer.idcustomer " +
					"JOIN detlayanan ON detlayanan.transaksi_idtransaksi = transaksi.idtransaksi " +
					"WHERE transaksi.hapus = '0' AND ( detlayanan.layanan_idlayanan IN (" + strings.Join(dataLayanan, ", ") + ") ) " +
					"AND ( customer.owner_idowner = '" + idowner + "') " +
					"GROUP BY transaksi.customer_idcustomer) ")

				return tx, nil
			}
			break
		default:
			return nil, errors.New("'Filter layanan by' tidak valid, harap hubungi customer support.")
		}
	}
	return tx, nil
}

func GetQuerySorting(tx *gorm.DB, sortBy, orderBy, idowner string, selectRaw []string, isJoinTransaksi bool, idoutlet []interface{}) (*gorm.DB, []string, bool) {
	if sortBy != "" {
		if sortBy == "banyak_transaksi" {
			if isJoinTransaksi != true {
				isJoinTransaksi = true
				tx = tx.Joins("LEFT JOIN transaksi ON customer.idcustomer = transaksi.customer_idcustomer AND transaksi.hapus = 0  ")
			}
			tx = tx.Group("transaksi.customer_idcustomer")

			selectRaw = append(selectRaw, "COUNT(transaksi.idtransaksi) as jumlah_transaksi")
			if orderBy != "" {
				tx = tx.Order("jumlah_transaksi " + orderBy)
			} else {
				tx = tx.Order("jumlah_transaksi DESC")
			}
		} else if sortBy == "transaksi_terakhir" {
			if isJoinTransaksi != true {
				isJoinTransaksi = true
				tx = tx.Joins("LEFT JOIN transaksi ON customer.idcustomer = transaksi.customer_idcustomer AND transaksi.hapus = 0  ")
			}
			tx = tx.Group("transaksi.customer_idcustomer")
			selectRaw = append(selectRaw, "MAX(transaksi.created_at) as transaksi_terakhir")
			if orderBy != "" {
				tx = tx.Order("transaksi_terakhir " + orderBy)
			} else {
				tx = tx.Order("transaksi_terakhir DESC")
			}
		} else if sortBy == "abjad" {
			if orderBy != "" {
				tx = tx.Order("customer.nama " + orderBy)
			} else {
				tx = tx.Order("customer.nama ASC")
			}
		} else if sortBy == "nominal_transaksi" {
			if !isJoinTransaksi {
				isJoinTransaksi = true
				if len(idoutlet) == 0 {
					var outletModel []model.Outlet
					dbOutlet := helper.GetDb()
					err := dbOutlet.
						Where("owner_idowner = ?", idowner).
						Where("hapus = ?", 0).
						Find(&outletModel).Error
					if err != nil {
						log.Println(err)
					}

					var idoutlets []interface{}
					for _, o := range outletModel {
						idoutlets = append(idoutlets, o.Idoutlet)
					}

					tx = tx.Joins("LEFT JOIN transaksi ON customer.idcustomer = transaksi.customer_idcustomer AND transaksi.hapus = 0  AND transaksi.outlet_idoutlet IN (?)", idoutlets)
				} else {
					tx = tx.Joins("LEFT JOIN transaksi ON customer.idcustomer = transaksi.customer_idcustomer AND transaksi.hapus = 0  ")
				}
			}
			tx = tx.Group("transaksi.customer_idcustomer")

			selectRaw = append(selectRaw, "SUM(transaksi.grandtotal) as nominal_transaksi")
			if orderBy != "" {
				tx = tx.Order("nominal_transaksi " + orderBy)
			} else {
				tx = tx.Order("nominal_transaksi DESC")
			}
		} else {
			if orderBy != "" {
				if orderBy != "" {
					tx = tx.Order("customer." + sortBy + " " + orderBy)
				}
			} else {
				tx = tx.Order("customer." + sortBy + " ASC")
			}
		}
	} else {
		tx = tx.Order("customer.nama ASC")
	}

	return tx, selectRaw, isJoinTransaksi
}
