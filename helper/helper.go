package helper

import (
	"bitbucket.org/smartlinkid/smartlink-go-base/model/template"
	"encoding/json"
	"math/rand"
	"strconv"
	"time"
)

func SendSms(p template.SMSPayload) {
	data, _ := json.Marshal(p)
	nc := NatsConn()
	_ = nc.Publish("sms.send", data)
}

func OtpGen() string {
	return strconv.Itoa(RangeIn(100000, 999999))
}

func RangeIn(low, hi int) int {
	return low + rand.Intn(hi-low)
}

func GetNumberInter(number string) string {
	if len(number) > 4 {
		if number[0] == '0' {
			return "+62" + number[1:]
		}
		if number[0] == '6' && number[1] == '2' {
			return "+" + number
		}
	}
	return number
}

func DateTimeToString(dateToStr time.Time) string {
	dateToStr = dateToStr.Local()
	format := "2006-01-02 15:04:05"
	result := dateToStr.Format(format)
	return result
}

func StringDateToTime(strToTime string) time.Time {
	format := "2006-01-02 15:04:05"
	result, _ := time.ParseInLocation(format, strToTime, time.Local)
	return result
}
