package helper

import (
	"fmt"
	"strconv"

	"bitbucket.org/smartlinkid/smartlink-go-base/model"
)

func x() {
	fmt.Println("Aaa")
}
func DuwikFormat(f float64) string {
	return strconv.FormatFloat(f, 'f', -1, 64)
}

func GetNominalConstant(key string) (string, error) {
	var constant model.Constant
	db = GetDb()
	err := db.
		Where("`key` = ?", key).
		First(&constant).
		Error
	if err != nil {
		return "", fmt.Errorf("get constans: %v", err)
	}
	return constant.Val, nil
}
