package helper

import (
	"encoding/json"
	"io/ioutil"
)

func ReadJson(path string) map[string]interface{} {
	var data map[string]interface{}
	file, _ := ioutil.ReadFile(path)
	_ = json.Unmarshal([]byte(file), &data)
	return data
}
