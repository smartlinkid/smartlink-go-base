package helper

import (
	"cloud.google.com/go/storage"
	"context"
	"log"
)

func GetStorageClient() (*storage.Client, error) {
	ctx := context.Background()

	client, err := storage.NewClient(ctx)
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	return client, err
}
