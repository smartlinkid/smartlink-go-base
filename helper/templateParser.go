package helper

import (
	"bitbucket.org/smartlinkid/smartlink-go-base/model"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/bhmj/jsonslice"
	"github.com/jinzhu/gorm"
	"strings"
)

func NewTemplateParser(db *gorm.DB) *TemplateParser {
	return &TemplateParser{DB: db}
}

type TemplateParserData struct {
	Payload  []model.JsonTemplatePathPayload
	Template string
	Data     interface{}
}

type TemplateParser struct {
	DB *gorm.DB
}

// GetJsonForTemplate for every model

func (d *TemplateParser) JsonPathTemplateParser(usage string, template string, data interface{}) (string, error) {
	// get master by usage
	var jsonData, _ = json.Marshal(data)
	var jsonpathUsage model.JsonpathUsage
	d.DB.Where(model.JsonpathUsage{Usage: usage}).First(&jsonpathUsage)
	if jsonpathUsage == (model.JsonpathUsage{}) {
		return "", errors.New("Usage tidak ditemukan")
	}
	var jsonpathMaster []model.JsonpathMaster
	d.DB.Where("jenis_data = ?", jsonpathUsage.JenisData).Find(&jsonpathMaster)
	processed := template
	for _, v := range jsonpathMaster {
		pathResult, _ := jsonslice.Get(jsonData, v.Jsonpath)
		strPathResult := strings.Trim(string(pathResult), "\"")
		processed = strings.Replace(processed, fmt.Sprintf("[%s]", v.Kode), strPathResult, -1)
	}
	return processed, nil
}

// Deprecated: Use JsonPathTemplateParser
func (tp *TemplateParser) ParsePayload(payload string, template string, data interface{}) (string, error) {
	var jsonTemplatePathPayload []model.JsonTemplatePathPayload
	var err error
	err = db.Table("json_template_payload").
		Select("json_template_payload. *, json_template_path.code, json_template_path.path").
		Joins("join json_template_path on json_template_payload.idpath = json_template_path.id").
		Where("json_template_payload.payload = ?", payload).
		Scan(&jsonTemplatePathPayload).Error
	if err != nil {
		panic(err)
	}
	tpdata := TemplateParserData{
		Payload:  jsonTemplatePathPayload,
		Template: template,
		Data:     data,
	}
	return tp.Parser(tpdata)
}

// Deprecated: Use JsonPathTemplateParser
func (tp *TemplateParser) Parser(data TemplateParserData) (s string, e error) {
	jsonData, err := json.Marshal(data.Data)
	if err != nil {
		return "", err
	}

	processedTemplate := data.Template
	for _, v := range data.Payload {
		jsonQueryResult, _ := jsonslice.Get(jsonData, v.Path)
		rawResult := string(jsonQueryResult)
		res := strings.Trim(rawResult, "\"")
		processedTemplate = strings.Replace(processedTemplate, v.Code, res, -1)
	}

	return processedTemplate, nil
}
