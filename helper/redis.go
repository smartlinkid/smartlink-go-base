package helper

import (
	"github.com/go-redis/redis"
	"github.com/joho/godotenv"
	"os"
)

var c *redis.Client
var cp *redis.Client

func GetRedis() *redis.Client {
	if c == nil {
		godotenv.Load()
		c = redis.NewClient(&redis.Options{
			Addr:     os.Getenv("REDIS_HOST"),
			Password: os.Getenv("REDIS_PASS"),
			DB:       0,
		})
	}
	return c
}

func PGetRedis() *redis.Client {
	if cp == nil {
		godotenv.Load()
		cp = redis.NewClient(&redis.Options{
			Addr:     os.Getenv("REDIS_HOST"),
			Password: os.Getenv("REDIS_PASS"),
			DB:       0,
		})
	}
	return cp
}
func GetRedisCustome(host string, pass string) *redis.Client {
	godotenv.Load()
	return redis.NewClient(&redis.Options{
		Addr:     host,
		Password: pass,
		DB:       0,
	})
}
