package helper

import (
	"bitbucket.org/smartlinkid/smartlink-go-base/model"
	"github.com/jinzhu/gorm"
	"time"
)

type JurnalerV2 struct {
	DB *gorm.DB
}

func NewJurnaler(db *gorm.DB) *JurnalerV2 {
	constructor := JurnalerV2{DB: db}
	return &constructor
}

func (j *JurnalerV2) JurnalDebet(
	idakun string,
	debet float64,
	keterangan string,
	relatedType string,
	relatedId string,
	idkaryawan string,
	idowner string,
	waktu int64,
	refType string,
	refId string,
	rev int,
	createdAt *time.Time,
) error {
	var err error

	jurnal := model.KeuanganJurnal{
		Idakun:      idakun,
		Debet:       debet,
		Keterangan:  keterangan,
		RelatedType: relatedType,
		RelatedId:   relatedId,
		Idkaryawan:  idkaryawan,
		Idowner:     idowner,
		Waktu:       waktu,
		RefType:     refType,
		RefId:       refId,
		Rev:         rev,
		CreatedAt:   createdAt,
	}

	err = j.DB.Save(&jurnal).Error
	if err != nil {
		return err
	}

	return nil
}

func (j *JurnalerV2) JurnalKredit(
	idakun string,
	kredit float64,
	keterangan string,
	relatedType string,
	relatedId string,
	idkaryawan string,
	idowner string,
	waktu int64,
	refType string,
	refId string,
	rev int,
	createdAt *time.Time,
) error {
	var err error

	jurnal := model.KeuanganJurnal{
		Idakun:      idakun,
		Kredit:      kredit,
		Keterangan:  keterangan,
		RelatedType: relatedType,
		RelatedId:   relatedId,
		Idkaryawan:  idkaryawan,
		Idowner:     idowner,
		Waktu:       waktu,
		RefType:     refType,
		RefId:       refId,
		Rev:         rev,
		CreatedAt:   createdAt,
	}

	err = j.DB.Save(&jurnal).Error
	if err != nil {
		return err
	}

	return nil
}
