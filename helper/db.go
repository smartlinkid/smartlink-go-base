package helper

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/joho/godotenv"
	gormv2 "gorm.io/gorm"
	"gorm.io/gorm/schema"
	"os"
)
import _ "github.com/go-sql-driver/mysql"
import _ "github.com/jinzhu/gorm/dialects/mysql"
import mysqlv2 "gorm.io/driver/mysql"

var db *gorm.DB
var dbReadOnly *gorm.DB

var dbv2 *gormv2.DB
var dbv2ReadOnly *gormv2.DB
var err error

type InTransaction func(tx *gorm.DB) error

func GetDbManual(host string, user string, pass string, port string, name string) (*gorm.DB, error) {
	godotenv.Load()
	sqlString := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=True&loc=Local",
		user,
		pass,
		host,
		port,
		name,
	)
	dbMan, err := gorm.Open("mysql", sqlString)
	if err != nil {
		return nil, err
	}
	dbMan.SingularTable(true)
	return dbMan, nil
}

func GetDb() *gorm.DB {
	godotenv.Load()
	sqlString := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=True&loc=Local",
		os.Getenv("DB_USER"),
		os.Getenv("DB_PASS"),
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
		os.Getenv("DB_DB"),
	)
	if os.Getenv("CLOUD_RUN") == "1" {
		sqlString = fmt.Sprintf("%s:%s@unix(/cloudsql/%s)/%s?parseTime=True&loc=Local",
			os.Getenv("DB_USER"),
			os.Getenv("DB_PASS"),
			os.Getenv("DB_CLOUDSQL"),
			os.Getenv("DB_DB"),
		)
	}
	//fmt.Println("SQLSTRING", sqlString)
	if db == nil {
		db, err = gorm.Open("mysql", sqlString)
	}
	if err != nil {
		fmt.Print(err)
	}
	_ = db.DB().Ping()
	db.SingularTable(true)
	return db
}

func GetDbStateless() *gorm.DB {
	godotenv.Load()
	sqlString := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=True&loc=Local",
		os.Getenv("DB_USER"),
		os.Getenv("DB_PASS"),
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
		os.Getenv("DB_DB"),
	)
	if os.Getenv("CLOUD_RUN") == "1" {
		sqlString = fmt.Sprintf("%s:%s@unix(/cloudsql/%s)/%s?parseTime=True&loc=Local",
			os.Getenv("DB_USER"),
			os.Getenv("DB_PASS"),
			os.Getenv("DB_CLOUDSQL"),
			os.Getenv("DB_DB"),
		)
	}
	//fmt.Println("SQLSTRING", sqlString)
	db2, err := gorm.Open("mysql", sqlString)
	if err != nil {
		fmt.Print(err)
	}
	_ = db2.DB().Ping()
	db2.SingularTable(true)
	return db2
}

func GetDbCanReadOnly(readonly bool) *gorm.DB {
	godotenv.Load()
	sqlStringReadOnly := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=True&loc=Local",
		os.Getenv("DB_USER_READ"),
		os.Getenv("DB_PASS_READ"),
		os.Getenv("DB_HOST_READ"),
		os.Getenv("DB_PORT_READ"),
		os.Getenv("DB_DB_READ"),
	)
	sqlString := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=True&loc=Local",
		os.Getenv("DB_USER"),
		os.Getenv("DB_PASS"),
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
		os.Getenv("DB_DB"),
	)

	//fmt.Println("SQLSTRING", sqlString)
	if db == nil {
		db, err = gorm.Open("mysql", sqlString)
	}
	if dbReadOnly == nil {
		dbReadOnly, err = gorm.Open("mysql", sqlStringReadOnly)
	}

	if err != nil {
		fmt.Print(err)
	}
	_ = db.DB().Ping()

	db.SingularTable(true)
	db.LogMode(os.Getenv("ISPRODUCTION") == "false")
	_ = dbReadOnly.DB().Ping()
	dbReadOnly.SingularTable(true)
	dbReadOnly.LogMode(os.Getenv("ISPRODUCTION") == "false")
	if readonly {
		return dbReadOnly
	}
	return db
}

func GetDbCanReadOnlyGormV2(readonly bool) *gormv2.DB {
	godotenv.Load()
	sqlStringReadOnly := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=True&loc=Local",
		os.Getenv("DB_USER_READ"),
		os.Getenv("DB_PASS_READ"),
		os.Getenv("DB_HOST_READ"),
		os.Getenv("DB_PORT_READ"),
		os.Getenv("DB_DB_READ"),
	)
	sqlString := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=True&loc=Local",
		os.Getenv("DB_USER"),
		os.Getenv("DB_PASS"),
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
		os.Getenv("DB_DB"),
	)

	//fmt.Println("SQLSTRING", sqlString)
	if dbv2 == nil {
		dbv2, err = gormv2.Open(mysqlv2.Open(sqlString), &gormv2.Config{
			NamingStrategy: schema.NamingStrategy{
				SingularTable: true,
			},
		})
	}
	if dbv2ReadOnly == nil {
		dbv2ReadOnly, err = gormv2.Open(mysqlv2.Open(sqlStringReadOnly), &gormv2.Config{
			NamingStrategy: schema.NamingStrategy{
				SingularTable: true,
			},
		})
	}

	if err != nil {
		fmt.Print(err)
	}
	if readonly {
		return dbv2ReadOnly
	}
	return dbv2
}

// db is db object, not transaction
func ManualTransaction(db *gorm.DB, fn InTransaction) error {
	tx := db.Begin()
	if tx.Error != nil {
		return tx.Error
	}
	err := fn(tx)
	if err != nil {
		xerr := tx.Rollback().Error
		if xerr != nil {
			return xerr
		}
		return err
	}
	if err = tx.Commit().Error; err != nil {
		return err
	}
	return nil
}

// tx is transaction object
func DoInTransactionManual(tx *gorm.DB, fn InTransaction) error {
	if tx.Error != nil {
		return tx.Error
	}
	err := fn(tx)
	if err != nil {
		xerr := tx.Rollback().Error
		if xerr != nil {
			return xerr
		}
		return err
	}
	if err = tx.Commit().Error; err != nil {
		return err
	}
	return nil
}

// transaction statefull, auto-generated
func DoInTransaction(fn InTransaction) error {
	tx := GetDb().Begin()
	if tx.Error != nil {
		return tx.Error
	}
	err := fn(tx)
	if err != nil {
		xerr := tx.Rollback().Error
		if xerr != nil {
			return xerr
		}
		return err
	}
	if err = tx.Commit().Error; err != nil {
		return err
	}
	return nil
}

// transaction stateless
func DoInTransactionStateless(fn InTransaction) error {
	db := GetDbStateless()
	tx := db.Begin()
	defer func() {
		tx.Close()
		db.Close()
	}()
	if tx.Error != nil {
		return tx.Error
	}
	err := fn(tx)
	if err != nil {
		xerr := tx.Rollback().Error
		if xerr != nil {
			return xerr
		}
		return err
	}
	if err = tx.Commit().Error; err != nil {
		return err
	}
	return nil
}

// deprecated, not used
func DoInTransactionBatch(fn []InTransaction) error {
	tx := db.Begin()
	if tx.Error != nil {
		return tx.Error
	}
	for _, v := range fn {
		err := v(tx)
		if err != nil {
			xerr := tx.Rollback().Error
			if xerr != nil {
				return xerr
			}
			return err
		}
	}
	if err = tx.Commit().Error; err != nil {
		return err
	}
	return nil
}
