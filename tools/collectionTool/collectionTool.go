package main

import "fmt"

func main() {
	x := Sum([]float64{1, 2, 3, 4}, func(d interface{}) float64 {
		return d.(float64)
	})
	fmt.Println(x)
}
func Sum(data interface{}, x func(d interface{}) float64) float64 {
	var total float64 = 0
	for _, datum := range data.([]interface{}) {
		total += x(datum)
	}
	return total
}
func Filter(data []interface{}, x func(d interface{}) bool) []interface{} {
	var filtered []interface{}
	for _, datum := range data {
		if x(datum) {
			filtered = append(filtered, datum)
		}
	}
	return filtered
}

func GroupBy(list []interface{}, x func(d interface{}) interface{}) map[interface{}][]interface{} {
	var xx = make(map[interface{}][]interface{})
	for _, t := range list {
		xx[x(t)] = append(xx[x(t)], t)
	}
	return xx
}
